[![pipeline status](https://gitlab.cern.ch/silab/bdaq53/badges/development/pipeline.svg)](https://gitlab.cern.ch/silab/bdaq53/commits/development) [![coverage report](https://gitlab.cern.ch/silab/bdaq53/badges/development/coverage.svg)](https://gitlab.cern.ch/silab/bdaq53/commits/development)

# bdaq53
DAQ and verification environment for the [RD53A](https://cds.cern.ch/record/2287593) prototype based on the [Basil](https://github.com/SiLab-Bonn/basil) framework.
The firmware and software are compatible with the various hardware platforms see [Readout-Hardware](https://gitlab.cern.ch/silab/bdaq53/wikis/Hardware/Readout-Hardware) for details.

### Installation and Usage
Please refer to the BDAQ53 Wiki: https://gitlab.cern.ch/silab/bdaq53/-/wikis/home

## Support
For questions and comments, visit https://mattermost.web.cern.ch/rd53-testing

### Repository structure
`/bdaq53` Software package including main configuration files and helper classes
- `/analysis` Software for data anlysis and plotting
- `/chips` Base- and dervied classes with chip-specific methods and definitions
- `/measurements` Predefined meta scripts for measurements campaigns like irradiation campaigns
- `/scans` Different scans to run on FE chips
- `/system` System files and base classes including basil drivers
- `/tests` Testbench and unittests to run a cosimulation with the DAQ firmare and the RD53 digital design as well as software unit tests.

`/data` Git-lfs folder with large binary files for examples and unit test fixtures

`/example` Simplified usage examples

`/firmware` Firmware sources
- `/src`  Verilog sources and constraint files for the RD53A-DAQ firmware.
- `/vivado` - run.tcl script to generate a Vivado project from the source files.