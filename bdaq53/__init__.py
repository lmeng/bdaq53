import sys
import os

__version__ = '2.1.0'
periphery_file = os.path.abspath(os.path.join(os.path.dirname(__file__), 'periphery.yaml'))
DEFAULT_TESTBENCH = os.path.abspath(os.path.join(os.path.dirname(__file__), 'testbench.yaml'))

if sys.version_info[0] < 3:
    raise Exception("This version of BDAQ53 requires Python 3! See https://gitlab.cern.ch/silab/bdaq53/wikis/User-guide/Installation")
