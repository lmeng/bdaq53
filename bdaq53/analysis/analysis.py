#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Script to convert raw data
'''


import os.path

import numpy as np
import numba
import warnings

import tables as tb
from tqdm import tqdm
from pixel_clusterizer.clusterizer import HitClusterizer

from bdaq53.system import logger
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import rd53a_analysis as ana
from bdaq53.analysis import rd53b_analysis as anb

warnings.filterwarnings('ignore')


class Analysis(object):
    """
        Class to analyze RD53A raw data
    """

    def __init__(self, raw_data_file=None, analyzed_data_file=None, hitor_calib_file=None,
                 store_hits=False, cluster_hits=False, analyze_tdc=False, use_tdc_trigger_dist=False,
                 align_method=None, upper_chi2_ndf=30, chunk_size=1000000, **_):
        '''
            Parameters
            ----------
            raw_data_file : string
                A string raw data file name. File ending (.h5).
            analyzed_data_file : string
                The file name of the output analyzed data file.
                File ending (.h5) does not have to be set.
            hitor_calib_file : string
                The file name of file with TDC calibration data (i.e. from calibrate_hitor.py).
                If provided and analyze_tdc is True, proper cluster charge in DVCAL is calculated during clustering.
            store_hits : boolean
                Create hit table. Large hit table is usually not needed.
            cluster_hits : boolean
                Create cluster table, histograms and plots
            analyze_tdc : boolean
                If analyze_tdc is True, interpret and analyze also TDC words. Default is False,
                meaning that TDC analysis is skipped. This is useful for scans which do no
                require an TDC word interpretation (e.g. threshold scan) in order to save time.
            use_tdc_trigger_dist : boolean
                If True use trigger distance (delay between Hitor and Trigger) in TDC word
                interpretation. If False use instead TDC timestamp from TDC word. Default
                is False.
            align_method : integer
                Methods to do event alignment
                0: New event when number of event headers exceeds number of
                   sub-triggers. Many fallbacks for corrupt data implemented.
                1: New event when data word is TLU trigger word, with error checks
                2: Force new event always at TLU trigger word, no error checks
            chunk_size: unsigned integer
                Number of raw data words in RAM.
        '''
        self.log = logger.setup_derived_logger('Analysis')

        self.raw_data_file = raw_data_file
        self.analyzed_data_file = analyzed_data_file
        self.store_hits = store_hits
        self.cluster_hits = cluster_hits
        self.chunk_size = chunk_size
        self.analyze_tdc = analyze_tdc
        self.use_tdc_trigger_dist = use_tdc_trigger_dist
        self.hitor_calib_file = hitor_calib_file
        self.upper_chi2_ndf = upper_chi2_ndf

        if not os.path.isfile(raw_data_file):
            raise IOError('Raw data file %s does not exist.', raw_data_file)

        if not self.analyzed_data_file:
            self.analyzed_data_file = raw_data_file[:-3] + '_interpreted.h5'

        # Global variables to store info between chunks
        self.event_number = 0
        self.chunk_offset = 0  # Remaining raw data words after chunk analysis
        self.trg_id = -1
        self.prev_trg_number = -1
        self.first_trg_number = -1
        self.last_chunk = False

        # Get/Set configs, chip types, ...
        self._get_configs()
        self._set_chip_type()
        self._set_chip_receiver_id()
        self._set_analysis_variables()

        # Set event alignment method
        if align_method is None:
            if self.chip_type == 'rd53a':
                self.align_method = 0  # default: use EH for event building
            else:
                self.align_method = 2  # default: use external trigger words for event building
        else:
            self.align_method = align_method

        # Setup clusterizer
        self._setup_clusterizer()

        self.threshold_map = np.ones(shape=(self.columns, self.rows)) * -1
        self.noise_map = np.ones_like(self.threshold_map) * -1
        self.chi2_map = np.zeros_like(self.threshold_map) * -1

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            self.log.exception('Exception during analysis', exc_info=(exc_type, exc_value, traceback))

    def _get_configs(self):
        ''' Load run config to allow analysis routines to access these info '''
        with tb.open_file(self.raw_data_file, 'r') as in_file:
            self.run_config = au.ConfigDict(in_file.root.configuration_in.scan.run_config[:])
            self.scan_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])
            self.chip_settings = au.ConfigDict(in_file.root.configuration_in.chip.settings[:])
            self.chip_registers = au.ConfigDict(in_file.root.configuration_in.chip.registers[:])

    def _get_trigger_pattern(self):
        '''
            Old files might not have trigger pattern information in the run_config dict.
            In these cases, 32 triggers were hardcoded, so we can manually set the trigger pattern.
        '''
        try:
            trigger_pattern = self.scan_config['trigger_pattern']
        except KeyError:
            trigger_pattern = 0xffffffff

        return trigger_pattern

    def _set_chip_type(self):
        ''' Load chip type to allow analysis routines to determine analysis procedure '''
        try:
            self.chip_type = self.run_config['chip_type']
        except KeyError:
            self.log.warning("Assuming old file without specified chip type. Data will be treated as produced by RD53A")
            self.chip_type = 'rd53a'

        self.columns = 400
        if self.chip_type.lower() in ['rd53a']:
            self.rows = 192
        elif self.chip_type.lower() in ['itkpixv1', 'itkpixv2']:
            self.rows = 384
        elif self.chip_type.lower() in ['crocv1']:
            self.columns = 432
            self.rows = 336

    def _set_chip_receiver_id(self):
        ''' Load Aurora receiver id to allow analysis routines to check for words from wrong chip/receiver '''
        self.rx_id = int(self.run_config.get('receiver')[2])

    def _set_analysis_variables(self):
        if self.chip_type.lower() == 'rd53a':  # No chip dependent analysis needed.
            self.analyze_ptot = False
        elif self.chip_type.lower() == 'itkpixv1':
            self.hmap_is_compressed = False if (self.chip_registers.get('CoreColEncoderConf', 0) & 0b01000000000) else True  # compression of hitmap word
            self.analyze_ptot = self.chip_settings.get('use_ptot', False)  # precision ToT
            self.eos_mode = False
            self.invert_ptot = False
        elif self.chip_type.lower() in ['crocv1', 'itkpixv2']:
            self.hmap_is_compressed = False if (self.chip_registers.get('CoreColEncoderConf', 0) & 0b01000000000000) else True
            self.analyze_ptot = self.chip_settings.get('use_ptot', False)  # precision ToT
            self.eos_mode = True
            self.invert_ptot = True
        else:
            self.log.error("Given chip type is not supported!")

    def get_scan_param_values(self, scan_param_index=None, scan_parameter=None):
        ''' Return the scan parameter value(s)

            scan_param_index: slicing notation of the scan parameter indeces
            scan_parameter: string
                Name of the scan parameter. If not defined all are returned.
        '''
        with tb.open_file(self.raw_data_file, 'r') as in_file:
            scan_param_table = in_file.root.configuration_out.scan.scan_params[:]
            if scan_param_index:
                scan_param_table = scan_param_table[scan_param_index]
            if scan_parameter:
                scan_param_table = scan_param_table[:][scan_parameter]
        return scan_param_table

    def _setup_clusterizer(self):
        ''' Define data structure and settings for hit clusterizer package '''
        # Define all field names and data types
        hit_fields = {'event_number': 'event_number',
                      'ext_trg_number': 'ext_trg_number',
                      'trigger_id': 'trigger_id',
                      'bcid': 'bcid',
                      'rel_bcid': 'frame',
                      'col': 'column',
                      'row': 'row',
                      'tot': 'charge',
                      'scan_param_id': 'scan_param_id',
                      'trigger_tag': 'trigger_tag',
                      'event_status': 'event_status'
                      }
        hit_description = [('event_number', '<i8'),
                           ('ext_trg_number', 'u4'),
                           ('trigger_id', 'u1'),
                           ('bcid', '<u2'),
                           ('rel_bcid', 'u1'),
                           ('col', '<u2'),
                           ('row', '<u2'),
                           ('tot', 'u1'),
                           ('scan_param_id', 'u4'),
                           ('trigger_tag', 'u1'),
                           ('event_status', 'u4')]
        cluster_fields = {'event_number': 'event_number',
                          'column': 'column',
                          'row': 'row',
                          'size': 'n_hits',
                          'id': 'ID',
                          'tot': 'charge',
                          'scan_param_id': 'scan_param_id',
                          'seed_col': 'seed_column',
                          'seed_row': 'seed_row',
                          'mean_col': 'mean_column',
                          'mean_row': 'mean_row'}
        cluster_description = [('event_number', '<i8'),
                               ('id', '<u2'),
                               ('size', '<u2'),
                               ('tot', '<u2'),
                               ('seed_col', '<u2'),
                               ('seed_row', '<u2'),
                               ('mean_col', '<f4'),
                               ('mean_row', '<f4'),
                               ('dist_col', '<u4'),
                               ('dist_row', '<u4'),
                               ('cluster_shape', '<i8'),
                               ('scan_param_id', 'u4')]

        # Add TDC data entries
        if self.analyze_tdc:
            hit_fields.update({'tdc_value': 'tdc_value', 'tdc_timestamp': 'tdc_timestamp', 'tdc_status': 'tdc_status'})
            hit_description.extend([('tdc_value', 'u2'), ('tdc_timestamp', 'u2'), ('tdc_status', 'u1')])
            cluster_fields.update({'tdc_status': 'tdc_status', 'cluster_charge': 'cluster_charge'})
            cluster_description.extend([('tdc_status', '<u1'), ('cluster_charge', '<u2')])
        # Add PTOT data entries
        if self.analyze_ptot:
            hit_fields.update({'ptot': 'ptot', 'ptoa': 'ptoa'})
            hit_description.extend([('ptot', 'u2'), ('ptoa', 'u1')])
            cluster_fields.update({'ptot': 'ptot', 'ptoa': 'ptoa'})
            cluster_description.extend([('ptot', '<u2'), ('ptoa', '<u1')])
        if self.cluster_hits:
            if self.hitor_calib_file is None:  # Do not use hitor calibration during clustering
                lookup_table = np.full(shape=(self.columns, self.rows, 500), fill_value=-1)
                start_column, stop_column, start_row, stop_row = 0, self.columns, 0, self.rows
                calibrate_tdc = False
            else:  # Use hitor calibration during clustering
                self.log.info('Using HitOR calibration from %s', self.hitor_calib_file)
                with tb.open_file(self.hitor_calib_file) as in_file:
                    lookup_table = in_file.root.lookup_table[:]
                    start_column = self.scan_config.get('start_column', 0)
                    stop_column = self.scan_config.get('stop_column', self.columns)
                    start_row = self.scan_config.get('start_row', 0)
                    stop_row = self.scan_config.get('stop_row', self.rows)
                    calibrate_tdc = True

        hit_dtype = np.dtype(hit_description)
        self.cluster_dtype = np.dtype(cluster_description)

        if self.cluster_hits:  # Allow analysis without clusterizer installed
            # Define end of cluster function to calculate cluster shape
            # and cluster distance in column and row direction
            @numba.njit
            def _end_of_cluster_function(hits, clusters, cluster_size,
                                         cluster_hit_indices, cluster_index,
                                         cluster_id, charge_correction,
                                         noisy_pixels, disabled_pixels,
                                         seed_hit_index):
                hit_arr = np.zeros((15, 15), dtype=np.bool_)
                center_col = hits[cluster_hit_indices[0]].column
                center_row = hits[cluster_hit_indices[0]].row
                hit_arr[7, 7] = 1
                min_col = hits[cluster_hit_indices[0]].column
                max_col = hits[cluster_hit_indices[0]].column
                min_row = hits[cluster_hit_indices[0]].row
                max_row = hits[cluster_hit_indices[0]].row
                for i in cluster_hit_indices[1:]:
                    if i < 0:  # Not used indeces = -1
                        break
                    diff_col = np.int32(hits[i].column - center_col)
                    diff_row = np.int32(hits[i].row - center_row)
                    if np.abs(diff_col) < 8 and np.abs(diff_row) < 8:
                        hit_arr[7 + hits[i].column - center_col,
                                7 + hits[i].row - center_row] = 1
                    if hits[i].column < min_col:
                        min_col = hits[i].column
                    if hits[i].column > max_col:
                        max_col = hits[i].column
                    if hits[i].row < min_row:
                        min_row = hits[i].row
                    if hits[i].row > max_row:
                        max_row = hits[i].row

                if max_col - min_col < 8 and max_row - min_row < 8:
                    # Make 8x8 array
                    col_base = 7 + min_col - center_col
                    row_base = 7 + min_row - center_row
                    cluster_arr = hit_arr[col_base:col_base + 8,
                                          row_base:row_base + 8]
                    # Finally calculate cluster shape
                    # uint64 desired, but numexpr and others limited to int64
                    if cluster_arr[7, 7] == 1:
                        cluster_shape = np.int64(-1)
                    else:
                        cluster_shape = np.int64(
                            au.calc_cluster_shape(cluster_arr))
                else:
                    # Cluster is exceeding 8x8 array
                    cluster_shape = np.int64(-1)

                clusters[cluster_index].cluster_shape = cluster_shape
                clusters[cluster_index].dist_col = max_col - min_col + 1
                clusters[cluster_index].dist_row = max_row - min_row + 1

            def end_of_cluster_function(hits, clusters, cluster_size,
                                        cluster_hit_indices, cluster_index,
                                        cluster_id, charge_correction,
                                        noisy_pixels, disabled_pixels,
                                        seed_hit_index):
                _end_of_cluster_function(hits, clusters, cluster_size,
                                         cluster_hit_indices, cluster_index,
                                         cluster_id, charge_correction,
                                         noisy_pixels, disabled_pixels,
                                         seed_hit_index)

            # Define end of cluster function for calculating TDC related cluster properties
            @numba.njit
            def end_of_cluster_function_tdc(hits, clusters, cluster_size,
                                            cluster_hit_indices, cluster_index,
                                            cluster_id, charge_correction,
                                            noisy_pixels, disabled_pixels,
                                            seed_hit_index):
                # FIXME: use same loop to safe time
                _end_of_cluster_function(hits, clusters, cluster_size,
                                         cluster_hit_indices, cluster_index,
                                         cluster_id, charge_correction,
                                         noisy_pixels, disabled_pixels,
                                         seed_hit_index)

                # Calculate cluster TDC and cluster TDC status
                cluster_charge = 0
                cluster_tdc_status = 0  # Logical OR of TDC status of hits belonging to the cluster
                one_hit_has_no_tdc = False  # Indicator that one hit belonging to the cluster has no TDC value, in this case set TDC status of cluster to zero
                charge_dvcal = 0

                for j in range(clusters[cluster_index].n_hits):  # Loop over hits of cluster
                    hit_index = cluster_hit_indices[j]
                    tdc_value = hits[hit_index].tdc_value
                    col = hits[hit_index].column
                    row = hits[hit_index].row

                    # Calculate TDC cluster charge
                    if calibrate_tdc:  # Use hitor calibration on the fly during clustering in order to calibrate TDC.
                        if tdc_value < lookup_table.shape[-1] and (start_column <= col <= stop_column) and (start_row <= row <= stop_row):  # Only take TDC values for which lookup values exist
                            charge_dvcal = lookup_table[col, row, tdc_value]
                            cluster_charge += charge_dvcal  # Sum up the charge
                        else:
                            # In case no calibration value available for this hit, set charge to zero.
                            cluster_charge = 0
                            cluster_tdc_status = 0
                            one_hit_has_no_tdc = True  # Treat it like one hit has no TDC value
                    else:  # Do not calibrate TDC values. Sum up all TDC values. Only correct for CS1!
                        cluster_charge += tdc_value

                    if hits[hit_index].tdc_status == 0:  # Hit has no TDC value
                        one_hit_has_no_tdc = True
                    cluster_tdc_status |= hits[hit_index].tdc_status  # OR all TDC status of hits belonging to the cluster
                # Write cluster variables
                clusters[cluster_index].cluster_charge = cluster_charge
                clusters[cluster_index].tdc_status = cluster_tdc_status if not one_hit_has_no_tdc else 0

            # Initialize clusterizer with custom hit/cluster fields
            self.clz = HitClusterizer(
                hit_fields=hit_fields,
                hit_dtype=hit_dtype,
                cluster_fields=cluster_fields,
                cluster_dtype=self.cluster_dtype,
                min_hit_charge=0,
                max_hit_charge=13,
                charge_correction=1,
                charge_weighted_clustering=True,
                column_cluster_distance=3,
                row_cluster_distance=3,
                frame_cluster_distance=2,
                ignore_same_hits=True)

            # Set end_of_cluster function for shape and distance calculation
            if self.analyze_tdc:
                # If analyze TDC data, set also end of cluster function for calculating TDC properties
                self.clz.set_end_of_cluster_function(end_of_cluster_function_tdc)
            else:
                self.clz.set_end_of_cluster_function(end_of_cluster_function)

    def _range_of_parameter(self, meta_data):
        ''' Calculate the raw data word indeces of each scan parameter id
        '''
        index = np.append(np.array([0]), (np.where(np.diff(meta_data['scan_param_id']) != 0)[0] + 1))

        expected_values = np.arange(np.max(meta_data['scan_param_id']) + 1)

        # Check for scan parameter IDs with no data
        sel = np.isin(expected_values, meta_data['scan_param_id'])
        if not np.all(sel):
            self.log.warning('No words for scan parameter IDs: %s', str(expected_values[~sel]))

        start = meta_data[index]['index_start']
        stop = np.append(start[:-1] + np.diff(start), meta_data[-1]['index_stop'])

        return np.column_stack((meta_data['scan_param_id'][index], start, stop))

    def _words_of_parameter(self, par_range, data):
        ''' Yield all raw data words of a scan parameter

            Do not exceed chunk_size. Use a global offset
            parameter.
        '''

        for scan_par_id, start, stop in par_range:
            for i in range(start, stop, self.chunk_size):
                # Shift chunk index to not split events. The offset is determined from previous analyzed chunk.
                # Restrict maximum offset, can happen for readouts without a single event, issue #171
                chunk_offset = max(self.chunk_offset, -self.chunk_size)
                start_chunk = i + chunk_offset

                # Limit maximum read words by chunk size
                stop_limited = min(i + self.chunk_size, stop)
                yield scan_par_id, data[start_chunk:stop_limited]

        # Remaining data of last chunk
        self.last_chunk = True  # Set flag for special treatmend
        if self.chunk_offset == 0 or stop + self.chunk_offset == 0:
            return
        yield scan_par_id, data[stop + self.chunk_offset:stop]

    def _create_hit_table(self, out_file, dtype):
        ''' Create hit table node for storage in out_file.
            Copy configuration nodes from raw data file.
        '''
        hit_table = out_file.create_table(out_file.root, name='Hits',
                                          description=dtype,
                                          title='hit_data',
                                          expectedrows=self.chunk_size,
                                          filters=tb.Filters(complib='blosc',
                                                             complevel=5,
                                                             fletcher32=False))

        return hit_table

    def stretch_map_data(self, map_data):
        return np.repeat(map_data['scan_param_id'], map_data['cmd_length'])

    def stretch_ptot_data(self, map_data):
        ptot_stretched = [('scan_param_id', '<i8'),
                          ('hit_or_1_col', np.uint16),
                          ('hit_or_1_row', np.uint16),
                          ('hit_or_2_col', np.uint16),
                          ('hit_or_2_row', np.uint16),
                          ('hit_or_3_col', np.uint16),
                          ('hit_or_3_row', np.uint16),
                          ('hit_or_4_col', np.uint16),
                          ('hit_or_4_row', np.uint16)]
        out_map = np.zeros((np.sum(map_data['cmd_length'])), dtype=np.dtype(ptot_stretched))

        out_map['scan_param_id'] = np.repeat(map_data['scan_param_id'], map_data['cmd_length'])
        out_map['hit_or_1_col'] = np.repeat(map_data['hit_or_1_col'], map_data['cmd_length'])
        out_map['hit_or_1_row'] = np.repeat(map_data['hit_or_1_row'], map_data['cmd_length'])
        out_map['hit_or_2_col'] = np.repeat(map_data['hit_or_2_col'], map_data['cmd_length'])
        out_map['hit_or_2_row'] = np.repeat(map_data['hit_or_2_row'], map_data['cmd_length'])
        out_map['hit_or_3_col'] = np.repeat(map_data['hit_or_3_col'], map_data['cmd_length'])
        out_map['hit_or_3_row'] = np.repeat(map_data['hit_or_3_row'], map_data['cmd_length'])
        out_map['hit_or_4_col'] = np.repeat(map_data['hit_or_4_col'], map_data['cmd_length'])
        out_map['hit_or_4_row'] = np.repeat(map_data['hit_or_4_row'], map_data['cmd_length'])
        return out_map

    def analyze_data(self):
        self.log.info('Analyzing data...')
        self.chunk_offset = 0
        with tb.open_file(self.raw_data_file) as in_file:
            n_words = in_file.root.raw_data.shape[0]
            meta_data = in_file.root.meta_data[:]

            if meta_data.shape[0] == 0:
                self.log.warning('Data is empty. Skip analysis!')
                return

            ptot_table_stretched = np.array([(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)], dtype=np.dtype(
                {'names': ['cmd_number_start', 'cmd_number_stop', 'cmd_length', 'scan_param_id', 'hit_or_1_col',
                           'hit_or_1_row', 'hit_or_2_col', 'hit_or_2_row', 'hit_or_3_col', 'hit_or_3_row',
                           'hit_or_4_col', 'hit_or_4_row'],
                 'formats': [np.uint32, np.uint32, np.uint32, np.uint32, np.uint16, np.uint16, np.uint16, np.uint16,
                             np.uint16, np.uint16, np.uint16, np.uint16]}))
            trigger_table_stretched = np.array([0])
            n_scan_params = np.max(meta_data['scan_param_id']) + 1
            try:
                in_file.root._v_children['trigger_table']
                trigger_table = in_file.root.trigger_table[:]
                if len(trigger_table) > 0:
                    trigger_table_stretched = np.array(self.stretch_map_data(trigger_table))
                    n_scan_params = np.max(trigger_table['scan_param_id']) + 1
            except KeyError:
                self.log.info("No trigger_table_stretched to scan_param_id map detected. Using readout_number to map scan_param_id.")
            try:
                in_file.root._v_children['ptot_table']
                ptot_table = in_file.root.ptot_table[:]
                if len(ptot_table) > 0:
                    ptot_table_stretched = np.array(anb.stretch_ptot_data(ptot_table))
                    n_scan_params = np.max(ptot_table_stretched['scan_param_id']) + 1
            except KeyError:
                self.log.info("No ptot_table_stretched to scan_param_id map detected. Using readout_number to map scan_param_id.")
            par_range = self._range_of_parameter(meta_data)

            (hits, hist_occ, hist_tot,
             hist_rel_bcid, hist_trigger_id, hist_event_status,
             hist_tdc_status, hist_tdc_value, hist_bcid_error, hist_ptot, hist_ptoa) = au.init_outs(n_hits=self.chunk_size * 4,
                                                                                                    n_scan_params=n_scan_params,
                                                                                                    rows=self.rows, cols=self.columns,
                                                                                                    analyze_tdc=self.analyze_tdc,
                                                                                                    analyze_ptot=self.analyze_ptot)

            with tb.open_file(self.analyzed_data_file, 'w', title=in_file.title) as out_file:
                out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)
                try:
                    out_file.copy_node(in_file.root.environmental_data, out_file.root, recursive=True)
                except tb.NoSuchNodeError:
                    pass
                if self.store_hits:
                    hit_table = self._create_hit_table(out_file, dtype=hits.dtype)

                if self.cluster_hits:
                    cluster_table = out_file.create_table(
                        out_file.root, name='Cluster',
                        description=self.cluster_dtype,
                        title='Cluster',
                        filters=tb.Filters(complib='blosc',
                                           complevel=5,
                                           fletcher32=False))
                    hist_cs_size = np.zeros(shape=(100, ), dtype=np.uint32)
                    hist_cs_tot = np.zeros(shape=(100, ), dtype=np.uint32)
                    hist_cs_shape = np.zeros(shape=(300, ), dtype=np.int32)

                self.last_chunk = False
                pbar = tqdm(total=n_words, unit=' Words', unit_scale=True)
                upd = 0
                for scan_param_id, words in self._words_of_parameter(par_range, in_file.root.raw_data):
                    prev_event_number = self.event_number
                    if self.chip_type.lower() in ['rd53a']:
                        (n_hits, self.event_number, self.chunk_offset, self.trg_id, self.prev_trg_number, self.first_trg_number) = ana.interpret_data(
                            rawdata=words,
                            hits=hits,
                            hist_occ=hist_occ,
                            hist_tot=hist_tot,
                            hist_rel_bcid=hist_rel_bcid,
                            hist_trigger_id=hist_trigger_id,
                            hist_event_status=hist_event_status,
                            hist_tdc_status=hist_tdc_status,
                            hist_tdc_value=hist_tdc_value,
                            hist_bcid_error=hist_bcid_error,
                            hist_ptot=hist_ptot,
                            hist_ptoa=hist_ptoa,
                            trigger_table_stretched=trigger_table_stretched,
                            align_method=self.align_method,
                            event_number=prev_event_number,
                            scan_param_id=scan_param_id,
                            prev_trig_id=self.trg_id,
                            prev_trg_number=self.prev_trg_number,
                            first_trg_number=self.first_trg_number,
                            analyze_tdc=self.analyze_tdc,
                            use_tdc_trigger_dist=self.use_tdc_trigger_dist,
                            last_chunk=self.last_chunk,
                            rx_id=self.rx_id,
                            trig_pattern=self._get_trigger_pattern())

                        upd = words.shape[0] + self.chunk_offset

                    elif self.chip_type.lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
                        (n_hits, self.event_number, self.chunk_offset, self.trg_id,
                         self.prev_trg_number, self.first_trg_number) = anb.interpret_data(
                            rawdata=words,
                            hits=hits,
                            hist_occ=hist_occ,
                            hist_tot=hist_tot,
                            hist_rel_bcid=hist_rel_bcid,
                            hist_trigger_id=hist_trigger_id,
                            hist_event_status=hist_event_status,
                            hist_tdc_status=hist_tdc_status,
                            hist_tdc_value=hist_tdc_value,
                            hist_bcid_error=hist_bcid_error,
                            hist_ptot=hist_ptot,
                            hist_ptoa=hist_ptoa,
                            trigger_table_stretched=trigger_table_stretched,
                            ptot_table_stretched=ptot_table_stretched,
                            align_method=self.align_method,
                            event_number=prev_event_number,
                            scan_param_id=scan_param_id,
                            prev_tag=self.trg_id,
                            prev_trg_number=self.prev_trg_number,
                            first_trg_number=self.first_trg_number,
                            analyze_tdc=self.analyze_tdc,
                            use_tdc_trigger_dist=self.use_tdc_trigger_dist,
                            last_chunk=self.last_chunk,
                            rx_id=self.rx_id,
                            trig_pattern=self._get_trigger_pattern(),
                            hmap_is_compressed=self.hmap_is_compressed,
                            eos_mode=self.eos_mode,
                            max_col=self.columns,
                            max_row=self.rows,
                            invert_ptot=self.invert_ptot)

                        upd = words.shape[0] + self.chunk_offset

                    else:
                        raise NotImplementedError('Specified chip type ({0}) is not supported.'.format(self.chip_type.lower()))

                    if prev_event_number == self.event_number:
                        self.log.warning('Not a single event found in %d raw data words. Likely corrupt data!\n Example raw data words %s', words.shape[0], str(words[:10]))

                    if self.store_hits:
                        hit_table.append(hits[:n_hits])
                        hit_table.flush()

                    if self.cluster_hits:
                        _, cluster = self.clz.cluster_hits(hits[:n_hits])
                        if self.analyze_tdc:
                            # Select only clusters where all hits have a valid TDC status
                            cluster_table.append(cluster[cluster['tdc_status'] == 1])
                        else:
                            cluster_table.append(cluster)
                        # Create actual cluster hists
                        cs_size = np.bincount(cluster['size'],
                                              minlength=100)[:100]
                        cs_tot = np.bincount(cluster['tot'],
                                             minlength=100)[:100]
                        sel = np.logical_and(cluster['cluster_shape'] > 0,
                                             cluster['cluster_shape'] < 300)
                        cs_shape = np.bincount(cluster['cluster_shape'][sel],
                                               minlength=300)[:300]
                        # Add to total hists
                        hist_cs_size += cs_size.astype(np.uint32)
                        hist_cs_tot += cs_tot.astype(np.uint32)
                        hist_cs_shape += cs_shape.astype(np.uint32)

                    pbar.update(upd)
                pbar.close()

        self._create_additional_hit_data(hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id, hist_event_status, hist_tdc_status, hist_bcid_error, hist_ptot, hist_ptoa)
        if self.cluster_hits:
            self._create_additional_cluster_data(
                hist_cs_size, hist_cs_tot, hist_cs_shape)

    def _create_additional_hit_data(self, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                                    hist_event_status, hist_tdc_status, hist_bcid_error, hist_ptot, hist_ptoa):
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            scan_id = self.run_config['scan_id']

            out_file.create_carray(out_file.root,
                                   name='HistOcc',
                                   title='Occupancy Histogram',
                                   obj=hist_occ,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistTot',
                                   title='ToT Histogram',
                                   obj=hist_tot,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistRelBCID',
                                   title='Relativ BCID Histogram',
                                   obj=hist_rel_bcid,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistTrigID',
                                   title='TrigID Histogram',
                                   obj=hist_trigger_id,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))

            out_file.create_carray(out_file.root,
                                   name='HistEventStatus',
                                   title='Event status Histogram',
                                   obj=hist_event_status,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))

            if self.analyze_tdc:  # Only store if TDC analysis is used.
                out_file.create_carray(out_file.root,
                                       name='HistTdcStatus',
                                       title='Tdc status Histogram',
                                       obj=hist_tdc_status,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

            out_file.create_carray(out_file.root,
                                   name='HistBCIDError',
                                   title='Event status Histogram',
                                   obj=hist_bcid_error,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))

            if self.analyze_ptot:  # Only store if PTOT anaylsis is used.
                out_file.create_carray(out_file.root,
                                       name='HistPToT',
                                       title='PToT Histogram',
                                       obj=hist_ptot,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

                out_file.create_carray(out_file.root,
                                       name='HistPToA',
                                       title='PToA Histogram',
                                       obj=hist_ptoa,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

            if scan_id in ['threshold_scan', 'fast_threshold_scan', 'autorange_threshold_scan', 'crosstalk_scan']:
                n_injections = self.scan_config['n_injections']
                hist_scurve = hist_occ.reshape((self.rows * self.columns, -1))

                if scan_id in ['threshold_scan', 'fast_threshold_scan', 'crosstalk_scan']:
                    scan_params = [v - self.scan_config['VCAL_MED'] for v in range(self.scan_config['VCAL_HIGH_start'],
                                                                                   self.scan_config['VCAL_HIGH_stop'], self.scan_config['VCAL_HIGH_step'])]
                    self.threshold_map, self.noise_map, self.chi2_map = au.fit_scurves_multithread(hist_scurve, scan_params, n_injections, optimize_fit_range=False, rows=self.rows, upper_chi2_ndf=self.upper_chi2_ndf)
                elif scan_id == 'autorange_threshold_scan':
                    scan_params = self.get_scan_param_values(scan_parameter='vcal_high') - self.get_scan_param_values(scan_parameter='vcal_med')
                    self.threshold_map, self.noise_map, self.chi2_map = au.fit_scurves_multithread(hist_scurve, scan_params, n_injections, optimize_fit_range=False, rows=self.rows, upper_chi2_ndf=self.upper_chi2_ndf)

                out_file.create_carray(out_file.root, name='ThresholdMap', title='Threshold Map', obj=self.threshold_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='NoiseMap', title='Noise Map', obj=self.noise_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='Chi2Map', title='Chi2 / ndf Map', obj=self.chi2_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

    def _create_additional_cluster_data(self, hist_cs_size, hist_cs_tot, hist_cs_shape):
        '''
            Store cluster histograms in analyzed data file
        '''
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            out_file.create_carray(out_file.root,
                                   name='HistClusterSize',
                                   title='Cluster Size Histogram',
                                   obj=hist_cs_size,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterTot',
                                   title='Cluster ToT Histogram',
                                   obj=hist_cs_tot,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterShape',
                                   title='Cluster Shape Histogram',
                                   obj=hist_cs_shape,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))

    def mask_disabled_pixels(self, enable_mask, scan_config):
        mask = np.invert(enable_mask)

        mask[:scan_config.get('start_column', 0), :] = True
        mask[scan_config.get('stop_column', self.columns):, :] = True
        mask[:, :scan_config.get('start_row', 0)] = True
        mask[:, scan_config.get('stop_row', self.rows):] = True

        return mask


if __name__ == "__main__":
    pass
