#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Online data analysis functions
'''

import ctypes
import logging
import multiprocessing
import time
import queue
from functools import reduce

import numpy as np
import numba

from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import rd53b_analysis as anb

logger = logging.getLogger('OnlineAnalysis')

numba_logger = logging.getLogger('numba')
numba_logger.setLevel(logging.WARNING)


@numba.njit(cache=True, fastmath=True)
def histogram(raw_data, occ_hist, data_word, is_fe_high_word, is_data_header, meta_data=None, rx_id=0):
    ''' Raw data to 2D occupancy histogram '''

    for word in raw_data:
        if word & au.TRIGGER_HEADER:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_0:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_1:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_2:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_3:
            continue

        # Aurora receiver ID encoded in header must match rx_id of currently analyzed chip
        if ((word >> 20) & 0xf) != rx_id:
            continue

        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.USERK_FRAME_ID:     # skip USER_K frame
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.HEADER_ID:          # data header
            is_data_header = 1
            is_fe_high_word = 1
        # Can only interpret FE words if high word is found, high word can
        # only be found in event header since event hit records do not
        # have a header
        elif is_fe_high_word < 0:
            continue
        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if is_fe_high_word == 1:
            data_word = word & 0xffff
            is_fe_high_word = 0  # Next is low word
            continue  # Low word still missing
        elif is_fe_high_word == 0:
            data_word = data_word << 16 | word & 0xffff
            is_fe_high_word = 1  # Next is high word

        if is_data_header == 1:
            # After data header follows header less hit data
            is_data_header = 0
        else:  # data word is hit data
            multicol = (data_word >> 26) & 0x3f
            region = (data_word >> 16) & 0x3ff
            for i in range(4):
                col, row = au.translate_mapping(multicol, region, i)
                tot = (data_word >> i * 4) & 0xf

                if col < 400 and row < 192:
                    if tot != 255 and tot != 15:
                        occ_hist[col, row] += 1
                        # print np.count_nonzero(occ_hist)
                        # print occ_hist.sum()
                else:  # unknown word
                    continue

    return data_word, is_fe_high_word, is_data_header


@numba.njit(cache=True, fastmath=True)
def histogram_rd53b(raw_data, occ_hist, word, stream_state, found_start_of_stream, left_over_word, ccol, is_last, is_neighbor,
                    q_row, hmap_word, hits_hmap, word_length, take_old_hitors, tag, trg_number, hmap_word_length, hmap_is_compressed=True, meta_data=None, rx_id=0, eos_mode=False, invert_ptot=False):
    ''' Raw data to 2D occupancy histogram '''

    ptot_table_stretched = meta_data
    analyze_tdc = False
    eos_bit = True

    # Loop over raw data words
    raw_len = len(raw_data)
    for i, actual_word in enumerate(raw_data):
        shift = 16  # Needed shift for appending left-over raw data word

        # Continue if raw data word is 0
        if not actual_word and not found_start_of_stream:
            continue

        if actual_word & au.TRIGGER_HEADER:  # TLU word
            trg_number = actual_word & au.TRG_MASK
            continue

        if au.is_tdc_word(actual_word) and analyze_tdc:  # Select first TDC word (no matter if it comes from TDC1 - 4)
            continue

        # Aurora receiver ID encoded in header must match rx_id of currently analyzed chip
        if ((actual_word >> 20) & 0xf) != rx_id:
            continue
        else:
            word = actual_word & 0x1ffff

        if (word & au.AURORA_HEADER) == au.USERK_FRAME_ID:  # skip USER_K frame
            continue

        if eos_mode:
            if actual_word & anb.HEADER_ID:  # Chip header
                word = word & 0xffff  # 16 bit chip data
                shift = 15
                if eos_bit:
                    found_start_of_stream = True  # Found start of stream
                    stream_state = 1
                    left_over_word = 0  # Reset old word. Left over bits should be orphan bits (frame alignment).
                    word_length = 0
                    if not actual_word & anb.NEW_STREAM_ID:  # End of stream
                        eos_bit = False
                        word = word + anb.NEW_STREAM_ID
                elif actual_word & anb.NEW_STREAM_ID:  # End of stream
                    word = word & 0x7fff
                    eos_bit = True
        else:
            if actual_word & anb.HEADER_ID:  # Chip header
                word = word & 0xffff  # 16 bit chip data
                if actual_word & anb.NEW_STREAM_ID:  # Beginning of new stream
                    # Set per stream variables
                    found_start_of_stream = True  # Found start of stream
                    # Check if left-over word are only orphan bits (all zero). Note: Still have leading 1.
                    # if au.number_of_set_bits(left_over_word) > 1:
                    #     print('WARNING HAVE STILL OLD WORD: %i', i, left_over_word, word_length, stream_state)
                    stream_state = 1  # Next information is tag. TODO: Need to check for R/O configuration. Depending on that, next word is tag (default) OR chip id and then tag.
                    left_over_word = 0  # Reset old word. Left over bits should be orphan bits (frame alignment).
                    word_length = 0
                else:  # Stream is still continuing
                    shift = 15  # Shift is only 15 bit since need to exclude NS bit.

        # In case stream just continues, append new raw data word (16 bit) to old data word
        if word_length < 47 and found_start_of_stream:  # Make sure not to be larger than 64 bits.
            word = (left_over_word << shift) + word  # Reassemble the complete raw data word.
            word_length = au.get_length(word)
        elif not found_start_of_stream:
            # If not found start of stream, continue (until start of stream is found)
            continue

        # Do not take new raw data word until no more bits are left or not enough bits for extracting an information
        while word_length != 0:
            # print('RAW {:<29} {:032b}'.format(*[word_length, word]))
            if stream_state == 1:  # Extract tag
                if word_length < 8:  # Check for complete 8 bit tag.
                    left_over_word = word  # Store left over data word
                    break

                tag = (word >> (word_length - 8)) & anb.TAG_MASK
                word = au.rm_msb(word, 8, word_length)
                word_length -= 8
                # print('TAG {:<29} {:032b}'.format(*[tag, word]))
                stream_state = 2  # Next information is ccol

            if stream_state == 2:  # Extract core column
                if word_length < 6:  # Check for complete 6 bit core column.
                    left_over_word = word  # Store left over data word
                    break
                ccol_buf = word >> (word_length - 6) & anb.CCOL_MASK
                if ccol_buf >= 56:  # Valid ccol values are < 56, >=56 is 11 bit INTERNAL tag (starts with 11 1XXXX XXXX = 111 + 8 bit tag).
                    # No valid ccol, no hit. Continue with next raw data word.
                    word = au.rm_msb(word, 3, word_length)
                    word_length -= 3
                    left_over_word = word
                    stream_state = 1  # Next information is INTERNAL tag
                elif ccol_buf == 0:  # End of stream
                    # Reset per stream variables. Continue with next raw data word
                    found_start_of_stream = False
                    left_over_word = 0
                    stream_state = 0  # Search for beginning of stream
                    break
                else:
                    # Valid hit, store core column value.
                    word = au.rm_msb(word, 6, word_length)
                    word_length -= 6
                    ccol = ccol_buf
                    left_over_word = word
                    stream_state = 4  # Next information is neighbor/last
                # print('CCOL {:<28} {:032b}'.format(*[ccol_buf, word]))

            if stream_state == 4:  # Extract islast and isneighbor
                if word_length < 3:  # Check for complete 2 bit islast and isneihbor
                    left_over_word = word  # Store left over data word
                    break
                is_last_neighbor = word >> (word_length - 2) & anb.ISLASTNEIGHBOUR_MASK
                is_last = (is_last_neighbor & 0x2) >> 1
                is_neighbor = is_last_neighbor & 0x1
                word = au.rm_msb(word, 2, word_length)
                word_length -= 2
                left_over_word = word  # Store left over data word
                # print('ISLAST/ISNEIGHBOR {:<1}/{:<13} {:032b} {:12}'.format(*[is_last, is_neighbor, word, word_length]))
                stream_state = 8  # Next information is qrow

            if stream_state == 8:  # Extract quarter row
                if word_length < 8:  # Check for complete 8 bit quarter row
                    left_over_word = word  # Store left over data word
                    break
                if is_neighbor:
                    q_row += 1
                else:
                    q_row = word >> (word_length - 8) & anb.QROW_MASK
                    word = au.rm_msb(word, 8, word_length)
                    word_length -= 8
                # print('QROW {:<28} {:032b}'.format(*[q_row, word]))
                stream_state = 16  # Next information is hmap

            if stream_state == 16:  # Extract hmap, decodes a quarter core (= 8 columns x 2 rows).
                if (raw_len > (i + 1)):
                    # Check for complete 30 bit hmap.
                    if ((word_length < 30 and hmap_is_compressed) or (word_length < 16 and not hmap_is_compressed)) and\
                            (eos_mode or not ((raw_data[i + 1] & anb.HEADER_ID and raw_data[i + 1] & anb.NEW_STREAM_ID) or raw_data[i + 1] & anb.TRIGGER_ID)):
                        left_over_word = word
                        break
                else:
                    left_over_word = word  # Store left over data word
                    break

                # Get decompressed 16 bit hitmap word and number of hits within quarter core
                hmap_word, hits_hmap, word, word_length = anb.get_hitmap(word, word_length, hmap_is_compressed)
                hmap_word_length = 16  # By construction
                # print('HMAP [NUMBER HITS OF HITS {:<6}] {:032b} {:12}'.format(*[hits_hmap, word, word_length]))
                stream_state = 32  # Next information is tot

            if stream_state == 32:  # Extract tot. ToT values for all hit pixels in quarter core (decompressed hitmap)
                if word_length < 4 * hits_hmap:  # Check for complete 4 bit tot for all hits.
                    if word_length < 32:  # Corresponds to 8 hits. Continue with next raw data word and extend word in next step.
                        left_over_word = word
                        break
                    else:  # Have enough bits to process first 8 hits (but can be less)
                        n_hitors = np.array([0, 1])
                        hits_hmap = hits_hmap - au.number_of_set_bits(hmap_word & 0xFF00)  # Count number of hits in first 8 bits
                        take_old_hitors = True
                else:
                    if take_old_hitors:
                        n_hitors = np.array([2, 3])
                        take_old_hitors = False
                    else:
                        n_hitors = np.array([0, 1, 2, 3])

                if len(ptot_table_stretched) > 1:
                    ptot_table_piece = ptot_table_stretched[trg_number]
                else:
                    ptot_table_piece = ptot_table_stretched[0]
                c1 = ptot_table_piece['hit_or_1_col']
                c2 = ptot_table_piece['hit_or_2_col']
                c3 = ptot_table_piece['hit_or_3_col']
                c4 = ptot_table_piece['hit_or_4_col']
                r1 = ptot_table_piece['hit_or_1_row']
                r2 = ptot_table_piece['hit_or_2_row']
                r3 = ptot_table_piece['hit_or_3_row']
                r4 = ptot_table_piece['hit_or_4_row']

                if q_row == 196:  # Precision TOT word
                    for hitor_id in n_hitors:  # Loop over data words from all hitors
                        if au.number_of_set_bits(hmap_word) == 1:  # Break in case no hits anymore
                            break
                        col, row, tot_val16, ptoa, word, word_length, hmap_word, hmap_word_length = anb.get_ptot_hit_data(hitor_id, c1, c2, c3, c4, r1, r2, r3, r4, word, word_length, hmap_word, hmap_word_length, invert_ptot)
                        if col < 65534 and tot_val16 < 2047 and (ccol - 1) * 8 + col < 400 and row < 384:  # Only add valid hits
                            occ_hist[(ccol - 1) * 8 + col, row] += 1
                else:  # Normal TOT word
                    start_hit = 0
                    stop_hit = 16
                    if np.array_equal(n_hitors, np.array([2, 3])):  # Processed already top part of hmap
                        start_hit = start_hit + 8
                    if np.array_equal(n_hitors, np.array([0, 1])):  # Process only top part of hmap
                        stop_hit = 8
                    for x in range(start_hit, stop_hit):  # Loop over all hits
                        hit = hmap_word >> (hmap_word_length - 1) & 0x1
                        hmap_word = au.rm_msb(hmap_word, 1, hmap_word_length)
                        hmap_word_length -= 1
                        # Calculate column and row using core column and position inside quarter core. Hmap words decodes hits in quater core (= 8 columns x 2 rows).
                        col = (ccol - 1) * 8 + np.mod(x, 8)
                        row = q_row * 2 + x // 8
                        if row > 383 or col > 399:
                            break
                        if hit == 1:  # High bit means hit
                            word = au.rm_msb(word, 4, word_length)
                            word_length -= 4
                            occ_hist[col, row] += 1

                if take_old_hitors:  # Could only process first 8 hit, thus continue with next raw data word.
                    left_over_word = word
                    break

                if is_last:
                    stream_state = 2  # Next information is ccol
                    left_over_word = word
                else:
                    stream_state = 4  # Next information is neighbor/last
                    left_over_word = word

    return (word, stream_state, found_start_of_stream, left_over_word, ccol, is_last, is_neighbor, q_row, hmap_word, hits_hmap,
            word_length, take_old_hitors, tag, trg_number, hmap_word_length)


@numba.njit
def histogram_tot(raw_data, hist_tot, data_word, is_fe_high_word, is_data_header, meta_data=None, rx_id=0):
    ''' Raw data to TOT histogram for each pixel'''
    for word in raw_data:
        if word & au.TRIGGER_HEADER:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_0:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_1:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_2:
            continue
        if word & au.TDC_HEADER == au.TDC_ID_3:
            continue

        # Aurora receiver ID encoded in header must match rx_id of currently analyzed chip
        if ((word >> 20) & 0xf) != rx_id:
            continue

        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.USERK_FRAME_ID:     # skip USER_K frame
            continue
        if (word & au.AURORA_HEADER & au.AURORA_HEADER_RXID_MASK) == au.HEADER_ID:          # data header
            is_data_header = 1
            is_fe_high_word = 1
        # Can only interpret FE words if high word is found, high word can
        # only be found in event header since event hit records do not
        # have a header
        elif is_fe_high_word < 0:
            continue
        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if is_fe_high_word == 1:
            data_word = word & 0xffff
            is_fe_high_word = 0  # Next is low word
            continue  # Low word still missing
        elif is_fe_high_word == 0:
            data_word = data_word << 16 | word & 0xffff
            is_fe_high_word = 1  # Next is high word

        if is_data_header == 1:
            # After data header follows header less hit data
            is_data_header = 0
        else:  # data word is hit data
            multicol = (data_word >> 26) & 0x3f
            region = (data_word >> 16) & 0x3ff
            for i in range(4):
                col, row = au.translate_mapping(multicol, region, i)
                tot = (data_word >> i * 4) & 0xf

                if col < 400 and row < 192:
                    if tot != 255 and tot != 15:
                        hist_tot[col, row, tot] += 1
                        # print np.count_nonzero(occ_hist)
                        # print occ_hist.sum()
                else:  # unknown word
                    continue

    return data_word, is_fe_high_word, is_data_header


class OnlineHistogrammingBase():
    ''' Base class to do online analysis with raw data from chip.

        The output data is a histogram of a given shape.
    '''
    _queue_timeout = 0.01  # max blocking time to delete object [s]

    def __init__(self, shape):
        self._raw_data_queue = multiprocessing.Queue()
        self.stop = multiprocessing.Event()
        self.lock = multiprocessing.Lock()
        self.last_add = None  # time of last add to queue
        self.shape = shape
        self.analysis_function_kwargs = {}
        self.p = None  # process

    def init(self):
        # Create shared memory 32 bit unsigned int numpy array
        n_values = reduce(lambda x, y: x * y, self.shape)
        shared_array_base = multiprocessing.Array(ctypes.c_uint, n_values)
        shared_array = np.ctypeslib.as_array(shared_array_base.get_obj())
        self.hist = shared_array.reshape(*self.shape)
        self.idle_worker = multiprocessing.Event()
        self.p = multiprocessing.Process(target=self.worker,
                                         args=(self._raw_data_queue, shared_array_base,
                                               self.lock, self.stop, self.idle_worker))
        self.p.start()
        logger.info('Starting process %d', self.p.pid)

    def analysis_function(self, raw_data, hist, *args):
        raise NotImplementedError("You have to implement the analysis_funtion")

    def add(self, raw_data, meta_data=None):
        ''' Add raw data to be histogrammed '''
        self.last_add = time.time()  # time of last add to queue
        self.idle_worker.clear()  # after addding data worker cannot be idle
        if meta_data is None:
            self._raw_data_queue.put(raw_data)
        else:
            self._raw_data_queue.put([raw_data, meta_data])

    def _reset_hist(self):
        with self.lock:
            self.hist = self.hist.reshape(-1)
            for i in range(self.hist.shape[0]):
                self.hist[i] = 0
            self.hist = self.hist.reshape(self.shape)

    def reset(self, wait=True, timeout=0.5):
        ''' Reset histogram '''
        if not wait:
            if not self._raw_data_queue.empty() or not self.idle_worker.is_set():
                logger.warning('Resetting histogram while filling data')
        else:
            if not self.idle_worker.wait(timeout):
                logger.warning('Resetting histogram while filling data')
        self._reset_hist()

    def get(self, wait=True, timeout=None, reset=True):
        ''' Get the result histogram '''
        if not wait:
            if not self._raw_data_queue.empty() or not self.idle_worker.is_set():
                logger.warning('Getting histogram while analyzing data')
        else:
            if not self.idle_worker.wait(timeout):
                logger.warning('Getting histogram while analyzing data. Consider increasing the timeout.')

        if reset:
            hist = self.hist.copy()
            # No overwrite with a new zero array due to shared memory
            self._reset_hist()
            return hist
        else:
            return self.hist

    def worker(self, raw_data_queue, shared_array_base, lock, stop, idle):
        ''' Histogramming in seperate process '''
        hist = np.ctypeslib.as_array(shared_array_base.get_obj()).reshape(self.shape)
        while not stop.is_set():
            try:
                data = raw_data_queue.get(timeout=self._queue_timeout)
                idle.clear()
                if len(data) == 2:  # Raw data and meta data
                    raw_data = data[0]
                    meta_data = anb.stretch_ptot_data(data[1])  # FIXME: This is bad (special case for ITkPixV1). Any other way?
                else:  # Only raw data
                    raw_data = data
                    meta_data = None
                with lock:
                    return_values = self.analysis_function(raw_data, hist, **self.analysis_function_kwargs, meta_data=meta_data)
                    self.analysis_function_kwargs.update(zip(self.analysis_function_kwargs, return_values))
            except queue.Empty:
                idle.set()
                continue
            except KeyboardInterrupt:   # Need to catch KeyboardInterrupt from main process
                stop.set()
        idle.set()

    def close(self):
        ''' Close process and wait till done. Likely needed to give access to pytable file handle.'''
        logger.info('Stopping process %d', self.p.pid)
        self._raw_data_queue.close()
        self._raw_data_queue.join_thread()  # Needed otherwise IOError: [Errno 232] The pipe is being closed
        self.stop.set()
        self.p.join()
        del self.p  # explicit delete required to free memory
        self.p = None

    def __del__(self):
        if self.p and self.p.is_alive():
            logger.warning('Process still running. Was close() called?')
            self.close()


class OccupancyHistogramming(OnlineHistogrammingBase):
    ''' Fast histogramming of raw data to a 2D hit histogramm

        No event building.
    '''

    def __init__(self, chip_type='rd53a', hmap_is_compressed=True, rx_id=0):
        if 'rd53a' in chip_type.lower():
            super().__init__(shape=(400, 192))
            self.analysis_function_kwargs = {'is_fe_high_word': -1, 'is_data_header': 0, 'data_word': 0}

            def analysis_function(self, raw_data, hist, is_fe_high_word, is_data_header, data_word, meta_data):
                return histogram(raw_data, hist, is_fe_high_word, is_data_header, data_word, meta_data, rx_id)
            setattr(OccupancyHistogramming, 'analysis_function', analysis_function)

        elif chip_type.lower() in ['itkpixv1', 'itkpixv2']:
            super().__init__(shape=(400, 384))
            if chip_type.lower() == 'itkpixv2':
                eos_mode = True
                invert_ptot = True
            else:
                eos_mode = False
                invert_ptot = False
            self.analysis_function_kwargs = {'word': 0, 'stream_state': 0, 'found_start_of_stream': False, 'left_over_word': 0, 'ccol': 0,
                                             'is_last': 0, 'is_neighbor': 0, 'q_row': 0, 'hmap_word': 0, 'hits_hmap': 0,
                                             'word_length': 0, 'take_old_hitors': False, 'tag': -1, 'trg_number': 0, 'hmap_word_length': 0}

            def analysis_function(self, raw_data, hist, word, stream_state, found_start_of_stream, left_over_word,
                                  ccol, is_last, is_neighbor, q_row, hmap_word, hits_hmap,
                                  word_length, take_old_hitors, tag, trg_number, hmap_word_length, meta_data):
                return histogram_rd53b(raw_data, hist, word, stream_state, found_start_of_stream, left_over_word,
                                       ccol, is_last, is_neighbor, q_row, hmap_word, hits_hmap,
                                       word_length, take_old_hitors, tag, trg_number, hmap_word_length, hmap_is_compressed, meta_data, rx_id, eos_mode=eos_mode, invert_ptot=invert_ptot)
            setattr(OccupancyHistogramming, 'analysis_function', analysis_function)
        else:
            raise NotImplementedError('Chip type %s not supported!' % chip_type)
        self.init()


class TotHistogramming(OnlineHistogrammingBase):
    ''' Fast histogramming of raw data to a TOT histogramm for each pixel

        No event building.
    '''
    _queue_timeout = 0.01  # max blocking time to delete object [s]

    def __init__(self, chip_type='rd53a', rx_id=0):
        if 'rd53a' in chip_type.lower():
            super().__init__(shape=(400, 192, 16))
            self.analysis_function_kwargs = {'is_fe_high_word': -1, 'is_data_header': 0, 'data_word': 0}

            def analysis_function(self, raw_data, hist, is_fe_high_word, is_data_header, data_word, meta_data):
                return histogram_tot(raw_data, hist, is_fe_high_word, is_data_header, data_word, meta_data, rx_id)
            setattr(TotHistogramming, 'analysis_function', analysis_function)
        else:
            raise NotImplementedError('Chip type %s not supported!' % chip_type)
        self.init()


if __name__ == "__main__":
    pass
