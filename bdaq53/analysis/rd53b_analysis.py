#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Collection of functions specific to the RD53B analysis
'''
import numpy as np
import numba
from collections import OrderedDict

import bdaq53.analysis.analysis_utils as au

# Word defines
USERK_FRAME_ID = 0x01000000
HEADER_ID = 0x00010000
NEW_STREAM_ID = 0x00008000  # Detect new stream
TRIGGER_ID = 0x80000000
TDC_ID_0 = 0x10000000
TDC_ID_1 = 0x20000000
TDC_ID_2 = 0x30000000
TDC_ID_3 = 0x40000000

# Data Masks
TRG_MASK = 0x7FFFFFFF  # Trigger data (number and/or time stamp)
TDC_HEADER_MASK = 0xF0000000  # TDC data header (decodes to which region the TDC word belongs to)
TDC_TRIG_DIST_MASK = 0x0FF00000  # TDC trigger distance
TDC_TIMESTAMP_MASK = 0x0FFFF000  # TDC timestamp
TDC_VALUE_MASK = 0x00000FFF  # TDC value
TAG_MASK = 0xFF  # RD53B tag (8 bit), location depends on data
CCOL_MASK = 0x3F  # RD53B core column (6 bit), location depends on data
ISLASTNEIGHBOUR_MASK = 0x3  # RD53B islast and isneighbour bit, location depends on data
QROW_MASK = 0xFF  # RD53B qrow (8 bit), location depends on data
TOT_MASK = 0xF  # RD53B (regular, no precision tot) tot (4 bit), location depends on data
HMAP_MASK = 0xFFFF  # RD53B non-compressed hitmap (16 bit), location depends on data


# Binary tree for decompressing hit map
BinaryTreeMaskSize = np.array([4, 2, 1])
BinaryTreeMask = np.array([[0x0F, 0x00, 0x00, 0x00, 0xF0, 0x00, 0x00, 0x00],
                           [0xF3, 0x00, 0xFC, 0x00, 0x3F, 0x00, 0xCF, 0x00],
                           [0xFD, 0xFE, 0xF7, 0xFB, 0xDF, 0xEF, 0x7F, 0xBF]])
BINARYTREE_DEPTH = 4


@numba.njit(cache=True, fastmath=True)
def is_new_event(n_event_header, n_trigger, start_tag, tag, prev_tag, last_tag, trg_header,
                 event_status, is_tag_offset, method):
    ''' Detect new event by different methods.

        Note:
        -----
        Checks for new events are rather complex to handle all possible
        cases. They are tested with high coverage in unit tests, do not
        make quick chances without checking that event building still works!

        Methods to do event alignment, create new event if:
        0: - Using tags: Not yet implemented
        1: - Previous data word is TLU trigger word. This is expected to be at the beginning of an event.
             OR
              - Number of event header (n_event_header) exceeds the number of sub-triggers used during data taking (n_trigger)
                This is a fallback to event header alignment and needed for rare cases where more BCIDs are readout.
        2: - Previous data word is TLU trigger word. This is expected to be at the beginning of an event. No error
             checks on FE data is applied (like number of event header). Needed for event reconstruction from TB data with sync flavor.
             This flavor has a lot of bugs in the digital part.
    '''
    if method == 0:
        # Number of trigge in event exceeded
        if n_event_header >= n_trigger:
            # Check if previous events data header are likely in this event
            return True
        else:
            return False
    elif method == 1:
        if not trg_header:
            # Fallback to event header alginment. Needed for missing or not recognized
            # trigger words.
            if n_event_header >= n_trigger:
                return True
            else:
                return False
        else:
            return True
    elif method == 2:
        # Every trigger creates a new event, independent how FE data looks like
        if not trg_header:
            return False
        else:
            return True
    else:
        raise


@numba.njit(cache=True, fastmath=True)
def interpret_data(rawdata, hits, hist_occ, hist_tot,
                   hist_rel_bcid, hist_trigger_id, hist_event_status,
                   hist_tdc_status, hist_tdc_value, hist_bcid_error,
                   hist_ptot, hist_ptoa,
                   scan_param_id, event_number=0,
                   trig_pattern=0b11111111111111111111111111111111,
                   align_method=0,
                   prev_tag=-1, prev_trg_number=-1, first_trg_number=-1, analyze_tdc=False,
                   use_tdc_trigger_dist=False, last_chunk=False, rx_id=0,
                   trigger_table_stretched=np.array([0]), ptot_table_stretched=np.array([0]),
                   hmap_is_compressed=True, eos_mode=False, max_col=400, max_row=384, invert_ptot=False):

    is_tag_offset = False
    trg_header = False
    data_out_i = 0

    # Per call variables
    n_trigger = au.number_of_set_bits(trig_pattern)
    # Calculate effective not byte aligned pattern (= ommit leading zeros)
    eff_trig_patt = trig_pattern
    for _ in range(32):
        if eff_trig_patt & 0x80000000:
            break
        eff_trig_patt = eff_trig_patt << 1

    # Hit buffer to store actual event hits, needed to set parameters
    # calculated at the end of an event and not available before
    hit_buffer_i = 0  # Index of entry of hit buffer
    hit_buffer = np.zeros_like(hits)
    last_word_index = 0  # Last raw data index of full event

    # Per stream variables
    # Indicates which information (tag, ccol, qrow, tot, ...) is next.
    # 0: search for beginning of stream
    # 1: next information is tag
    # 2: next information is ccol
    # 4: next information is neighbor/last
    # 8: next information is qrow
    # 16: next information is hmap
    # 32: next information is tot
    stream_state = 0
    found_start_of_stream = False
    left_over_word = 0  # Store left-over raw data word. Needed since RD53B data words have variable length, thus FPGA-splitting of raw data words (16 bit) can lead to incomplete raw data words
    ccol = 0  # core column
    is_last = 0
    is_neighbor = 0
    q_row = 0  # quarter core
    hmap_word = 0
    hits_hmap = 0
    word_length = 0  # Number of bits of word
    take_old_hitors = False

    # Per event variables
    tag = -1
    start_tag = -1  # First tag of new event
    prev_tag = -1  # Tag before the expected tag
    last_tag = -1
    n_tags = 0  # Number of tags in event
    event_status = 0
    trg_number = 0
    tdc_word_count = 0  # TDC word counter per event
    eos_bit = True
    tdc_status = np.full(shape=(4,), fill_value=0, dtype=np.uint8)  # Status for each TDC line
    tdc_status_buffer = np.full(shape=(4,), fill_value=0, dtype=np.uint8)  # Array in order to buffer TDC status
    tdc_value = np.full(shape=(4,), fill_value=2 ** 16 - 1, dtype=np.uint16)  # TDC value for each Hitor line
    tdc_value_buffer = np.full(shape=(4,), fill_value=2 ** 16 - 1,
                               dtype=np.uint16)  # Array in order to buffer TDC values
    tdc_timestamp = np.zeros(shape=(4,),
                             dtype=np.uint16)  # TDC timestamp for each Hitor line. If use_trigger_dist is True, this will be trigger distance
    tdc_timestamp_buffer = np.zeros(shape=(4,), dtype=np.uint16)  # Array in order to buffer TDC timestamps
    tdc_word = np.full(shape=(4,), fill_value=False, dtype=np.bool_)  # Indicator for TDC word for each Hitor line
    event_status_tdc = 0  # Separate variable for TDC event status in order to buffer the event status of TDC (TDC comes before actual event if no external trigger word is provided)
    c1, c2, c3, c4, r1, r2, r3, r4 = 0, 0, 0, 0, 0, 0, 0, 0

    # Loop over raw data words
    raw_len = len(rawdata)
    for i, actual_word in enumerate(rawdata):
        shift = 16  # Needed shift for appending left-over raw data word

        # Continue if raw data word is 0
        if not actual_word and not found_start_of_stream:
            continue

        if actual_word & au.TRIGGER_HEADER:  # TLU word
            trg_word_id = i
            trg_number = actual_word & au.TRG_MASK
            event_status |= au.E_EXT_TRG
            trg_header = True
            if first_trg_number < 0:
                first_trg_number = actual_word & au.TRG_MASK
            # Special case for first trigger word
            if (align_method == 1 or align_method == 2):
                if i == 0:
                    prev_trg_number = trg_number
                    trg_header = False
            continue

        if au.is_tdc_word(actual_word) and analyze_tdc:  # Select first TDC word (no matter if it comes from TDC1 - 4)
            tdc_word_count += 1  # Increase TDC word counter (per event).
            tdc_index = (actual_word & 0x70000000) >> 28  # Get index of TDC module (Hitor line).
            if tdc_word[tdc_index - 1]:  # If event has already TDC word from the actual tdc index, set TDC ambiguous status.
                if not use_tdc_trigger_dist:
                    # Set TDC status per TDC line.
                    tdc_status_buffer[tdc_index - 1] |= au.H_TDC_AMBIGUOUS
                elif (au.TDC_TRIG_DIST_MASK & actual_word) >> 20 < 254:  # In case of trigger distance measurement, a valid trigger distance (smaller than 254) defines the valid TDC word
                    if tdc_timestamp_buffer[tdc_index - 1] < 254:
                        # Set TDC ambiguous if have already a valid TDC word (trigger distance < 254)
                        tdc_status_buffer[tdc_index - 1] |= au.H_TDC_AMBIGUOUS
                    else:
                        # If still had no valid, update TDC word with the first valid one
                        tdc_timestamp_buffer[tdc_index - 1] = (au.TDC_TRIG_DIST_MASK & actual_word) >> 20
                        tdc_value_buffer[tdc_index - 1] = actual_word & au.TDC_VALUE_MASK
                        if tdc_value_buffer[tdc_index - 1] != 0xFFF and not (tdc_timestamp_buffer[tdc_index - 1] == 254 and use_tdc_trigger_dist):
                            # Un-set TDC overflow in case found valid TDC word
                            tdc_status_buffer[tdc_index - 1] = tdc_status_buffer[tdc_index - 1] & ~au.H_TDC_OVF
                        if tdc_value_buffer[tdc_index - 1] != 0 and not (tdc_timestamp_buffer[tdc_index - 1] == 255 and use_tdc_trigger_dist):
                            # Un-set TDC error in case found valid TDC word
                            tdc_status_buffer[tdc_index - 1] = tdc_status_buffer[tdc_index - 1] & ~au.H_TDC_ERR
            else:  # Set for the first TDC word the corresponding event status
                # Do not use event status directly, in order to buffer TDC event status, since it comes before
                # the actual event (before EH0).
                event_status_tdc = au.E_TDC
                tdc_word[tdc_index - 1] = True  # Set TDC indicator for actual TDC index to True
                tdc_value_buffer[tdc_index - 1] = actual_word & au.TDC_VALUE_MASK  # Extract the TDC value from TDC word
                if use_tdc_trigger_dist:
                    # If use_tdc_trigger_dist is True, extract trigger distance (delay between Hitor and Trigger) from TDC word
                    tdc_timestamp_buffer[tdc_index - 1] = (au.TDC_TRIG_DIST_MASK & actual_word) >> 20
                else:
                    # If use_tdc_trigger_dist is False, extract TDC timestamp from TDC word
                    tdc_timestamp_buffer[tdc_index - 1] = (au.TDC_TIMESTAMP_MASK & actual_word) >> 12

                # Set error/overflow
                if tdc_value_buffer[tdc_index - 1] == 0xFFF or (
                        tdc_timestamp_buffer[tdc_index - 1] == 254 and use_tdc_trigger_dist):
                    tdc_status_buffer[tdc_index - 1] |= au.H_TDC_OVF
                if tdc_value_buffer[tdc_index - 1] == 0 or (
                        tdc_timestamp_buffer[tdc_index - 1] == 255 and use_tdc_trigger_dist):
                    tdc_status_buffer[tdc_index - 1] |= au.H_TDC_ERR

            if i == 0 and align_method == 0:  # Special case if first word is TDC word. Only possible if no external trigger word is provided.
                event_status |= event_status_tdc
                for index in range(4):
                    tdc_value[index] = tdc_value_buffer[index]
                    tdc_timestamp[index] = tdc_timestamp_buffer[index]
                    tdc_status[index] = tdc_status_buffer[index]
                tdc_word[:] = False
                event_status_tdc = 0

            continue

        # Aurora receiver ID encoded in header must match rx_id of currently analyzed chip
        if ((actual_word >> 20) & 0xf) != rx_id:
            event_status |= au.E_INV_RX_ID
            continue
        else:
            word = actual_word & 0x1ffff

        if (word & au.AURORA_HEADER) == au.USERK_FRAME_ID:  # skip USER_K frame
            event_status |= au.E_USER_K
            continue
        if eos_mode:
            if actual_word & HEADER_ID:  # Chip header
                word = word & 0xffff  # 16 bit chip data
                shift = 15
                if eos_bit:
                    found_start_of_stream = True  # Found start of stream
                    if au.number_of_set_bits(left_over_word) > 1:
                        event_status |= au.E_NON_Z_OB
                    stream_state = 1
                    left_over_word = 0  # Reset old word. Left over bits should be orphan bits (frame alignment).
                    word_length = 0
                    if not actual_word & NEW_STREAM_ID:  # End of stream
                        eos_bit = False
                        word = word + NEW_STREAM_ID
                elif actual_word & NEW_STREAM_ID:  # End of stream
                    word = word & 0x7fff
                    eos_bit = True
        else:
            if actual_word & HEADER_ID:  # Chip header
                word = word & 0xffff  # 16 bit chip data
                if actual_word & NEW_STREAM_ID:  # Beginning of new stream
                    # Set per stream variables
                    found_start_of_stream = True  # Found start of stream
                    stream_state = 1  # Next information is tag. TODO: Need to check for R/O configuration. Depending on that, next word is tag (default) OR chip id and then tag.
                    # Check if left-over word are only orphan bits (all zero). Note: Still have leading 1.
                    if au.number_of_set_bits(left_over_word) > 1:
                        event_status |= au.E_NON_Z_OB
                        # print('WARNING HAVE STILL OLD WORD: %i', left_over_word, word_length, stream_state)
                    left_over_word = 0  # Reset old word. Left over bits should be orphan bits (frame alignment).
                    word_length = 0
                else:  # Stream is still continuing
                    shift = 15  # Shift is only 15 bit since need to exclude NS bit.

        # In case stream just continues, append new raw data word (16 bit) to old data word
        if word_length < 47 and found_start_of_stream:  # Make sure not to be larger than 64 bits.
            word = (left_over_word << shift) + word  # Reassemble the complete raw data word.
            word_length = au.get_length(word)
        elif not found_start_of_stream:
            # If not found start of stream, continue (until start of stream is found)
            continue

        # Do not take new raw data word until no more bits are left or not enough bits for extracting an information
        while word_length != 0:
            if stream_state == 1:  # Extract tag
                if word_length < 8:  # Check for complete 8 bit tag.
                    left_over_word = word  # Store left over data word
                    break

                tag = (word >> (word_length - 8)) & TAG_MASK
                word = au.rm_msb(word, 8, word_length)
                word_length -= 8

                # Special cases for first event/tag
                if start_tag == -1:
                    start_tag = tag
                    if align_method == 0:
                        prev_trg_number = trg_number
                    event_status |= event_status_tdc
                    for index in range(4):
                        tdc_value[index] = tdc_value_buffer[index]
                        tdc_timestamp[index] = tdc_timestamp_buffer[index]
                        tdc_status[index] = tdc_status_buffer[index]
                    tdc_word[:] = False
                    event_status_tdc = 0
                    tdc_value_buffer[:] = 2 ** 16 - 1
                    tdc_timestamp_buffer[:] = 0
                    tdc_status_buffer[:] = 0

                if is_new_event(n_tags, n_trigger, start_tag, tag, prev_tag, last_tag, trg_header, event_status, is_tag_offset, align_method):
                    # Set event errors of old event
                    if n_tags != n_trigger:
                        event_status |= au.E_STRUCT_WRONG
                    if tag < 0:
                        event_status |= au.E_STRUCT_WRONG
                    if trg_header and not au.check_difference(prev_trg_number, trg_number, 31):
                        event_status |= au.E_EXT_TRG_ERR
                    # Handle case of too many event header than expected by setting
                    # flag to check BCIDs in next event more carfully (allow to exceed n_triggers event header)
                    if au.check_difference(last_tag, tag, 5):
                        is_tag_offset = 1
                    else:
                        is_tag_offset = False

                    if len(trigger_table_stretched) > 1:
                        scan_param_id = trigger_table_stretched[trg_number - first_trg_number]
                    elif len(ptot_table_stretched) > 1:
                        scan_param_id = ptot_table_stretched[trg_number - first_trg_number]['scan_param_id']

                    data_out_i = au.build_event(hits, data_out_i,
                                                hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                                                hist_event_status, hist_tdc_status, hist_tdc_value,
                                                hist_ptot, hist_ptoa,
                                                hit_buffer, hit_buffer_i, start_tag,
                                                scan_param_id, event_status, tdc_status)
                    event_number += 1

                    # Calculate last_word_index in order to split properly the chunks
                    if align_method == 2:
                        last_word_index = trg_word_id  # in case of force trg numer calculate offset always to last trigger word
                    else:
                        # TODO: last word index should always be beginning of stream?
                        if not trg_header:
                            if analyze_tdc:  # Since TDC words come before the actual event (before EH 0) need to set last_word_index to first TDC word
                                last_word_index = i - tdc_word_count - 1  # to get first TDC word
                            else:
                                last_word_index = i - 1  # to get high word
                        else:
                            if analyze_tdc:
                                # FIXME: i - tdc_word_count - 2 should also work.
                                # But 1 of 1000000 it can happen that trigger word comes in between TDC words.
                                last_word_index = trg_word_id  # to get trigger word
                            else:
                                last_word_index = i - 2  # to get trigger word

                    # Reset per event variables
                    hit_buffer_i = 0
                    start_tag = tag
                    prev_trg_number = trg_number
                    trg_header = False
                    n_tags = 0

                    # If old event struture is wrong this event is likely wrong too
                    # Assume trigger ID error if no external trigger is available
                    if (event_status & au.E_STRUCT_WRONG and align_method == 0):
                        event_status = au.E_BCID_INC_ERROR
                    else:
                        event_status = 0

                    # Set after reset of event variables the TDC event status and write tdc value and timestamp.
                    # Needed since TDC words come before actual event.
                    event_status |= event_status_tdc
                    for index in range(4):
                        tdc_value[index] = tdc_value_buffer[index]
                        tdc_timestamp[index] = tdc_timestamp_buffer[index]
                        tdc_status[index] = tdc_status_buffer[index]
                    # Reset TDC variables
                    event_status_tdc = 0
                    tdc_value_buffer[:] = 2 ** 16 - 1
                    tdc_timestamp_buffer[:] = 0
                    tdc_status_buffer[:] = 0
                    tdc_word[:] = False
                    tdc_word_count = 0

                stream_state = 2  # Next information is ccol
                # Set tag counter variables
                n_tags += 1
                prev_tag = tag
                last_tag = tag

            if stream_state == 2:  # Extract core column
                if word_length < 6:  # Check for complete 6 bit core column.
                    left_over_word = word  # Store left over data word
                    break
                ccol_buf = word >> (word_length - 6) & CCOL_MASK
                if ccol_buf >= 56:  # Valid ccol values are < 56, >=56 is 11 bit INTERNAL tag (starts with 11 1XXXX XXXX = 111 + 8 bit tag).
                    # No valid ccol, no hit. Continue with next raw data word.
                    word = au.rm_msb(word, 3, word_length)
                    word_length -= 3
                    left_over_word = word
                    stream_state = 1  # Next information is INTERNAL tag
                elif ccol_buf == 0:  # End of stream
                    # Reset per stream variables. Continue with next raw data word
                    found_start_of_stream = False
                    left_over_word = 0
                    stream_state = 0  # Search for beginning of stream
                    break
                else:
                    # Valid hit, store core column value.
                    ccol = ccol_buf
                    word = au.rm_msb(word, 6, word_length)
                    word_length -= 6
                    left_over_word = word
                    stream_state = 4  # Next information is neighbor/last

            if stream_state == 4:  # Extract islast and isneighbor
                if word_length < 3:  # Check for complete 2 bit islast and isneihbor
                    left_over_word = word  # Store left over data word
                    break
                is_last_neighbor = word >> (word_length - 2) & ISLASTNEIGHBOUR_MASK
                is_last = (is_last_neighbor & 0x2) >> 1
                is_neighbor = is_last_neighbor & 0x1
                word = au.rm_msb(word, 2, word_length)
                word_length -= 2
                left_over_word = word  # Store left over data word
                stream_state = 8  # Next information is qrow

            if stream_state == 8:  # Extract quarter row
                if word_length < 8:  # Check for complete 8 bit quarter row
                    left_over_word = word  # Store left over data word
                    break
                if is_neighbor:
                    q_row += 1
                else:
                    q_row = word >> (word_length - 8) & QROW_MASK
                    word = au.rm_msb(word, 8, word_length)
                    word_length -= 8

                stream_state = 16  # Next information is hmap

            if stream_state == 16:  # Extract hmap, decodes a quarter core (= 8 columns x 2 rows).
                if (raw_len > (i + 1)):
                    # Check for complete 30 bit hmap.
                    if ((word_length < 30 and hmap_is_compressed) or (word_length < 16 and not hmap_is_compressed)) and\
                            (eos_mode or not ((rawdata[i + 1] & HEADER_ID and rawdata[i + 1] & NEW_STREAM_ID) or rawdata[i + 1] & TRIGGER_ID)):
                        left_over_word = word
                        break
                else:
                    left_over_word = word  # Store left over data word
                    break

                # Get decompressed 16 bit hitmap word and number of hits within quarter core
                hmap_word, hits_hmap, word, word_length = get_hitmap(word, word_length, hmap_is_compressed)
                hmap_word_length = 16  # By construction
                stream_state = 32  # Next information is tot

            if stream_state == 32:  # Extract tot. ToT values for all hit pixels in quarter core (decompressed hitmap)
                if word_length < 4 * hits_hmap:  # Check for complete 4 bit tot for all hits.
                    if word_length < 32:  # Corresponds to 8 hits. Continue with next raw data word and extend word in next step.
                        left_over_word = word
                        break
                    else:  # Have enough bits to process first 8 hits (but can be less)
                        n_hitors = np.array([0, 1])
                        hits_hmap = hits_hmap - au.number_of_set_bits(hmap_word & 0xFF00)  # Count number of hits in first 8 bits
                        take_old_hitors = True
                else:
                    if take_old_hitors:  # Processed already first 8 hits
                        n_hitors = np.array([2, 3])
                        take_old_hitors = False
                    else:
                        n_hitors = np.array([0, 1, 2, 3])  # Process all hits

                if len(ptot_table_stretched) > 1:
                    ptot_table_piece = ptot_table_stretched[trg_number]
                else:
                    ptot_table_piece = ptot_table_stretched[0]
                c1 = ptot_table_piece['hit_or_1_col']
                c2 = ptot_table_piece['hit_or_2_col']
                c3 = ptot_table_piece['hit_or_3_col']
                c4 = ptot_table_piece['hit_or_4_col']
                r1 = ptot_table_piece['hit_or_1_row']
                r2 = ptot_table_piece['hit_or_2_row']
                r3 = ptot_table_piece['hit_or_3_row']
                r4 = ptot_table_piece['hit_or_4_row']

                hit_buffer, hit_buffer_i, word, word_length, hmap_word, hmap_word_length = add_hits(hit_buffer, tag, ccol, q_row,
                                                                                                    event_number, trg_number, hit_buffer_i,
                                                                                                    c1, c2, c3, c4, r1, r2, r3, r4,
                                                                                                    tdc_value, tdc_timestamp, n_hitors,
                                                                                                    word, word_length, hmap_word, hmap_word_length,
                                                                                                    max_col, max_row, invert_ptot)

                if take_old_hitors:
                    left_over_word = word
                    break

                if is_last:
                    stream_state = 2  # Next information is ccol
                    left_over_word = word
                else:
                    stream_state = 4  # Next information is neighbor/last
                    left_over_word = word

    if last_chunk:
        if len(trigger_table_stretched) > 1:
            scan_param_id = trigger_table_stretched[trg_number - first_trg_number]
        data_out_i = au.build_event(hits, data_out_i,
                                    hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
                                    hist_event_status, hist_tdc_status, hist_tdc_value,
                                    hist_ptot, hist_ptoa,
                                    hit_buffer, hit_buffer_i, start_tag,
                                    scan_param_id, event_status, tdc_status)
        event_number += 1
        last_word_index = i + 1

    return data_out_i, event_number, last_word_index - rawdata.shape[0], start_tag - 1, prev_trg_number - 1, first_trg_number


@numba.njit(cache=True, fastmath=True)
def get_hitmap(word, word_length, hmap_is_compressed=True):
    ''' Decode hitmap and number of hits (within 2 x 8 quarter core)
    '''
    if hmap_is_compressed:
        hmap_word = 0x10000  # 16 bit decompressed hitmap + 1 bit indicated length of word
        # Get first bitpair of compressed hitmap word
        bitpair, word, word_length = get_bit_pair(word, word_length)
        h_map_reco, word, word_length = decompress_hitmap(word, word_length)
        if bitpair == 0x2:  # PAIR = 10 --> only upper 8 pixels contain hits
            hmap_word |= (h_map_reco << 8)  # decompressed 16 bit hitmap word
            hits_hmap = au.number_of_set_bits(hmap_word) - 1  # Number of hits in hit map
        elif bitpair == 0x1:  # PAIR = 01 --> only lower 8 pixels contain hits
            hmap_word |= h_map_reco & 0x100FF  # decompressed 16 bit hitmap word
            hits_hmap = au.number_of_set_bits(hmap_word) - 1  # Number of hits in hit map
        else:  # PAIR = 11  --> upper AND lower 8 pixels contain hits
            h_map_reco_low, word, word_length = decompress_hitmap(word, word_length)
            hmap_word |= (h_map_reco << 8)
            hmap_word |= h_map_reco_low & 0x100FF
            hits_hmap = au.number_of_set_bits(hmap_word) - 1  # Number of hits in hit map
    else:
        hmap_word = (word >> (word_length - 16) & HMAP_MASK) + 0x10000
        # Need to re-arrange the hitmap word. Dont know why this is needed.
        hmap_word = ((hmap_word & 0x000FF) << 8) + ((hmap_word & 0x0FF00) >> 8) + 0x10000  # need to swap rows (swap the two 8bit parts of 16bit hit map)
        hmap_word = 0x10000 + (au.reverse_bits((hmap_word & 0x0FF00) >> 8) << 8) + (au.reverse_bits(hmap_word & 0x000FF))  # need to swap columns (swap bits inside the two 8bit parts of 16bit hit map)
        hits_hmap = au.number_of_set_bits(hmap_word) - 1  # Number of hits in hit map
        word = au.rm_msb(word, 16, word_length)
        word_length -= 16
    return hmap_word, hits_hmap, word, word_length


@numba.njit(cache=True, fastmath=True)
def get_bit_pair(word, word_length):
    ''' Get bit pair (01, 10 or 11). Pair 01 is huffman-converted to 0.
    '''
    pair = word >> (word_length - 2) & 0x3
    if pair == 0x0 or pair == 0x1:
        word = au.rm_msb(word, 1, word_length)
        return 0x1, word, word_length - 1
    else:
        word = au.rm_msb(word, 2, word_length)
        return pair, word, word_length - 2


@numba.njit(cache=True, fastmath=True)
def decompress_hitmap(word, word_length):
    ''' Reconstruct decompressed hitmap.
    '''
    hitmap = 0xFF  # Start with fully occupied hitmap, ones will be crossed out.
    n_read = np.array([1, 0, 0, 0], dtype=np.int_)
    n_shift = np.zeros(shape=(BINARYTREE_DEPTH, 16), dtype=np.int_)
    for lv in range(BINARYTREE_DEPTH - 1):  # levels of tree
        for ir in range(n_read[lv]):
            bitpair, word, word_length = get_bit_pair(word, word_length)

            if bitpair == 0x1:  # 01
                hitmap = hitmap & BinaryTreeMask[lv][n_shift[lv][ir]]
                n_shift[lv + 1][n_read[lv + 1]] = n_shift[lv][ir]
                n_read[lv + 1] += 1

            if bitpair == 0x2:  # 10
                hitmap = hitmap & BinaryTreeMask[lv][n_shift[lv][ir] + BinaryTreeMaskSize[lv]]
                n_shift[lv + 1][n_read[lv + 1]] = n_shift[lv][ir] + BinaryTreeMaskSize[lv]
                n_read[lv + 1] += 1

            if bitpair == 0x3:  # 11
                hitmap = hitmap & (BinaryTreeMask[lv][n_shift[lv][ir]] | BinaryTreeMask[lv][n_shift[lv][ir] + BinaryTreeMaskSize[lv]])
                n_shift[lv + 1][n_read[lv + 1]] = n_shift[lv][ir] + BinaryTreeMaskSize[lv]
                n_read[lv + 1] += 1
                n_shift[lv + 1][n_read[lv + 1]] = n_shift[lv][ir]
                n_read[lv + 1] += 1

    return hitmap, word, word_length


@numba.njit(cache=True, fastmath=True)
def get_ptot_hit_data(hitor_id, c1, c2, c3, c4, r1, r2, r3, r4, word, word_length, hmap_word, hmap_word_length, invert_ptot):
    tot_val16 = 0
    for _ in range(4):  # Loop over four the 4-bit fragments and re-construct 16 bit ptot word from the 4-bit fragments.
        hit = hmap_word >> (hmap_word_length - 1) & 0x1
        hmap_word = au.rm_msb(hmap_word, 1, hmap_word_length)
        hmap_word_length -= 1
        if hit:  # Hit
            tot_val = word >> (word_length - 4) & TOT_MASK
            word = au.rm_msb(word, 4, word_length)
            word_length -= 4
            tot_val16 += tot_val
        else:
            tot_val16 += 15  # No hit
        tot_val16 = tot_val16 << 4
    tot_val16 = tot_val16 >> 4  # revert last shift
    if invert_ptot:
        tot_val16 = ~tot_val16 & 0xffff  # invert

    ptoa = (tot_val16 & 0x000f) << 1 + ((tot_val16 & 0x0080) >> 7)
    ptot = ((tot_val16 & 0xf000) >> 12) + ((tot_val16 & 0x0f00) >> 4) + ((tot_val16 & 0x0070) << 4)

    if hitor_id == 0:
        col = c1
        row = r1
        if c1 == 65535 and c2 == 65535 and c3 == 65535 and c4 == 65535:
            col = 0
            row = 0
    if hitor_id == 1:
        col = c2
        row = r2
        if c1 == 65535 and c2 == 65535 and c3 == 65535 and c4 == 65535:
            col = 1
            row = 0
    if hitor_id == 2:
        col = c3
        row = r3
        if c1 == 65535 and c2 == 65535 and c3 == 65535 and c4 == 65535:
            col = 2
            row = 0
    if hitor_id == 3:
        col = c4
        row = r4
        if c1 == 65535 and c2 == 65535 and c3 == 65535 and c4 == 65535:
            col = 3
            row = 0

    return (col, row, ptot, ptoa, word, word_length, hmap_word, hmap_word_length)


@numba.njit(cache=True, fastmath=True)
def add_hits(hits, tag, ccol, q_row, event_number, trg_number, hit_buffer_i, c1, c2, c3, c4, r1, r2, r3, r4, tdc_value, tdc_timestamp, n_hitors, word, word_length, hmap_word, hmap_word_length, max_col, max_row, invert_ptot):
    if q_row == 196:  # Precision TOT word
        tot_val4 = 0  # No regular 4 bit ToT in precision ToT mode
        for hitor_id in n_hitors:  # Loop over data words from all hitors
            if au.number_of_set_bits(hmap_word) == 1:  # Break in case no hits anymore
                break
            if not (hmap_word >> (hmap_word_length - 4) & 0xF):
                hmap_word = au.rm_msb(hmap_word, 4, hmap_word_length)
                hmap_word_length -= 4
                continue
            col, row, ptot, ptoa, word, word_length, hmap_word, hmap_word_length = get_ptot_hit_data(hitor_id, c1, c2, c3, c4, r1, r2, r3, r4, word, word_length, hmap_word, hmap_word_length, invert_ptot)
            if col < 65534 and ptot < 2047 and (ccol - 1) * 8 + col < max_col and row < max_row:  # FIXME: Why? Only add valid hits
                au.fill_buffer(hits, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, tag, tag, tag, (ccol - 1) * 8 + col, row, tot_val4, ptot)
                hit_buffer_i += 1
    else:  # Normal TOT word
        ptot = 0  # No PToT available in case of normal TOT mode
        ptoa = 0  # No PtoA available in case of normal TOT mode
        start_hit = 0
        stop_hit = 16
        if np.array_equal(n_hitors, np.array([2, 3])):  # Processed already top part of hmap
            start_hit = start_hit + 8
        if np.array_equal(n_hitors, np.array([0, 1])):  # Process only top part of hmap
            stop_hit = 8
        for x in range(start_hit, stop_hit):  # Loop over all hits
            hit = hmap_word >> (hmap_word_length - 1) & 0x1
            hmap_word = au.rm_msb(hmap_word, 1, hmap_word_length)
            hmap_word_length -= 1
            # Calculate column and row using core column and position inside quarter core. Hmap words decodes hits in quarter core (= 8 columns x 2 rows).
            col = (ccol - 1) * 8 + np.mod(x, 8)
            row = q_row * 2 + x // 8
            if row > max_row or col > max_col:
                break
            if hit == 1:  # High bit means hit
                tot_val = word >> (word_length - 4) & TOT_MASK
                word = au.rm_msb(word, 4, word_length)
                word_length -= 4
                if col < max_col and row < max_row:  # FIXME: Why is ccol 51 observed?
                    au.fill_buffer(hits, hit_buffer_i, event_number, trg_number, tdc_value, tdc_timestamp, ptoa, tag, tag, tag, col, row, tot_val, ptot)
                    hit_buffer_i += 1

    return hits, hit_buffer_i, word, word_length, hmap_word, hmap_word_length


def analyze_chunk(rawdata, return_hists=('HistOcc',), return_hits=False,
                  scan_param_id=0,
                  trig_pattern=0b11111111111111111111111111111111,
                  align_method=0,
                  analyze_tdc=False,
                  use_tdc_trigger_dist=False,
                  analyze_ptot=False,
                  rx_id=0,
                  ext_trig_id_map=np.array([0]),
                  ptot_table_stretched=np.array([0])):
    ''' Helper function to quickly analyze a data chunk.

        Warning
        -------
            If the rawdata contains incomplete event data only data that do
            not need event building are correct (occupancy + tot histograms)

        Parameters
        ----------
        rawdata : np.array, 32-bit dtype
            The raw data containing FE, trigger and TDC words
        return_hists : iterable of strings
            Names to select the histograms to return. Is not case sensitive
            and string must contain only e.g.: occ, tot, bcid, event
        return_hits : boolean
            Return the hit array
        scan_param_id : integer
            Set scan par id in hit info table
        trig_pattern : integer (32-bits)
            Indicate the position of the sub-triggers. Needed to check
            the BCIDs.
        align_method : integer
            Methods to do event alignment
            0: New event when number if event headers exceeds number of
               sub-triggers. Many fallbacks for corrupt data implemented.
            1: New event when data word is TLU trigger word, with error checks
            2: Force new event always at TLU trigger word, no error checks
        analyze_tdc : boolean
            If analyze_tdc is True, interpret and analyze also TDC words. Default is False,
            meaning that TDC analysis is skipped. This is useful for scans which do no
            require an TDC word interpretation (e.g. threshold scan) in order to save time.
        analyze_ptot : boolean
            If analyze_ptot is True, RD53B ToT words will be interpreted as PTOT words (contain precision ToT and ToA).
            Only possible for RD53B.
        use_tdc_trigger_dist : boolean
            If True use trigger distance (delay between Hitor and Trigger) in TDC word
            interpretation. If False use instead TDC timestamp from TDC word. Default
            is False.

        Usefull for tuning. Analysis per scan parameter not possible.
        Chunks should not be too large.

        Returns
        -------
            ordered dict: With key = return_hists string, value = data
            and first entry are hits when selected
    '''

    n_hits = rawdata.shape[0] * 4
    (hits, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id, hist_event_status, hist_tdc_status, hist_tdc_value,
     hist_bcid_error, hist_ptot, hist_ptoa) = au.init_outs(n_hits, n_scan_params=1, rows=384, analyze_ptot=analyze_ptot)

    if len(ptot_table_stretched) == 1:
        ptot_table_stretched = np.array([(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)], dtype=np.dtype(
            {'names': ['cmd_number_start', 'cmd_number_stop', 'cmd_length', 'scan_param_id', 'hit_or_1_col',
                       'hit_or_1_row', 'hit_or_2_col', 'hit_or_2_row', 'hit_or_3_col', 'hit_or_3_row',
                       'hit_or_4_col', 'hit_or_4_row'],
             'formats': [np.uint32, np.uint32, np.uint32, np.uint32, np.uint16, np.uint16, np.uint16, np.uint16,
                         np.uint16, np.uint16, np.uint16, np.uint16]}))

    interpret_data(rawdata, hits, hist_occ, hist_tot,
                   hist_rel_bcid, hist_trigger_id, hist_event_status,
                   hist_tdc_status, hist_tdc_value, hist_bcid_error,
                   hist_ptot, hist_ptoa,
                   scan_param_id=scan_param_id, event_number=0,
                   trig_pattern=trig_pattern,
                   align_method=align_method,
                   prev_tag=-1,
                   analyze_tdc=analyze_tdc,
                   use_tdc_trigger_dist=use_tdc_trigger_dist,
                   last_chunk=True,
                   rx_id=rx_id,
                   trigger_table_stretched=np.array([0]),
                   ptot_table_stretched=ptot_table_stretched)

    hists = {'occ': hist_occ,
             'tot': hist_tot,
             'rel': hist_rel_bcid,
             'event': hist_event_status,
             'error': hist_bcid_error
             }

    ret = OrderedDict()

    if return_hits:
        ret['hits'] = hits

    for key, value in hists.items():
        for word in return_hists:
            if key in word.lower():
                ret[word] = value
                break

    return ret


def interpret_userk_data(rawdata):
    userk_data = np.zeros(shape=rawdata.shape[0],
                          dtype={
                              'names': ['ChipID', 'AuroraKWord', 'Status', 'Data1', 'Data1_AddrFlag', 'Data1_Addr',
                                        'Data1_Data',
                                        'Data0', 'Data0_AddrFlag', 'Data0_Addr', 'Data0_Data'],
                              'formats': ['uint8', 'uint8', 'uint8', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16',
                                          'uint16',
                                          'uint16', 'uint16']})
    userk_data_i = _jit_userk_data(rawdata, userk_data)
    return userk_data[:userk_data_i]


@numba.njit(cache=True, fastmath=True)
def _jit_userk_data(rawdata, userk_data):
    userk_word_cnt = 0
    userk_data_i = 0

    for word in rawdata:
        if (word & au.USERK_FRAME_ID):
            if userk_word_cnt == 0:
                userk_word = word & 0x0fff
                userk_data[userk_data_i]['Status'] = (word >> 12) & 0x3
                userk_data[userk_data_i]['ChipID'] = (word >> 14) & 0x3
            else:
                userk_word = userk_word << 16 | word & 0xffff
            userk_word_cnt += 1
            if userk_word_cnt == 4:
                userk_data[userk_data_i]['AuroraKWord'] = userk_word & 0xff
                Data1 = (userk_word >> 34) & 0x7ffffff
                Data0 = (userk_word >> 8) & 0x7ffffff
                userk_data[userk_data_i]['Data1'] = Data1
                userk_data[userk_data_i]['Data1_AddrFlag'] = (Data1 >> 25) & 0x1
                userk_data[userk_data_i]['Data1_Addr'] = (Data1 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data1_Data'] = (Data1 >> 0) & 0xffff
                userk_data[userk_data_i]['Data0'] = Data0
                userk_data[userk_data_i]['Data0_AddrFlag'] = (Data0 >> 25) & 0x1
                userk_data[userk_data_i]['Data0_Addr'] = (Data0 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data0_Data'] = (Data0 >> 0) & 0xffff
                userk_data_i += 1
                userk_word_cnt = 0
    return userk_data_i


def stretch_ptot_data(map_data):
    ptot_stretched = [('scan_param_id', '<i8')]
    for i in range(1, 5):
        ptot_stretched.append(('hit_or_%i_col' % i, np.uint16))
        ptot_stretched.append(('hit_or_%i_row' % i, np.uint16))
    out_map = np.zeros((np.sum(map_data['cmd_length'])), dtype=np.dtype(ptot_stretched))

    out_map['scan_param_id'] = np.repeat(map_data['scan_param_id'], map_data['cmd_length'])
    for i in range(1, 5):
        out_map['hit_or_%i_col' % i] = np.repeat(map_data['hit_or_%i_col' % i], map_data['cmd_length'])
        out_map['hit_or_%i_row' % i] = np.repeat(map_data['hit_or_%i_row' % i], map_data['cmd_length'])

    return out_map


def print_raw_data(raw_data, ptot_table_stretched=None, hmap_is_compressed=False, eos_mode=False, invert_ptot=False):
    ''' Print raw data with interpretation for debugging '''

    if ptot_table_stretched is None:
        ptot_table_stretched = np.array([(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)], dtype=np.dtype(
            {'names': ['cmd_number_start', 'cmd_number_stop', 'cmd_length', 'scan_param_id', 'hit_or_1_col',
                       'hit_or_1_row', 'hit_or_2_col', 'hit_or_2_row', 'hit_or_3_col', 'hit_or_3_row',
                       'hit_or_4_col', 'hit_or_4_row'],
             'formats': [np.uint32, np.uint32, np.uint32, np.uint32, np.uint16, np.uint16, np.uint16, np.uint16,
                         np.uint16, np.uint16, np.uint16, np.uint16]}))
    rx_id = 0  # Needs to be adjusted
    take_old_hitors = False
    q_row = 0
    found_start_of_stream = False
    word_length = 0
    eos_bit = True
    stream_state = 0
    raw_len = len(raw_data)
    for i, actual_word in enumerate(raw_data):
        shift = 16
        if actual_word & au.TRIGGER_HEADER:
            trg_number = actual_word & au.TRG_MASK
            print('TRG {:<29} {:032b}'.format(*[actual_word & au.TRG_MASK, actual_word]))
            continue

        if actual_word & au.TDC_HEADER == au.TDC_ID_0:
            print('TDC VAL0 {:<26} TDC TS0 {:<26} TDC DIST0 {:<26} {:032b}'.format(*[actual_word & au.TDC_VALUE_MASK, (au.TDC_TIMESTAMP_MASK & actual_word) >> 12, (au.TDC_TRIG_DIST_MASK & actual_word) >> 20, actual_word]))
            continue
        if actual_word & au.TDC_HEADER == au.TDC_ID_1:
            print('TDC VAL1 {:<26} TDC TS1 {:<26} TDC DIST1 {:<26} {:032b}'.format(*[actual_word & au.TDC_VALUE_MASK, (au.TDC_TIMESTAMP_MASK & actual_word) >> 12, (au.TDC_TRIG_DIST_MASK & actual_word) >> 20, actual_word]))
            continue
        if actual_word & au.TDC_HEADER == au.TDC_ID_2:
            print('TDC VAL2 {:<26} TDC TS2 {:<26} TDC DIST2 {:<26} {:032b}'.format(*[actual_word & au.TDC_VALUE_MASK, (au.TDC_TIMESTAMP_MASK & actual_word) >> 12, (au.TDC_TRIG_DIST_MASK & actual_word) >> 20, actual_word]))
            continue
        if actual_word & au.TDC_HEADER == au.TDC_ID_3:
            print('TDC VAL3 {:<26} TDC TS3 {:<26} TDC DIST3 {:<26} {:032b}'.format(*[actual_word & au.TDC_VALUE_MASK, (au.TDC_TIMESTAMP_MASK & actual_word) >> 12, (au.TDC_TRIG_DIST_MASK & actual_word) >> 20, actual_word]))
            continue
        if ((actual_word >> 20) & 0xf) != rx_id:
            continue
        else:
            word = actual_word & 0x1ffff
        if actual_word & au.USERK_FRAME_ID:
            print('U-K {:032b}'.format(*[actual_word]) + '\t{:08x}'.format(*[actual_word]))
            continue
        if eos_mode:
            if actual_word & HEADER_ID:  # Chip header
                word = word & 0xffff  # 16 bit chip data
                shift = 15
                if eos_bit:
                    found_start_of_stream = True  # Found start of stream
                    stream_state = 1
                    left_over_word = 0  # Reset old word. Left over bits should be orphan bits (frame alignment).
                    word_length = 0
                    if not actual_word & NEW_STREAM_ID:  # End of stream
                        eos_bit = False
                        word = word + NEW_STREAM_ID
                elif actual_word & NEW_STREAM_ID:  # End of stream
                        word = word & 0x7fff
                        eos_bit = True
                # Set per stream variables
                print('START OF STREAM {:<17} {:032b}'.format(*[found_start_of_stream, word]))
            else:  # Stream is still continuing
                shift = 16
        else:
            if actual_word & HEADER_ID:  # Chip header
                word = word & 0xffff  # 16 bit chip data
                if actual_word & NEW_STREAM_ID:  # Beginning of new stream
                    # Set per stream variables
                    found_start_of_stream = True  # Found start of stream
                    stream_state = 1  # Next information is tag. TODO: Need to check for R/O configuration. Depending on that, next word is tag (default) OR chip id and then tag.
                    left_over_word = 0  # Reset old word. Left over bits should be orphan bits (frame alignment).
                    word_length = 0
                    print('START OF STREAM {:<17} {:032b}'.format(*[found_start_of_stream, word]))
                else:  # Stream is still continuing
                    shift = 15  # Shift is only 15 bit since need to exclude NS bit.

        # In case stream just continues, append new raw data word (16 bit) to old data word
        if word_length < 47 and found_start_of_stream:  # Make sure not to be larger than 64 bits.
            word = (left_over_word << shift) + word  # Reassemble the complete raw data word.
            word_length = au.get_length(word)
        elif not found_start_of_stream:
            # If not found start of stream, continue (until start of stream is found)
            continue

        # Do not take new raw data word until no more bits are left or not enough bits for extracting an information
        while word_length != 0:
            if stream_state == 1:  # Extract tag
                if word_length < 8:  # Check for complete 8 bit tag.
                    left_over_word = word  # Store left over data word
                    break

                tag = (word >> (word_length - 8)) & TAG_MASK
                print('TAG {:<29} {:032b}'.format(*[tag, word]))
                word = au.rm_msb(word, 8, word_length)
                word_length -= 8
                stream_state = 2  # Next information is ccol

            if stream_state == 2:  # Extract core column
                if word_length < 6:  # Check for complete 6 bit core column.
                    left_over_word = word  # Store left over data word
                    break
                ccol_buf = word >> (word_length - 6) & CCOL_MASK
                if ccol_buf >= 56:  # Valid ccol values are < 56, >=56 is 11 bit INTERNAL tag (starts with 11 1XXXX XXXX = 111 + 8 bit tag).
                    # No valid ccol, no hit. Continue with next raw data word.
                    word = au.rm_msb(word, 3, word_length)
                    word_length -= 3
                    left_over_word = word
                    stream_state = 1  # Next information is INTERNAL tag
                elif ccol_buf == 0:  # End of stream
                    # Reset per stream variables. Continue with next raw data word
                    found_start_of_stream = False
                    left_over_word = 0
                    stream_state = 0  # Search for beginning of stream
                    print('END OF STREAM {:<19} {:032b}'.format(*[found_start_of_stream, word]))
                    break
                else:
                    # Valid hit, store core column value.
                    print('CCOL {:<28} {:032b}'.format(*[ccol_buf, word]))
                    word = au.rm_msb(word, 6, word_length)
                    word_length -= 6
                    ccol = ccol_buf
                    left_over_word = word
                    stream_state = 4  # Next information is neighbor/last

            if stream_state == 4:  # Extract islast and isneighbor
                if word_length < 3:  # Check for complete 2 bit islast and isneihbor
                    left_over_word = word  # Store left over data word
                    break
                is_last_neighbor = word >> (word_length - 2) & ISLASTNEIGHBOUR_MASK
                is_last = (is_last_neighbor & 0x2) >> 1
                is_neighbor = is_last_neighbor & 0x1
                print('ISLAST/ISNEIGHBOR {:<1}/{:<13} {:032b}'.format(*[is_last, is_neighbor, word]))
                word = au.rm_msb(word, 2, word_length)
                word_length -= 2
                left_over_word = word  # Store left over data word
                stream_state = 8  # Next information is qrow

            if stream_state == 8:  # Extract quarter row
                if word_length < 8:  # Check for complete 8 bit quarter row
                    left_over_word = word  # Store left over data word
                    break
                if is_neighbor:
                    q_row += 1
                else:
                    q_row = word >> (word_length - 8) & QROW_MASK
                    word = au.rm_msb(word, 8, word_length)
                    word_length -= 8
                print('QROW {:<28} {:032b}'.format(*[q_row, word]))

                stream_state = 16  # Next information is hmap

            if stream_state == 16:  # Extract hmap, decodes a quarter core (= 8 columns x 2 rows).
                if (raw_len > (i + 1)):
                    if ((word_length < 30 and hmap_is_compressed) or (word_length < 16 and not hmap_is_compressed)) and not (raw_data[i + 1] & HEADER_ID and raw_data[i + 1] & NEW_STREAM_ID):  # Check for complete 30 bit hmap.
                        left_over_word = word  # Store left over data word
                        break
                else:
                    left_over_word = word  # Store left over data word
                    break

                # Get decompressed 16 bit hitmap word and number of hits within quarter core
                hmap_word, hits_hmap, word, word_length = get_hitmap(word, word_length, hmap_is_compressed)
                hmap_word_length = 16  # By construction
                stream_state = 32  # Next information is tot
                print('HMAP [NUMBER HITS OF HITS {:<6}] {:032b}'.format(*[hits_hmap, hmap_word]))

            if stream_state == 32:  # Extract tot. ToT values for all hit pixels in quarter core (decompressed hitmap)
                if word_length <= 4 * hits_hmap:  # Check for complete 4 bit tot for all hits.
                    if word_length <= 32:  # Corresponds to 8 hits. Continue with next raw data word and extend word in next step.
                        left_over_word = word
                        break
                    else:  # Have enough bits to process first 8 hits (but can be less)
                        n_hitors = np.array([0, 1])
                        hits_hmap = hits_hmap - au.number_of_set_bits(hmap_word & 0xFF00)  # Count number of hits in first 8 bits
                        take_old_hitors = True
                else:
                    if take_old_hitors:
                        n_hitors = np.array([2, 3])
                        take_old_hitors = False
                    else:
                        n_hitors = np.array([0, 1, 2, 3])

                if len(ptot_table_stretched) > 1:
                    ptot_table_piece = ptot_table_stretched[trg_number]
                else:
                    ptot_table_piece = ptot_table_stretched[0]
                c1 = ptot_table_piece['hit_or_1_col']
                c2 = ptot_table_piece['hit_or_2_col']
                c3 = ptot_table_piece['hit_or_3_col']
                c4 = ptot_table_piece['hit_or_4_col']
                r1 = ptot_table_piece['hit_or_1_row']
                r2 = ptot_table_piece['hit_or_2_row']
                r3 = ptot_table_piece['hit_or_3_row']
                r4 = ptot_table_piece['hit_or_4_row']

                if q_row == 196:  # Precision TOT word
                    for hitor_id in n_hitors:  # Loop over data words from all hitors
                        if au.number_of_set_bits(hmap_word) == 1:  # Break in case no hits anymore
                            break
                        if not (hmap_word >> (hmap_word_length - 4) & 0xF):
                            hmap_word = au.rm_msb(hmap_word, 4, hmap_word_length)
                            hmap_word_length -= 4
                            continue
                        col, row, ptot, ptoa, word, word_length, hmap_word, hmap_word_length = get_ptot_hit_data(hitor_id, c1, c2, c3, c4, r1, r2, r3, r4, word, word_length, hmap_word, hmap_word_length, invert_ptot)
                        if col < 65534 and ptot < 2047:  # Only add valid hits
                            print('HIT DATA (PTOT) {:<3} {:<3} {:<3}'.format(*[(ccol - 1) * 8 + col, row, ptot]))
                else:  # Normal TOT word
                    start_hit = 0
                    stop_hit = 16
                    if np.array_equal(n_hitors, np.array([2, 3])):  # Processed already top part of hmap
                        start_hit = start_hit + 8
                    if np.array_equal(n_hitors, np.array([0, 1])):  # Process only top part of hmap
                        stop_hit = 8
                    for x in range(start_hit, stop_hit):  # Loop over all hits
                        hit = hmap_word >> (hmap_word_length - 1) & 0x1
                        hmap_word = au.rm_msb(hmap_word, 1, hmap_word_length)
                        hmap_word_length -= 1
                        # Calculate column and row using core column and position inside quarter core. Hmap words decodes hits in quater core (= 8 columns x 2 rows).
                        col = (ccol - 1) * 8 + np.mod(x, 8)
                        row = q_row * 2 + x // 8
                        if row > 383:
                            break
                        if hit == 1:  # High bit means hit
                            tot_val = word >> (word_length - 4) & TOT_MASK
                            word = au.rm_msb(word, 4, word_length)
                            word_length -= 4
                            print('HIT DATA (TOT) {:<26} {:<26} {:<26}'.format(*[col, row, tot_val]))

                if take_old_hitors:  # Could only process first 8 hit, thus continue with next raw data word.
                    left_over_word = word
                    break

                if is_last:
                    stream_state = 2  # Next information is ccol
                    left_over_word = word
                else:
                    stream_state = 4  # Next information is neighbor/last
                    left_over_word = word
