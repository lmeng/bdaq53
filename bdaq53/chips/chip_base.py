#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os

import numpy as np

import yaml
# Try to use fast c parser based on libyaml
try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:  # fallback to python parser
    from yaml import SafeLoader  # noqa

from bdaq53.system import logger


class ChipBase(object):

    def __init__(self, max_reg_read_failures):
        self.reg_read_failures = 0
        self.max_reg_read_failures = max_reg_read_failures

    def get_sn(self):
        return self.chip_sn

    def get_type(self):
        return self.chip_type

    def add_reg_read_failure(self, n=1):
        self.reg_read_failures += n
        if self.max_reg_read_failures is not None and self.reg_read_failures > self.max_reg_read_failures:
            raise RuntimeError('Waited for register response too often.')


class Register(dict):
    def __init__(self, chip, name, address, size, default, value, mode, reset, description):
        self.log = logger.setup_derived_logger('RD53ARegister')

        self.chip = chip
        self.changed = False
        super(Register, self).__init__()

        self['name'] = name
        self['address'] = eval(address)
        self['size'] = size
        self['default'] = eval(default)
        self['value'] = eval(value)
        self['mode'] = mode
        self['reset'] = reset
        self['description'] = description

    def __str__(self, *args, **kwargs):
        text = self['name']
        text += ': '
        text += self['description']
        text += '\nAddress : '
        text += hex(self['address'])
        text += '\nSize    : '
        text += str(self['size'])
        text += '\nValue   : '
        text += ('0b{0:0' + str(self['size']) + 'b} ({1:d})').format(self['value'], self['value'])
        text += '\nDefault : '
        text += ('0b{0:0' + str(self['size']) + 'b} ({1:s})').format(self['default'], str(self['default']))
        text += '\nMode    : '
        if self['mode'] == 1:
            text += 'r/w'
        else:
            text += 'r'

        return text

    def _assert_value(self, value):
        if isinstance(value, (int, np.integer)):
            return value
        if isinstance(value, str):
            if value[:2] == '0b':
                return int(value, 2)
            if value[:2] == '0x':
                return int(value, 16)
        raise ValueError('Invalid value of type {0}: {1}'.format(type(value), value))

    def set(self, value):
        value = self._assert_value(value)
        if value != self['value']:
            self.changed = True
            self['value'] = value

    def get(self):
        return self['value']

    def print_value(self):
        print(('{0} = 0b{1:0' + str(self['size']) + 'b} ({2:d})').format(self['name'], self['value'], self['value']))

    def write(self, value=None, verify=False, write_ctr=0):
        if value is not None:
            value = self._assert_value(value)
            self['value'] = value
        self.log.debug(('Writing value 0b{0:0' + str(self['size']) + 'b} to register {1}').format(self['value'], self['name']))
        self.chip._write_register(self['address'], self['value'])

        if verify:
            if self.read() != value:
                if write_ctr >= 10:
                    raise RuntimeError('Could not verify value {0} in register {1}'.format(value, self['name']))
                self.write(value=value, verify=True, write_ctr=write_ctr + 1)

    def get_write_command(self, value=None):
        if value is not None:
            self.set(value)
        return self.chip._write_register(self['address'], self['value'], write=False)

    def get_read_command(self):
        return self.chip._read_register(self['address'], write=False)

    def read(self):
        val = self.chip._get_register_value(self['address'])
        if val != self['value'] and self['mode'] == 1 and self['name'] != 'PIX_PORTAL':
            self.log.warning(('Register {0} did not have the expected value: Expected 0b{2:0' + str(self['size']) + 'b}, got 0b{1:0' + str(self['size']) + 'b}').format(self['name'], val, self['value']))
        return val

    def reset(self):
        ''' Overwrite a register with its default value'''
        if self['reset'] == 1 and self['mode'] == 1:
            self.write(self['default'])


class RegisterObject(dict):
    ''' General register object collects all chip registers,
        creates them on initialization from a yaml file and
        handles all manipulation of all registers at once.
    '''

    def __init__(self, chip, lookup_file=None):
        self.chip = chip
        super(RegisterObject, self).__init__()

        if lookup_file is None:
            lookup_file = 'registers.yaml'
        if not os.path.isfile(lookup_file):
            lookup_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), lookup_file)

        with open(lookup_file, 'r') as f:
            regs = yaml.load(f, Loader=SafeLoader)

        for reg in regs['registers']:
            self._add(name=reg['name'], address=reg['address'], size=reg['size'], default=reg['default'], mode=reg['mode'], reset=reg['reset'], description=reg['description'])

    def _add(self, name, address, size, default, mode, reset, value=None, description=''):
        if value is None:
            value = default
        self[name] = Register(chip=self.chip, name=name, address=address, size=size, default=default, mode=mode, reset=reset, value=value, description=description)

    def get_name_from_address(self, address):
        for reg in self.values():
            if reg['address'] is address:
                return reg['name']
        else:
            raise ValueError('No register found with address {0}'.format(address))

    def write_all(self, force=False):
        '''
        Collect all registers that were changed in software and write to chip
        If force==True, write all software values to chip
        '''
        indata = self.chip.write_sync(write=False)
        for reg in self.values():
            if reg['mode'] != 1 or (not force and not reg.changed):
                continue

            indata += self.chip._write_register(reg['address'], reg['value'], write=False)
            indata += self.chip.write_sync(write=False) * 100

            if len(indata) > 3500:
                indata += self.chip.write_sync(write=False) * 100
                self.chip.write_command(indata)
                indata = self.chip.write_sync(write=False)

        self.chip.write_command(indata)

    def check_all(self, correct=False):
        ''' Compare all chip registers to software and log result '''
        errors = 0
        for reg in self.values():
            if reg['mode'] == 1 and reg['name'] not in ['PIX_PORTAL']:
                val = reg.read()
                if val != reg['value']:
                    errors += 1
                    if correct:
                        reg.write()

        return errors

    def update_all(self):
        ''' Set software status to chip registers '''
        for reg in self.values():
            if reg['mode'] == 1:
                reg.read()

    def reset_all(self):
        ''' Set all chip registers to default values '''
        for reg in self.values():
            if reg['reset'] == 1:
                reg.set(reg['default'])
        self.write_all(force=True)

    def dump_all(self, outfile=None):
        ''' Dump all current register values to configuration-style YAML file. '''
        if outfile is None:
            outfile = 'register_dump.yaml'

        self.chip.log.warning('Dumping all registers to file {0}'.format(os.path.abspath(outfile)))

        data = {}
        for reg in self.values():
            data[reg['name']] = reg['value']

        with open(outfile, 'w') as f:
            yaml.dump({'registers': data}, f)


class MaskObject(dict):

    def __init__(self, chip, masks, dimensions):
        self.chip = chip
        self.masks = masks
        self.dimensions = dimensions
        self.defaults = {}
        self.disable_mask = np.ones(self.dimensions, bool)
        self.to_write = np.zeros(self.dimensions, bool)
        self.was = {}

        for name, props in masks.items():
            self.defaults[name] = props['default']
            self.was[name] = np.full(dimensions, props['default'])
            self[name] = np.full(dimensions, props['default'])

        self.supported_mask_patterns = ['double', 'ptot', 'double_inv', 'classic',
                                        'vertical_injection', 'horizontal_injection', 'cross_injection', 'corner_injection',
                                        'ring_injection', 'odd-left_injection', 'odd-right_injection', 'even-right_injection', 'even-left_injection',
                                        'hitbus', 'lp_mode', 'default']

        # Initialize mask patterns to None and create pattern only on demand to save time
        self.shift_patterns = dict.fromkeys(self.supported_mask_patterns)

        self.mask_cache = []

        super(MaskObject, self).__init__()

    def _create_shift_pattern(self, pattern):
        ''' Called when pattern is required '''
        if pattern not in self.supported_mask_patterns:
            raise NotImplementedError('Mask pattern not supported:', pattern)
        if not self.shift_patterns[pattern]:  # check if pattern is already created
            if pattern == 'double':
                self.shift_patterns['double'] = DoubleShiftPattern(self.dimensions, mask_step=24)
            elif pattern == 'ptot':
                self.shift_patterns['ptot'] = PTotPattern(self.dimensions, mask_step=786)
            elif pattern == 'double_inv':
                self.shift_patterns['double_inv'] = DoubleShiftPatternInv(self.dimensions, mask_step=24)
            elif pattern == 'classic':
                self.shift_patterns['classic'] = ClassicShiftPattern(self.dimensions, mask_step=24)
            elif pattern == 'vertical_injection':
                self.shift_patterns['vertical_injection'] = CrosstalkShiftPattern(self.dimensions, inj_pat='vertical', mask_step=24)
            elif pattern == 'horizontal_injection':
                self.shift_patterns['horizontal_injection'] = CrosstalkShiftPattern(self.dimensions, inj_pat='horizontal', mask_step=24)
            elif pattern == 'cross_injection':
                self.shift_patterns['cross_injection'] = CrosstalkShiftPattern(self.dimensions, inj_pat='cross', mask_step=24)
            elif pattern == 'corner_injection':
                self.shift_patterns['corner_injection'] = CrosstalkShiftPattern(self.dimensions, inj_pat='corner', mask_step=24)
            elif pattern == 'ring_injection':
                self.shift_patterns['ring_injection'] = CrosstalkShiftPattern(self.dimensions, inj_pat='ring', mask_step=24)
            elif pattern == 'odd-left_injection':
                self.shift_patterns['odd-left_injection'] = CrosstalkShiftPattern(self.dimensions, inj_pat='odd-left', mask_step=24)
            elif pattern == 'odd-right_injection':
                self.shift_patterns['odd-right_injection'] = CrosstalkShiftPattern(self.dimensions, inj_pat='odd-right', mask_step=24)
            elif pattern == 'even-right_injection':
                self.shift_patterns['even-right_injection'] = CrosstalkShiftPattern(self.dimensions, inj_pat='even-right', mask_step=24)
            elif pattern == 'even-left_injection':
                self.shift_patterns['even-left_injection'] = CrosstalkShiftPattern(self.dimensions, inj_pat='even-left', mask_step=24)
            elif pattern == 'hitbus':
                self.shift_patterns['hitbus'] = HitBusShiftPattern(self.dimensions, self.masks)
            elif pattern == 'lp_mode':
                self.shift_patterns['lp_mode'] = LpModeShiftPattern(self.dimensions, self.masks)
            elif pattern == 'default':
                self.shift_patterns['default'] = DoubleShiftPattern(self.dimensions, mask_step=24)  # default is double shift pattern

    def get_mask_steps(self, pattern='default'):
        fe_multiplier = 0
        for _, cols in self.chip.flavor_cols.items():
            fe_mask = np.zeros(self.dimensions, bool)
            fe_mask[cols[0]:cols[-1] + 1, :] = True
            if np.any(np.logical_and(fe_mask, self['enable'])):
                fe_multiplier += 1
        self._create_shift_pattern(pattern)
        return self.shift_patterns[pattern]._get_mask_steps() * fe_multiplier

    def shift(self, masks=['enable'], pattern='default', cache=False, skip_empty=True, disable_unused_core_cols=False):
        '''
            This function is called from scan loops to loop over 1 FE at a time and
            over the shifting masks as defined by the mask shift pattern
        '''

        original_masks = {name: mask for name, mask in self.items()}
        active_pixels = []

        self._create_shift_pattern(pattern)

        # Cache calculation to speed up repeated use
        if cache and self.mask_cache:
            for fe, data in self.mask_cache:
                for d in data:
                    self.chip.write_command(d)
                if fe != 'reset':
                    yield fe, active_pixels

            for name, mask in original_masks.items():
                self[name] = mask
            return

        for fe, cols in self.chip.flavor_cols.items():
            fe_mask = np.zeros(self.dimensions, bool)
            fe_mask[cols[0]:cols[-1] + 1, :] = True

            if not np.any(np.logical_and(fe_mask, original_masks['enable'])):  # Only loop over frontends that have enabled columns
                continue
            if not disable_unused_core_cols:
                data = [self.chip.enable_core_col_clock(range(int(cols[0] / 8), int((cols[-1] - 1) / 8 + 1)), write=True)]   # Enable only one frontend at a time

            self.shift_patterns[pattern].reset()
            for pat in self.shift_patterns[pattern]:
                if disable_unused_core_cols:
                    en_pixels = np.where(pat.T)  # get enabled pixels
                    en_core_cols = en_pixels[1] // 8  # get enabled core_cols
                    en_core_cols = list(set(en_core_cols))  # remove duplicates and convert to list
                    data = [self.chip.enable_core_col_clock(en_core_cols, write=True)]  # Enable only enabled core cols
                if isinstance(pat, np.ndarray):  # If DoubleShiftPattern or ClassicShiftPattern is used
                    for mask in masks:
                        self[mask] = np.logical_and(np.logical_and(original_masks[mask], pat), fe_mask)
                    if not np.any(self['enable'][:]) and skip_empty:   # Skip empty steps for speedup
                        if cache:
                            self.mask_cache.append(('skipped', []))
                        yield 'skipped', active_pixels
                        continue
                else:  # If CrosstalkShiftPattern is used
                    for name, mask in pat.items():
                        self[name] = np.logical_and(np.logical_and(original_masks[name], mask), fe_mask)
                data.extend(self.update())
                if cache:
                    self.mask_cache.append((fe, data))
                    data = []
                active_pixels = np.where(self['enable'][0:self.dimensions[0], 0:self.dimensions[1]])
                yield fe, active_pixels

        for name, mask in original_masks.items():
            self[name] = mask
        self.mask_cache.append(('reset', self.update()))

    def _find_changes(self):
        '''
            Find out which values have changed in any mask compared to last update()
        '''
        self.to_write = np.zeros(self.dimensions, bool)
        for name, mask in self.items():
            self.to_write = np.logical_or(self.to_write, np.not_equal(mask, self.was[name]))

    def apply_disable_mask(self):
        self['enable'] = np.logical_and(self['enable'], self.disable_mask)

    def reset_all(self):
        for name, _ in self.items():
            np.full(self.dimensions, self.defaults[name])
        self.apply_disable_mask()

    def update(self, force=False):
        raise NotImplementedError('MaskObject.update() should be defined in chip object')

    def _get_hitor_lane(self, row, col):
        lane = 0
        if col % 4 == 0 or col % 4 == 1:
            if row % 2 == 0 and col % 2 == 0:  # even even
                lane = 0
            if row % 2 == 0 and col % 2 == 1:  # even odd
                lane = 1
            if row % 2 == 1 and col % 2 == 0:  # odd even
                lane = 2
            if row % 2 == 1 and col % 2 == 1:  # odd odd
                lane = 3
        else:
            if row % 2 == 0 and col % 2 == 0:  # even even
                lane = 2
            if row % 2 == 0 and col % 2 == 1:  # even odd
                lane = 3
            if row % 2 == 1 and col % 2 == 0:  # odd even
                lane = 0
            if row % 2 == 1 and col % 2 == 1:  # odd odd
                lane = 1
        return lane


class ShiftPatternBase(object):
    '''
        Base class for shift patterns
    '''

    def __init__(self, dimensions, mask_step):
        self.dimensions = dimensions
        self.mask_step = mask_step
        self.current_step = -1
        self.base_mask = self.make_first_mask()

    def __iter__(self):
        return self

    def reset(self):
        self.current_step = -1

    def _get_mask_steps(self):
        return self.dimensions[1]

    def make_first_mask(self):
        raise NotImplementedError('You have to define the initial mask')

    def make_mask_for_step(self, step):
        raise NotImplementedError('You have to define the mask at a given step')

    def __next__(self):
        if self.current_step >= self.dimensions[1] - 1:
            raise StopIteration
        else:
            self.current_step += 1
            return self.make_mask_for_step(self.current_step)


class ClassicShiftPattern(ShiftPatternBase):
    '''
        Enables 1 pixel per column
        mask_step defines by how much the pixel in the (n+1)th column is below the pixel in the nth column.
        This mask is shifted down dimension[1] times with rollover
    '''

    def make_first_mask(self):
        mask = np.zeros(self.dimensions, bool)

        for col in range(self.dimensions[0]):
            for row in range(self.dimensions[1]):
                if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                    mask[col, row] = True
        return mask

    def make_mask_for_step(self, step):
        return np.roll(self.base_mask, step, 1)


class DoubleShiftPattern(ShiftPatternBase):
    '''
        Enables 2 neighboring pixel per core
        mask_step defines by how much the pixel in the (n+1)th column is below the pixel in the nth column.
        This mask is shifted down dimension[1] times with rollover
    '''

    def make_first_mask(self):
        mask = np.zeros(self.dimensions, bool)

        for col in range(0, self.dimensions[0], 2):
            for row in range(self.dimensions[1]):
                if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                    mask[col, row] = True
                    mask[col + 1, row] = True
        return mask

    def make_mask_for_step(self, step):
        return np.roll(self.base_mask, step, 1)


class PTotPattern(ShiftPatternBase):
    '''
        Enables 2 neighboring pixel per core
        mask_step defines by how much the pixel in the (n+1)th column is below the pixel in the nth column.
        This mask is shifted down dimension[1] times with rollover
    '''

    def __init__(self, dimensions, mask_step):
        self.n_pixels_per_step = 50 * 4  # activate only four pixels per step
        super(PTotPattern, self).__init__(dimensions, mask_step)

    def _get_mask_steps(self):
        return int(self.dimensions[1] * self.dimensions[0] / self.n_pixels_per_step)

    def make_first_mask(self):
        mask = np.zeros(self.dimensions, bool)
        for col in range(0, self.dimensions[0], 8):
            done = False
            for row in range(0, self.dimensions[1], 1):
                if row % (self.dimensions[1] / 2) == 0:
                    if done:
                        mask[col + 6, row] = True
                        mask[col + 7, row] = True
                    else:
                        mask[col + 0, row] = True
                        mask[col + 1, row] = True
                        done = True
        return mask

    def make_mask_for_step(self, column_shift, row_shift):
        mask_shifted = np.roll(self.base_mask, row_shift, 1)  # shift along rows
        if column_shift:
            mask_shifted = np.roll(mask_shifted, 4, 0)  # shift along columns
        self.active_pix = np.where(mask_shifted)
        return mask_shifted

    def __next__(self):
        column_shift = False
        if self.current_step >= (self.dimensions[1] * self.dimensions[0] / self.n_pixels_per_step) - 1:
            raise StopIteration
        else:
            self.current_step += 1
            row_shift = np.mod(self.current_step, self.dimensions[1])
            if self.current_step >= self.dimensions[1]:
                column_shift = True
            return self.make_mask_for_step(column_shift, row_shift)


class DoubleShiftPatternInv(DoubleShiftPattern):
    '''
        Inverted ClassicShiftPattern
        Disables 1 pixel per column
    '''

    def make_first_mask(self):
        return ~super(DoubleShiftPatternInv, self).make_first_mask()


class CrosstalkShiftPattern(ShiftPatternBase):
    '''
        This class is used for the initialization of the masks during crosstalk scan.
        Six (8) different injection patterns are available.
        In 4 of the patterns 1 pixel per column is enabled and charge is injected to some of its neighbours.
        In the other 4 patterns 1 pixel per second column is enabled and charge is injected to one of its neighbours.
        mask_step defines by how much the pixel in the (n+1)th column is below the pixel in the nth column.
        This mask is shifted down dimension[1] times with rollover
    '''

    def __init__(self, dimensions, inj_pat, mask_step):
        self.inj_pat = inj_pat
        super(CrosstalkShiftPattern, self).__init__(dimensions, mask_step)

    def make_first_mask(self):
        '''
        It takes a keyword depending on the injection pattern
        (vertical, horizontal, cross, corner, odd-left, odd-right, even-right, even-left injection)
        and returns a dictionary of two masks (enable and injection).
        The function could be shorter, but this structure was preferred to increase readability
        '''

        mask = {'enable': np.zeros(self.dimensions, bool),
                'injection': np.zeros(self.dimensions, bool)}

        if self.inj_pat == 'vertical':  # up-down injection
            for col in range(self.dimensions[0]):
                for row in range(self.dimensions[1]):
                    if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                        mask['enable'][col, row] = True
                        mask['injection'][col, row - 1] = True
                        mask['injection'][col, row + 1] = True
        elif self.inj_pat == 'horizontal':  # left-right injection
            for col in range(self.dimensions[0]):
                for row in range(self.dimensions[1]):
                    if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                        mask['enable'][col, row] = True
                        if col - 1 >= 0:
                            mask['injection'][col - 1, row] = True
                        if col + 1 < self.dimensions[0]:
                            mask['injection'][col + 1, row] = True
        elif self.inj_pat == 'cross':  # up-down and left-right injection
            for col in range(self.dimensions[0]):
                for row in range(self.dimensions[1]):
                    if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                        mask['enable'][col, row] = True
                        mask['injection'][col, row - 1] = True
                        mask['injection'][col, row + 1] = True
                        if col - 1 >= 0:
                            mask['injection'][col - 1, row] = True
                        if col + 1 < self.dimensions[0]:
                            mask['injection'][col + 1, row] = True
        elif self.inj_pat == 'corner':  # injection to the 4 corner pixels
            for col in range(self.dimensions[0]):
                for row in range(self.dimensions[1]):
                    if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                        mask['enable'][col, row] = True
                        if col - 1 >= 0:
                            mask['injection'][col - 1, row - 1] = True
                            mask['injection'][col - 1, row + 1] = True
                        if col + 1 < self.dimensions[0]:
                            mask['injection'][col + 1, row + 1] = True
                            mask['injection'][col + 1, row - 1] = True
        elif self.inj_pat == 'ring':  # injection to all 8 neighbouring pixels
            for col in range(self.dimensions[0]):
                for row in range(self.dimensions[1]):
                    if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                        mask['enable'][col, row] = True
                        if col - 1 >= 0:
                            mask['injection'][col - 1, row - 1] = True
                            mask['injection'][col - 1, row + 0] = True
                            mask['injection'][col - 1, row + 1] = True
                        mask['injection'][col, row + 1] = True
                        mask['injection'][col, row - 1] = True
                        if col + 1 < self.dimensions[0]:
                            mask['injection'][col + 1, row + 1] = True
                            mask['injection'][col + 1, row + 0] = True
                            mask['injection'][col + 1, row - 1] = True
        elif self.inj_pat == 'odd-left':  # injection to odd-column pixels and reading out the pixels on the right
            for col in range(1, self.dimensions[0], 2):
                for row in range(self.dimensions[1]):
                    if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                        mask['enable'][col, row] = True
                        mask['injection'][col - 1, row] = True
        elif self.inj_pat == 'odd-right':  # injection to odd-column pixels and reading out the pixels on the right
            for col in range(1, self.dimensions[0] - 1, 2):
                for row in range(self.dimensions[1]):
                    if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                        mask['enable'][col, row] = True
                        mask['injection'][col + 1, row] = True
        elif self.inj_pat == 'even-right':  # injection to even-column pixels and reading out the pixels on the left
            for col in range(0, self.dimensions[0], 2):
                for row in range(self.dimensions[1]):
                    if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                        mask['enable'][col, row] = True
                        mask['injection'][col + 1, row] = True
        elif self.inj_pat == 'even-left':  # injection to even-column pixels and reading out the pixels on the left
            for col in range(2, self.dimensions[0], 2):
                for row in range(self.dimensions[1]):
                    if row == (col % int(self.dimensions[1] / self.mask_step)) * self.mask_step:
                        mask['enable'][col, row] = True
                        mask['injection'][col - 1, row] = True
        else:
            raise NotImplementedError('Crosstalk shit pattern nit supprted:', self.inj_pat)

        return mask

    def make_mask_for_step(self, step):
        step_mask = self.base_mask.copy()
        for name, _ in self.base_mask.items():
            step_mask[name] = np.roll(self.base_mask[name], step, 1)
        return step_mask


class HitBusShiftPattern(ShiftPatternBase):
    '''
        Shift pattern for scans (e.g. calibrate_hitor) which use the HitOr (Hitbus). Cannot use default shift pattern for these scans
        since need to make sure that each Hitor line is used by only one pixel (otherwise cannot properly measure TDC).
        Thus inject with every step only into four pixels of different Hitor lines (RD53A has four different hitor lines).
    '''

    def __init__(self, dimensions, mask_step):
        self.n_pixels_per_step = 4  # activate only four pixels per step
        super(HitBusShiftPattern, self).__init__(dimensions, mask_step)

    def _get_mask_steps(self):
        return int(self.dimensions[1] * self.dimensions[0] / self.n_pixels_per_step)

    def make_first_mask(self):
        mask = np.zeros(self.dimensions, bool)
        # Selects 4 pixels on different hitbus lines
        for p in range(self.n_pixels_per_step):
            mask[p, 2 * p] = True
        return mask

    def make_mask_for_step(self, column_shift, row_shift):
        mask_shifted = np.roll(self.base_mask, row_shift, 1)  # shift along rows
        if column_shift > 0:
            mask_shifted = np.roll(mask_shifted, column_shift, 0)  # shift along columns
        return mask_shifted

    def __next__(self):
        if self.current_step >= (self.dimensions[1] * self.dimensions[0] / self.n_pixels_per_step) - 1:
            raise StopIteration
        else:
            self.current_step += 1
            row_shift = np.mod(self.current_step, self.dimensions[1])
            column_shift = 4 * (self.current_step // self.dimensions[1])
            return self.make_mask_for_step(column_shift, row_shift)


class LpModeShiftPattern(ShiftPatternBase):
    '''
        Shift pattern for low power mode scans. Only a single core column is enabled at a time.
    '''

    def __init__(self, dimensions, mask_step):
        self.n_pixels_per_step = 8  # activate only four pixels per step
        super(LpModeShiftPattern, self).__init__(dimensions, mask_step)

    def _get_mask_steps(self):
        return int(self.dimensions[1] * self.dimensions[0] / self.n_pixels_per_step)

    def make_first_mask(self):
        mask = np.zeros(self.dimensions, bool)
        # Selects n_pixels_per_step pixels on different hitbus lines
        for p in range(self.n_pixels_per_step):
            mask[p, p] = True
        return mask

    def make_mask_for_step(self, column_shift, row_shift):
        mask_shifted = np.roll(self.base_mask, row_shift, 1)  # shift along rows
        if column_shift > 0:
            mask_shifted = np.roll(mask_shifted, column_shift, 0)  # shift along columns
        return mask_shifted

    def __next__(self):
        if self.current_step >= (self.dimensions[1] * self.dimensions[0] / self.n_pixels_per_step) - 1:
            raise StopIteration
        else:
            self.current_step += 1
            row_shift = np.mod(self.current_step, self.dimensions[1])
            column_shift = self.n_pixels_per_step * (self.current_step // self.dimensions[1])
            return self.make_mask_for_step(column_shift, row_shift)
