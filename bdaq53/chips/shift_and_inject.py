#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This module takes care of the mask shifting and injection of the supported chips
    in order to keep actual scans cleaner.
'''


import numpy as np


def shift_and_inject_digital(scan, n_injections, pbar=None, scan_param_id=0, masks=['injection', 'enable'], pattern='default', disable_unused_core_cols=False, cal_edge_width=None):
    ''' Regular mask shift and digital injection function.

    Parameters:
    ----------
        scan : scan object
            BDAQ53 scan object
        n_injections : int
            Number of injections per loop.
        pbar : tqdm progressbar
            Tqdm progressbar
        scan_param_id : int
            Scan parameter id of actual scan loop
        masks : list
            List of masks ('injection', 'enable', 'hitbus') which should be shifted during scan loop.
        pattern : string
            BDAQ53 injection patter ('default', 'hitbus', ...)
    '''

    if scan.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1']:
        # Change scan loop parameters in case ptot is enabled
        if scan.chip.ptot_enabled:
            pattern = 'ptot' if pattern == 'default' else pattern
            if 'hitbus' not in masks:
                masks.append('hitbus')
            latency = 23
            cal_edge_width = 32
        else:
            latency = 120
            cal_edge_width = 4
    elif scan.chip_settings['chip_type'].lower() in ['itkpixv2']:
        # Change scan loop parameters in case ptot is enabled
        if scan.chip.ptot_enabled:
            pattern = 'ptot' if pattern == 'default' else pattern
            if 'hitbus' not in masks:
                masks.append('hitbus')
            latency = 23
            if cal_edge_width is None:
                cal_edge_width = 32
        else:
            latency = 120
            if cal_edge_width is None:
                cal_edge_width = 12
    else:
        latency = 121
        cal_edge_width = 10

    for fe, active_pixels in scan.chip.masks.shift(masks=masks, pattern=pattern, disable_unused_core_cols=disable_unused_core_cols):
        if not scan.fifo_readout.force_stop.is_set():  # exit scan loop if fifo readout is stopped
            if not fe == 'skipped':
                if scan.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
                    if scan.chip.ptot_enabled:
                        scan.add_ptot_table_data(cmd_repetitions=n_injections, scan_param_id=scan_param_id, active_pixels=active_pixels)
                scan.chip.inject_digital(repetitions=n_injections, latency=latency, cal_edge_width=cal_edge_width)
            if pbar is not None:
                pbar.update(1)
        else:
            break


def shift_and_inject(scan, n_injections, pbar=None, scan_param_id=0, masks=['injection', 'enable'], pattern='default', cache=False, skip_empty=True, send_trigger=True, disable_unused_core_cols=False):
    ''' Regular mask shift and analog injection function.

    Parameters:
    ----------
        scan : scan object
            BDAQ53 scan object
        n_injections : int
            Number of injections per loop.
        pbar : tqdm progressbar
            Tqdm progressbar
        scan_param_id : int
            Scan parameter id of actual scan loop
        masks : list
            List of masks ('injection', 'enable', 'hitbus') which should be shifted during scan loop.
        pattern : string
            BDAQ53 injection patter ('default', 'hitbus', ...)
        cache : boolean
            If True use mask caching for speedup. Default is False.
        skip_empty : boolean
            If True skip empty mask steps for speedup. Default is True.
        send_trigger : boolean
            If False do not send trigger command to chip. Default is True.
    '''
    if scan.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
        # Change scan loop parameters in case ptot is enabled
        if scan.chip.ptot_enabled:
            pattern = 'ptot' if pattern == 'default' else pattern
            if 'hitbus' not in masks:
                masks = masks + ['hitbus']
            latency = 23
            core_column_loops = 8  # Every 8th core columns will be activated per scan step.
        else:
            latency = 121
            core_column_loops = 1  # All core columns will be activated per scan step.
    else:
        latency = 122
        core_column_loops = 1  # All core columns will be activated per scan step.

    for fe, active_pixels in scan.chip.masks.shift(masks=masks, pattern=pattern, cache=cache, skip_empty=skip_empty, disable_unused_core_cols=disable_unused_core_cols):
        if not scan.fifo_readout.force_stop.is_set():
            if not fe == 'skipped':
                for n in range(core_column_loops):  # ITkPixV1 needs additional loop in order to not activate all core columns at the same time.
                    if scan.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
                        # scan.chip.enable_core_col_clock(core_cols=[i + n for i in range(0, 50, core_column_loops)])
                        if scan.chip.ptot_enabled:
                            # Enable only specific core columns.
                            scan.chip.enable_core_col_clock(core_cols=[i + n for i in range(0, 50, core_column_loops)])
                            scan.add_ptot_table_data(cmd_repetitions=n_injections, scan_param_id=scan_param_id, active_pixels=active_pixels)
                    if (not fe == 'skipped' or not skip_empty) and fe == 'SYNC':
                        scan.chip.inject_analog_single(repetitions=n_injections, latency=latency, send_ecr=True, send_trigger=send_trigger)
                    elif (not fe == 'skipped' or not skip_empty):
                        scan.chip.inject_analog_single(repetitions=n_injections, latency=latency, send_trigger=send_trigger)
            if pbar is not None:
                pbar.update(1)
        else:
            break


def shift_and_inject_fast(scan, n_injections, vcal_high_range, scan_param_id_range, pbar=None, masks=['injection', 'enable'], pattern='default', disable_unused_core_cols=False):
    ''' Faster implementation of mask shift and injection function.

    Parameters:
    ----------
        scan : scan object
            BDAQ53 scan object
        n_injections : int
            Number of injections per loop.
        vcal_high_range : list
            List of VCAL_HIGH values which should be scanned.
        scan_param_id_range : list
            List of scan parameter ids.
        pbar : tqdm progressbar
            Tqdm progressbar
        masks : list
            List of masks ('injection', 'enable', 'hitbus') which should be shifted during scan loop.
        pattern : string
            BDAQ53 injection patter ('default', 'hitbus', ...)
    '''

    if scan.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
        # Change scan loop parameters in case ptot is enabled
        if scan.chip.ptot_enabled:
            pattern = 'ptot' if pattern == 'default' else pattern
            if 'hitbus' not in masks:
                masks.append('hitbus')
            latency = 23
            wait_cycles = 200
            core_column_loops = 8  # Every 8th core columns will be activated per scan step.
        else:
            latency = 121
            wait_cycles = 300
            core_column_loops = 1  # All core columns will be activated per scan step.
    else:
        latency = 9
        wait_cycles = 300
        core_column_loops = 1  # All core columns will be activated per scan step.

    if scan.chip_settings['chip_type'].lower() == 'rd53a':
        trigger_latency = scan.chip.registers['LATENCY_CONFIG'].get()
        scan.chip.registers['LATENCY_CONFIG'].write(latency * 4 + 12)

    for fe, active_pixels in (scan.chip.masks.shift(masks=masks, pattern=pattern, cache=True, disable_unused_core_cols=disable_unused_core_cols)):
        if not scan.fifo_readout.force_stop.is_set():
            for cnt, vcal_high in enumerate(vcal_high_range):
                scan_param_id = scan_param_id_range[cnt]
                scan.chip.registers['VCAL_HIGH'].write(vcal_high)
                if not fe == 'skipped':
                    for n in range(core_column_loops):  # ITkPixV1 needs additional loop in order to not activate all core columns at the same time.
                        if scan.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
                            if scan.chip.ptot_enabled:
                                # Enable only specific core columns.
                                scan.chip.enable_core_col_clock(core_cols=[i + n for i in range(0, 50, core_column_loops)])
                                scan.add_ptot_table_data(cmd_repetitions=n_injections, scan_param_id=scan_param_id, active_pixels=active_pixels)
                            else:
                                scan.add_trigger_table_data(cmd_repetitions=n_injections, scan_param_id=scan_param_id)
                        else:
                            scan.add_trigger_table_data(cmd_repetitions=n_injections, scan_param_id=scan_param_id)
                        if fe == 'SYNC':
                            scan.chip.inject_analog_single(repetitions=n_injections, latency=latency,
                                                           wait_cycles=wait_cycles, send_ecr=True)
                        else:
                            scan.chip.inject_analog_single(repetitions=n_injections, latency=latency,
                                                           wait_cycles=wait_cycles)
                if pbar is not None:
                    pbar.update(1)
            vcal_high_range = np.flip(vcal_high_range)
            scan_param_id_range = np.flip(scan_param_id_range)
        else:
            break

    # FIXME: Workaround in order to reset trigger latency to value before scan. Otherwise consecutive scans have wrong trigger latency and therefore no hits.
    if scan.chip_settings['chip_type'].lower() == 'rd53a':
        scan.chip.registers['LATENCY_CONFIG'].write(trigger_latency)


def get_scan_loop_mask_steps(scan, pattern='default'):
    ''' Returns total number of mask steps for specific pattern

    Parameters:
    ----------
        scan : scan object
            BDAQ53 scan object
        pattern : string
            BDAQ53 injection patter ('default', 'hitbus', ...)
    '''

    if scan.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
        # Change scan loop parameters in case ptot is enabled
        if scan.chip.ptot_enabled:
            pattern = 'ptot' if pattern == 'default' else pattern
    return scan.chip.masks.get_mask_steps(pattern=pattern)


if __name__ == '__main__':
    pass
