#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
This module handles interfacing with databases.
So far only reading from the RD53 database is supported.
'''

import argparse
import sys
import os
import time
from datetime import datetime
import yaml
import urllib.request
import urllib.error
import sqlite3
from lxml import html

# FIXME: Find a better solution for import of internal modules so this script can be called via CLI from both outside and within the package
try:
    from bdaq53.system import logger
except ImportError:
    import logger


log = logger.setup_derived_logger('Database Manager')
proj_dir = os.path.dirname(os.path.abspath(__file__))
rd53_db_uri = 'http://rd53a-chips.web.cern.ch/'
default_cache_filename = '_database_cache_rd53a.sqlite3'


DB_KEYS = {'chip_sn': {'rd53a_db': {'key': 'SN', 'format': "'{0}'"}},
           'thickness': {'rd53a_db': {'key': 'thickness', 'format': "float({0})"}},
           'bumped': {'rd53a_db': {'key': 'BUMPED', 'format': "bool({0})"}},
           'sensor': {'rd53a_db': {'key': 'Sensor', 'format': "'{0}'"}},
           'board_type': {'rd53a_db': {'key': 'Board_type', 'format': "int({0})"}},
           'board_id': {'rd53a_db': {'key': 'Board', 'format': "int({0})"}},
           'location_str': {'rd53a_db': {'key': 'Location', 'format': "'{0}'.replace('$', '')"}},
           'dose': {'rd53a_db': {'key': 'Mrad', 'format': "float({0})"}},
           'comment': {'rd53a_db': {'key': 'Chip_Comment', 'format': "'{0}'"}},
           'IREF_TRIM': {'rd53a_db': {'key': 'IREF_TRIM', 'format': "int({0})"}},
           'VREF_A_TRIM': {'rd53a_db': {'key': 'VREF_A_TRIM', 'format': "int({0})"}},
           'VREF_D_TRIM': {'rd53a_db': {'key': 'VREF_D_TRIM', 'format': "int({0})"}},
           'MON_BG_TRIM': {'rd53a_db': {'key': 'MON_BG_TRIM', 'format': "int({0})"}},
           'MON_ADC_TRIM': {'rd53a_db': {'key': 'MON_ADC_TRIM', 'format': "int({0})"}},
           'VCAL_SLOPE': {'rd53a_db': {'key': 'VCAL_SLOPE', 'format': "float({0})"}},
           'VCAL_OFFSET': {'rd53a_db': {'key': 'VCAL_OFFSET', 'format': "float({0})"}},
           }


class DBError(Exception):
    pass


class DBUnreachableError(Exception):
    pass


class DBCacheNotFoundError(Exception):
    pass


def _cache_rd53a_db(outfile=None, **_):
    '''
    Create a offline cache of the RD53A database

    Parameters
    ----------
    outfile : str
        Full path to cache file to be created. Default cache file is used if None.
    '''

    start_timer = time.time()

    if outfile is None:
        outfile = os.path.join(proj_dir, default_cache_filename)

    uri = rd53_db_uri + 'getdb'

    try:
        urllib.request.urlretrieve(uri, outfile)
    except (urllib.error.HTTPError, urllib.error.URLError):
        raise DBUnreachableError('Cannot connect to RD53A database. Are you offline?')

    log.debug('RD53 database has been cached to {0} in {1:1.3f}s'.format(outfile, time.time() - start_timer))


def _get_chip_from_rd53a_db_cache(wafer_no, col, row, infile=None, **_):
    '''
    Query the cached version of the RD53 internal DB for a specific chip and parse the output.

    Parameters
    ----------
    wafer_no : int
        RD53-internal wafer number. E.g. 16.

    col : int
        Column position of the chip pn the wafer.

    row : int
        Row position of the chip on the wafer.

    infile : str
        Full path to cached database file. Default cachefile is used if None.

    Returns
    -------
    data : dict
        Parsed output as dictionary.
    '''

    def dict_factory(cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    if infile is None:
        infile = os.path.join(proj_dir, default_cache_filename)

    if not os.path.isfile(infile):
        log.error('Cannot find cached version of RD53A database!')
        raise DBCacheNotFoundError("No such file: '{0}'".format(infile))

    log.debug('Reading data from cache file {}'.format(infile))

    creation_date = os.path.getmtime(infile)
    if time.time() - creation_date > (24 * 60 * 60):
        log.warning('Cached data is older than 24h, consider creating a new cache. Creation date {}'.format(datetime.fromtimestamp(creation_date)))
    elif time.time() - creation_date > 1:
        log.debug('Cache file was created {}'.format(datetime.fromtimestamp(creation_date)))

    cache = sqlite3.connect(infile)
    cache.row_factory = dict_factory
    c = cache.cursor()

    res = list(c.execute("SELECT * FROM CHIP WHERE Wafer=? AND col=? AND row=?", (wafer_no, col, row, )))
    if len(res) == 0:
        raise DBError('Chip 0x{0:02X}{1:0X}{2:0X} does not exist in cached RD53 database!'.format(wafer_no, col, row))
    elif len(res) != 1:
        raise RuntimeError('SQL query returned more than one chip')

    chip_cache = res[0]
    chip_sn = '0x{0:02X}{1:1X}{2:1X}'.format(wafer_no, col, row)

    if chip_sn != chip_cache['SN']:
        raise RuntimeError('SQL query returned the wrong chip')

    data = {}
    for key, val in DB_KEYS.items():
        db_key = val['rd53a_db']['key']
        fmt = val['rd53a_db']['format']
        try:
            raw_dat = chip_cache[db_key]
            if raw_dat == "NONE" or raw_dat == "None" or raw_dat is None:
                data[key] = None
            else:
                data[key] = eval(fmt.format(raw_dat))
        except KeyError:
            log.warning('Found no data for key {0} in cached database!'.format(key))

    return data


#
# API definition
#


def query_rd53a_db(chip_sn, offline=False, cache_file=None):
    '''
    Query the RD53A database on lxplus by caching the db to local disk and running the query on the cached file

    Parameters
    ----------
    chip_sn : str
        Chip serial number in format '0xWWCR' as string.

    offline : bool
        If True, do not attempt to refresh the database cache.

    cache_file : str
        Full path to cached database file. Default cachefile is used if None.

    Returns
    -------
    data : dict
        Parsed output as dictionary as definde by DB_KEYS.
    '''

    if not offline:
        if cache_file is None:
            cache_file = os.path.join(proj_dir, default_cache_filename)

        try:
            _cache_rd53a_db(outfile=cache_file)
        except DBUnreachableError as e:
            log.error(e)

    wafer_no = int(chip_sn[2:4], 16)
    col = int(chip_sn[4:5], 16)
    row = int(chip_sn[5:6], 16)

    return _get_chip_from_rd53a_db_cache(wafer_no, col, row, infile=cache_file)


def check_chip_in_database(chip_type, chip_sn):
    '''
    Check if the chip of type 'chip_type' with SN 'chip_sn' exists in the RD53 database
    and log errors if the chip entry or some properties are missing.
    Uses a cached version of the DB by default. If no cache is found or the check fails on the cached DB,
    the cache is refreshed automatically.

    Parameters
    ----------
    chip_type : str
        Type of the chip to check. Defines database or cahce file to use.

    chip_sn : str
        Chip serial number in format '0xWWCR' as string.
    '''

    if chip_type != 'rd53a':
        log.warning('Database lookup activated, but not available for {}...'.format(chip_type))
        return

    log.info('Checking database for chip {}...'.format(chip_sn))

    # Check if chip is in DB at all
    try:
        data = query_rd53a_db(chip_sn)
    except DBError:
        log.error('Chip was not found in RD53 database!')
        log.error('Please add this chip to the RD53 database before you continue testing!')
        return
    except DBCacheNotFoundError:
        log.error('Could not check if chip is in database, since there is no local database cache!')
        log.error('Please resolve this issue before you continue testing!')
        return

    log.success('Found chip {} in the RD53 database!'.format(chip_sn))

    # If data was found, check if board type is set
    msg = 'Board type is not set in the RD53 database. Please add this property before you continue testing!'
    try:
        if data['board_type'] == 0:
            log.error(msg)
    except KeyError:
        log.error(msg)

    # TODO: Location check?
    # print(data['location'])


#
# CLI definition
#


def main():
    parser = argparse.ArgumentParser(description='Interface with the RD53 database.',
                                     usage="""bdaq_db <command>

        Possible commands are:
            query   Get data for a specific chip
            cache   Manually create an offline cache of the RD53 database.""")

    parser.add_argument('command', type=str, help="Subcommand to execute.")

    args = parser.parse_args(sys.argv[1:2])

    if args.command in ['query']:
        query_db()
    elif args.command in ['cache']:
        cache_db()
    else:
        log.error('Unknown command: {}'.format(args.command))


def query_db():
    parser = argparse.ArgumentParser(description='Get data for a specific chip.',
                                     usage="""bdaq_db query <chip_type> <chip_sn> [<options>]""")
    parser.add_argument('chip_type', type=str, help="Chip type. So far, only 'rd53a' is supported.")
    parser.add_argument('chip_sn', type=str, help="Chip SN in format '0xWWCR' to query the DB for.")
    parser.add_argument('-o', '--offline', action='store_true', help='Only use offline version of cache')
    parser.add_argument('-f', '--cache_file', type=str, default=None, help="Input filename of the cached data.")

    args = parser.parse_args(sys.argv[2:])

    if not args.chip_sn[0:2] == '0x':
        msg = "Chip SN has wrong format. Format needs to be '0xWWCR'"
        log.error(msg)
        raise RuntimeError(msg)

    args_dict = vars(args)
    chip_type = args_dict.pop('chip_type')
    if chip_type == 'rd53a':
        data = query_rd53a_db(**args_dict)
    else:
        raise NotImplementedError('This functionality is not yet implement for {}'. format(chip_type))

    print('----------')
    print('Data for chip {}:'.format(data['chip_sn']))
    for key, val in data.items():
        if key == 'chip_sn':
            continue
        print('{0}: {1}'.format(key, val))
    print('----------')


def cache_db():
    parser = argparse.ArgumentParser(description='Create an offline cache of the RD53 database.',
                                     usage="""bdaq_db cache [<options>]""")
    parser.add_argument('-f', '--outfile', type=str, default=None, help="Ouput filename for the cache data.")

    args = parser.parse_args(sys.argv[2:])

    _cache_rd53a_db(**vars(args))


if __name__ == '__main__':
    main()
