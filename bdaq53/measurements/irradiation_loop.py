#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Irradiation scan looping script to keep the chip busy during irradiation steps
'''

import os
import time
import yaml
import logging
import copy
import numpy as np

from basil.dut import Dut
from bdaq53.system import logger
from bdaq53.chips.rd53a import RD53A
from bdaq53.system.bdaq53_base import BDAQ53
from bdaq53.analysis import analysis_utils as au
from bdaq53.scans.scan_threshold import ThresholdScan
from bdaq53.scans.meta_tune_local_threshold import MetaTDACTuning


output_dir = ''
initial_maskfile_LIN = ''
initial_maskfile_DIFF = ''

configuration_untuned_coarse_threshold_scan = {
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 4000,
    'VCAL_HIGH_step': 100,
    'maskfile': None,
    'use_good_pixels_diff': False
}

configuration_untuned_fine_threshold_scan = {
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10,
    'maskfile': None,
    'use_good_pixels_diff': False
}

configuration_initial_tuning_lin_threshold_scan = {
    'start_column': 128,
    'stop_column': 264,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10,
    'maskfile': initial_maskfile_LIN,
    'use_good_pixels_diff': False
}

configuration_initial_tuning_diff_threshold_scan = {
    'start_column': 264,
    'stop_column': 400,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10,
    'maskfile': initial_maskfile_DIFF,
    'use_good_pixels_diff': False
}

configuration_tuning_lin = {
    'start_column': 128,
    'stop_column': 264,
    'maskfile': None,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10
}

configuration_tuned_lin_threshold_scan = {
    'start_column': 128,
    'stop_column': 264,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10,
    'maskfile': 'auto',
    'use_good_pixels_diff': False
}

configuration_tuning_diff = {
    'start_column': 264,
    'stop_column': 400,
    'maskfile': None,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10
}

configuration_tuned_diff_threshold_scan = {
    'start_column': 264,
    'stop_column': 400,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10,
    'maskfile': 'auto',
    'use_good_pixels_diff': False
}


def append_data(scan, data):
    timestamp = time.time()

    try:
        with open(out_file, 'r') as of:
            old_data = yaml.full_load(of) or {}
    except IOError:
        old_data = {}

    if scan not in old_data.keys():
        old_data[scan] = {}

    with open(out_file, 'w') as of:
        old_data[scan][timestamp] = {}
        for key in data.keys():
            old_data[scan][timestamp][key] = data[key]
        yaml.dump(old_data, of)


if __name__ == "__main__":
    try:
        with open('../testbench.yaml', 'r') as tb:
            config = yaml.full_load(tb)

        config['output_directory'] = output_dir

        with open('../testbench.yaml', 'w') as tb:
            yaml.dump(config, tb)

        log = logger.setup_derived_logger('IrradiationLoop')
        logfile = logger.setup_logfile('irradiation.log')

        dut = Dut('irradiation.yaml')
        dut.init()

        out_file = os.path.join(output_dir, 'irradiation.dat')

        log.info('Starting new irradiation loop at timestamp %1.6f...' % (time.time()))
        while True:
            try:    # Coarse untuned threshold scan
                log.info('Start coarse untuned threshold scan...')
                with ThresholdScan(scan_config=configuration_untuned_coarse_threshold_scan) as scan:
                    scan.start()
                    mean_thr, mean_noise = scan.analyze()
                    append_data('threshold_scan_untuned_coarse', {'threshold': float(mean_thr), 'noise': float(mean_noise)})
                log.success('Coarse untuned threshold scan finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
            except Exception as e:
                log.exception('Exception during coarse untuned threshold scan: %s' % e)

            try:    # Fine untuned threshold scan
                log.info('Start fine untuned threshold scan...')
                with ThresholdScan(scan_config=configuration_untuned_fine_threshold_scan) as scan:
                    scan.start()
                    mean_thr, mean_noise = scan.analyze()
                    append_data('threshold_scan_untuned_fine', {'threshold': float(mean_thr), 'noise': float(mean_noise)})
                log.success('Fine untuned threshold scan finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
            except Exception as e:
                log.exception('Exception during fine untuned threshold scan: %s' % e)

            try:    # Initial tuning threshold scan for LIN
                log.info('Start initial tuning threshold scan for LIN...')
                with ThresholdScan(scan_config=configuration_initial_tuning_lin_threshold_scan) as scan:
                    scan.start()
                    mean_thr, mean_noise = scan.analyze()
                    append_data('threshold_scan_initial_tuning_LIN', {'threshold': float(mean_thr), 'noise': float(mean_noise)})
                log.success('Initial tuning threshold scan for LIN finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
            except Exception as e:
                log.exception('Exception during initial tuning LIN threshold scan: %s' % e)

            try:    # Initial tuning threshold scan for DIFF
                log.info('Start initial tuning threshold scan for DIFF...')
                with ThresholdScan(scan_config=configuration_initial_tuning_diff_threshold_scan) as scan:
                    scan.start()
                    mean_thr, mean_noise = scan.analyze()
                    append_data('threshold_scan_initial_tuning_DIFF', {'threshold': float(mean_thr), 'noise': float(mean_noise)})
                log.success('Initial tuning threshold scan for DIFF finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
            except Exception as e:
                log.exception('Exception during initial tuning DIFF threshold scan: %s' % e)

            try:    # Tuning LIN
                log.info('Start threshold tuning for LIN...')
                with MetaTDACTuning(scan_config=configuration_tuning_lin) as tuning:
                    tuning.start()
                    logger.success('LIN tuning finished!')
            except Exception as e:
                log.exception('Exception during tuning of LIN: %s' % e)

            try:    # Fine tuned threshold scan for LIN
                log.info('Start tuned threshold scan for LIN...')
                with ThresholdScan(scan_config=configuration_tuned_lin_threshold_scan) as scan:
                    scan.start()
                    mean_thr, mean_noise = scan.analyze()
                    append_data('threshold_scan_tuned_LIN', {'threshold': float(mean_thr), 'noise': float(mean_noise)})
                log.success('Tuned threshold scan for LIN finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
            except Exception as e:
                log.exception('Exception during tuned LIN threshold scan: %s' % e)

            try:    # Tuning DIFF
                log.info('Start threshold tuning for DIFF...')
                with MetaTDACTuning(scan_config=configuration_tuning_diff) as tuning:
                    tuning.start()
                    log.success('DIFF tuning finished!')
            except Exception as e:
                log.exception('Exception during tuning of DIFF: %s' % e)

            try:    # Fine tuned threshold scan for LIN
                log.info('Start tuned threshold scan for DIFF...')
                with ThresholdScan(scan_config=configuration_tuned_diff_threshold_scan) as scan:
                    scan.start()
                    mean_thr, mean_noise = scan.analyze()
                    append_data('threshold_scan_tuned_DIFF', {'threshold': float(mean_thr), 'noise': float(mean_noise)})
                log.success('Tuned threshold scan for DIFF finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
            except Exception as e:
                scan.close()
                log.exception('Exception during tuned DIFF threshold scan: %s' % e)

            try:    # Read Ring Oscillators
                bdaq = BDAQ53()
                bdaq.init()

                chip = RD53A(bdaq)
                chip.init()
                chip.init_communication()

                T_NTC = float(chip.get_temperature())
                log.info("Temperature with NTC is: %1.2f C", T_NTC)

                T_Sens = dut['Thermohygrometer'].get_temperature()[0]
                RH_Sens = dut['Thermohygrometer'].get_humidity()[0]
                append_data('Temperature', {'NTC': T_NTC, 'Sensirion': T_Sens, 'Sensirion_RH': RH_Sens})

                append_data('ring_oscillators', chip.get_ring_oscillators())
                bdaq.close()
            except Exception as e:
                bdaq.close()
                log.exception('Exception during Ring Oscillator readout: %s' % e)

    except KeyboardInterrupt:
        log.exception('Stopping irradiation loop at timestamp %1.6f...' % (time.time()))
