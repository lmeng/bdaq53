#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#


'''
    This meta script performs a simple source scan at different thresholds.
    Thresholds are tuned automatically to the given target and a threshold scan is performed.
'''

from bdaq53.scans.meta_tune_threshold import tune
from bdaq53.scans.scan_source import SourceScan


scan_parameters = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'use_good_pixels_diff': False,
    'maskfile': 'auto',

    # Target threshold
    'VCAL_MED': 500,
    'VCAL_HIGH_start': 720,   # 220
    'VCAL_HIGH_stop': 579,  # 80
    'VCAL_HIGH_step': -5,   # (220 - 80) / 5 = 28 steps

    # Noise occupancy scan settings
    'n_triggers': 1e8,
    'min_occupancy': 10000,

    # Source scan settings
    'scan_timeout': 60,  # timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,  # number of maximum received triggers after stopping readout, if False no limit on received trigger

    'trigger_latency': 100,  # latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 65,  # trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,  # length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,  # length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Should be adjusted for longer trigger length.

    'use_tdc': False,

    # Trigger configuration
    'bdaq': {'TLU': {
        'TRIGGER_MODE': 0,             # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 1            # Selecting trigger input: HitOR (1), disabled (0)
    }
    }
}


if __name__ == '__main__':
    vcal_high_range = range(scan_parameters['VCAL_HIGH_start'], scan_parameters['VCAL_HIGH_stop'], scan_parameters['VCAL_HIGH_step'])
    for vcal_high in vcal_high_range:
        tuning_params = scan_parameters.copy()
        tuning_params['VCAL_HIGH'] = vcal_high
        tune(tuning_params)

        source_params = scan_parameters.copy()
        source_params['maskfile'] = 'auto'
        with SourceScan(scan_config=source_params) as scan:
            try:
                scan.start()
                scan.notify('BDAQ53 source scan has finished for threshold {0}!'.format(vcal_high - scan_parameters['VCAL_MED']))
                scan.analyze()
            except Exception as e:
                scan.log.error(e)
                scan.notify('ERROR: BDAQ53 source scan has failed!')
