from colorama import Fore, Style

color_dict = {
    True: Fore.GREEN,
    False: Fore.RED
}


class QC_Test:
    DEFAULT_QC_PARAMETER_FILE = "QC_config_files/SLDO_analysis.yaml"
    unit_prefix = ['', 'm', 'mu', 'n', 'p', 'f']

    def __init__(self, output_file=None):
        if output_file is not None:
            self.write_to_txt = True
            self.file = open(output_file + ".txt", "w")
        else:
            self.write_to_txt = False
        print("{:<25s}|{:<15s}|{:<15s}|{:<15s}|{:<10s}".format("Parameter", "Value", "DesignValue", "Tolerance", "QC Passed"))
        print(('-' * 25 + "|") + ('-' * 15 + "|") * 3 + '-' * 10)
        print(('-' * 25 + "|") + ('-' * 15 + "|") * 3 + '-' * 10)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.write_to_txt:
            self.file.close()

    def QC_test(self, name, parameters, data):
        if 'Match' in name:
            if len(data) == 2:
                design_value = data[1] + float(parameters.get('add', 0))
                value = data[0]
            else:
                print('WRONG DATA FORMAT')
                return
        else:
            value = data + float(parameters.get('add', 0))
            design_value = float(parameters.get('design_value', 0))
        pass_tolerance = parameters.get('pass_tolerance', False)
        pass_above = parameters.get('pass_above', False)
        pass_below = parameters.get('pass_below', False)
        passed = True

        if parameters['measure'] == 'current':
            unit = 'A'
        elif parameters['measure'] == 'temperature':
            unit = '°C'
        elif parameters['measure'] == 'voltage':
            unit = 'V'
        elif parameters['measure'] == 'capacitance':
            unit = 'F'
        else:
            unit = ''

        if pass_tolerance:
            deviation = abs(value - design_value)
            if deviation > float(pass_tolerance):
                passed = False

            if unit != '':
                for i in range(len(self.unit_prefix)):
                    n = i
                    if abs(value * 1000 ** i) > 0.01:
                        break
            else:
                n = 0

            print_value = "%f %s" % (value * 1000 ** n, self.unit_prefix[n] + unit)
            print_design = "%f %s" % (design_value * 1000 ** n, self.unit_prefix[n] + unit)
            print_tolerance = "%f %s" % (pass_tolerance * 1000 ** n, self.unit_prefix[n] + unit)

            print(color_dict[passed] + "{:<25s}|{:<15s}|{:<15s}|{:<15s}|{:<10s}".format(name, print_value, print_design, print_tolerance, 'PASS' if passed else 'FAIL') + Style.RESET_ALL)
            print(('-' * 25 + "|") + ('-' * 15 + "|") * 3 + '-' * 10)
            if self.write_to_txt:
                self.file.write("{:<25s}|{:<15s}|{:<15s}|{:<15s}|{:<10s}".format(name, print_value, print_design, print_tolerance, 'PASS' if passed else 'FAIL'))
                self.file.write(('-' * 25 + "|") + ('-' * 15 + "|") * 3 + '-' * 10)
        if pass_above:
            if value < pass_above:
                passed = False

            if unit != '':
                for i in range(len(self.unit_prefix)):
                    n = i
                    if abs(value * 1000 ** i) > 0.01:
                        break
            else:
                n = 0

            print_value = "%f %s" % (value * 1000 ** n, self.unit_prefix[n] + unit)
            print_design = "%f %s" % (pass_above * 1000 ** n, self.unit_prefix[n] + unit)
            print_tolerance = "above"

            print(color_dict[passed] + "{:<25s}|{:<15s}|{:<15s}|{:<15s}|{:<10s}".format(name, print_value, print_design, print_tolerance, 'PASS' if passed else 'FAIL') + Style.RESET_ALL)
            print(('-' * 25 + "|") + ('-' * 15 + "|") * 3 + '-' * 10)
            if self.write_to_txt:
                self.file.write("{:<25s}|{:<15s}|{:<15s}|{:<15s}|{:<10s}".format(name, print_value, print_design, print_tolerance, 'PASS' if passed else 'FAIL'))
                self.file.write(('-' * 25 + "|") + ('-' * 15 + "|") * 3 + '-' * 10)

        if pass_below:
            if value > pass_below:
                passed = False

            if unit != '':
                for i in range(len(self.unit_prefix)):
                    n = i
                    if abs(value * 1000 ** i) > 0.01:
                        break
            else:
                n = 0

            print_value = "%f %s" % (value * 1000 ** n, self.unit_prefix[n] + unit)
            print_design = "%f %s" % (pass_below * 1000 ** n, self.unit_prefix[n] + unit)
            print_tolerance = "below"

            print(color_dict[passed] + "{:<25s}|{:<15s}|{:<15s}|{:<15s}|{:<10s}".format(name, print_value, print_design, print_tolerance, 'PASS' if passed else 'FAIL') + Style.RESET_ALL)
            print(('-' * 25 + "|") + ('-' * 15 + "|") * 3 + '-' * 10)

            if self.write_to_txt:
                self.file.write("{:<25s}|{:<15s}|{:<15s}|{:<15s}|{:<10s}".format(name, print_value, print_design, print_tolerance, 'PASS' if passed else 'FAIL'))
                self.file.write(('-' * 25 + "|") + ('-' * 15 + "|") * 3 + '-' * 10)

        return passed
