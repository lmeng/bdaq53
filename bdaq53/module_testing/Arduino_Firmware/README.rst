======================================
Arduino Firmware for NTC+Preassure Readout
======================================

Arduino Nano firmware to read out NTCs via the 7 analog input pins A0 to A6 and the internal (multiplexed) ADC. Preassure readout via analog input pin A7.
DO NOT CONNECT REF and 3V3 on the Arduino, since the preassure sensor requires 5V ADC range!!!
DO NOT USE NTC7! Connect preassure sensor directly to pin A7!
