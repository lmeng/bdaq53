import pyqtgraph as pg
from pyqtgraph.dockarea import DockArea, Dock
from PyQt5 import QtWidgets

from datetime import datetime
import numpy as np
import zmq

from online_monitor.utils import utils
from online_monitor.receiver.receiver import Receiver
from threading import Thread, Event
from queue import Queue


class TimeAxisItem(pg.AxisItem):
    def tickStrings(self, values, scale, spacing):
        return [datetime.fromtimestamp(value).strftime("%x %X") for value in values]


class Env_Receiver(Receiver):
    def setup_receiver(self):
        """
        initialize all data variables and internal flags
        """
        self.set_bidirectional_communication()  # We want to change converter settings
        self.avg_window = 180
        self.cmd_port = 29321

        # Data arrays to store recieved data
        self.data_temp_sensor_1 = None
        self.data_temp_sensor_2 = None
        self.data_temp_NTC = None
        self.data_humid_sensor_chuck = None
        self.data_humid_sensor_2 = None
        self.data_dew_sensor_1 = None
        self.data_dew_sensor_2 = None
        self.data_LV_voltage = None
        self.data_LV_current = None
        self.data_HV_voltage = None
        self.data_HV_current = None
        self.data_vacuum_pressure = None
        self.data_measuring_time = None

        # interlock flags
        self.interlock_temp_soft = None
        self.interlock_temp_hard = None
        self.interlock_humid_soft = None
        self.interlock_humid_hard = None
        self.interlock_pressure = None

        self.selected_module = None
        self.module_list = ["Unknown"]

        # Set up sender thread to pass button commands back to interlock
        self.ctx = zmq.Context()
        self.cmd_queue = Queue()
        self.stop_send_cmds = Event()
        t = Thread(target=self.send_out_cmd)
        t.start()

    # GUI SETUP
    def setup_widgets(self, parent, name):
        """
        set up the main GUI for the online monitor
        """
        DCS_MainWidget = QtWidgets.QWidget()
        parent.addTab(DCS_MainWidget, name)

        main_layout = QtWidgets.QGridLayout()
        DCS_MainWidget.setLayout(main_layout)

        status_widget = self.setup_statusbar()
        main_layout.addWidget(status_widget)

        dock_tabs = QtWidgets.QTabWidget()
        main_layout.addWidget(dock_tabs)

        dock_temp = self.setup_temperature_dock()
        dock_tabs.addTab(dock_temp, 'Environment')

        dock_power = self.setup_power_dock()
        dock_tabs.addTab(dock_power, 'Powersupply')

        dock_pressure = self.setup_pressure_dock()
        dock_tabs.addTab(dock_pressure, 'Vacuum Pressure')

    def setup_statusbar(self):
        """
        sets up the status bar holding DCS control and interlock indicators
        """
        status_widget = QtWidgets.QWidget()
        layout = QtWidgets.QGridLayout()
        status_widget.setLayout(layout)

        dock_status = DockArea()

        title_temp = Dock("Temperature", size=(10, 20), autoOrientation=False)
        self.label_temp_NTC = QtWidgets.QLabel("NTC:\n -- C")
        self.label_temp_Chuck = QtWidgets.QLabel("Chuck:\n-- C")
        title_temp.addWidget(self.label_temp_NTC, 0, 1, 1, 1)
        title_temp.addWidget(self.label_temp_Chuck, 0, 2, 1, 1)

        title_humidity = Dock("Humidity", size=(10, 20), autoOrientation=False)
        self.label_humidity_chuck = QtWidgets.QLabel("Chuck:\n-- %")
        self.label_humidity_box = QtWidgets.QLabel("Box:\n-- %")
        title_humidity.addWidget(self.label_humidity_chuck, 0, 1, 1, 1)
        title_humidity.addWidget(self.label_humidity_box, 0, 2, 1, 1)

        title_dew = Dock("Dew point", size=(10, 10), autoOrientation=False)
        self.dewpoint_label = QtWidgets.QLabel("Chuck:\n-- C")
        title_dew.addWidget(self.dewpoint_label, 1, 8, 1, 1)

        title_power = Dock("Powersupply", size=(10, 30), autoOrientation=False)
        self.label_power_lv = QtWidgets.QLabel("LV Voltage:\n-- V")
        self.label_power_lv_c = QtWidgets.QLabel("LV Current:\n-- A")
        self.label_power_hv = QtWidgets.QLabel("HV:\n-- V")
        title_power.addWidget(self.label_power_lv, 0, 1, 1, 1)
        title_power.addWidget(self.label_power_lv_c, 0, 2, 1, 1)
        title_power.addWidget(self.label_power_hv, 0, 3, 1, 1)

        dock_status.addDock(title_temp)
        dock_status.addDock(title_humidity, position='right', relativeTo=title_temp)
        dock_status.addDock(title_dew, position='right', relativeTo=title_humidity)
        dock_status.addDock(title_power, position='right', relativeTo=title_dew)

        self.last_timestamp_label = QtWidgets.QLabel("Last timestamp:\n%s" % "no")

        self.setup_settings_popup()

        self._setup_interlock_buttons(layout)
        self._setup_DCS_control_buttons(layout)

        layout.addWidget(dock_status, 0, 1, 1, 6)

        layout.addWidget(self.last_timestamp_label, 0, 7, 1, 1)

        return status_widget

    def setup_temperature_dock(self):
        dock_area = DockArea()
        dock_temperature = Dock("Temperature and Humidity", size=(800, 800))
        dock_area.addDock(dock_temperature)

        cooling_graphics = pg.GraphicsLayoutWidget()
        cooling_graphics.show()

        plot_temp = self._setup_temperature_plot()
        plot_humidity = self._setup_humidity_plot(anchor_Xaxis=plot_temp)

        cooling_graphics.addItem(plot_temp, row=0, col=1, rowspan=1, colspan=2)
        cooling_graphics.addItem(plot_humidity, row=1, col=1, rowspan=1, colspan=2)
        dock_temperature.addWidget(cooling_graphics)

        return dock_area

    def setup_power_dock(self):
        dock_area = DockArea()
        dock_power = Dock("Powersupply", size=(800, 800))
        dock_area.addDock(dock_power)

        power_graphics = pg.GraphicsLayoutWidget()
        power_graphics.show()

        ps_layout = pg.GraphicsLayout()
        power_graphics.setCentralWidget(ps_layout)
        lv_plot = self._setup_powersupply_plot(ps_layout)

        dock_power.addWidget(power_graphics)

        hv_graphics = pg.GraphicsLayoutWidget()
        hv_graphics.show()

        hv_layout = pg.GraphicsLayout()
        hv_graphics.setCentralWidget(hv_layout)
        self._setup_powersupply_hv_plot(hv_layout, anchor_Xaxis=lv_plot)

        dock_power.addWidget(hv_graphics)

        return dock_area

    def setup_pressure_dock(self):
        dock_area = DockArea()
        dock_pressure = Dock("Vacuum Pressure", size=(600, 600))
        dock_time = Dock("Time", size=(800, 800))
        dock_area.addDock(dock_pressure)
        dock_area.addDock(dock_time)

        pressure_graphics = pg.GraphicsLayoutWidget()
        pressure_graphics.show()
        plot_pressure = self._setup_pressure_plot()
        pressure_graphics.addItem(plot_pressure, row=0, col=1, rowspan=1, colspan=2)
        dock_pressure.addWidget(pressure_graphics)

        time_graphics = pg.GraphicsLayoutWidget()
        time_graphics.show()
        plot_time = self._setup_time_plot(anchor_Xaxis=plot_pressure)
        time_graphics.addItem(plot_time, row=0, col=1, rowspan=1, colspan=2)
        dock_time.addWidget(time_graphics)

        return dock_area

    def _setup_interlock_buttons(self, layout):
        """
        sets up the interlock indicators
        """
        self.interlock_label = QtWidgets.QLabel("Interlock State:")
        layout.addWidget(self.interlock_label, 1, 1, 1, 6)

        self.temperature_button = QtWidgets.QPushButton('Temperature')
        self.temperature_button.setStyleSheet("background-color: grey")
        self.temperature_button.clicked.connect(self.reset_interlock)
        layout.addWidget(self.temperature_button, 2, 1, 1, 1)

        self.dewpoint_button = QtWidgets.QPushButton('Dew Point')
        self.dewpoint_button.setStyleSheet("background-color: grey")
        self.dewpoint_button.clicked.connect(self.reset_interlock)
        layout.addWidget(self.dewpoint_button, 2, 2, 1, 1)

        self.pressure_button = QtWidgets.QPushButton('Vacuum Pressure')
        self.pressure_button.setStyleSheet("background-color: grey")
        self.pressure_button.clicked.connect(self.reset_interlock)
        layout.addWidget(self.pressure_button, 2, 3, 1, 1)

    def _setup_DCS_control_buttons(self, layout):
        """
        sets up the buttons that control the DCS
        """

        # Current Module and Module Switching
        self.module_label = QtWidgets.QLabel("Module under test:\n Not Available")
        layout.addWidget(self.module_label, 0, 9, 1, 1)

        self.module_button = QtWidgets.QPushButton('Switch Modules')
        self.module_button.clicked.connect(self.switch_modules)
        layout.addWidget(self.module_button, 0, 11, 1, 1)

        self.module_selection_combo = QtWidgets.QComboBox()
        self.module_selection_combo.addItems(self.module_list)

        def module_selection_funct(module):
            self.selected_module = module

        self.module_selection_combo.currentTextChanged.connect(module_selection_funct)
        layout.addWidget(self.module_selection_combo, 0, 10, 1, 1)

        # Button to power on/off the selected module
        self.power_module_button = QtWidgets.QPushButton('Module Power On/Off')
        self.power_module_button.setStyleSheet("background-color: grey")
        self.power_is_on = Event()
        self.power_module_button.clicked.connect(self.power_module)
        layout.addWidget(self.power_module_button, 1, 11, 1, 1)

        self.cooling_button = QtWidgets.QPushButton('Stop Cooling')
        self.cooling_button.clicked.connect(self.stop_cooling)
        layout.addWidget(self.cooling_button, 1, 10, 1, 1)

        self.reset_button = QtWidgets.QPushButton('Reset Plot')
        self.reset_button.clicked.connect(lambda: self.send_command("RESET"))
        layout.addWidget(self.reset_button, 2, 11, 1, 1)

        self.settings_button = QtWidgets.QPushButton('Settings')
        self.settings_button.clicked.connect(self.popup_window.show)
        layout.addWidget(self.settings_button, 2, 10, 1, 1)

    def setup_settings_popup(self):
        """
        sets up a popup window with settings for LV/HV and Temperature
        """
        self.popup_window = QtWidgets.QWidget()
        self.popup_window.setStyleSheet("QWidget {background-color:white}")
        layout = QtWidgets.QGridLayout()
        self.popup_window.setLayout(layout)

        HV_voltage_label = QtWidgets.QLabel("HV Voltage:")
        LV_voltage_label = QtWidgets.QLabel("LV Voltage:")
        LV_current_label = QtWidgets.QLabel("LV Current:")
        Temp_label = QtWidgets.QLabel("Set Temperature:")

        self.HV_voltage_set = QtWidgets.QLabel("-- V")
        self.LV_voltage_set = QtWidgets.QLabel("-- V")
        self.LV_current_set = QtWidgets.QLabel("-- A")
        self.Temp_set = QtWidgets.QLabel("-- T")

        self.LV_current_settings = QtWidgets.QLineEdit()
        self.LV_voltage_settings = QtWidgets.QLineEdit()
        self.Temp_settings_field = QtWidgets.QLineEdit()
        self.HV_settings_field = QtWidgets.QLineEdit()

        confirm_settings_button = QtWidgets.QPushButton('OK')
        confirm_settings_button.clicked.connect(lambda: self.confirm_settings(True))
        cancel_settings_button = QtWidgets.QPushButton('Close')
        cancel_settings_button.clicked.connect(lambda: self.confirm_settings(False))

        layout.addWidget(LV_voltage_label, 0, 1, 1, 1)
        layout.addWidget(self.LV_voltage_set, 0, 2, 1, 1)
        layout.addWidget(self.LV_voltage_settings, 0, 3, 1, 2)

        layout.addWidget(LV_current_label, 1, 1, 1, 1)
        layout.addWidget(self.LV_current_set, 1, 2, 1, 1)
        layout.addWidget(self.LV_current_settings, 1, 3, 1, 2)

        layout.addWidget(HV_voltage_label, 2, 1, 1, 1)
        layout.addWidget(self.HV_voltage_set, 2, 2, 1, 1)
        layout.addWidget(self.HV_settings_field, 2, 3, 1, 2)

        layout.addWidget(Temp_label, 3, 1, 1, 1)
        layout.addWidget(self.Temp_set, 3, 2, 1, 1)
        layout.addWidget(self.Temp_settings_field, 3, 3, 1, 2)

        layout.addWidget(confirm_settings_button, 5, 3, 1, 1)
        layout.addWidget(cancel_settings_button, 5, 4, 1, 1)

    def _setup_temperature_plot(self, anchor_Xaxis=None):
        date_axis_temp = TimeAxisItem(orientation="bottom")
        plot_temp = pg.PlotItem(axisItems={"bottom": date_axis_temp}, labels={"left": "Temperature / °C"})

        self.temp_sensor_curve_1 = pg.PlotCurveItem(pen=pg.mkPen("b", width=2))
        self.temp_sensor_curve_2 = pg.PlotCurveItem(pen=pg.mkPen("purple", width=2))
        self.temp_NTC_curve = pg.PlotCurveItem(pen=pg.mkPen(0, 181, 97), width=2)
        self.dewpoint_curve_1 = pg.PlotCurveItem(pen=pg.mkPen("r", width=2))
        self.dewpoint_curve_2 = pg.PlotCurveItem(pen=pg.mkPen("y", width=2))

        legend_temp = pg.LegendItem(offset=(50, 1))
        legend_temp.setParentItem(plot_temp)
        legend_temp.addItem(self.temp_NTC_curve, '<math>T<sub>ModuleNTC</sub></math>')  # module temperature
        legend_temp.addItem(self.temp_sensor_curve_1, '<math>T<sub>Chuck</sub></math>')  # temperatre in testing box
        legend_temp.addItem(self.temp_sensor_curve_2, '<math>T<sub>Box</sub></math>')  # temperature on cooling block
        legend_temp.addItem(self.dewpoint_curve_1,
                            '<math>T<sub>DewPoint Chuck</sub></math>')  # dewpoint calculated through T_A and RH
        legend_temp.addItem(self.dewpoint_curve_2,
                            '<math>T<sub>DewPoint Box</sub></math>')  # dewpoint calculated through T_B and RH

        plot_temp.addItem(self.temp_sensor_curve_1)
        plot_temp.addItem(self.temp_sensor_curve_2)
        plot_temp.addItem(self.dewpoint_curve_1)
        plot_temp.addItem(self.dewpoint_curve_2)
        plot_temp.addItem(self.temp_NTC_curve)

        plot_temp.vb.setBackgroundColor("#E6E5F4")
        plot_temp.getAxis("left").setZValue(0)
        plot_temp.getAxis("left").setGrid(155)

        if anchor_Xaxis is not None:
            plot_temp.setXLink(anchor_Xaxis)

        return plot_temp

    def _setup_humidity_plot(self, anchor_Xaxis=None):
        date_axis_humid = TimeAxisItem(orientation="bottom")
        plot_humidity = pg.PlotItem(axisItems={"bottom": date_axis_humid}, labels={"left": "rel. Humidity / %"})

        self.humid_sensor_curve_chuck = pg.PlotCurveItem(pen=pg.mkPen("r", width=2))
        self.humid_sensor_curve_2 = pg.PlotCurveItem(pen=pg.mkPen("y", width=2))

        # add legend
        legend_humid_sensor = pg.LegendItem(offset=(50, 1))
        legend_humid_sensor.setParentItem(plot_humidity)
        legend_humid_sensor.addItem(self.humid_sensor_curve_chuck,
                                    '<math>RH<sub>Chuck</sub></math>')  # relative humidity
        legend_humid_sensor.addItem(self.humid_sensor_curve_2, '<math>RH<sub>Box</sub></math>')
        # add items to plots and customize plots viewboxes

        plot_humidity.addItem(self.humid_sensor_curve_chuck)
        plot_humidity.addItem(self.humid_sensor_curve_2)

        plot_humidity.vb.setBackgroundColor("#E6E5F4")
        plot_humidity.showGrid(x=True, y=True)
        plot_humidity.getAxis("left").setZValue(0)
        plot_humidity.getAxis("bottom").setGrid(155)

        if anchor_Xaxis is not None:
            plot_humidity.setXLink(anchor_Xaxis)

        return plot_humidity

    def _setup_powersupply_plot(self, layout, anchor_Xaxis=None):
        date_axis = TimeAxisItem(orientation="bottom")

        current_plot = pg.PlotItem(axisItems={"bottom": date_axis}, labels={"right": "Output Current HMP / A"})
        current_axis = current_plot.getAxis('right')
        current_box = current_plot.vb
        # current_axis = pg.AxisItem("right")
        # current_box = pg.ViewBox()

        layout.addItem(current_axis, row=2, col=2, rowspan=1, colspan=1)

        plot = pg.PlotItem(axisItems={"bottom": date_axis}, labels={"left": "Output Voltage HMP / Volt"})
        voltage_box = plot.vb

        layout.addItem(plot, row=2, col=1, rowspan=1, colspan=1)
        layout.scene().addItem(current_box)

        current_axis.linkToView(current_box)
        current_box.setXLink(voltage_box)

        # current_axis.setLabel("Output Current HMP / Ampere")

        self.LV_voltage_curve = pg.PlotCurveItem(pen=pg.mkPen("r", width=2))
        self.LV_current_curve = pg.PlotCurveItem(pen=pg.mkPen("b", width=2))

        legend = pg.LegendItem(offset=(50, 1))
        legend.setParentItem(plot)
        legend.addItem(self.LV_voltage_curve, "LV Voltage")
        legend.addItem(self.LV_current_curve, "LV Current")

        voltage_box.addItem(self.LV_voltage_curve)
        current_box.addItem(self.LV_current_curve)

        current_box.setBackgroundColor("#E6E5F4")
        plot.showGrid(x=True, y=True)
        plot.getAxis("left").setZValue(0)
        plot.getAxis("bottom").setGrid(155)
        voltage_box.setYRange(0, 4)
        current_box.setYRange(0, 12)

        if anchor_Xaxis is not None:
            plot.setXLink(anchor_Xaxis)

        def updateViews():
            current_box.setGeometry(voltage_box.sceneBoundingRect())

        voltage_box.sigResized.connect(updateViews)

        return plot

    def _setup_powersupply_hv_plot(self, layout, anchor_Xaxis=None):
        date_axis = TimeAxisItem(orientation="bottom")

        current_plot = pg.PlotItem(axisItems={"bottom": date_axis}, labels={"right": "Leakage Current / A"})
        current_axis = current_plot.getAxis('right')
        current_box = current_plot.vb
        # current_axis = pg.AxisItem("right")
        # current_box = pg.ViewBox()

        layout.addItem(current_axis, row=2, col=2, rowspan=1, colspan=1)

        plot = pg.PlotItem(axisItems={"bottom": date_axis}, labels={"left": "Bias Voltage / Volt"})
        voltage_box = plot.vb

        layout.addItem(plot, row=2, col=1, rowspan=1, colspan=1)
        layout.scene().addItem(current_box)

        current_axis.linkToView(current_box)
        current_box.setXLink(voltage_box)

        # current_axis.setLabel("Output Current HMP / Ampere")

        self.HV_voltage_curve = pg.PlotCurveItem(pen=pg.mkPen("r", width=2))
        self.HV_current_curve = pg.PlotCurveItem(pen=pg.mkPen("b", width=2))

        legend = pg.LegendItem(offset=(50, 1))
        legend.setParentItem(plot)
        legend.addItem(self.HV_voltage_curve, "HV Voltage")
        legend.addItem(self.HV_current_curve, "HV leakage current")

        voltage_box.addItem(self.HV_voltage_curve)
        current_box.addItem(self.HV_current_curve)

        current_box.setBackgroundColor("#E6E5F4")
        plot.showGrid(x=True, y=True)
        plot.getAxis("left").setZValue(0)
        plot.getAxis("bottom").setGrid(155)

        if anchor_Xaxis is not None:
            plot.setXLink(anchor_Xaxis)

        def updateViews():
            current_box.setGeometry(voltage_box.sceneBoundingRect())

        voltage_box.sigResized.connect(updateViews)

    def _setup_pressure_plot(self, anchor_Xaxis=None):
        date_axis = TimeAxisItem(orientation="bottom")
        plot_pressure = pg.PlotItem(axisItems={"bottom": date_axis}, labels={"left": "Vacuum Pressure / kPa"})

        self.pressure_sensor_curve = pg.PlotCurveItem(pen=pg.mkPen("b", width=2))

        plot_pressure.addItem(self.pressure_sensor_curve)

        plot_pressure.vb.setBackgroundColor("#E6E5F4")
        plot_pressure.getAxis("left").setZValue(0)
        plot_pressure.getAxis("left").setGrid(155)

        if anchor_Xaxis is not None:
            plot_pressure.setXLink(anchor_Xaxis)

        return plot_pressure

    def _setup_time_plot(self, anchor_Xaxis=None):
        date_axis = TimeAxisItem(orientation="bottom")
        plot_time = pg.PlotItem(axisItems={"bottom": date_axis}, labels={"left": "Readout Time / s"})

        self.time_curve = pg.PlotCurveItem(pen=pg.mkPen("b", width=2))

        plot_time.addItem(self.time_curve)

        plot_time.vb.setBackgroundColor("#E6E5F4")
        plot_time.getAxis("left").setZValue(0)
        plot_time.getAxis("left").setGrid(155)

        if anchor_Xaxis is not None:
            plot_time.setXLink(anchor_Xaxis)

        return plot_time

    # DATA RECEIVAL AND HANDLING
    def deserialize_data(self, data):
        _, meta = utils.simple_dec(data)
        return meta

    def handle_data(self, data):
        if "temp_1" in data:
            mask = ~np.isnan(data["temp_1"])
            self.data_temp_sensor_1 = (data["time"][mask], data["temp_1"][mask])
            self.label_temp_Chuck.setText("Chuck:\n %.1f C" % float(data["temp_1"][-1]))
        if "temp_2" in data:
            mask = ~np.isnan(data["temp_2"])
            self.data_temp_sensor_2 = (data["time"][mask], data["temp_2"][mask])
        if "humidity_chuck" in data:
            mask = ~np.isnan(data["humidity_chuck"])
            self.data_humid_sensor_chuck = (data["time"][mask], data["humidity_chuck"][mask])
            self.label_humidity_chuck.setText("Chuck:\n %.1f" % float(data["humidity_chuck"][-1]) + " %")
        if "humidity_2" in data:
            mask = ~np.isnan(data["humidity_2"])
            self.data_humid_sensor_2 = (data["time"][mask], data["humidity_2"][mask])
            self.label_humidity_box.setText("Box:\n %.1f" % float(data["humidity_2"][-1]) + " %")
        if "temp_NTC" in data:
            mask = ~np.isnan(data["temp_NTC"])
            self.data_temp_NTC = (data["time"][mask], data["temp_NTC"][mask])
            self.label_temp_NTC.setText("NTC:\n %.1f C" % float(data["temp_NTC"][-1]))
        if "dew_1" in data:
            mask = ~np.isnan(data["dew_1"])
            self.data_dew_sensor_1 = (data["time"][mask], data["dew_1"][mask])
        if "dew_2" in data:
            mask = ~np.isnan(data["dew_2"])
            self.data_dew_sensor_2 = (data["time"][mask], data["dew_2"][mask])
            self.dewpoint_label.setText("Average Dew point:\n%.1f C" % float(data["dew_2"][-1]))

        if "Vin" in data:
            mask = ~np.isnan(data["Vin"])
            self.data_LV_voltage = (data["time"][mask], data["Vin"][mask])
            self.label_power_lv.setText("LV Voltage:\n %.2f V" % float(data["Vin"][-1]))
        if "Iin" in data:
            mask = ~np.isnan(data["Iin"])
            self.data_LV_current = (data["time"][mask], data["Iin"][mask])
            self.label_power_lv_c.setText("LV Current:\n %.2f A" % float(data["Iin"][-1]))
        if "HV_V" in data:
            mask = ~np.isnan(data["HV_V"])
            self.data_HV_voltage = (data["time"][mask], data["HV_V"][mask])
            self.label_power_hv.setText("HV:\n %.1f V" % float(data["HV_V"][-1]))
        if "HV_leak" in data:
            mask = ~np.isnan(data["HV_leak"])
            self.data_HV_current = (data["time"][mask], data["HV_leak"][mask])
        if "vacuum_pressure" in data:
            mask = ~np.isnan(data["vacuum_pressure"])
            self.data_vacuum_pressure = (data["time"][mask], data["vacuum_pressure"][mask])
        if "measuring_time" in data:
            mask = ~np.isnan(data["measuring_time"])
            self.data_measuring_time = (data["time"][mask], data["measuring_time"][mask])
        if "module_name" in data:
            self.module_label.setText("Module under test:\n%s" % data["module_name"])
            if self.selected_module is None:
                self.selected_module = data["module_name"]
        if "module_list" in data:
            if data["module_list"] != self.module_list:
                self.module_list = data["module_list"]
                self.module_selection_combo.clear()
                self.module_selection_combo.addItems(self.module_list)
        if "set_values" in data:
            self.HV_voltage_set.setText(str(data["set_values"].get('HV_voltage', "--")) + " V")
            self.LV_voltage_set.setText(str(data["set_values"].get('LV_voltage', "--")) + " V")
            self.LV_current_set.setText(str(data["set_values"].get('LV_current', "--")) + " A")
            self.Temp_set.setText(str(data["set_values"].get('Temp', "--")) + " C")

        if "interlock_temp_soft" in data:
            self.interlock_temp_soft = (data["interlock_temp_soft"])
        if "interlock_temp_hard" in data:
            self.interlock_temp_hard = (data["interlock_temp_hard"])
        if "interlock_humid_soft" in data:
            self.interlock_humid_soft = (data["interlock_humid_soft"])
        if "interlock_humid_hard" in data:
            self.interlock_humid_hard = (data["interlock_humid_hard"])
        if "interlock_pressure" in data:
            self.interlock_pressure = (data["interlock_pressure"])

        self.last_timestamp_label.setText(
            "Last timestamp:\n%s" % datetime.fromtimestamp(data["last_timestamp"]).strftime("%x %X"))

    def refresh_data(self):
        if self.data_temp_sensor_1 is not None:
            self.temp_sensor_curve_1.setData(self.data_temp_sensor_1[0], self.data_temp_sensor_1[1],
                                             autoDownsample=True)
        if self.data_temp_sensor_2 is not None:
            self.temp_sensor_curve_2.setData(self.data_temp_sensor_2[0], self.data_temp_sensor_2[1],
                                             autoDownsample=True)
        if self.data_temp_NTC is not None:
            self.temp_NTC_curve.setData(self.data_temp_NTC[0], self.data_temp_NTC[1], autoDownsample=True)
        if self.data_humid_sensor_chuck is not None:
            self.humid_sensor_curve_chuck.setData(self.data_humid_sensor_chuck[0], self.data_humid_sensor_chuck[1],
                                                  autoDownsample=True)
        if self.data_humid_sensor_2 is not None:
            self.humid_sensor_curve_2.setData(self.data_humid_sensor_2[0], self.data_humid_sensor_2[1],
                                              autoDownsample=True)
        if self.data_dew_sensor_1 is not None:
            self.dewpoint_curve_1.setData(self.data_dew_sensor_1[0], self.data_dew_sensor_1[1], autoDownsample=True)
        if self.data_dew_sensor_2 is not None:
            self.dewpoint_curve_2.setData(self.data_dew_sensor_2[0], self.data_dew_sensor_2[1], autoDownsample=True)
        if self.data_LV_voltage is not None:
            self.LV_voltage_curve.setData(self.data_LV_voltage[0], self.data_LV_voltage[1], autoDownsample=True)
        if self.data_LV_current is not None:
            self.LV_current_curve.setData(self.data_LV_current[0], self.data_LV_current[1], autoDownsample=True)
            if self.data_LV_voltage[1][-1] > 0.01:
                if not self.power_is_on.is_set():
                    self.power_is_on.set()
                    self.power_module_button.setStyleSheet("background-color: green")
                    self.power_module_button.setEnabled(True)
            else:
                if self.power_is_on.is_set():
                    self.power_is_on.clear()
                    self.power_module_button.setStyleSheet("background-color: grey")
                    self.power_module_button.setEnabled(True)
        else:
            if self.power_is_on.is_set():
                self.power_is_on.clear()
                self.power_module_button.setStyleSheet("background-color: grey")
                self.power_module_button.setEnabled(True)
        if self.data_HV_voltage is not None:
            self.HV_voltage_curve.setData(self.data_HV_voltage[0], self.data_HV_voltage[1], autoDownsample=True)
        if self.data_HV_current is not None:
            self.HV_current_curve.setData(self.data_HV_current[0], self.data_HV_current[1], autoDownsample=True)
        if self.data_vacuum_pressure is not None:
            self.pressure_sensor_curve.setData(self.data_vacuum_pressure[0], self.data_vacuum_pressure[1],
                                               autoDownsample=True)
        if self.data_measuring_time is not None:
            self.time_curve.setData(self.data_measuring_time[0], self.data_measuring_time[1], autoDownsample=True)

        # change button color if interlock is set
        if self.interlock_temp_soft is not None:
            if self.interlock_temp_soft:
                self.temperature_button.setStyleSheet("background-color: yellow")
            else:
                self.temperature_button.setStyleSheet("background-color: green")
        else:
            self.temperature_button.setStyleSheet("background-color: grey")
        if self.interlock_temp_hard is not None:
            if self.interlock_temp_hard:
                self.temperature_button.setStyleSheet("background-color: red")

        if self.interlock_humid_soft is not None:
            if self.interlock_humid_soft:
                self.dewpoint_button.setStyleSheet("background-color: yellow")
            else:
                self.dewpoint_button.setStyleSheet("background-color: green")
        else:
            self.dewpoint_button.setStyleSheet("background-color: grey")
        if self.interlock_humid_hard is not None:
            if self.interlock_humid_hard:
                self.dewpoint_button.setStyleSheet("background-color: red")
        if self.interlock_pressure is not None:
            if self.interlock_pressure:
                self.pressure_button.setStyleSheet("background-color: red")
            else:
                self.pressure_button.setStyleSheet("background-color: green")
        else:
            self.dewpoint_button.setStyleSheet("background-color: grey")

    def _update_avg_window(self, value):
        self.avg_window = value
        self.send_command(str(value))

    # CMD HANDLING
    def send_out_cmd(self):
        req = self.ctx.socket(zmq.REQ)
        req.connect(f'tcp://localhost:{self.cmd_port}')

        while not self.stop_send_cmds.wait(1e-1):

            while not self.cmd_queue.empty():
                cmd_to_send = self.cmd_queue.get()
                req.send_json(cmd_to_send)
                print(req.recv_json())

    def reset_interlock(self):
        self.cmd_queue.put({'cmd': 'RESET'})

    def stop_cooling(self):
        self.cooling_button.setEnabled(False)
        self.cmd_queue.put({'cmd': 'COOLING OFF'})
        self.cooling_button.clicked.connect(self.start_cooling)
        self.cooling_button.setText('Start Cooling')
        self.cooling_button.setEnabled(True)

    def start_cooling(self):
        self.cooling_button.setEnabled(False)
        self.cmd_queue.put({'cmd': 'COOLING ON'})
        self.cooling_button.clicked.connect(self.stop_cooling)
        self.cooling_button.setText('Stop Cooling')
        self.cooling_button.setEnabled(True)

    def power_module(self):
        self.power_module_button.setEnabled(False)

        if self.power_is_on.is_set():
            state = 'OFF'
        else:
            state = 'ON'
        self.cmd_queue.put({'cmd': 'POWER', 'value': state})

    def switch_modules(self):
        self.cmd_queue.put({'cmd': 'SWITCH MODULES', 'value': self.selected_module})

    def confirm_settings(self, confirm):
        if confirm:
            if self.HV_settings_field.text() is not None and self.HV_settings_field.text() != '':
                print(self.HV_settings_field.text())
                self.cmd_queue.put({'cmd': 'HV SET VOLTAGE', 'value': float(self.HV_settings_field.text())})
                self.HV_settings_field.clear()
            if self.LV_current_settings.text() is not None and self.LV_current_settings.text() != '':
                print(self.LV_current_settings.text())
                self.cmd_queue.put({'cmd': 'LV SET CURRENT', 'value': float(self.LV_current_settings.text())})
                self.LV_current_settings.clear()
            if self.LV_voltage_settings.text() is not None and self.LV_voltage_settings.text() != '':
                print(self.LV_voltage_settings.text())
                self.cmd_queue.put({'cmd': 'LV SET VOLTAGE', 'value': float(self.LV_voltage_settings.text())})
                self.LV_voltage_settings.clear()
            if self.Temp_settings_field.text() is not None and self.Temp_settings_field.text() != '':
                print(self.Temp_settings_field.text())
                self.cmd_queue.put({'cmd': 'TEMP SET TARGET', 'value': float(self.Temp_settings_field.text())})
                self.Temp_settings_field.clear()
        else:
            self.popup_window.close()
