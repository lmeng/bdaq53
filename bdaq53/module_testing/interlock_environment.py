import time
from datetime import datetime
import yaml
import os
import requests
import traceback
import socket

from pathlib import Path
from online_monitor.utils import utils
import tables as tb
import numpy as np
import math
import zmq
from threading import Thread, Event
from queue import Queue

from influxdb import InfluxDBClient
from bdaq53.analysis import analysis_utils as au

import logging
from pyvisa.errors import VisaIOError
from serial import SerialException
from simple_pid import PID
from bdaq53.system.periphery import BDAQ53Periphery
from bdaq53 import DEFAULT_TESTBENCH

from colorama import Fore


color_dict = {
    True: Fore.GREEN,
    False: Fore.RED
}

DEFAULT_CMD_PORT = 29321
DEFAULT_DATA_PORT = 7000


def _load_testbench_cfg(bench_config):
    """
        Load the bench config into the scan

        Parameters:
        ----------
        bench_config : str or dict
                Testbench configuration (configuration as dict or its filename as string)
    """
    conf = au.ConfigDict()
    try:
        with open(bench_config) as f:
            conf = yaml.full_load(f)
    except TypeError:
        conf = bench_config
    return conf


def get_testbench(testbench_path, log):
    if os.path.isfile(testbench_path):
        log.info(f'Single testbench is loaded: {testbench_path}')
        with open(testbench_path, 'r') as f:
            testbench = yaml.full_load(f)
        return testbench
    elif os.path.isdir(testbench_path):
        log.info(f'Testbench directory: {testbench_path}')
        testbench_files = sorted(Path(testbench_path).glob('*testbench*.yaml'))

        combined_testbench = {}

        for file in testbench_files:
            if os.path.isfile(file):
                log.info(f'Reading testbench {file}')
                with open(file, 'r') as f:
                    testbench = yaml.full_load(f)

                for k, v in testbench.items():
                    if not combined_testbench.get(k):
                        combined_testbench[k] = v
                    else:
                        if k == 'modules':
                            for k_m, v_m in testbench['modules'].items():
                                combined_testbench['modules'][k_m] = v_m
                                log.info(f'Adding module {k_m} to testbench...')
                        elif k == 'hardware':
                            qms_dict = v.get('qms_dict', {})
                            if qms_dict:
                                for slot, module in qms_dict.items():
                                    if module is not None and module != 'None':
                                        combined_testbench['hardware']['qms_dict'][slot] = module
                        else:
                            if combined_testbench.get(k) != v:
                                log.error(f'Merging testbenches with different configuration: {k}')
                                for k_m, v_m in v.items():
                                    if v_m != combined_testbench[k].get(k_m):
                                        if combined_testbench[k].get(k_m) is None:
                                            combined_testbench[k][k_m] = v_m

                                        log.info(f'Configuration for {k_m} does not match. Using {k_m}: {combined_testbench[k][k_m]}')

        return combined_testbench


def calc_dew_point(T, RH):
    if T < 0:
        T_n = 243.12
        m = 17.6
    else:
        T_n = 272.62
        m = 22.46
    if RH == 0:
        RH = 0.01
    a = np.log(RH / 100.0) + m * T / (T_n + T)
    return T_n * a / (m - a)


class Env_Monitoring(object):
    def __init__(self, *args, **kwargs):
        # Set up ZQM
        self.context = zmq.Context()

        # Set up DCS logger
        self.set_up_logger()

        # Load configurations from interlock.yaml
        self.configuration = _load_testbench_cfg(os.path.join(os.path.dirname(__file__), 'interlock.yaml'))

        # Initialise flags and values
        self.init_flags()
        self.init_values()

        # get zmq-ports from interlock.yaml
        if self.configuration.get('ports') is not None:
            self.cmd_port = self.configuration['ports'].get('cmd_port', DEFAULT_CMD_PORT)
            self.data_port = self.configuration['ports'].get('data_port', DEFAULT_DATA_PORT)
        else:
            self.cmd_port = DEFAULT_CMD_PORT
            self.data_port = DEFAULT_DATA_PORT

        # connecting to influx database
        if not self.configuration['general'].get("save_as_h5"):
            self.init_influx_connection()

        # Main working directory
        if self.configuration['general']['output_directory'] is not None:
            self.working_dir = self.configuration['general']['output_directory']
        else:
            self.working_dir = os.path.join(os.path.dirname(__file__), 'Env_Data')

        # File for environmental data output
        if self.configuration['general'].get("save_as_h5"):
            self.init_env_data_file()
        else:
            self.t_file = None

        # Set target chiller temperature from interlock.yaml
        target_temperature = self.configuration['general'].get('target_temperature', 20)
        if target_temperature is not None:
            self.target_temperature = target_temperature
        else:
            self.target_temperature = 20

        if self.configuration['general'].get('use_chuck_for_pid', False):
            self.PID_temp_from_chuck.set()

        # Get interlock limits from interlock.yaml
        max_mod_temp = self.configuration['general'].get('maximum_module_temperature', {'soft': 30, 'hard': 45})
        self.maximum_module_temperature_soft = max_mod_temp.get('soft', 30)
        self.maximum_module_temperature_hard = max_mod_temp.get('hard', 45)

        dew_point_safety = self.configuration['general'].get('dew_point_safety_margin', {'soft': 5., 'hard': 2.})
        self.dew_point_safety_margin_soft = dew_point_safety.get('soft', 5.)
        self.dew_point_safety_margin_hard = dew_point_safety.get('hard', 2.)

        self.HV_current_limit = self.configuration['general'].get('hv_current_limit', 1e-5)

        self.minimum_vacuum_pressure = self.configuration['general'].get('minimum_vacuum_pressure', 20.)

        # Load module settings from testbench
        # If testbench is not specified in interlock.yaml, testbench.yaml from bdaq is used.
        if self.configuration['general']['bdaq_testbech_yaml'] is not None:
            if not os.path.isabs(self.configuration['general']['bdaq_testbech_yaml']):
                file_path = os.path.join(os.path.dirname(__file__), self.configuration['general']['bdaq_testbech_yaml'])
                self.configuration['general']['bdaq_testbech_yaml'] = file_path
            self.testbench_config = get_testbench(self.configuration['general']['bdaq_testbech_yaml'], self.logger)
        else:
            self.testbench_config = get_testbench(DEFAULT_TESTBENCH, self.logger)
        self.module_cfgs = {k: v for k, v in self.testbench_config['modules'].items() if 'identifier' in v.keys()}

        self.modules = {}
        self.module = None  # selected module

        slot_assignment_tmp = {}
        self.qms_dict = self.testbench_config['hardware'].get('qms_dict', {})
        for slot, module in self.qms_dict.items():
            if module is not None:
                if module != 'None' and module != '':
                    slot_assignment_tmp[module] = slot

        self.logger.info(f'Connected modules: {slot_assignment_tmp}')
        for module_cfg_key, module_cfg in self.module_cfgs.items():
            self.module = module_cfg_key  # use last module in testbench as default
            self.modules[module_cfg_key] = {'slot': int(slot_assignment_tmp.get(module_cfg_key, 'module_1').split('_')[-1])}
            self.modules[module_cfg_key]['identifier'] = module_cfg.get('identifier', 'unknown')
            self.modules[module_cfg_key]['powersupply'] = module_cfg.get('powersupply', {})
            self.modules[module_cfg_key]['power_cycle'] = module_cfg.get('power_cycle', False)
            if not self.modules[module_cfg_key]['powersupply'].get('hv_current_limit'):
                self.modules[module_cfg_key]['powersupply']['hv_current_limit'] = self.HV_current_limit

    def set_up_logger(self):
        self.logger = logging.getLogger("Env Monitoring")
        self.logger.setLevel(logging.INFO)
        file_handler = logging.FileHandler('sample.log')
        formatter = logging.Formatter('%(asctime)s:%(message)s')
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)
        self.logger.info("init Monitoring")

    def init_flags(self):
        # Flags for different interlock events
        self.interlock = Event()
        self.interlock_temp_soft = Event()
        self.interlock_temp_hard = Event()
        self.interlock_humid_soft = Event()
        self.interlock_humid_hard = Event()
        self.interlock_pressure = Event()

        # Messaging flags
        self.message_queue = Queue()

        # DCS smoothing
        self.write_DCS = Event()

        # Stop Flags
        self.devices_are_stopped = Event()
        self.stop_recv_cmd = Event()

        if self.configuration["general"]["use_chiller"]:
            self.stop_chiller = Event()

        # self.stop_n2 = Event()
        self.stop_dcs = Event()

        # Temperature control
        self.target_temp_changed = Event()
        self.set_temp_changed = Event()
        self.warming_up = Event()
        self.PID_temp_from_chuck = Event()

    def init_values(self):
        # Output values
        self.NTC = None
        self.T_chuck = None
        self.hum_chuck = None
        self.dew_chuck = None
        self.T_box = None
        self.hum_box = None
        self.dew_box = None
        self.I_in = 0
        self.I_leak = 0
        self.Vin = None
        self.P_vacuum = None

        # last written output values. Used for selecting when to write new values
        self.Vin_last = None
        self.hum_chuck_last = None
        self.NTC_last = None
        self.dew_chuck_last = None
        self.HV_voltage = None

        self.set_HV_voltage = None
        self.set_LV_voltage = None
        self.set_LV_current = None
        self.set_temperature = None

    def init_influx_connection(self):
        try:
            self.config_influx = self.configuration["local_db"]

            self.logger.info(f"Connecting to InfluxDB ({self.config_influx['influxdb_host']}:{self.config_influx['influxdb_port']}))")
            # InfluxDBClient(url="http://localhost:8086", username="my-user", password="my-password")
            self.influx = InfluxDBClient(host=self.config_influx['influxdb_host'], port=self.config_influx['influxdb_port'], username=self.config_influx["influxdb_user"], password=self.config_influx["influxdb_password"])

            if not self.config_influx["influxdb_database"] in self.influx.get_list_database():
                self.logger.info(f"Creating InfluxDB database ({self.config_influx['influxdb_database']})")
                self.influx.create_database(self.config_influx["influxdb_database"])

            self.logger.info(f"Switching to InfluxDB database ({self.config_influx['influxdb_database']})")
            self.influx.switch_database(self.config_influx["influxdb_database"])
        except OSError:
            self.logger.error('Connecting to DB failed')
            self.logger.info('Writing DCS data to local file')
            self.configuration['general']['save_as_h5'] = True

    def init_env_data_file(self):
        self.t_file = os.path.join(self.working_dir, f'{time.strftime("%Y-%m-%d")}-environment_monitoring.h5')
        with tb.open_file(self.t_file, 'a') as infile:
            try:
                base_description = dict({'timestamp': tb.Float64Col(pos=0), 'NTC_temp': tb.Float64Col(pos=1), 'T_1': tb.Float64Col(pos=2),
                                         'T_2': tb.Float64Col(pos=3), 'Hum_1': tb.Float64Col(pos=4), 'Hum_2': tb.Float64Col(pos=5),
                                         'dew_1': tb.Float64Col(pos=6), 'dew_2': tb.Float64Col(pos=7), 'dew_NTC': tb.Float64Col(pos=8),
                                         'I_leak': tb.Float64Col(pos=9), 'Vin': tb.Float64Col(pos=10), 'Iin': tb.Float64Col(pos=11),
                                         'P_vacuum': tb.Float64Col(pos=12)})
                root = infile.root
                infile.create_table(where=root, name='environment_monitoring', description=base_description)
            except tb.NodeError:
                pass

    def init(self):
        self.logger.info('initializing...')

        # init of all devices
        self.logger.info('initializing devices...')

        if self.configuration['general']['devices_yaml'] is not None:
            self.devices_yaml = self.configuration['general']['devices_yaml']
        else:
            self.devices_yaml = os.path.join(os.path.dirname(__file__), 'devices.yaml')

        self.periphery = BDAQ53Periphery(bench_config=self.testbench_config, name='DCS')
        self.periphery.init()

        self.logger.info('devices initialized.')
        time.sleep(0.1)
        self.logger.info('Resetting Interlock...')
        self.reset_interlock(restart=False)
        time.sleep(0.1)

        # Assign measuring devices
        self.sensor1 = self.periphery.aux_devices['Thermohygrometer1']
        self.sensor2 = self.periphery.aux_devices['Thermohygrometer2']  # was Thermohygrometer3

        if self.configuration["general"]["use_chiller"]:
            self.chiller = self.periphery.aux_devices['Chiller']

        # if hasattr(self.dut, 'hot_n2'):
        #     self.valve_n2 = self.dut['hot_n2']
        #     self.valve_n2.set_setpoint(int(13000))
        # else:
        #     self.valve_n2 = None
        if self.target_temperature is not None:
            self.target_temp_changed.set()

        self.logger.info("All devices are initialized")

        # exit()

    def recv_cmd(self):
        rep = self.context.socket(zmq.REP)
        rep.bind(f"tcp://*:{self.cmd_port}")
        self.logger.info(f'Recv commands on port {self.cmd_port}')

        while not self.stop_recv_cmd.wait(1e-1):
            try:
                cmd_dict = rep.recv_json(flags=zmq.NOBLOCK)
            except zmq.Again:
                continue

            if cmd_dict['cmd'] == 'SHUTDOWN_DCS':
                self.shutdown()
                result_dict = {'cmd': cmd_dict['cmd'], 'result': 'Stopflags are set.'}
            else:
                error = self.handle_cmd(cmd_dict=cmd_dict)
                result_dict = {'cmd': cmd_dict['cmd'], 'error': error}
            rep.send_json(result_dict)

    def handle_cmd(self, cmd_dict):
        error = False
        # Handle stuff and check for error
        if cmd_dict['cmd'] == 'COOLING OFF':
            thread_warming_up = Thread(target=self.warm_up_box)
            thread_warming_up.start()

            if self.configuration["general"]["use_chiller"]:
                error = self.stop_chiller.is_set()
        elif cmd_dict['cmd'] == 'COOLING ON':
            if self.stop_chiller.is_set():
                self.warming_up.clear()
                self.thread_cooling.join()

                if self.configuration["general"]["use_chiller"]:
                    self.stop_chiller.clear()

                if self.configuration['general'].get('use_chuck_for_pid', False):
                    self.PID_temp_from_chuck.set()
                else:
                    self.PID_temp_from_chuck.clear()

                if self.set_temperature is not None:
                    self.set_temp_changed.set()
                else:
                    self.target_temp_changed.set()
                self.thread_cooling = Thread(target=self.run_pid)
                self.thread_cooling.start()

        elif cmd_dict['cmd'] == 'TEMP SET TARGET':
            try:
                self.target_temperature = float(cmd_dict['value'])
                self.target_temp_changed.set()
            except TypeError:
                self.logger.error(f'Could not change target temperature.\n Target Temperature: {self.target_temperature}')

        elif cmd_dict['cmd'] == 'LV SET CURRENT':
            if cmd_dict.get('value') is not None and cmd_dict.get('value') != '':
                self.logger.info(f'LV Current changed to {cmd_dict.get("value")}A')
                for module in self.modules.keys():
                    self.periphery.module_devices[module]['LV'].set_current_limit(cmd_dict['value'], interlock=self.interlock.is_set())

        elif cmd_dict['cmd'] == 'LV SET VOLTAGE':
            if cmd_dict.get('value') is not None and cmd_dict.get('value') != '':
                self.logger.info(f'LV Voltage changed to {cmd_dict.get("value")}V')
                for module in self.modules.keys():
                    self.periphery.module_devices[module]['LV'].set_voltage(cmd_dict['value'], interlock=self.interlock.is_set())

        elif cmd_dict['cmd'] == 'HV SET VOLTAGE':
            if cmd_dict.get('value') is not None and cmd_dict.get('value') != '':
                self.logger.info(f'HV Voltage set to {cmd_dict.get("value")}V')
                for module in self.modules.keys():
                    self.periphery.module_devices[module]['HV'].ramp_voltage(cmd_dict['value'], interlock=False)

        elif cmd_dict['cmd'] == 'RESET':
            self.logger.info('RESETTING INTERLOCK')
            error = self.reset_interlock()

        elif cmd_dict['cmd'] == 'SWITCH MODULES':
            new_module = cmd_dict.get('value', self.module)
            if new_module == self.module:
                self.logger.info('Module %s already selected!' % new_module)
            else:
                self.logger.info('Switch from module %s to module %s' % (self.module, new_module))
                self.switch_modules(new_module)

            error = new_module != self.module

        elif cmd_dict['cmd'] == 'POWER':
            if cmd_dict['value'] == 'ON':
                self.logger.info(f'Powering module {self.module}...')
                self.start_devices()
            else:
                self.logger.info(f'Powering off module {self.module}...')
                self.stop_devices(flag=False)
        else:
            self.logger.error(f'Command not implemented: {cmd_dict}')
            error = True

        return error

    def reset_interlock(self, restart=True):
        self.interlock.clear()
        self.interlock_temp_soft.clear()
        self.interlock_temp_hard.clear()
        self.interlock_humid_soft.clear()
        self.interlock_humid_hard.clear()
        self.interlock_pressure.clear()
        time.sleep(1)
        self.periphery.reset_interlock()
        # should this also reset set temperature and voltage?
        if restart:
            if 'powersupply' in self.modules[self.module]:
                self.start_devices()
            if self.set_temperature is not None:
                if self.set_temperature != self.target_temperature:
                    self.target_temp_changed.set()
            else:
                self.target_temp_changed.set()

        return self.interlock.is_set()

    def send_data(self, socket, data, name="CoolingData"):
        data_meta_data = dict(
            name=name,
            dtype=str(data.dtype),
            shape=data.shape,
            module=self.module,
            module_list=list(self.modules.keys()),
            set_values={
                'HV_voltage': "--" if self.set_HV_voltage is None else self.set_HV_voltage,
                'LV_voltage': "--" if self.set_LV_voltage is None else self.set_LV_voltage,
                'LV_current': "--" if self.set_LV_current is None else self.set_LV_current,
                'Temp': "--" if self.set_temperature is None else self.set_temperature
            },
            interlock={
                'interlock': self.interlock.is_set(),
                'humidity_soft': self.interlock_humid_soft.is_set(),
                'humidity_hard': self.interlock_humid_hard.is_set(),
                'temp_soft': self.interlock_temp_soft.is_set(),
                'temp_hard': self.interlock_temp_hard.is_set(),
                'pressure': self.interlock_pressure.is_set()
            },
            timestamp=time.time()
        )
        try:
            data_ser = utils.simple_enc(data, meta=data_meta_data)
            socket.send(data_ser, flags=zmq.NOBLOCK)
        except zmq.Again:
            pass

    def read_DCS(self):
        self.logger.info("Start DCS Readout")
        socket = self.context.socket(zmq.PUB)
        socket.bind(f'tcp://127.0.0.1:{self.data_port}')
        time.sleep(2)  # wait for Arduino, TODO: could be optimised
        if self.t_file:
            infile = tb.open_file(self.t_file, 'a')

        start_time = time.time()
        while not self.stop_dcs.is_set():
            try:
                self._read_DCS()
                if self.Vin is not None:
                    if self.write_DCS.is_set():
                        # print(self.NTC, self.T_chuck, self.T_box, self.hum_chuck, self.hum_box, self.Vin, self.I_in)
                        self._write_DCS(table=infile.root.environment_monitoring if self.t_file else None)
                        self.write_DCS.clear()
                read_time = time.time() - start_time
                self.send_data(socket=socket, data=np.array([self.T_chuck, self.T_box, self.hum_chuck, self.hum_box, self.dew_chuck, self.dew_box, self.NTC, self.HV_voltage, self.I_leak, self.Vin, self.I_in, self.P_vacuum, read_time], dtype=np.float64))

                # slack message handling
                while not self.message_queue.empty():
                    message = self.message_queue.get_nowait()

                    if message is not None:

                        self.send_slack_message(f'Date:{time.strftime("%Y-%m-%d")}' + "\n" + f'Time:{time.strftime("%H-%M-%S")}' + "\n" + f"{message}")

                start_time = time.time()
            except Exception:
                traceback.print_exc()
                continue

        if self.t_file:
            infile.close()

    def _read_DCS(self):
        # TODO get vacuum pressure
        self.timestamp = datetime.now()
        # self.Vin = 0 # float(self.keith2.get_voltage()) - self.I_in * 0.084 * (1 + 3.9e-3 * (self.NTC - 25))

        thread_read_arduino = Thread(target=self.read_arduino)
        thread_read_arduino.start()
        thread_read_sensirion = Thread(target=self.read_sensirion)
        thread_read_sensirion.start()
        thread_read_powersupply = Thread(target=self.read_powersupply)
        thread_read_powersupply.start()

        thread_read_arduino.join()

        thread_read_sensirion.join()

        thread_read_powersupply.join()

        if self.NTC is not None and self.hum_chuck is not None:
            self.dew_ntc = calc_dew_point(T=self.NTC, RH=self.hum_chuck)
        else:
            self.dew_ntc = None

    def measure_NTC(self):
        sensor = self.modules[self.module].get('slot', 1) - 1
        temp_dict = self.periphery.aux_devices['Arduino'].get_temperature(sensor=sensor, interlock=self.interlock.is_set())
        return temp_dict.get(str(sensor))

    def measure_LV(self):
        self.I_in = float(self.periphery.module_devices[self.module]['LV'].get_current(interlock=self.interlock.is_set()))
        Vin = float(self.periphery.module_devices[self.module]['LV'].get_voltage(interlock=self.interlock.is_set()))
        # if self.NTC is not None:
        #    self.Vin = Vin - self.I_in * 0.275 * (1 + 3.9e-3 * (self.NTC - 25))
        # else:
        self.Vin = Vin

    def measure_HV(self):
        if self.modules[self.module].get('HV', False):
            if self.periphery.module_devices[self.module]['HV'].output:
                self.I_leak = self.periphery.module_devices[self.module]['HV'].get_current(interlock=self.interlock.is_set())
                self.I_leak = float(self.I_leak) if self.I_leak else None
                self.HV_voltage = self.periphery.module_devices[self.module]['HV'].get_voltage(
                    interlock=self.interlock.is_set())
                self.HV_voltage = float(self.HV_voltage) if self.HV_voltage else None
            else:
                self.I_leak = None
                self.HV_voltage = None

    def get_set_values(self):  # FIX ME: Implement this! Needs to implement function in periphery!
        if self.modules[self.module].get('LV', False):
            self.set_LV_voltage = self.periphery.module_devices[self.module]['LV'].get_set_voltage(interlock=self.interlock.is_set())
            self.set_LV_current = self.periphery.module_devices[self.module]['LV'].get_current_limit(interlock=self.interlock.is_set())
        if self.modules[self.module].get('HV', False):
            self.set_HV_voltage = self.periphery.module_devices[self.module]['HV'].get_source_voltage(interlock=self.interlock.is_set())

    def read_arduino(self):
        ntc = self.measure_NTC()
        self.NTC = ntc if not math.isnan(ntc) else None
        self.P_vacuum = float(self.periphery.aux_devices['Arduino'].get_pressure(interlock=self.interlock.is_set()))

    def read_sensirion(self):
        self.T_chuck = self.sensor1.get_temperature(interlock=self.interlock.is_set())
        if self.T_chuck is not None:
            self.T_chuck = float(self.T_chuck)
        self.T_box = self.sensor2.get_temperature(interlock=self.interlock.is_set())
        if self.T_box is not None:
            self.T_box = float(self.T_box)
        self.hum_chuck = self.sensor1.get_humidity(interlock=self.interlock.is_set())
        if self.hum_chuck is not None:
            self.hum_chuck = float(self.hum_chuck)
        self.hum_box = self.sensor2.get_humidity(interlock=self.interlock.is_set())
        if self.hum_box is not None:
            self.hum_box = float(self.hum_box)
        if self.T_chuck is not None and self.hum_chuck is not None:
            self.dew_chuck = calc_dew_point(T=self.T_chuck, RH=self.hum_chuck)
        if self.T_box is not None and self.hum_box is not None:
            self.dew_box = calc_dew_point(T=self.T_box, RH=self.hum_box)

    def read_powersupply(self):
        if self.modules[self.module].get('LV', False):
            self.measure_LV()
            self.measure_HV()
        self.get_set_values()

    def _get_module_sn_from_slot(self, slot):
        return self.modules[self.qms_dict[f"module_{slot}"]]["identifier"] if (self.qms_dict[f"module_{slot}"] is not None) and (self.qms_dict[f"module_{slot}"] != 'None') else None

    def _write_DCS(self, table):
        if table is not None:  # deprecated ?
            row = table.row
            row['NTC_temp'] = self.NTC_last = self.NTC
            row['timestamp'] = time.time()  # FIX ME: Find formatting for datetime in tables
            row['T_1'] = self.T_chuck
            row['T_2'] = self.T_box
            row['Hum_1'] = self.hum_chuck_last = self.hum_chuck
            row['Hum_2'] = self.hum_box
            row['dew_1'] = self.dew_chuck_last = self.dew_chuck
            row['dew_2'] = self.dew_box
            row['dew_NTC'] = self.dew_ntc
            row['I_leak'] = self.I_leak
            row["Vin"] = self.Vin_last = self.Vin
            row["Iin"] = self.I_in
            row["P_vacuum"] = self.P_vacuum
            row.append()
            table.flush()
        else:
            try:
                powered_module_sn = self.modules[self.module]['identifier']

                measurements = [{
                    "measurement": self.config_influx["influxdb_database"],
                    "time": datetime.utcnow(),
                    "tags": {
                        "host"          : socket.gethostname(),
                        "box"           : 1,
                        "module_1"      : self._get_module_sn_from_slot(1),
                        "module_2"      : self._get_module_sn_from_slot(2),
                        "module_3"      : self._get_module_sn_from_slot(3),
                        "module_4"      : self._get_module_sn_from_slot(4),
                        "module_powered": powered_module_sn,
                    },
                    "fields": {
                        "Temp_chuck": float(self.T_chuck) if self.T_chuck is not None else None,
                        "Temp_box"  : float(self.T_box) if self.T_box is not None else None,
                        "Hum_chuck" : float(self.hum_chuck) if self.hum_chuck is not None else None,
                        "Hum_box"   : float(self.hum_box) if self.hum_box is not None else None,
                        "Dew_chuck" : float(self.dew_chuck) if self.dew_chuck is not None else None,
                        "Dew_box"   : float(self.dew_box) if self.dew_box is not None else None,
                        "P_vacuum"  : float(self.P_vacuum) if self.P_vacuum is not None else None,
                        "NTC_temp"  : float(self.NTC) if self.NTC is not None else None,
                        "NTC_dew"   : float(self.dew_ntc) if self.dew_ntc is not None else None,
                        "Iin"       : float(self.I_in) if self.I_in is not None else None,
                        "I_leak"    : float(self.I_leak) if self.I_leak is not None else None,
                        "Vin"       : float(self.Vin) if self.Vin is not None else None,
                        "Interlock_Temp": 'hard' if self.interlock_temp_hard.is_set() else 'soft' if self.interlock_temp_soft.is_set() else None,
                        "Interlock_Humid": 'hard' if self.interlock_humid_hard.is_set() else 'soft' if self.interlock_humid_soft.is_set() else None,
                        "Interlock_Pressure": 'soft' if self.interlock_pressure.is_set() else None
                    }
                }]

                self.influx.write_points(measurements)

                self.hum_chuck_last = self.hum_chuck
                self.dew_chuck_last = self.dew_chuck
                self.NTC_last = self.NTC
                self.Vin_last = self.Vin
            except OSError:
                self.logger.error('Connection to DB lost')
                self.logger.info('Switching to local backup-file')
                self.init_env_data_file()

    def start_devices(self):
        self.devices_are_stopped.clear()
        self.modules[self.module]['LV'] = True
        self.modules[self.module]['HV'] = True
        if self.modules[self.module].get('power_cycle', False):
            self.periphery.power_off_module(self.module, interlock=self.interlock.is_set())
            time.sleep(1)
        self.modules[self.module]['LV'] = self.periphery.power_on_LV(self.module, **self.modules[self.module]['powersupply'], interlock=self.interlock.is_set())
        self.modules[self.module]['HV'] = self.periphery.power_on_HV(self.module, **self.modules[self.module]['powersupply'], interlock=self.interlock.is_set())
        self.logger.info('DEVICES STARTED')

    def stop_devices(self, flag=True):
        # This loops over all modules to make sure all are turned off.
        time.sleep(0.1)
        for module in self.modules.keys():
            if flag:
                self.modules[module]['HV'] = self.periphery.power_off_HV(module, interlock=self.interlock.is_set())
                time.sleep(0.1)
                self.modules[module]['LV'] = self.periphery.power_off_LV(module, interlock=self.interlock.is_set())
            else:
                self.periphery.power_off_HV(module, interlock=self.interlock.is_set())
                time.sleep(0.1)
                self.periphery.power_off_LV(module, interlock=self.interlock.is_set())
        self.devices_are_stopped.set()

    def switch_modules(self, module):
        self.stop_devices()
        time.sleep(1)

        self.module = module
        self.start_devices()
        if self.configuration['general'].get('use_chuck_for_pid', False):
            self.PID_temp_from_chuck.set()
        else:
            self.PID_temp_from_chuck.clear()

    def run_pid(self):
        if not self.configuration["general"]["use_chiller"]:
            return

        self.logger.info(self.chiller.get_status())
        self.chiller.start_chiller()
        self.logger.info(self.chiller.get_status())

        integral_active = Event()
        level_reached = Event()

        temp_avg = np.full(5, np.nan)
        trys = 0

        while not self.stop_chiller.is_set() or self.warming_up.is_set():
            if self.target_temp_changed.is_set():
                self.set_temperature = self.target_temperature
                self.target_temp_changed.clear()
                self.set_temp_changed.set()
                self.logger.info(f'New target temperature: {self.target_temperature}°C')
            if self.set_temp_changed.is_set():
                if self.warming_up.is_set():
                    self.logger.info('Warming up...')
                    self.set_temperature = 20
                pid = PID(Kp=25, Ki=0, Kd=50, output_limits=(-100, 100), setpoint=self.set_temperature)
                self.set_temp_changed.clear()
                level_reached.clear()
                integral_active.clear()
            if level_reached.is_set() and not integral_active.is_set():
                pid = PID(Kp=25, Ki=0.01, Kd=50, output_limits=(-100, 100), setpoint=self.set_temperature)
                integral_active.set()

            if (self.NTC is not None and not self.PID_temp_from_chuck.is_set()) or (self.T_chuck is not None and self.PID_temp_from_chuck.is_set()):
                trys = 0
                temp_avg = np.roll(temp_avg, -1)
                if self.PID_temp_from_chuck.is_set():
                    temp_avg[-1] = self.T_chuck
                else:
                    temp_avg[-1] = self.NTC

                temp = temp_avg[~np.isnan(temp_avg)].mean()
                pid_value = pid(temp)
                # print(f'Control Temperature: {temp}, PID: {pid_value}, Target: {self.set_temperature}')
                self.chiller.set_power(pid_value)

                if not integral_active.is_set():
                    if abs(self.set_temperature - temp) < 1.5:
                        level_reached.set()

            else:
                self.logger.error('Control temperature not available!')
                if trys > 5:
                    if not self.PID_temp_from_chuck.is_set():
                        self.PID_temp_from_chuck.set()
                    else:
                        self.PID_temp_from_chuck.clear()

                trys += 1
                time.sleep(1)

        self.chiller.set_power(0)
        self.chiller.stop_chiller()
        self.logger.info('Chiller stopped.')

        self.logger.info(self.chiller.get_status())
        # if self.valve_n2:
        #     self.valve_n2.set_setpoint(int(0))

    def warm_up_box(self):
        if not self.configuration["general"]["use_chiller"]:
            return

        self.logger.info('Warming up to room temperature...')
        self.warming_up.set()
        self.stop_chiller.set()

        room_temperature = False
        while self.warming_up.is_set():
            if self.PID_temp_from_chuck.is_set():
                if self.T_chuck is not None:
                    room_temperature = abs(self.T_chuck - 21) < 2
            else:
                if self.NTC is not None:
                    room_temperature = abs(self.NTC - 21) < 2
            if room_temperature:
                self.warming_up.clear()

        self.logger.info('Box at room temperature.')

#    def pid_n2(self, set_point=10, use_NTC=False, default_value=13000):
#        n_no_data = 0
#        pid_value_n2 = default_value
#
#        while not self.stop_n2.is_set():
#            temp = self.NTC if use_NTC else self.T_chuck
#            dew = self.dew_ntc if use_NTC else self.dew_chuck
#
#            if temp is not None and dew is not None:
#                n_no_data = 0
#                dew_set = temp - 10.0
#                pid_n2 = PID(Kp=-130, Ki=-5, Kd=0, output_limits=(11500, 15000), setpoint=dew_set)
#
#                pid_value_n2 = pid_n2(temp)
#            else:
#                n_no_data += 1
#
#                if n_no_data > 10:
#                    pid_value_n2 = default_value
#                    self.logger.error('No N2 PID available. Flow set to default.')
#
#            # flow = self.valve_n2.get_flow()
#
#            # print(self.NTC, self.hum_chuck, self.dew_chuck, pid_value_n2)
#
#            # self.valve_n2.set_setpoint(int(pid_value_n2))  # setpoint func takes only int values as input!
#
#            time.sleep(1)  # give the valve time to set

    def check_interlock(self):
        self.logger.info("Start Interlock")
        while not self.stop_dcs.is_set():
            thread_check_pressure = Thread(target=self._check_pressure)
            thread_check_pressure.start()
            thread_check_pressure.join()

            # if self.modules[self.module].get('LV', False):
            #    thread_check_LV = Thread(target=self._check_LV_HV, kwargs={'module': self.module})
            #    thread_check_LV.start()
            #    thread_check_LV.join()

            thread_check_temp = Thread(target=self._check_temperature)
            thread_check_temp.start()
            thread_check_temp.join()

            thread_check_humid = Thread(target=self._check_humidity)
            thread_check_humid.start()
            thread_check_humid.join()

            # if not self.interlock.is_set():
            #    self._check_power_of_HV()
            if self.interlock.is_set() and not self.devices_are_stopped.is_set():
                self.stop_devices(flag=False)
                trys = 0
                while not self.devices_are_stopped.is_set() and trys < 10:
                    time.sleep(1)
                    trys += 1

    # def _check_power_of_HV(self):
    #    if (self.HV is not None) and (self.LV is not None):
    #        try:
    #            if (int(self.keith.get_on()) == 1) & (int((self.hmp.get_status()[:-1])) != 1):
    #                self.interlock.set()
    #                self.message = "HV is powered and LV not"
    #                if not self.message_was_send.is_set():
    #                    self.logger.error(self.message)
    #        except (VisaIOError, SerialException) as e:
    #            self.logger.info(f"{e}")

    def _check_LV_HV(self, module):
        try:
            if self.modules[module].get('LV', False):
                if self.modules[module].get('HV', False) and self.I_leak:
                    if not self.interlock.is_set():
                        if self.I_leak <= -0.95 * float(self.HV_current_limit):
                            self.interlock.set()
                            message = "*95% of HV current limit of " + "{:.4e}".format(self.I_leak) + " A  is reached*"
                            self.message_queue.put(message)
                            self.logger.error(message)
        except (VisaIOError, SerialException) as e:
            self.logger.info(f"{e}")

    def _check_humidity(self):
        if (self.dew_chuck is not None) & (self.T_chuck is not None):
            # Check dew point
            if self.T_chuck <= self.dew_chuck + self.dew_point_safety_margin_soft:
                if not self.interlock_humid_soft.is_set():
                    self.interlock_humid_soft.set()
                    self.write_DCS.set()

                    if self.set_temperature is not None:
                        if self.NTC is not None:
                            current_temp = self.NTC
                        else:
                            current_temp = self.T_chuck

                        if current_temp > self.set_temperature:
                            self.set_temperature = current_temp + 2
                        else:
                            self.set_temperature += 2
                        self.set_temp_changed.set()
                    else:
                        if self.NTC is not None:
                            self.set_temperature = self.NTC + 2
                        else:
                            self.set_temperature = self.T_chuck + 2
                            self.PID_temp_from_chuck.set()
                    self.set_temp_changed.set()

                    message = "*Dewpoint is to high*" + "\n" + "T = {:.2f} ".format(self.T_chuck) + "C" + " and Dp={:.2f}".format(self.dew_chuck) + "C\n" + "*Target temperature raised to {:.1f}*".format(self.set_temperature)
                    self.message_queue.put(message)
                    self.logger.error(message)

                if self.T_chuck <= self.dew_chuck + self.dew_point_safety_margin_hard:
                    if not self.interlock_humid_hard.is_set():
                        self.interlock_humid_hard.set()
                        self.interlock.set()
                        self.write_DCS.set()
                        self.set_temperature = 20
                        self.set_temp_changed.set()

                        message = "*Dewpoint is to high*" + "\n" + "T = {:.2f} ".format(self.T_chuck) + "C" + " and Dp={:.2f}".format(self.dew_chuck) + "C\n" + "*INTERLOCK RAISED*"
                        self.message_queue.put(message)
                        self.logger.error(message)

    def _check_temperature(self):
        if self.NTC is not None:
            try:
                # Check for high temperature
                if self.NTC >= self.maximum_module_temperature_soft:
                    if not self.interlock_temp_soft.is_set():
                        self.interlock_temp_soft.set()
                        self.write_DCS.set()

                        message = "*Module temperature is higher than " + "{:.1f}".format(self.maximum_module_temperature_soft) + "C*\n" + "T = {:.2f}".format(self.NTC) + "C"
                        self.message_queue.put(message)
                        self.logger.error(message)

                    if self.NTC >= self.maximum_module_temperature_hard:
                        if not self.interlock_temp_hard.is_set():
                            self.interlock_temp_hard.set()
                            self.interlock.set()
                            self.write_DCS.set()

                            message = "*Module temperature is higher than " + "{:.1f}".format(self.maximum_module_temperature_hard) + "C*\n" + "T = {:.2f}".format(self.NTC) + "C\n" + "*INTERLOCK RAISED*"
                            self.message_queue.put(message)
                            self.logger.error(message)
            except Exception:
                traceback.print_exc()

    def _check_pressure(self):
        if self.P_vacuum is not None:
            if self.P_vacuum < self.minimum_vacuum_pressure:
                if not self.interlock_pressure.is_set():
                    self.interlock_pressure.set()
                    self.write_DCS.set()

                    message = "*Vacuum Pressure is too low!*" + "\n" + r"P = {:.1f} kPa".format(-self.P_vacuum)
                    self.message_queue.put(message)
                    self.logger.error(message)

    def send_slack_message(self, message):
        payload = '{"text":"%s"}' % message
        response = requests.post("https://hooks.slack.com/services/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", data=payload)
        self.logger.info(response.text)

    def DCS_write_timer(self):
        while not self.stop_dcs.is_set():
            self.write_DCS.set()
            continue
            for i in range(60):
                self._check_smoothing()
                time.sleep(1)
                if self.write_DCS.is_set():
                    break
            self.write_DCS.set()

    def _check_smoothing(self):
        if self.Vin_last is not None:
            if self.Vin_last == 0.:
                if self.Vin != 0.:
                    self.write_DCS.set()
                    return 0
            elif abs(self.Vin - self.Vin_last) / self.Vin_last > 0.05:
                self.write_DCS.set()
                return 0
        else:
            self.write_DCS.set()
            return 0
        if self.NTC_last is not None:
            if abs(self.NTC - self.NTC_last) > 0.2:
                self.write_DCS.set()
                return 0
        else:
            self.write_DCS.set()
            return 0
        if self.hum_chuck_last is not None:
            if abs(self.hum_chuck - self.hum_chuck_last) > 0.5:
                self.write_DCS.set()
                return 0
        else:
            self.write_DCS.set()
        if self.dew_chuck_last is not None:
            if abs(self.dew_chuck - self.dew_chuck_last) > 0.2:
                self.write_DCS.set()
                return 0
        else:
            self.write_DCS.set()
            return 0

    def run(self):
        self.thread_dcs = Thread(target=self.read_DCS)
        self.thread_dcs.start()
        self.logger.info('STARTED DCS')

        self.thread_interlock = Thread(target=self.check_interlock)
        self.thread_interlock.start()
        self.logger.info('STARTED INTERLOCK')

        self.thread_cooling = Thread(target=self.run_pid)
        self.thread_cooling.start()

        # thread_n2 = Thread(target=self.pid_n2)
        # thread_n2.start()
        # self.logger.info('STARTED N2 CONTROL')

        thread_DCS_write_timer = Thread(target=self.DCS_write_timer)
        thread_DCS_write_timer.start()

        # start listening to cmds
        cmd_recv_thread = Thread(target=self.recv_cmd)
        cmd_recv_thread.start()

        self.start_devices()

    def shutdown(self):
        self.stop_recv_cmd.set()
        time.sleep(1)  # prevent outside commands during shutdown

        thread_warm_up = Thread(target=self.warm_up_box)
        thread_warm_up.start()

        self.stop_devices()

        self.thread_cooling.join()  # Only stop monitoring after cooling is stopped

        # self.stop_n2.set()
        self.stop_dcs.set()


def start():
    Monitor = Env_Monitoring()
    Monitor.init()
    Monitor.logger.info('INIT')
    try:
        Monitor.run()
    except KeyboardInterrupt:
        try:
            Monitor.stop_devices()
            Monitor.shutdown()
        except KeyboardInterrupt:
            Monitor.warming_up.clear()


def stop():
    configuration = _load_testbench_cfg(os.path.join(os.path.dirname(__file__), 'interlock.yaml'))
    if configuration.get('ports') is not None:
        cmd_port = configuration['ports'].get('cmd_port', DEFAULT_CMD_PORT)
    else:
        cmd_port = DEFAULT_CMD_PORT

    ctx = zmq.Context()
    req = ctx.socket(zmq.REQ)
    req.connect(f'tcp://localhost:{cmd_port}')
    print(f'Sending SHUTDOWN to tcp://localhost:{cmd_port}')
    cmd_to_send = {'cmd': 'SHUTDOWN_DCS'}
    req.send_json(cmd_to_send)
    print('Waiting for DCS to shut down...')
    print(req.recv_json())
    print('DCS has stopped.')


if __name__ == '__main__':
    start()
