import numpy as np

from online_monitor.converter.transceiver import Transceiver
from online_monitor.utils import utils

from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import rd53a_analysis as ana
from bdaq53.analysis import rd53b_analysis as anb


def stretch_ptot_data(map_data):
    ptot_stretched = [('scan_param_id', '<i8')]
    for i in range(1, 5):
        ptot_stretched.append(('hit_or_%i_col' % i, np.uint16))
        ptot_stretched.append(('hit_or_%i_row' % i, np.uint16))
    out_map = np.zeros((np.sum(map_data['cmd_length'])), dtype=np.dtype(ptot_stretched))

    out_map['scan_param_id'] = np.repeat(map_data['scan_param_id'], map_data['cmd_length'])
    for i in range(1, 5):
        out_map['hit_or_%i_col' % i] = np.repeat(map_data['hit_or_%i_col' % i], map_data['cmd_length'])
        out_map['hit_or_%i_row' % i] = np.repeat(map_data['hit_or_%i_row' % i], map_data['cmd_length'])

    return out_map


class Bdaq53(Transceiver):

    def setup_transceiver(self):
        ''' Called at the beginning

            We want to be able to change the histogrammmer settings
            thus bidirectional communication needed
        '''
        self.set_bidirectional_communication()

    def setup_interpretation(self):
        ''' Objects defined here are available in interpretation process '''
        utils.setup_logging(self.loglevel)

        self.chunk_size = self.config.get('chunk_size', 1000000)
        self.analyze_tdc = self.config.get('analyze_tdc', False)
        self.rx_id = int(self.config.get('rx', 'rx0')[2])
        # Mask pixels that have a higher occupancy than 3 * the median of all firering pixels
        self.noisy_threshold = self.config.get('noisy_threshold', 3)
        self.chip_type = self.config.get('chip_type', 'rd53a').lower()
        self.hmap_is_compressed = self.config.get('hmap_is_compressed', False)
        self.invert_ptot = self.config.get('invert_ptot', None)
        self.eos_mode = self.config.get('eos_mode', False)
        if self.chip_type == 'rd53a':
            self.rows = 192
        else:
            self.rows = 384
        self.columns = 400

        # Set invert_ptot attribute according to chip type
        if self.invert_ptot is None:
            if self.chip_type.lower() in ('itkpixv1',):
                self.invert_ptot = False
            elif self.chip_type.lower() in ('crocv1', 'itkpixv2'):
                self.invert_ptot = True

        self.mask_noisy_pixel = False

        # Init result hists
        self.reset_hists()

        # Number of readouts to integrate
        self.int_readouts = 0

        # Variables for meta data time calculations
        self.ts_last_readout = 0.  # Time stamp last readout
        self.hits_last_readout = 0.  # Number of hits
        self.events_last_readout = 0.  # Number of events in last chunk
        self.fps = 0.  # Readouts per second
        self.hps = 0.  # Hits per second
        self.eps = 0.  # Events per second
        self.last_event = -1  # Last chunk last event number
        self.trigger_id = -1  # Last chunk trigger id
        self.ext_trg_num = -1  # external trigger number
        self.last_rawdata = None  # Leftover from last chunk

    def deserialize_data(self, data):
        ''' Inverse of Bdaq53 serialization '''
        return utils.simple_dec(data)

    def _add_to_meta_data(self, meta_data):
        ''' Meta data interpratation is deducing timings '''

        ts_now = float(meta_data['timestamp_stop'])

        # Calculate readout per second with smoothing
        if ts_now != self.ts_last_readout:
            recent_fps = 1.0 / (ts_now - self.ts_last_readout)
            self.fps = self.fps * 0.95 + recent_fps * 0.05

            # Calulate hits per second with smoothing
            recent_hps = self.hits_last_readout * recent_fps
            self.hps = self.hps * 0.95 + recent_hps * 0.05

            # Calulate hits per second with smoothing
            recent_eps = self.events_last_readout * recent_fps
            self.eps = self.eps * 0.95 + recent_eps * 0.05

        self.ts_last_readout = ts_now

        # Add info to meta data
        meta_data.update(
            {'fps': self.fps,
             'hps': self.hps,
             'total_hits': self.total_hits,
             'eps': self.eps,
             'total_events': self.total_events})
        return meta_data

    def interpret_data(self, data):
        ''' Called for every chunk received '''
        raw_data, meta_data = data[0][1]
        ptot_table = meta_data.pop('ptot_data')
        meta_data = self._add_to_meta_data(meta_data)

        if np.any(self.last_rawdata):
            raw_data = np.concatenate((self.last_rawdata, raw_data))

        # Data is raw data
        if self.chip_type == 'rd53a':
            (n_hits, last_event_number, chunk_offset, self.trigger_id, self.ext_trg_num) = ana.interpret_data(rawdata=raw_data,
                                                                                                              hits=self.hits,
                                                                                                              hist_occ=self.hist_occ,
                                                                                                              hist_tot=self.hist_tot,
                                                                                                              hist_trigger_id=self.hist_trigger_id,
                                                                                                              hist_rel_bcid=self.hist_rel_bcid,
                                                                                                              hist_ptot=self.hist_ptot,
                                                                                                              hist_ptoa=self.hist_ptoa,
                                                                                                              hist_event_status=self.hist_event_status,
                                                                                                              hist_tdc_status=self.hist_tdc_status,
                                                                                                              hist_tdc_value=self.hist_tdc_value,
                                                                                                              hist_bcid_error=self.hist_bcid_error,
                                                                                                              scan_param_id=0,
                                                                                                              event_number=self.last_event,
                                                                                                              trig_pattern=0b11111111111111111111111111111111,
                                                                                                              align_method=0,
                                                                                                              prev_trig_id=-1,
                                                                                                              prev_trg_number=self.ext_trg_num,
                                                                                                              analyze_tdc=self.analyze_tdc,
                                                                                                              last_chunk=False,
                                                                                                              rx_id=self.rx_id
                                                                                                              )
        elif self.chip_type in ['itkpixv1', 'crocv1', 'itkpixv2']:
            ptot_table_stretched = stretch_ptot_data(ptot_table)
            (n_hits, last_event_number, chunk_offset, self.trigger_id, self.ext_trg_num) = anb.interpret_data(rawdata=raw_data,
                                                                                                              hits=self.hits,
                                                                                                              hist_occ=self.hist_occ,
                                                                                                              hist_tot=self.hist_tot,
                                                                                                              hist_trigger_id=self.hist_trigger_id,
                                                                                                              hist_rel_bcid=self.hist_rel_bcid,
                                                                                                              hist_ptot=self.hist_ptot,
                                                                                                              hist_ptoa=self.hist_ptoa,
                                                                                                              hist_event_status=self.hist_event_status,
                                                                                                              hist_tdc_status=self.hist_tdc_status,
                                                                                                              hist_tdc_value=self.hist_tdc_value,
                                                                                                              hist_bcid_error=self.hist_bcid_error,
                                                                                                              scan_param_id=0,
                                                                                                              event_number=self.last_event,
                                                                                                              trig_pattern=0b11111111111111111111111111111111,
                                                                                                              align_method=2,
                                                                                                              prev_tag=self.trigger_id,
                                                                                                              prev_trg_number=self.ext_trg_num,
                                                                                                              analyze_tdc=self.analyze_tdc,
                                                                                                              last_chunk=False,
                                                                                                              rx_id=self.rx_id,
                                                                                                              ptot_table_stretched=ptot_table_stretched,
                                                                                                              hmap_is_compressed=self.hmap_is_compressed,
                                                                                                              eos_mode=self.eos_mode,
                                                                                                              max_col=self.columns,
                                                                                                              max_row=self.rows,
                                                                                                              invert_ptot=self.invert_ptot)
        else:
            raise NotImplementedError('Specified chip type ({0}) is not supported.'.format(self.chip_type))

        self.last_rawdata = raw_data[chunk_offset:]
        self.hits_last_readout = n_hits

        self.events_last_readout = last_event_number - self.last_event
        self.last_event = last_event_number

        self.total_events += self.events_last_readout
        self.total_hits += n_hits
        self.readout += 1

        occupancy_hist = self.hist_occ[:, :, 0]
        # Mask Noisy pixels
        if self.mask_noisy_pixel:
            sel = occupancy_hist > self.noisy_threshold * np.median(occupancy_hist[occupancy_hist > 0])
            occupancy_hist[sel] = 0

        interpreted_data = {
            'meta_data': meta_data,
            'occupancy': occupancy_hist,
            'tot_hist': self.hist_tot.sum(axis=(0, 1, 2)),
            'rel_bcid_hist': self.hist_rel_bcid.sum(axis=(0, 1, 2)),
            'hist_event_status': self.hist_event_status,
            'hist_tdc_value': self.hist_tdc_value,
        }

        if self.int_readouts != 0:  # = 0 for infinite integration
            if self.readout % self.int_readouts == 0:
                self.reset_hists()

        return [interpreted_data]

    def serialize_data(self, data):
        ''' Serialize data from interpretation '''
        return utils.simple_enc(None, data)

    def handle_command(self, command):
        ''' Received commands from GUI receiver '''
        if command[0] == 'RESET':
            self.reset_hists()
            self.last_event = -1
            self.trigger_id = -1
        elif 'MASK' in command[0]:
            if '0' in command[0]:
                self.mask_noisy_pixel = False
            else:
                self.mask_noisy_pixel = True
        else:
            self.int_readouts = int(command[0])

    def reset_hists(self):
        ''' Reset the histograms '''
        self.total_hits = 0
        self.total_events = 0
        # Readout number
        self.readout = 0

        (self.hits,
         self.hist_occ,
         self.hist_tot,
         self.hist_rel_bcid,
         self.hist_trigger_id,
         self.hist_event_status,
         self.hist_tdc_status,
         self.hist_tdc_value,
         self.hist_bcid_error,
         self.hist_ptot,
         self.hist_ptoa) = au.init_outs(n_hits=self.chunk_size,
                                        n_scan_params=1,
                                        rows=self.rows, cols=self.columns,
                                        analyze_tdc=self.analyze_tdc)
