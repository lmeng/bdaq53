#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import yaml
import logging
import os
import bdaq53

from bdaq53.system.bdaq53_base import BDAQ53
from bdaq53.system import logger
from basil.HL.XPT import ADN_XPT, MuxPca9540B, GpioPca9554
from argparse import ArgumentParser


bdaq_dir = os.path.dirname(bdaq53.__file__)
bdaq_conf = os.path.join(bdaq_dir, os.path.join('system', 'bdaq53.yaml'))

bdaq_default = BDAQ53(bdaq_conf)
conf = {'name': 'Name', 'type': 'Type', 'interface': 'intf', 'base_addr': 0x00}


class QMS_Card(ADN_XPT):
    '''
    System-level class for QMS-card configuration.
    '''
    _config_dict = {
        'module_1': {
            'module_switch_addr': 0x90,
            'routing_file': "routing_configs/qms_mod1.yaml"
        },
        'module_2': {
            'module_switch_addr': 0x92,
            'routing_file': "routing_configs/qms_mod2.yaml"
        },
        'module_3': {
            'module_switch_addr': 0x94,
            'routing_file': "routing_configs/qms_mod3.yaml"
        },
        'module_4': {
            'module_switch_addr': 0x96,
            'routing_file': "routing_configs/qms_mod4.yaml"
        },
        'SCC': {
            'module_switch_addr': 0x90,
            'routing_file': "routing_configs/qms_SCC.yaml"
        },
    }

    _mux_map = {
        'module_1': {
            'chip_1': 0x0,
            'chip_2': 0x1,
            'chip_3': 0x2,
            'chip_4': 0x3
        },
        'module_2': {
            'chip_1': 0x4,
            'chip_2': 0x5,
            'chip_3': 0x6,
            'chip_4': 0x7
        },
        'module_3': {
            'chip_1': 0x8,
            'chip_2': 0x9,
            'chip_3': 0xa,
            'chip_4': 0xb
        },
        'module_4': {
            'chip_1': 0xc,
            'chip_2': 0xd,
            'chip_3': 0xe,
            'chip_4': 0xf
        },
    }

    def __init__(self, intf=bdaq_default['i2c'], conf=conf, module_switch_addr=0x96, qms_root_dir=None):
        self.log = logger.setup_derived_logger('QMS Card')
        super().__init__(intf, conf)
        self.gpio = MuxPca9540B(intf, conf={'name': 'GpioPca9540B', 'type': 'GpioPca9540B',
                                            'interface': 'intf', 'base_addr': 0xe0})

        self.GPIO_PCA9554 = GpioPca9554(intf, conf={'name': 'GpioPca9554', 'type': 'GpioPca9554',
                                                    'interface': 'intf', 'base_addr': 0x00})

        self.ADN4604_DATA = ADN_XPT(intf, conf={'name': 'ADN4604_DATA', 'type': 'ADN4604',
                                                'interface': 'intf', 'base_addr': 0x90})
        self.ADN4604_CMD = ADN_XPT(intf, conf={'name': 'ADN4604_CMD', 'type': 'ADN4604',
                                               'interface': 'intf', 'base_addr': 0x92})

        self.ADN4604_MOD = ADN_XPT(intf, conf={'name': 'ADN4604_MOD', 'type': 'ADN4604',
                                               'interface': 'intf', 'base_addr': module_switch_addr})
        self.qms_root_dir = os.path.dirname(os.path.abspath(__file__))

    def init(self, bdaq=bdaq_default):
        '''
        Register map in yaml.
        Keys: General, XPT_Map0, XPT_Map1, XPT_Status, XPT_Headroom,
        TX_Lane_Control, TX_Sign_Control, TX_Drive_Control,
        TX_Headroom, TX_Termination_Control, RX_EQ_Control,
        RX_Sign_Control, RX_Termination_Control
        '''
        self.bdaq = bdaq
        self.bdaq.init()
        self.log.info('Initialize X-switches')
        self.gpio._set_i2c_mux(0x05)
        self.ADN4604_CMD.init(os.path.join(self.qms_root_dir, 'ADN_4604_RegisterMap.yaml'))
        self.ADN4604_DATA.init(os.path.join(self.qms_root_dir, 'ADN_4604_RegisterMap.yaml'))
        self.gpio._set_i2c_mux(0x04)
        self.ADN4604_MOD.init(os.path.join(self.qms_root_dir, 'ADN_4604_RegisterMap.yaml'))
        self.gpio._set_i2c_mux(0x05)
        self.GPIO_PCA9554.init()

    def config(self, cmd=False, routing_file=None, routing_dict=None):
        '''
        Configure output channels (assignment, driver settings, termination) of XPT
        '''
        # with open(routing_file, 'r') as conf_: #qms routing config
        #     self.qms_routing = yaml.safe_load(conf_)
        if routing_file:
            with open(routing_file, 'r') as conf_:  # qms routing config
                self.qms_routing = yaml.safe_load(conf_)
        elif routing_dict:
            self.qms_routing = routing_dict
        else:
            with open(os.path.join(self.qms_root_dir, 'routing_configs', 'qms_default.yaml', 'r')) as conf_:  # qms routing config
                self.qms_routing = yaml.safe_load(conf_)
        '''
        16x16 XPT for ADN4604
        '''
        self.gpio._set_i2c_mux(0x05)
        self.log.info('Configure X-switches on I2C address %i', self.gpio._get_i2c_mux())
        self.ADN4604_DATA.reset()
        self.ADN4604_DATA.io_config(self.qms_routing)
        self.ADN4604_DATA.set_termination()
        self.ADN4604_DATA.xpt_update()

        if cmd:
            '''
            16x16 XPT for CMD
            '''
            self.ADN4604_CMD.reset()
            self.ADN4604_CMD.io_config(self.qms_routing)
            self.ADN4604_CMD.set_termination()
            self.ADN4604_CMD.xpt_update()

        self.gpio._set_i2c_mux(0x04)
        self.log.info('Configure X-switches on I2C address %i', self.gpio._get_i2c_mux())
        self.ADN4604_MOD.reset()
        self.ADN4604_MOD.io_config(self.qms_routing)
        self.ADN4604_MOD.set_termination()
        self.ADN4604_MOD.xpt_update()

    def status(self):
        '''
        Query status
        '''
        self.gpio._set_i2c_mux(0x05)
        self.log.info('Check status of X-Switches on I2C address %i', self.gpio._get_i2c_mux())
        for x in range(0, 16):
            self.ADN4604_DATA.get_output_status(channel='OUT' + str(x))
            self.ADN4604_CMD.get_output_status(channel='OUT' + str(x))

        self.gpio._set_i2c_mux(0x04)
        self.log.info('Check status of X-Switches on I2C address %i', self.gpio._get_i2c_mux())
        for x in range(0, 16):
            self.ADN4604_MOD.get_output_status(channel='OUT' + str(x))

    def __enter__(self):
        self.init()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def close(self):
        self.bdaq.close()
        logging.info('QMS card configured')

    def select_mux(self, module, chip):
        if module == 'SCC':
            return
        self.gpio._set_i2c_mux(0x05)
        reg_value = self._mux_map[module][chip]
        self.GPIO_PCA9554._write_output_port(reg_value)

    def get_mux_selection(self):
        self.gpio._set_i2c_mux(0x05)
        reg_value = self.GPIO_PCA9554._read_output_port()
        return reg_value

    def switch_module(self, module, intf=bdaq_default['i2c']):
        module_switch_addr = self._config_dict[module]['module_switch_addr']
        routing_file = self._config_dict[module]['routing_file']

        self.ADN4604_MOD = ADN_XPT(intf, conf={'name': 'ADN4604_MOD', 'type': 'ADN4604',
                                               'interface': 'intf', 'base_addr': module_switch_addr})
        self.gpio._set_i2c_mux(0x04)
        self.ADN4604_MOD.init(os.path.join(self.qms_root_dir, 'ADN_4604_RegisterMap.yaml'))

        self.config(routing_file=routing_file, cmd=True)
        self.status()


if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument('-m', '--module', action='store', required=True, help='chosen module slot')
    args = parser.parse_args()

    def QMS(module):
        # routing_dict = {} # Provide example routing dict here
        qms = QMS_Card(module_switch_addr=0x96)
        qms.init()
        qms.switch_module(module)

    QMS(args.module)
