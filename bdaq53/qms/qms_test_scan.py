#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.

    Version adapted to ITk_Pix_V1
'''
import logging
import time

from bdaq53.system.scan_base import ScanBase
from bdaq53.qms.qms_control import QMS_Card

qms_routing = {
    'module_1': 0x90,
    'module_2': 0x92,
    'module_3': 0x94,
    'module_4': 0x96
}


probe_locations = {
    '1': 'Bonn'
}

scan_configuration = {
    'periphery_device': 'Multimeter',
    'DAC_1': 'VINA_HALF',
    'DAC_2': 'VDDA_HALF',
    'DAC_3': 'VCAL_HIGH',
    'measure': 'voltage',
    'type': 'U'  # needed since configure_in is written before configuring the chip
}


class QMS_testing_scan(ScanBase):
    scan_id = 'test_qms'

    def _configure(self, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

    def _scan(self, DAC_1='VINA_HALF', DAC_2='VINA_HALF', DAC_3='VINA_HALF', periphery_device='Multimeter', **_):
        efuse_read = str(hex(self.chip.read_efuses()))
        probe_loc = efuse_read[2]
        chip_sn = '0x' + efuse_read[3:8]
        parity_bit = efuse_read[-2:]

        try:
            probe_loc = probe_locations[probe_loc]
        except KeyError:
            logging.exception('Unknown Probe Location')

        print('------------------------------------------')
        print()

        print('Probe Location: %s' % probe_loc)
        print('Parity bits: %s' % parity_bit)
        print('Chip SN: %s' % chip_sn)

        match = chip_sn == self.chip_settings['chip_sn']
        if not match:
            print('Chip SN from settings: %s' % self.chip_settings['chip_sn'])
        print('Does serial number match: %s' % match)
        print('ATLAS SN: %s' % self._convert_chip_sn(chip_sn))
        print()
        print('------------------------------------------')
        print()
        print('Mux test')
        print(DAC_1 + ': ' + str(self._get_voltage_with_gnd(DAC=DAC_1, periphery_device=periphery_device)))
        print(DAC_2 + ': ' + str(self._get_voltage_with_gnd(DAC=DAC_2, periphery_device=periphery_device)))
        print(DAC_3 + ': ' + str(self._get_voltage_with_gnd(DAC=DAC_3, periphery_device=periphery_device)))
        print()
        print('------------------------------------------')
        print()

        self.log.success('Scan finished')

    def _convert_chip_sn(self, chip_sn):
        ''' Converts chip S/N (0x....) to ATLAS S/N (20PGFC).
        '''
        return '20UPGFC{0:07d}'.format(int(chip_sn, 16))

    def _get_voltage_with_gnd(self, DAC='VCAL_HIGH', measure='voltage', periphery_device='Multimeter'):
        self.chip.set_mux(DAC, measure)
        time.sleep(0.005)
        _voltage = self.periphery.aux_devices[periphery_device].get_voltage()
        self.chip.set_mux('GNDA30', measure)
        time.sleep(0.005)
        _voltage_gnd = self.periphery.aux_devices[periphery_device].get_voltage()

        return _voltage, _voltage_gnd

    def _analyze(self):
        pass


def QMS(module):
    md_nr = module.split('_')[-1]
    routing_file = "routing_configs/qms_mod" + md_nr + ".yaml"
    print(routing_file)
    with QMS_Card(module_switch_addr=qms_routing[module]) as qms:
        qms.init()
        qms.config(routing_file=routing_file, cmd=True)
        qms.status()


if __name__ == "__main__":
    QMS("module_4")
    with QMS_testing_scan(scan_config=scan_configuration) as scan:
        scan.start()
