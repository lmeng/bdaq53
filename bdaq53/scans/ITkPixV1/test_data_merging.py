#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script tests the data merging feature of RD53B chips
'''

import tables as tb
import random
import numpy as np

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis_utils
from bdaq53.analysis import rd53b_analysis


class ValueTable(tb.IsDescription):
    register = tb.StringCol(64, pos=0)
    write = tb.UInt16Col(pos=1)
    read = tb.UInt16Col(pos=2)
    rx = tb.UInt16Col(pos=3)
    clk_pol = tb.UInt16Col(pos=4)
    tries = tb.UInt16Col(pos=5)


class TestPatternTable(tb.IsDescription):
    in_pattern = tb.UInt64Col(pos=0)
    out_pattern = tb.UInt64Col(pos=1)
    rx = tb.UInt16Col(pos=2)
    clk_pol = tb.UInt16Col(pos=3)
    tries = tb.UInt16Col(pos=4)


class DataMergingTest(ScanBase):
    scan_id = 'data_merging_test'

    hit_data = []

    def _configure(self, **_):
        self.chip.write_command(self.chip.CMD_SYNC, repetitions=1000)

    def _scan(self, invert=True):
        value_table = self.h5_file.create_table(self.h5_file.root, name='values', title='Values', description=ValueTable)
        pattern_table = self.h5_file.create_table(self.h5_file.root, name='pattern_table', title='pattern_table', description=TestPatternTable)

        # Configure BDAQ board for data merging
        self.bdaq.dm_invert(invert)  # Invert the DM signal (because of the Mini-DP cable) TODO: Invert in FW?
        self.bdaq.disable_tlu_module()
        timeout = 3
        self.bdaq.dm_output_enable('all')
        self.chip.disable_data_merging()
        chip_in_to_bdaq_out_dict = {0b0001: 1, 0b0010: 2, 0b0100: 3, 0b1000: 0}
        for delay, test_lanes in [(0, 0b0001), (0, 0b0010), (0, 0b0100), (0, 0b1000), (1, 0b0001), (1, 0b0010), (1, 0b0100), (1, 0b1000)]:
            self.bdaq['DATA_MERGING']['DM_ENABLE'] = 0
            self.bdaq['DATA_MERGING'].write()
            self.bdaq.dm_delay(delay)
            self.bdaq.dm_output_enable(chip_in_to_bdaq_out_dict[test_lanes])
            self.chip.registers['MonitorConfig'].write(0)
            # self.chip.write_command(self.chip.CMD_SYNC, repetitions=1000, wait_for_done='true')
            self.bdaq['FIFO'].get_data()
            self.log.info('Establishing Datamerging communication')
            self.bdaq.dm_type('hitdata')
            self.chip.registers['MonitorConfig'].write(0)
            self.bdaq.set_monitor_filter(mode='block')  # Block monitor frames
            try_cnt = 0
            self.chip.write_command(self.chip.CMD_SYNC, repetitions=3000, wait_for_done='true')
            for n in range(10):
                try_cnt += 1
                self.chip.enable_data_merging(data_en=test_lanes)
                self.chip.write_command(self.chip.CMD_SYNC * 100, repetitions=1, wait_for_done='true')
                self.bdaq['FIFO']['RESET'] = 1
                self.bdaq['FIFO'].get_data()
                rx_data = 0
                write_dat = 0xaaaaaaaa

                self.chip.write_command(self.chip.CMD_SYNC * 10, repetitions=1, wait_for_done='true')
                self.bdaq.dm_send_hitdata(data=write_dat)
                self.chip.write_command(self.chip.CMD_SYNC * 10, repetitions=1, wait_for_done='true')
                for _ in range(timeout):
                    self.chip.write_command(self.chip.CMD_SYNC * 10, repetitions=1, wait_for_done='true')
                    if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                        data = self.bdaq['FIFO'].get_data()
                        for elem in data:
                            if not elem & 0x80000000:
                                rx_data = rx_data << 16
                                rx_data += elem
                        if len(data) == 0:
                            continue
                if write_dat == rx_data:
                    self.log.success('Successfully established Datamerging communication')
                    break
                else:
                    self.chip.send_global_pulse(bitnames=['reset_data_path'], pulse_width=5)
                    self.log.warning('Communication not established.')
                    self.chip.disable_data_merging()

            # Test 1: Hit data
            self.log.info('Testing data merging.')
            self.bdaq.dm_type('hitdata')
            self.chip.registers['MonitorConfig'].write(0)
            self.bdaq.set_monitor_filter(mode='block')  # Block monitor frames
            self.bdaq['FIFO'].get_data()
            for x in range(20):
                rx_data = 0
                row = pattern_table.row
                write_dat = random.randint(0, int(2**32 - 1))
                # write_dat = (x << 28) + (x << 24) + (x << 20) + (x << 16) + (x << 12) + (x << 8) + (x << 4) + x
                row['in_pattern'] = write_dat
                row['rx'] = test_lanes
                row['tries'] = try_cnt
                row['clk_pol'] = delay
                self.chip.write_command(self.chip.CMD_SYNC * 10, repetitions=1, wait_for_done='true')
                self.bdaq.dm_send_hitdata(data=write_dat)
                self.chip.write_command(self.chip.CMD_SYNC * 10, repetitions=1, wait_for_done='true')
                for _ in range(timeout):
                    self.chip.write_command(self.chip.CMD_SYNC * 10, repetitions=1, wait_for_done='true')
                    if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                        data = self.bdaq['FIFO'].get_data()
                        for elem in data:
                            if not elem & 0x80010000:
                                rx_data = rx_data << 16
                                rx_data += elem
                        if len(data) == 0:
                            continue
                row['out_pattern'] = rx_data
                row.append()

            # Test 2: Register data
            self.log.info('Starting data merging test. Testing {} registers with random data'.format(10))
            self.chip.registers['MonitorConfig'].write(0)
            self.bdaq.dm_type('monitor')
            self.bdaq.set_monitor_filter(mode='filter')  # Allow monitor frames to pass
            self.chip.write_command(self.chip.CMD_SYNC, repetitions=100, wait_for_done='true')

            for dm_addr in range(20):
                dm_data = random.randint(0, int(2**16 - 1))
                row = value_table.row
                row['register'] = dm_addr
                row['write'] = dm_data
                row['read'] = 0
                row['rx'] = test_lanes
                row['tries'] = try_cnt
                row['clk_pol'] = delay

                for BTF in [0x99]:  # d2:MM, 99:MA, 55:AM, AA:B4
                    self.bdaq['FIFO'].get_data()
                    self.chip.write_command(self.chip.CMD_SYNC * 10, repetitions=1, wait_for_done='true')
                    self.bdaq.dm_send_register(header=BTF, address=dm_addr, data=dm_data)
                    self.chip.write_command(self.chip.CMD_SYNC * 10, repetitions=1, wait_for_done='true')
                    # if self.bdaq.board_version != 'SIMULATION':
                    #     self.chip.write_command(self.chip.CMD_SYNC, repetitions=100, wait_for_done='true')

                    for _ in range(timeout):
                        self.chip.write_command(self.chip.CMD_SYNC * 10, repetitions=1, wait_for_done='true')
                        if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                            data = self.bdaq['FIFO'].get_data()
                            interpreted_userk_data = rd53b_analysis.interpret_userk_data(data)
                            processed_userk_data = analysis_utils.process_userk(interpreted_userk_data)
                            if len(processed_userk_data) == 0:
                                continue
                            for val in processed_userk_data:
                                self.log.debug('Reading data merging data. Address= {}, Data= {})'.format(hex(val['Address']), hex(val['Data'])))
                                row['read'] = val['Data']

                        # if self.bdaq.board_version != 'SIMULATION':
                        #     self.chip.write_command(self.chip.CMD_SYNC, repetitions=10, wait_for_done='true')
                    row.append()
        value_table.flush()
        pattern_table.flush()
        self.bdaq['DATA_MERGING']['DM_ENABLE'] = 0
        self.bdaq['DATA_MERGING'].write()
        self.chip.disable_data_merging()
        self.log.success('Scan finished')

        value_table = value_table[:]
        pattern_table = pattern_table[:]
        pol = np.zeros((2, 4))
        mapping_dict = {1: 0, 2: 1, 4: 2, 8: 3}
        self.data_merging_bool = True
        wrong_values = []
        for res in value_table:
            if res[2] != res[1]:
                pol[res[4], mapping_dict[res[3]]] += 1
                wrong_values.append(('reg', res[0], res[1], res[2], ))
        for res in pattern_table:
            if res[0] != res[1]:
                pol[res[3], mapping_dict[res[2]]] += 1
                wrong_values.append(('dat', res[0], res[1]))
        for elem in pol.T:
            if (elem[0] + elem[1] > 0):
                self.data_merging_bool = False
        print(wrong_values)

    def analyze_data_online(self, data_tuple, receiver=None):
        self.hit_data.extend(data_tuple[0])
        super(DataMergingTest, self).handle_data(data_tuple, receiver)

    def _analyze(self):
        result = True
        self.log.info('Comparing data...')
        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
            data = in_file.root.values[:]
            pattern_table = in_file.root.pattern_table[:]
            if len(data) == 0:
                self.log.error('Data Merging test failed! No register value received.')
        pol = np.ones((2, 4))
        mapping_dict = {1: 0, 2: 1, 4: 2, 8: 3}
        for res in data:
            if res[2] != res[1]:
                # self.log.error('Register read back a wrong value! Expected {}\treceived {}'.format(res[1], res[2]))
                pol[res[4], mapping_dict[res[3]]] = False
        for res in pattern_table:
            if res[0] != res[1]:
                # self.log.error('Hit data read back a wrong value! Expected {}\treceived {}'.format(res[0], res[1]))
                pol[res[3], mapping_dict[res[2]]] = False
        for elem in pol.T:
            if (elem[0] == 0 and elem[1] == 0):
                result = False
        print(pol)
        if result is True:
            self.log.success('Successfully tested {} registers.'.format(len(data)))
        else:
            self.log.info('Test Failed tested {} registers and discovered at least one non working lane.'.format(len(data)))
        return result


if __name__ == '__main__':
    with DataMergingTest() as test:
        test.start()
