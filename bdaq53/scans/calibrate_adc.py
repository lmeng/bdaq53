#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This calibration routine calibrates the VREF ADC to 0.9V and outputs the ideal calibration value for the internal
    RD53A ADC.

    Please make sure you have a Sourcemeter connected to your PC and activated the periphery module in
    testbench.yaml. Connect your Sourcemeter to the VMUX Lemo on the single chip card.

    For optimal results, you should solder a O(1 uF) capacitor in parallel to your VMUX output on the SCC.
'''

import time
import tables as tb
import numpy as np
from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import plotting

scan_configuration = {
    'max_trim_value': 10,
    'steps': 29,
    'samples': 2
}


class DacTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    dac = tb.Float32Col(pos=1)
    adc = tb.Float32Col(pos=2)
    voltage = tb.Float32Col(pos=3)


class ADCCalib(ScanBase):
    scan_id = 'adc_tuning'

    def _configure(self, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

    def find_scan_range(self, start_v=0.025, stop_v=0.04, n_tries=15):
        self.log.info('Finding scan range...')
        self.periphery.aux_devices['Sourcemeter'].on()
        self.chip.get_ADC_value('ground', measure='v')
        for n in range(n_tries):
            voltage_n = start_v + (stop_v - start_v) / 2
            self.periphery.aux_devices['Sourcemeter'].set_voltage(voltage_n)
            time.sleep(0.1)
            adc_value = self.chip.get_ADC_value('ground', measure='v')[0]
            if adc_value == 128:
                self.periphery.aux_devices['Sourcemeter'].off()
                return voltage_n
            elif adc_value > 128:
                stop_v = voltage_n
            elif adc_value < 128:
                start_v = voltage_n

        self.periphery.aux_devices['Sourcemeter'].off()
        raise ValueError('Scan range could not be determined!')

    def scan_one_param(self, scan_points, samples=1, data_name='adc_data'):
        output_array = np.zeros((len(scan_points)), dtype=[('adc', 'f8'), ('voltage', 'f8')])
        adc_data_table = self.h5_file.create_table(self.h5_file.root.adc_data, name=data_name, title=data_name, description=DacTable)
        row = adc_data_table.row

        for scan_param_id, voltage_n in enumerate(scan_points):
            adc_voltages = []
            self.periphery.aux_devices['Sourcemeter'].set_voltage(voltage_n)
            time.sleep(0.1)

            for _ in range(samples):
                adc_voltages.append(self.chip.get_ADC_value('ground', measure='v')[0])
            adc_voltage = np.mean(adc_voltages)

            output_array[scan_param_id]['adc'] = adc_voltage
            output_array[scan_param_id]['voltage'] = voltage_n
            row['dac'] = voltage_n
            row['adc'] = adc_voltage
            row['voltage'] = voltage_n
            row['scan_param_id'] = scan_param_id
            row.append()
            adc_data_table.flush()

            self.pbar.update(1)
        return output_array

    def _scan(self, max_trim_value=10, steps=29, samples=1, **_):
        '''
        DAC linearity scan main loop

        Parameters
        ----------
        max_trim_value : int [1:64]
            Maximum trim value to be scanned. Typically the linearity gets worse vor values larger than 10.
        steps : int
            Measurement resolution. The routine estimates the ideal measurement intervals and the step
            parameter defines how many steps are taken within the interval.
        samples : int
            Samples per measurement point to be taken.
        '''

        self.configuration['scan']['type'] = 'U'
        self.configuration['scan']['value_step'] = 1
        self.h5_file.create_group(self.h5_file.root, 'adc_data')

        # Check if VREF_ADC is trimmed correctly
        vref_adc_target = 0.9
        self.chip.registers['MONITOR_SELECT'].write(int('1' + format(32, '06b') + format(9, '07b'), 2))
        vref_adc = self.periphery.aux_devices['Sourcemeter'].get_voltage() * 2.
        if vref_adc < vref_adc_target * 0.95 or vref_adc > vref_adc_target * 1.05:
            raise ValueError('VREF_ADC is not trimmed correctly. Value is {0:1.3f}V'.format(vref_adc))
        self.log.success('VREF_ADC is trimmed to {0:1.3f}V'.format(vref_adc))
        vref_adc_trim = int(format(self.chip.registers['MONITOR_CONFIG'].read(), '011b')[:5], 2)

        adc_128_v = self.find_scan_range()

        self.log.info('Starting scan...')
        fit_quality = 9999
        self.pbar = tqdm(total=(max_trim_value + 1) * (steps + 1), unit=' Measurement steps')
        for adc_trim in range(max_trim_value, -1, -1):
            self.chip.registers['MONITOR_CONFIG'].write(int(format(vref_adc_trim, '05b') + format(adc_trim, '06b'), 2))
            scan_points = np.arange(adc_128_v - 0.002, adc_128_v + 0.001, 0.003 / steps)
            output_array = self.scan_one_param(scan_points, samples=samples, data_name='adc_data_' + str(adc_trim))
            chi2 = np.polyfit(output_array[:]['voltage'], output_array[:]['adc'], 1, cov=True)[1][1][1]
            if fit_quality > chi2:
                fit_quality = chi2
                best_adc_trim = adc_trim

        self.pbar.close()
        self.periphery.aux_devices['Sourcemeter'].off()
        self.h5_file.copy_node(eval('self.h5_file.root.adc_data.adc_data_{}'.format(best_adc_trim)), newparent=self.h5_file.root, newname='dac_data')

        self.chip.registers['MONITOR_CONFIG'].write(int(format(vref_adc_trim, '05b') + format(best_adc_trim, '06b'), 2))
        self.chip.configuration['trim']['MON_ADC_TRIM'] = best_adc_trim
        self.log.success('Optimal MON_ADC_TRIM is {0}.'.format(best_adc_trim))

    def _analyze(self):
        with plotting.Plotting(analyzed_data_file=self.output_filename + '.h5') as p:
            p.create_standard_plots()


if __name__ == "__main__":
    with ADCCalib(scan_config=scan_configuration) as scan:
        scan.start()
