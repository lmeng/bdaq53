#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
   Creates the ToT calibration per pixel using the charge injection circuit.
'''

from tqdm import tqdm
import numpy as np
import tables as tb

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject, get_scan_loop_mask_steps
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import analysis_utils as au

scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'dual_edge_counting': True,

    'VCAL_MED': 500,
    'VCAL_HIGH_values': range(900, 2001, 100)
}


def get_rms_from_2d_hist(counts, bin_positions, result):
    ''' Compute the standard deviation along the last axis of a 2d histogram
        by repeating elements of (bin_positions) a number of (counts) times.

        Parameters
        ----------
            counts: Array containing data to be averaged
            bin_positions: array_like associated with the values in counts.
            result: array like
                Filled with result
    '''

    for i in tqdm(np.arange(0, counts.shape[0]), unit=' Pixels', unit_scale=True):  # pixel loop
        for j in np.arange(0, counts.shape[1]):  # parameter loop
            if np.any(counts[i][j]):
                result[i][j] = np.std(np.repeat(bin_positions, counts[i][j]))
            else:
                result[i][j] = np.NaN


def hist_2d_mean(counts, bin_positions, axis=0, param_count=0):
    ''' Mean along axis 2d histogram of two data samples.
        Parameters

        ----------
        counts : Array containing data to be histogramed
        bin_positions: array_like associated with the values in counts.
        axis: None or int
        param_count : binning values
    '''

    mean = au.get_mean_from_histogram(counts, bin_positions, axis)
    x_bins = np.arange(param_count)
    x = np.tile(x_bins, mean[:].shape[0])
    y = mean[:].ravel()
    histogram = np.histogram2d(x, y, bins=(param_count, counts.shape[2]), range=((x_bins.min(), len(x_bins)), (0, counts.shape[2])))[0]
    return histogram


class TotCalibration(ScanBase):
    scan_id = 'tot_calibration'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, dual_edge_counting=False, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        if dual_edge_counting:
            self.chip.enable_dual_edge_tot_count()

    def _scan(self, n_injections=100, VCAL_MED=500, VCAL_HIGH_values=range(800, 4001, 200), **_):
        '''
        Threshold scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_values : list
            List of VCAL_HIGH DAC values to scan.
            This API allows for a list of arbitrary values since, e.g. the response of DIFF is not linear with charge.
        '''

        vcal_high_range = VCAL_HIGH_values

        pbar = tqdm(total=get_scan_loop_mask_steps(scan=self) * len(vcal_high_range), unit=' Mask steps')
        for scan_param_id, value in enumerate(vcal_high_range):
            self.chip.setup_analog_injection(vcal_high=value, vcal_med=VCAL_MED)
            self.store_scan_par_values(scan_param_id=scan_param_id, vcal_high=value, vcal_med=VCAL_MED)
            with self.readout(scan_param_id=scan_param_id):
                shift_and_inject(scan=self, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        self.configuration['bench']['analysis']['store_hits'] = True
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            values = np.array(a.get_scan_param_values(scan_parameter='vcal_high'), dtype=float)
            chunk_size = a.chunk_size
            tot_max = 512 if a.analyze_ptot else 16
            n_cols = a.columns
            n_rows = a.rows

        # Create col, row, tot histograms from hits
        with tb.open_file(self.output_filename + '_interpreted.h5', "r+") as io_file:
            shape_3d = (n_cols * n_rows, values.shape[0], tot_max)
            hist_3d = np.zeros(shape=shape_3d, dtype=np.uint8)

            # Loop over all words in the actual raw data file in chunks
            self.log.info('Histogramming hits...')
            for i in tqdm(range(0, io_file.root.Hits.shape[0], chunk_size)):
                hits = io_file.root.Hits[i:i + chunk_size]
                channel = hits["col"] + n_cols * hits["row"]
                scan_param_id = hits["scan_param_id"]
                tot_value = hits['ptot'] if a.analyze_ptot else hits['tot']
                hist_3d += au.hist_3d_index(x=channel, y=scan_param_id, z=tot_value, shape=shape_3d)

            # Create pixel, scan par id, tot hist
            io_file.create_carray(io_file.root,
                                  name='HistTotCal',
                                  title='Tot calibration histogram with channel, scan par id and tot',
                                  obj=hist_3d,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))
            io_file.create_carray(io_file.root,
                                  name='HistTotCalMean',
                                  title='Mean tot calibration histogram',
                                  obj=au.get_mean_from_histogram(hist_3d, bin_positions=np.arange(tot_max), axis=2),
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            result = np.zeros(shape=(n_cols * n_rows, values.shape[0]))
            self.log.info('Calculate RMS per pixel...')
            get_rms_from_2d_hist(hist_3d, np.arange(tot_max), result)
            io_file.create_carray(io_file.root,
                                  name='HistTotCalStd',
                                  title='Std tot calibration histogram',
                                  obj=result,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))
            io_file.create_carray(io_file.root,
                                  name='TOThist',
                                  title='2d histogram for TOT values',
                                  obj=hist_2d_mean(hist_3d, bin_positions=np.arange(tot_max), axis=2, param_count=values.shape[0]),
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


if __name__ == '__main__':
    with TotCalibration(scan_config=scan_configuration) as calibration:
        calibration.start()
