#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This meta script first scans over the global threshold at fixed TDAC of 7 and sets the chip to the mean optimal VTH.
    It then scans over the local threshold (TDAC) with fixed global threshold to find the optimal TDAC value for each pixel.
'''

import zlib  # Workaround

import tables as tb

from bdaq53.meta_scan_base import MetaScanBase
from bdaq53.scans.scan_threshold import ThresholdScan
from bdaq53.scans.tune_global_threshold import ThresholdTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.analysis.plotting import Plotting


local_configuration = {
    # Scan parameters
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': None,

    # Global threshold tuning parameters
    'VCAL_MED': 500,
    'VCAL_HIGH': 1000,

    'VTH_name': 'Vthreshold_LIN',
    'VTH_start': 380,
    'VTH_stop': 280,
    'VTH_step': 2,

    # Final threshold scan parameters
    'VCAL_HIGH_start': 800,
    'VCAL_HIGH_stop': 1200,
    'VCAL_HIGH_step': 5
}


class MetaThrTuning(MetaScanBase):
    scan_id = 'meta_threshold_tuning'

    def start(self, **kwargs):
        self.scan(**kwargs)
        self.clean_up()

    def scan(self, **kwargs):
        self.logger.info('Centering TDAC and starting global threshold tuning...')
        kwargs['TDAC'] = 7

        self.gt = ThresholdTuning(record_chip_status=False)
        self.scans.append(self.gt)
        self.gt.logger.addHandler(self.fh)
        self.gt.start(**kwargs)
        vth_opt = self.gt.analyze(create_pdf=False)
        kwargs[kwargs['VTH_name']] = vth_opt
        self.logger.info('Found optimal %s = %i.' % (kwargs['VTH_name'], vth_opt))
        self.gt.close()

        self.logger.info('Setting global threshold to optimum and starting TDAC tuning...')
        self.lt = TDACTuning(record_chip_status=False)
        self.scans.append(self.lt)
        self.lt.logger.addHandler(self.fh)
        self.lt.start(**kwargs)
        self.lt.analyze(create_pdf=False, create_mask_file=True)
        kwargs['maskfile'] = self.lt.maskfile
        self.logger.info('Found optimal TDAC values for selected pixels.')
        self.lt.close()

        self.logger.info('Applying TDAC mask and running final threshold scan...')
        self.ts = ThresholdScan()
        self.scans.append(self.ts)
        self.ts.logger.addHandler(self.fh)
        self.ts.start(**kwargs)
        self.ts.analyze(create_pdf=False)

        self.logger.info('Tuning finished!')

    def analyze(self, level='preliminary'):
        with tb.open_file(self.output_filename + '_interpreted.h5', 'r') as in_file:
            for item in in_file.get_node(in_file.root, "/"):
                if 'threshold_scan' in item._v_name:
                    threshold_scan = item
                if 'local_threshold_tuning' in item._v_name:
                    local_threshold_tuning = item

            with Plotting(analyzed_data_file=threshold_scan, pdf_file=self.output_filename + '_interpreted.pdf', level=level,
                          internal=False, save_single_pdf=False, save_png=False) as p:
                p.logger.info('Creating selected plots...')
                p.create_parameter_page()
                p.create_event_status_plot()
                p.create_occupancy_map()
                p.create_tot_plot()
                p.create_rel_bcid_plot()

                p._plot_distribution(local_threshold_tuning.ThresholdMap[:].T,
                                     plot_range=range(16),
                                     title='TDAC distribution',
                                     x_axis_title='TDAC',
                                     y_axis_title='#')

                p.create_scurves_plot()
                p.create_threshold_plot()
                p.create_threshold_map()
                p.create_noise_plot()
                p.create_noise_map()


if __name__ == "__main__":
    mtt = MetaThrTuning()
    mtt.start(**local_configuration)
    mtt.analyze()
