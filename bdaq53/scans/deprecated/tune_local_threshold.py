#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Injection tuning:
    iteratively run analog scans and evaluate if more or less than 50% of expected hits are seen in any pixel
'''

from tqdm import tqdm
import numpy as np

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.chips import rd53a

local_configuration = {
    # Scan parameters
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': None,
    'use_good_pixels_diff': False,
    'n_injections': 100,

    # Maximum amount of untunable pixels to disable in percent
    'limit': 1,

    # Target threshold
    'VCAL_MED': 500,
    'VCAL_HIGH': 800
}


class TDACTuning(ScanBase):
    scan_id = 'local_threshold_tuning'

    def configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, use_good_pixels_diff=False, VCAL_MED=1000, VCAL_HIGH=4000, **_):
        if start_column < 128:
            raise NotImplementedError('TDAC tuning is not necessary for SYNC flavor!')
        if start_column < 264 and stop_column > 264:
            raise NotImplementedError('Tuning more than one flavor at once is not implemented yet.')
        self.scan_range = ((start_column, stop_column), (start_row, stop_row))

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        if use_good_pixels_diff:
            self.chip.masks.apply_good_pixel_mask_diff()

        self.chip.masks.update(force=True)

        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)

    def scan(self, n_injections=100, limit=1, max_iterations=25, **kwargs):
        self.n_injections = n_injections
        self.target = self.n_injections / 2
        self.max_n_disabled_pixels = limit / 100. * (self.scan_range[0][1] - self.scan_range[0][0]) * (self.scan_range[1][1] - self.scan_range[1][0])

        min_tdac, max_tdac, _, tdac_incr = self.chip.get_tdac_range(self.scan_range[0][0])
        tdac_range = range(max_tdac, min_tdac - tdac_incr, -1 * tdac_incr)
        self.scan_param_dict = {}
        pbar = tqdm(total=self.chip.masks.get_mask_steps() * len(tdac_range), unit=' Mask steps')
        for scan_param, tdac in enumerate(tdac_range):
            self.scan_param_dict[scan_param] = tdac
            self.chip.masks['tdac'][self.scan_range[0][0]:self.scan_range[0][1], self.scan_range[1][0]:self.scan_range[1][1]] = tdac
            self.chip.masks.update()
            with self.readout(scan_param_id=scan_param):
                for fe in self.chip.masks.shift(masks=['enable', 'injection']):
                    if not fe == 'skipped':
                        self.chip.inject_analog_single(repetitions=n_injections, send_ecr=True)  # TODO Does sending ECR here help to revive stuck pixels?
                    pbar.update(1)

        pbar.close()

    def analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            _, result_hists, _, _ = a.analyze_data()

            noisy_pixels = {}
            for col in range(400):
                for row in range(192):
                    tdac = int(round(a.threshold_map[col, row]))
                    if tdac > 15:
                        tdac = 15
                    if tdac < 0:
                        tdac = 0
                    if not np.any(result_hists[col, row] > self.n_injections * 0.25):
                        # If almost no hits, threshold is too high. Set to lowest local threshold
                        tdac = rd53a.get_tdac_range(col)[0]
                    if not np.any(result_hists[col, row] < self.n_injections * 0.75):
                        # If almost below n_injection hits, threshold is too low. Set to highest local threshold
                        tdac = rd53a.get_tdac_range(col)[1]

                    self.chip.masks['tdac'][col, row] = tdac

                    if np.any(result_hists[col, row] > self.n_injections * 1.01):
                        # If nore hits than n_injections, mark as noisy and disable later if possible
                        noisy_pixels[(col, row)] = max(result_hists[col, row])

            # Disable random noisy pixels
            # TODO go by decreasing amount of noise hits
            n_disabled_pixels = len(np.where(~self.chip.masks.disable_mask[self.scan_range[0][0]:self.scan_range[0][1],
                                                                           self.scan_range[1][0]:self.scan_range[1][1]])[0])
            while n_disabled_pixels < self.max_n_disabled_pixels and len(noisy_pixels) > 0:
                n_disabled_pixels = len(np.where(~self.chip.masks.disable_mask[self.scan_range[0][0]:self.scan_range[0][1],
                                                                               self.scan_range[1][0]:self.scan_range[1][1]])[0])

                idx = max(noisy_pixels, key=noisy_pixels.get)

                (col, row) = idx
                noisy_pixels.pop(idx)
                self.chip.masks.disable_mask[col, row] = False
                self.chip.masks.apply_disable_mask()

        n_disabled_pixels = len(np.where(~self.chip.masks.disable_mask[self.scan_range[0][0]:self.scan_range[0][1],
                                                                       self.scan_range[1][0]:self.scan_range[1][1]])[0])
        mean_tdac = np.mean(self.chip.masks['tdac'][self.scan_range[0][0]:self.scan_range[0][1], self.scan_range[1][0]:self.scan_range[1][1]])

        self.log.success('Found optimal TDAC settings with a mean of {0:1.2f} and disabled {1} untunable pixels.'.format(mean_tdac, n_disabled_pixels))
        self.save_maskfile()


if __name__ == '__main__':
    tuning = TDACTuning()
    tuning.start(**local_configuration)
    tuning.close()
