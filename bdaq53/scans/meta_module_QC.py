#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

"""
    Script that uses existing scans to perform the pass/fail classification for the
    ITkPix Module QC.

    Configurations for individual scans is specified in module_QC_config.yaml
    The module configuration is specified in testbench.yaml
"""

import time
import os
import yaml
import zmq

from pathlib import Path

from bdaq53.qms.qms_control import QMS_Card
from bdaq53.system.periphery import BDAQ53Periphery
from bdaq53.system import logger
from bdaq53 import DEFAULT_TESTBENCH

from copy import deepcopy

from bdaq53.module_QC.QC_routine import QC_routine
from bdaq53.module_QC.QC_tuning import QC_tuning


# !!! configuration for the scans is done in the module_QC_config.yaml file !!!
qc_config_file = '../module_QC/QC_config_files/module_QC_config.yaml'
testbench_path = '../module_QC/QC_testbenches/testbench.yaml'

log = logger.setup_derived_logger('Module QC')
ctx = zmq.Context()

cmd_port = 29321


def QMS(module):
    qms_routing = {
        'module_1': 0x90,
        'module_2': 0x92,
        'module_3': 0x94,
        'module_4': 0x96
    }
    md_nr = module.split('_')[-1]
    routing_file = "../qms/routing_configs/qms_mod" + md_nr + ".yaml"
    log.info(f'Configuring QMS Card with file: {routing_file}')
    with QMS_Card(module_switch_addr=qms_routing[module]) as qms:
        qms.init()
        qms.config(routing_file=routing_file, cmd=True)
        qms.status()


def get_testbench(testbench_path):
    if os.path.isfile(testbench_path):
        log.info(f'Single testbench is loaded: {testbench_path}')
        with open(testbench_path, 'r') as f:
            testbench = yaml.full_load(f)
        return testbench
    elif os.path.isdir(testbench_path):
        log.info(f'Testbench directory: {testbench_path}')
        testbench_files = sorted(Path(testbench_path).glob('*testbench*.yaml'))

        combined_testbench = {}

        for file in testbench_files:
            if os.path.isfile(file):
                log.info(f'Reading testbench {file}')
                with open(file, 'r') as f:
                    testbench = yaml.full_load(f)

                for k, v in testbench.items():
                    if not combined_testbench.get(k):
                        combined_testbench[k] = v
                    else:
                        if k == 'modules':
                            for k_m, v_m in testbench['modules'].items():
                                combined_testbench['modules'][k_m] = v_m
                                log.info(f'Adding module {k_m} to testbench...')
                        elif k == 'hardware':
                            qms_dict = v.get('qms_dict', {})
                            if qms_dict:
                                for slot, module in qms_dict.items():
                                    if module is not None and module != 'None':
                                        combined_testbench['hardware']['qms_dict'][slot] = module
                        else:
                            if combined_testbench.get(k) != v:
                                log.error(f'Merging testbenches with different configuration: {k}')
                                for k_m, v_m in v.items():
                                    if v_m != combined_testbench[k].get(k_m):
                                        if combined_testbench[k].get(k_m) is None:
                                            combined_testbench[k][k_m] = v_m

                                        log.info(f'Configuration for {k_m} does not match. Using {k_m}: {combined_testbench[k][k_m]}')

        return combined_testbench

    else:
        log.error(f'Specified testbench not found: {testbench_path}')
        log.info(f'Using default testbench: {DEFAULT_TESTBENCH}')
        with open(DEFAULT_TESTBENCH, 'r') as f:
            testbench = yaml.full_load(f)

        return testbench


if __name__ == "__main__":
    with open(qc_config_file, 'r') as QC_config_file:
        log.info(f'Loading QC configuration from {qc_config_file}')
        QC_configuration = yaml.safe_load(QC_config_file)

    testbench = get_testbench(testbench_path)
    print(testbench)

    qms_dict = {}
    for slot, module in testbench['hardware'].get('qms_dict', {}).items():
        qms_dict[module] = slot
    print(qms_dict)

    periphery = BDAQ53Periphery(bench_config=testbench)
    periphery.init()

    req = ctx.socket(zmq.REQ)
    req.connect(f'tcp://localhost:{cmd_port}')

    for module, mod_config in testbench['modules'].items():
        log.info(f'Starting QC on module {module}')

        testbench_run = deepcopy(testbench)
        testbench_run['modules'] = {module: mod_config}

        QMS(qms_dict[module])
        req.send_json({'cmd': 'SWITCH MODULES', 'value': module})
        log.info(req.recv_json())
        time.sleep(1)

        QC_routine(periphery=periphery, QC_configuration=QC_configuration, testbench=testbench_run, module_name=module)

        QC_tuning(testbench=testbench_run, module_name=module)

    req.close()
    periphery.close()
