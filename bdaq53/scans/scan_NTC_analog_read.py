#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test checks the internal voltages and currents of the chip NTCs.

    WARNING: If QMS card is used, the module slot on the QMS card has to correspond to the pin on the arduino used for
    temperature readout. If no QMS card is used, pin 0 should be used.
'''
import time
import yaml

import numpy as np
import tables as tb
from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis_utils as au
from bdaq53.module_QC.QC_test import QC_Test
from bdaq53.analysis.convert_to_json import JSONConverter

scan_configuration = {
    'periphery_device': 'Multimeter',
    'power_settings': {
        'Iin': 4.41,
        'Vlim': 2.0
    },

    'analysis_parameter_path': '../module_QC/QC_config_files/analog_readback_213.yaml'
}


class NTCTempRawTable(tb.IsDescription):
    V_NTC_PAD = tb.Float32Col(pos=0)
    voltage_gnd = tb.Float32Col(pos=1)
    I_NTC_PAD = tb.Float32Col(pos=2)
    current_gnd = tb.Float32Col(pos=3)
    NTC_ext = tb.Float32Col(pos=4)


class DiodeTempRawTable(tb.IsDescription):
    DEM = tb.UInt32Col(pos=0)
    lower = tb.Float32Col(pos=1)
    upper = tb.Float32Col(pos=2)


class DiodeTempTable(tb.IsDescription):
    sensor = tb.StringCol(64, pos=0)
    delta_V = tb.Float32Col(pos=1)
    Nf = tb.Float32Col(pos=2)
    Temperature = tb.Float32Col(pos=3)
    passed_QC = tb.StringCol(64, pos=4)


class NTCReadout(ScanBase):
    scan_id = 'NTC_readout'
    e = 1.6021766208e-19
    kb = 1.38064852e-23

    def _configure(self, **_):
        if hasattr(self, 'module_slot'):
            self.ntc_slot = int(self.module_slot.split('_')[-1])
            self.ntc_slot -= 1
        else:
            self.ntc_slot = 0

    def _scan(self, periphery_device='Multimeter', **_):
        self.log.info('Measuring Temperature at TEMPSENS_A')
        self._measure_diode_temperature_sensor('TEMPSENS_A', periphery_device=periphery_device)
        self.log.info('Measuring Temperature at TEMPSENS_D')
        self._measure_diode_temperature_sensor('TEMPSENS_D', periphery_device=periphery_device)
        self.log.info('Measuring Temperature at TEMPSENS_C')
        self._measure_diode_temperature_sensor('TEMPSENS_C', periphery_device=periphery_device)
        self.log.info('Measuring Temperature at NTC')
        self._measure_NTC_temperature('NTC', sampling=32, periphery_device=periphery_device)

    def _measure_diode_temperature_sensor(self, sensor, periphery_device='Mulitmeter'):
        temp_data_table = self.h5_file.create_table(self.h5_file.root, name=sensor, title=sensor, description=DiodeTempRawTable)
        row = temp_data_table.row
        for diode_current in tqdm(range(0, 16, 1)):
            row['DEM'] = diode_current

            # measure lower value
            if sensor == 'TEMPSENS_A':
                bitstring_SLDO = int('0' + format(0, '04b') + '01' + format(diode_current, '04b') + '0', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_D':
                bitstring_SLDO = int('1' + format(diode_current, '04b') + '00' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_C':
                bitstring_SLDO = int('0' + format(0, '04b') + '00' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('1' + format(diode_current, '04b') + '0', 2)
            self.chip.registers['MON_SENS_SLDO'].write(bitstring_SLDO)
            self.chip.registers['MON_SENS_ACB'].write(bitstring_ACB)
            self.chip.write_sync()

            row['lower'] = self._set_mux_get_voltage(mux_select=sensor, measure='voltage', periphery_device=periphery_device)

            # measure upper value
            if sensor == 'TEMPSENS_A':
                bitstring_SLDO = int('0' + format(0, '04b') + '01' + format(diode_current, '04b') + '1', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_D':
                bitstring_SLDO = int('1' + format(diode_current, '04b') + '10' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_C':
                bitstring_SLDO = int('0' + format(0, '04b') + '00' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('1' + format(diode_current, '04b') + '1', 2)
            self.chip.registers['MON_SENS_SLDO'].write(bitstring_SLDO)
            self.chip.registers['MON_SENS_ACB'].write(bitstring_ACB)
            self.chip.write_sync()

            row['upper'] = self._set_mux_get_voltage(mux_select=sensor, measure='voltage', periphery_device=periphery_device)

            row.append()
            temp_data_table.flush()

    def _measure_NTC_temperature(self, sensor, sampling=1, periphery_device='Mulitmeter'):  # FIX ME: How does the NTC work?
        self.data.NTC_data_table = self.h5_file.create_table(self.h5_file.root, name=sensor, title=sensor, description=NTCTempRawTable)
        row = self.data.NTC_data_table.row
        for reps in tqdm(range(sampling)):
            row['V_NTC_PAD'] = self._set_mux_get_voltage(mux_select='NTC_PAD', measure='voltage', periphery_device=periphery_device)
            row['voltage_gnd'] = self._set_mux_get_voltage(mux_select='GNDA30', measure='voltage', periphery_device=periphery_device)
            row['I_NTC_PAD'] = self._set_mux_get_voltage(mux_select='NTC_PAD', measure='current', periphery_device=periphery_device)
            row['current_gnd'] = self._set_mux_get_voltage(mux_select='I_MUX', measure='voltage', periphery_device=periphery_device)

            row['NTC_ext'] = self.periphery.aux_devices['Arduino'].get_temperature(sensor=self.ntc_slot)[str(self.ntc_slot)]
            row.append()
            self.data.NTC_data_table.flush()

    def _set_mux_get_voltage(self, mux_select='VINA_HALF', measure='voltage', periphery_device='Multimeter'):
        self.chip.set_mux(mux_select, measure)
        time.sleep(0.1)
        if measure in ('voltage', 'v'):
            return self.periphery.aux_devices[periphery_device].get_voltage()
        else:
            return self.periphery.aux_devices[periphery_device].get_voltage()

    def _analyze(self, raw_data_file=None, analyzed_data_file=None, analysis_parameter_path=None):
        if not raw_data_file:
            raw_data_file = self.output_filename + '.h5'
        if not analyzed_data_file:
            analyzed_data_file = self.output_filename + '_interpreted.h5'

        with tb.open_file(raw_data_file, 'r') as in_file:
            scan_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])
            chip_calibration = au.ConfigDict(in_file.root.configuration_in.chip.calibration[:])
            sensor_id_dict = {'TEMPSENS_A': 0, 'TEMPSENS_D': 1, 'TEMPSENS_C': 2}
            temp_data = {}
            sensor_Nf = {}
            for sensor in ['TEMPSENS_A', 'TEMPSENS_D', 'TEMPSENS_C']:
                if hasattr(in_file.root, sensor):
                    temp_data[sensor] = eval('in_file.root.' + sensor + '[:]')
                    sensor_Nf[sensor] = chip_calibration.get('Nf_{0}'.format(sensor_id_dict[sensor]), 'fail')
            if hasattr(in_file.root, 'NTC'):
                temp_data['NTC'] = in_file.root.NTC[:]
                temp_data['NTC_ext'] = in_file.root.NTC[:]
            with tb.open_file(analyzed_data_file, 'w', title=in_file.title) as out_file:
                out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)

                output = out_file.create_table(out_file.root, name='TempSensor', title='Temperatures_from_diode_sensors', description=DiodeTempTable)
                row = output.row

                if temp_data.get('NTC_ext') is not None:
                    data = temp_data['NTC_ext']
                    temp_arr = np.array([row[4] for row in data])
                    module_NTC_temp = np.mean(temp_arr)
                    row['sensor'] = 'NTC_Ext'
                    row['Temperature'] = module_NTC_temp
                    row.append()
                    output.flush()
                else:
                    module_NTC_temp = None

                if not analysis_parameter_path:
                    analysis_parameter_path = scan_config.get('analysis_parameter_path', None)

                if analysis_parameter_path and analysis_parameter_path != '':
                    with open(analysis_parameter_path, 'r') as analysis_parameter_file:
                        self.analysis_parameters = yaml.safe_load(analysis_parameter_file)

                QC_passed = True

                with QC_Test(output_file=self.output_filename) as QC:
                    if temp_data.get('NTC') is not None:
                        data = temp_data['NTC']

                        Vntc = np.array([row[0] - row[1] for row in data])
                        Intc = np.array([row[2] - row[3] for row in data]) / 10000
                        chip_temp = self.calculate_T_from_NTC(Vntc, Intc)

                        parameters = self.analysis_parameters.get('NTC_Ext_Match', False)

                        if module_NTC_temp is not None and parameters:
                            passed_test = QC.QC_test('NTC_Ext_Match', parameters, [chip_temp, module_NTC_temp])
                        else:
                            passed_test = None

                        row['sensor'] = 'NTC'
                        row['Temperature'] = chip_temp
                        row['passed_QC'] = passed_test
                        row.append()
                        output.flush()

                        if passed_test is not None:
                            QC_passed = QC_passed and passed_test
                    else:
                        chip_temp = None

                    for sensor, data in temp_data.items():
                        if 'NTC' in sensor:
                            pass
                        else:
                            delta_v = np.array([row[2] - row[1] for row in data])
                            temp = self.calculate_T_from_sensor(np.mean(delta_v), sensor_Nf[sensor])
                            row['Nf'] = sensor_Nf[sensor]
                            row['delta_V'] = np.mean(delta_v)

                            parameters = self.analysis_parameters.get('Temp_MOS_Match')
                            if chip_temp is not None and parameters is not None:
                                passed_test = QC.QC_test('Temp_MOS_Match:' + sensor, parameters, [temp, chip_temp])
                            else:
                                passed_test = None

                            row['sensor'] = sensor
                            row['Temperature'] = temp
                            row['passed_QC'] = passed_test
                            row.append()
                            output.flush()

                            if passed_test is not None:
                                QC_passed = QC_passed and passed_test

                self.chip.QC_passed = QC_passed

        conv = JSONConverter(data_file=raw_data_file, outpath=self.working_dir)
        conv.convert_h5_to_json()

    def calculate_T_from_sensor(self, dV, Nf):  # FIX ME: This should be in the chip class.
        return self.e / (Nf * self.kb * np.log(15)) * dV - 273.15

    def calculate_T_from_NTC(self, Vntc, Intc, NTC_CalPar=[0.0007488999981433153, 0.0002769000129774213, 7.059500006789676e-08]):
        # FIX ME: Get NTC calibration parameters from config
        Vntc = np.array(Vntc)
        Intc = np.array(Intc)

        Rntc = np.mean(Vntc / Intc)

        A = NTC_CalPar[0]
        B = NTC_CalPar[1]
        C = NTC_CalPar[2]

        return 1 / (A + B * np.log(Rntc) + C * ((np.log(Rntc)) ** 3)) - 273.15


if __name__ == "__main__":
    with NTCReadout(scan_config=scan_configuration) as scan:
        scan.start()
