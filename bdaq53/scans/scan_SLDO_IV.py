#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This test measures the SLDO performance. The input current is increased in steps and for
    each step, the resulting voltages VinA, VinD, VDDA, VDDD, Vofs, Vref_pre and the currents
    Iref, IinA, IshuntA, IinD, IshuntD are measured.
'''

import time

import numpy as np
import tables as tb
from tqdm import tqdm
import yaml
import traceback

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import plotting
from bdaq53.module_QC.QC_test import QC_Test
from bdaq53.analysis.convert_to_json import JSONConverter


scan_configuration = {
    'periphery_device': 'Multimeter',
    'Iin_start': 5.88,
    'Iin_stop': 9.48,
    'Iin_step': 0.2,
    'voltage_limit': 5.0  # !!! Voltage limit to protect the chip. Be careful when setting it above 2.0V !!!
}


class DacTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    Iin = tb.Float32Col(pos=1)
    VinA = tb.Float32Col(pos=2)
    VinD = tb.Float32Col(pos=3)
    VDDA = tb.Float32Col(pos=4)
    VDDD = tb.Float32Col(pos=5)
    Vofs = tb.Float32Col(pos=6)
    Vref_pre = tb.Float32Col(pos=7)
    voltage_gnd = tb.Float32Col(pos=8)
    Iref = tb.Float32Col(pos=9)
    IinA = tb.Float32Col(pos=10)
    IshuntA = tb.Float32Col(pos=11)
    IinD = tb.Float32Col(pos=12)
    IshuntD = tb.Float32Col(pos=13)
    current_gnd = tb.Float32Col(pos=16)
    T_NTC = tb.Float32Col(pos=14)
    IinMeas = tb.Float32Col(pos=15)


class SLDOAnalysedTable(tb.IsDescription):
    Iin = tb.Float32Col(pos=0)
    IinMeas = tb.Float32Col(pos=1)
    VinTheo = tb.Float32Col(pos=2)
    VinA = tb.Float32Col(pos=3)
    VinD = tb.Float32Col(pos=4)
    VDDA = tb.Float32Col(pos=5)
    VDDD = tb.Float32Col(pos=6)
    Vofs = tb.Float32Col(pos=7)
    Vref_pre = tb.Float32Col(pos=8)
    Iref = tb.Float32Col(pos=9)
    IinA = tb.Float32Col(pos=10)
    IshuntA = tb.Float32Col(pos=11)
    IinD = tb.Float32Col(pos=12)
    IshuntD = tb.Float32Col(pos=13)

    T_NTC = tb.Float32Col(pos=14)


class SLDOQCTable(tb.IsDescription):
    attribute = tb.StringCol(64, pos=0)
    value = tb.StringCol(64, pos=1)
    passed_QC = tb.StringCol(64, pos=2)


class SLDO_IV_Scan(ScanBase):
    scan_id = 'sldo_iv_scan'

    def _configure(self, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

        if hasattr(self, 'module_slot'):
            self.ntc_slot = int(self.module_slot.split('_')[-1])
            self.ntc_slot -= 1
        else:
            self.ntc_slot = 0

    def _scan(self, Iin_start=0.6, Iin_stop=6, Iin_step=0.2, voltage_limit=2.0, periphery_device='Multimeter', NTC_periphery='Arduino', **_):
        """
        SLDO IV scan main loop

        Parameters
        ----------

        Iin_start : float
            First value for the input current
        Iin_stop : float
            Last value for the input current, exclusive
        Iin_step : float
            Steps between Iin values
        """

        Iin_range = np.arange(Iin_stop, Iin_start - 0.005, -Iin_step)

        self._set_mux_get_voltage(mux_select='VINA_HALF', measure='voltage', periphery_device=periphery_device)
        # FIX ME: Multimeter returns negative value on first measurement. On subsequent measurements the correct value is returned.

        self.data.dac_data_table = self.h5_file.create_table(self.h5_file.root, name='dac_data',
                                                             title='dac_data', description=DacTable)
        row = self.data.dac_data_table.row
        for scan_param_id, value in tqdm(enumerate(Iin_range), total=len(Iin_range), unit=' I_in Step'):
            row['scan_param_id'] = scan_param_id
            row['Iin'] = value
            # set Iin on Powersupply
            self.periphery.module_devices[self.module_settings['name']]['LV'].set_voltage(voltage_limit)

            self.periphery.module_devices[self.module_settings['name']]['LV'].set_current_limit(value)
            time.sleep(0.5)

            row['IinMeas'] = self.periphery.module_devices[self.module_settings['name']]['LV'].get_current()
            # get voltages
            row['VinA'] = self._set_mux_get_voltage(mux_select='VINA_HALF', measure='voltage', periphery_device=periphery_device)
            row['VinD'] = self._set_mux_get_voltage(mux_select='VIND_HALF', measure='voltage', periphery_device=periphery_device)
            row['VDDA'] = self._set_mux_get_voltage(mux_select='VDDA_HALF', measure='voltage', periphery_device=periphery_device)
            row['VDDD'] = self._set_mux_get_voltage(mux_select='VDDD_HALF', measure='voltage', periphery_device=periphery_device)
            row['Vofs'] = self._set_mux_get_voltage(mux_select='VOFS_HALF', measure='voltage', periphery_device=periphery_device)
            row['Vref_pre'] = self._set_mux_get_voltage(mux_select='VREF_PRE', measure='voltage', periphery_device=periphery_device)
            row['voltage_gnd'] = self._set_mux_get_voltage(mux_select='GNDA30', measure='voltage', periphery_device=periphery_device)

            # get currents
            row['Iref'] = self._set_mux_get_voltage(mux_select='IREF', measure='current', periphery_device=periphery_device)
            row['IinA'] = self._set_mux_get_voltage(mux_select='IINA', measure='current', periphery_device=periphery_device)
            row['IshuntA'] = self._set_mux_get_voltage(mux_select='I_SHUNT_A', measure='current', periphery_device=periphery_device)
            row['IinD'] = self._set_mux_get_voltage(mux_select='IIND', measure='current', periphery_device=periphery_device)
            row['IshuntD'] = self._set_mux_get_voltage(mux_select='I_SHUNT_D', measure='current', periphery_device=periphery_device)
            row['current_gnd'] = self._set_mux_get_voltage(mux_select='I_MUX', measure='voltage', periphery_device=periphery_device)
            # get temperature
            if NTC_periphery in self.periphery.aux_devices:
                row['T_NTC'] = self.periphery.aux_devices[NTC_periphery].get_temperature(sensor=self.ntc_slot)[str(self.ntc_slot)]
            else:
                print(self.periphery.aux_devices)
                row['T_NTC'] = 0
            row.append()
            self.data.dac_data_table.flush()

        self.log.success('Scan finished')

    def _set_mux_get_voltage(self, mux_select='VINA_HALF', measure='voltage', periphery_device='Multimeter'):
        self.chip.set_mux(mux_select, measure)
        time.sleep(0.1)
        if measure in ('voltage', 'v'):
            return self.periphery.aux_devices[periphery_device].get_voltage()
        else:
            return self.periphery.aux_devices[periphery_device].get_voltage()

    def _analyze(self, raw_data_file=None, analyzed_data_file=None):
        if not raw_data_file:
            raw_data_file = self.output_filename + '.h5'
        if not analyzed_data_file:
            analyzed_data_file = self.output_filename + '_interpreted.h5'
        with tb.open_file(raw_data_file, 'r') as in_file:
            DAC_data = in_file.root.dac_data[:]
            with tb.open_file(analyzed_data_file, 'w', title=in_file.title) as self.out_file:
                self.out_file.create_group(self.out_file.root, name='configuration_in', title='Configuration after scan step')
                self.out_file.copy_children(in_file.root.configuration_out, self.out_file.root.configuration_in, recursive=True)

                self._process_raw_data(DAC_data)
                self._test_module_QC_working_point(analysis_parameter_path='../module_QC/QC_config_files/SLDO_analysis.yaml')

        self._test_module_QC_IV(0.03)

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=analyzed_data_file) as p:
                p.create_standard_plots()

        conv = JSONConverter(data_file=raw_data_file, outpath=self.working_dir)
        conv.convert_h5_to_json()

    def _process_raw_data(self, DAC_data):
        R_Imux = 10000
        k_IinA = self.chip.calibration.shunt_sense_kInA
        k_IinD = self.chip.calibration.shunt_sense_kInD
        k_IshuntA = self.chip.calibration.shunt_sense_kA
        k_IshuntD = self.chip.calibration.shunt_sense_kD
        m_IinA = k_IinA / R_Imux
        m_IinD = k_IinD / R_Imux
        m_shuntA = k_IshuntA / R_Imux
        m_shuntD = k_IshuntD / R_Imux

        self.data.Iin, self.data.IinMeas, self.data.VinTheo = [], [], []
        self.data.VinA, self.data.VinD, self.data.VDDA, self.data.VDDD, self.data.Vofs, self.data.Vref_pre = [], [], [], [], [], []
        self.data.Iref, self.data.IinA, self.data.IshuntA, self.data.IinD, self.data.IshuntD = [], [], [], [], []

        output = self.out_file.create_table(self.out_file.root, name='sldo_iv_data', title='SLDO_IV_data', description=SLDOAnalysedTable)
        row = output.row
        for dac in DAC_data:
            row['Iin'] = dac[1]
            self.data.Iin.append(row['Iin'])
            row['IinMeas'] = dac[15]
            self.data.IinMeas.append(row['IinMeas'])
            row['VinTheo'] = self._SLDO_vin_theo(dac[15])
            self.data.VinTheo.append(row['VinTheo'])
            row['VinA'] = (dac[2] - dac[8]) * 4
            self.data.VinA.append(row['VinA'])
            row['VinD'] = (dac[3] - dac[8]) * 4
            self.data.VinD.append(row['VinD'])
            row['VDDA'] = (dac[4] - dac[8]) * 2
            self.data.VDDA.append(row['VDDA'])
            row['VDDD'] = (dac[5] - dac[8]) * 2
            self.data.VDDD.append(row['VDDD'])
            row['Vofs'] = (dac[6] - dac[8]) * 4
            self.data.Vofs.append(row['Vofs'])
            row['Vref_pre'] = dac[7] - dac[8]
            self.data.Vref_pre.append(row['Vref_pre'])

            row['Iref'] = (dac[9] - dac[16]) / R_Imux
            self.data.Iref.append(row['Iref'])
            row['IinA'] = (dac[10] - dac[16]) * m_IinA
            self.data.IinA.append(row['IinA'])
            row['IshuntA'] = (dac[11] - dac[16]) * m_shuntA
            self.data.IshuntA.append(row['IshuntA'])
            row['IinD'] = (dac[12] - dac[16]) * m_IinD
            self.data.IinD.append(row['IinD'])
            row['IshuntD'] = (dac[13] - dac[16]) * m_shuntD
            self.data.IshuntD.append(row['IshuntD'])

            row.append()
            output.flush()

    def _SLDO_vin_theo(self, Iin, nr_of_modules=4):
        Vofs = 1  # FIX ME: Vofs = 1.1 for L0 modules
        RextA = 866 * 4 / nr_of_modules  # 575
        RextD = 590 * 4 / nr_of_modules  # 535
        kA = self.chip.calibration.shunt_kA
        kD = self.chip.calibration.shunt_kD
        print(kA, kD)

        Reff = 1 / ((kA / RextA) + (kD / RextD))

        return Reff * Iin / nr_of_modules + Vofs

    def _test_module_QC_IV(self, QC_tolerance):
        VinA = np.array(self.data.VinA)
        VinD = np.array(self.data.VinD)
        VinTheo = np.array(self.data.VinTheo)
        test_QC = np.logical_and((abs(VinA - VinTheo) < QC_tolerance), (abs(VinD - VinTheo) < QC_tolerance))
        print(test_QC)

        if all(test_QC):
            print('QC test passed')
        else:
            for i in range(len(test_QC)):
                if not test_QC[-(i + 1)]:
                    IinMax = self.data.IinMeas[-(i + 1)]
                    break

            print('--------------------')
            print('!!! Test Failed !!!')
            print('QC criteria failed for Iin > %f A' % IinMax)
            print('--------------------')

        self.chip.QC_passed = all(test_QC)

    def _test_module_QC_working_point(self, Iin_nominal=5.9, analysis_parameter_path=None):

        if analysis_parameter_path and analysis_parameter_path != '':
            try:
                with open(analysis_parameter_path, 'r') as analysis_parameter_file:
                    self.analysis_parameters = yaml.safe_load(analysis_parameter_file)

                Iin = np.array(self.data.IinMeas)
                index = (np.abs(Iin - Iin_nominal)).argmin()
                print('Nominal current of %f at step %i' % (self.data.IinMeas[index], index))

                QC_passed = True

                with QC_Test() as QC:
                    output = self.out_file.create_table(self.out_file.root, name='sldo_at_nominal', title='SLDO_Performance_at_Nominal_Iin', description=SLDOQCTable)
                    row = output.row
                    for key, parameters in self.analysis_parameters.items():
                        if key == 'VinMatch':
                            parameters['measure'] = 'voltage'
                            test_passed = QC.QC_test(key, parameters, data=[self.data.VinA[index], self.data.VinD[index]])
                            test_value = abs(self.data.VinA[index] - self.data.VinD[index])

                        else:
                            data = getattr(self.data, key, False)
                            if data:
                                test_value = data[index]
                                test_passed = QC.QC_test(key, parameters, test_value)
                            else:
                                continue

                        QC_passed = QC_passed and test_passed
                        row['attribute'] = key
                        row['value'] = test_value
                        row['passed_QC'] = test_passed
                        row.append()
                        output.flush()
                return QC_passed

            except Exception:
                self.log.warning('Could not perform QC at nominal current')
                traceback.print_exc()


if __name__ == "__main__":
    with SLDO_IV_Scan(scan_config=scan_configuration, bench_config='../testbench_preprod_4.yaml') as scan:
        scan.start()
        for chip in scan.chips.values():
            print('%s: %s' % (chip.name, chip.chip.QC_passed))
