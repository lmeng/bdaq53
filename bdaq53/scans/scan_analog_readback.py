#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test checks the internal voltages and currents against the design parameters.
'''
import time
import yaml
import traceback

import tables as tb
from tqdm import tqdm
from bdaq53.module_QC.QC_test import QC_Test

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis.convert_to_json import JSONConverter

scan_configuration = {
    'periphery_device': 'Multimeter',
    'power_settings': {
        'Iin': 4.41,
        'Vlim': 2.0
    },
    'parameter_path': '../module_QC/QC_config_files/analog_readback_213.yaml'
}


class AnalogReadbackTable(tb.IsDescription):
    mux = tb.StringCol(64, pos=0)
    value = tb.Float32Col(pos=1)
    gnd = tb.Float32Col(pos=2)


class TrimTable(tb.IsDescription):
    attribute = tb.StringCol(64, pos=0)
    value = tb.StringCol(64, pos=1)
    passed_QC = tb.StringCol(64, pos=2)


class AnalogReadbackScan(ScanBase):
    scan_id = 'analog_readback'

    def _configure(self, **_):
        pass

    def _scan(self, periphery_device='Multimeter', parameter_path='../module_QC/QC_config_files/SLDO_analysis.yaml', **_):
        test_mux = []
        with open(parameter_path, 'r') as parameter_file:
            test_parameters = yaml.safe_load(parameter_file)
        for key, parameters in test_parameters.items():
            if parameters.get('measure', False):
                if parameters['measure'] != 'temperature':
                    test_mux.append((parameters['mux'], parameters['measure']))

        self.data.analog_readback_table = self.h5_file.create_table(self.h5_file.root, name='analog_readback', title='Analog_Readback_Table', description=AnalogReadbackTable)
        row = self.data.analog_readback_table.row

        for mux_setting in tqdm(test_mux, total=len(test_mux), unit=' internal parameters measured'):
            self.log.info('measuring %s' % mux_setting[0])
            mux_prefix = 'V'
            if mux_setting[1].lower() == 'voltage':
                value = self._set_mux_get_voltage(mux_select=mux_setting[0], measure='voltage', periphery_device=periphery_device)
                value_gnd = self._set_mux_get_voltage(mux_select='GNDA30', measure='voltage', periphery_device=periphery_device)
            elif mux_setting[1].lower() == 'current':
                value = self._set_mux_get_voltage(mux_select=mux_setting[0], measure='current', periphery_device=periphery_device)
                value_gnd = self._set_mux_get_voltage(mux_select='I_MUX', measure='voltage', periphery_device=periphery_device)
                mux_prefix = 'I'
            else:
                self.log.error('measure type not voltage or current')
                value = None
                value_gnd = None

            row['mux'] = mux_prefix + '_' + mux_setting[0]
            row['value'] = value
            row['gnd'] = value_gnd
            row.append()
            self.data.analog_readback_table.flush()

        self.log.success('Scan finished')

    def _set_mux_get_voltage(self, mux_select='VINA_HALF', measure='voltage', periphery_device='Multimeter'):
        self.chip.set_mux(mux_select, measure)
        time.sleep(0.1)
        if measure in ('voltage', 'v'):
            return self.periphery.aux_devices[periphery_device].get_voltage()
        else:
            return self.periphery.aux_devices[periphery_device].get_voltage()

    def _analyze(self, raw_data_file=None, analyzed_data_file=None, analysis_parameter_path=None):
        if not raw_data_file:
            raw_data_file = self.output_filename + '.h5'
        if not analyzed_data_file:
            analyzed_data_file = self.output_filename + '_interpreted.h5'

        with tb.open_file(raw_data_file, 'r') as in_file:
            raw_data = in_file.root.analog_readback[:]
            scan_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])

            readout_data = {}
            for row in raw_data:
                readout_data[row[0].decode('utf-8')] = {'value': row[1], 'gnd': row[2]}

            if not analysis_parameter_path:
                analysis_parameter_path = scan_config.get('parameter_path', None)

            if analysis_parameter_path and analysis_parameter_path != '':
                try:
                    with open(analysis_parameter_path, 'r') as analysis_parameter_file:
                        self.analysis_parameters = yaml.safe_load(analysis_parameter_file)

                    QC_passed = True
                    QC_results = []

                    with QC_Test(output_file=self.output_filename) as QC:
                        for key, parameters in self.analysis_parameters.items():
                            if key == 'VinMatch':
                                if 'VINA_HALF' in readout_data.keys() and 'VIND_HALF' in readout_data.keys():
                                    parameters['measure'] = 'voltage'
                                    VinA = (readout_data['VINA_HALF']['value'] - readout_data['VINA_HALF']['gnd']) * 4
                                    VinD = (readout_data['VIND_HALF']['value'] - readout_data['VIND_HALF']['gnd']) * 4

                                    test_passed = QC.QC_test(key, parameters, data=[VinA, VinD])
                                    test_value = abs(VinA - VinD)

                                    QC_passed = QC_passed and test_passed
                                    QC_results.append((key, test_value, test_passed))
                            elif 'NTC' in key or 'Temp' in key:
                                pass
                            else:
                                mux = parameters['mux']
                                prefix = 'V' if parameters['measure'] == 'voltage' else 'I'
                                mux = prefix + '_' + mux
                                if mux in readout_data.keys():
                                    test_value = self._get_value(parameters, readout_data[mux])
                                    test_passed = QC.QC_test(key, parameters, test_value)

                                    QC_passed = QC_passed and test_passed
                                    QC_results.append((key, test_value, test_passed))

                    self.chip.QC_passed = QC_passed

                    with tb.open_file(analyzed_data_file, 'w', title=in_file.title) as out_file:
                        out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                        out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)

                        output = out_file.create_table(out_file.root, name='analog_readback', title='Analog_Readback', description=TrimTable)
                        row = output.row
                        for attribute, value, passed_QC in QC_results:
                            row['attribute'] = attribute
                            row['value'] = value
                            row['passed_QC'] = passed_QC
                            row.append()
                            output.flush()

                except Exception:
                    self.log.warning('Could not perform Analysis')
                    traceback.print_exc()

        conv = JSONConverter(data_file=raw_data_file, outpath=self.working_dir)
        conv.convert_h5_to_json()

    def _get_value(self, parameters, data):
        return (data['value'] - data['gnd']) * float(parameters.get('mux_multiply', 1))


if __name__ == "__main__":
    with AnalogReadbackScan(scan_config=scan_configuration) as scan:
        scan.start()
