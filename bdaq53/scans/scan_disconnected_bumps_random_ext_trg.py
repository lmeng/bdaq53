#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Regular source scan that runs a bump connectivity analysis afterwards.
'''

from bdaq53.scans.scan_random_ext_trigger import RandomExtTriggerScan
from bdaq53.analysis import analysis
from bdaq53.analysis import analyze_disconnected_bumbs_from_source_scan
from bdaq53.analysis import plotting
from bdaq53.scans.calibrate_hitor import HitorCalib
from bdaq53.utils import get_latest_h5file


scan_configuration = {
    'start_column': 0,        # Start column for mask
    'stop_column': 400,         # Stop column for mask
    'start_row': 0,             # Start row for mask
    'stop_row': 192,            # Stop row for mask

    # Stop conditions (choose one)
    'scan_timeout': 60 - 10,          # Timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,          # Number of maximum received triggers after stopping readout, if False no limit on received trigger
    'min_spec_occupancy': False,    # Minimum hits for each pixel above which the scan will be stopped; only a fraction of all pixels needs to reach this limit (see below)

    # For stop condition 'min_spec_occupancy' only
    'fraction': 0.99,   # Fraction of enabled pixels that need to reach the minimum occupancy (no hits in dead/disconnected pixels!)

    'trigger_frequency': 40,   # Trigger frequency, 100 corresponds to 10 kHz
    'trigger_latency': 100,     # Latency of trigger in units of 25 ns (BCs), RD53A: 100, ITkPix: 98
    'trigger_delay': 57,        # Trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,       # Length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 50,         # Length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Increase by factor of number of connected chips/hitors. Should also be adjusted for longer trigger length.

    'use_tdc': False,           # Enable TDC modules

    'hitor_calib_file': None,

    # Trigger configuration
    'bench': {'TLU': {
        'TRIGGER_MODE': 0,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 512    # Selecting trigger input: HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }
}


class BumpConSourceScan(RandomExtTriggerScan):
    scan_id = 'bump_connection_source_scan'

    def _analyze(self):
        hitor_calib_file = self.configuration['scan'].get('hitor_calib_file', None)
        self.configuration['bench']['analysis']['analyze_tdc'] = self.configuration['scan'].get('use_tdc', False)

        if hitor_calib_file == 'auto':
            hitor_calib_file = get_latest_h5file(directory=self.output_directory, scan_pattern=HitorCalib.scan_id, interpreted=True)

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', hitor_calib_file=hitor_calib_file, **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        analyze_disconnected_bumbs_from_source_scan.analyze_chip(self.output_filename, analyze_id='simple')

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

        return


if __name__ == '__main__':
    with BumpConSourceScan(scan_config=scan_configuration) as scan:
        scan.start()
