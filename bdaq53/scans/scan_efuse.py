#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.

    Version adapted to ITk_Pix_V1
'''
import logging
import tables as tb
from bdaq53.system.scan_base import ScanBase

scan_configuration = {}  # Needed for test_scan pipeline

probe_locations = {
    '1': 'Bonn'
}


class EfuseTable(tb.IsDescription):
    attribute = tb.StringCol(64, pos=0)
    value = tb.StringCol(64, pos=1)


class EFuseScan(ScanBase):
    scan_id = 'test_efuse'

    def _configure(self, **_):
        pass

    def _scan(self, **_):
        try:
            efuse_read = str(hex(self.chip.read_efuses()))

            self.data.efuse_table = self.h5_file.create_table(self.h5_file.root, name='Efuse', title='Efuse_Data', description=EfuseTable)

            row = self.data.efuse_table.row

            row['attribute'] = 'efuse_read'
            row['value'] = efuse_read
            row.append()
            self.data.efuse_table.flush()

            self.log.success('Scan finished')

        except Exception as e:
            self.chip.QC_passed = False
            self.log.error('Scan failed:', e)

    def _convert_chip_sn(self, chip_sn):
        ''' Converts chip S/N (0x....) to ATLAS S/N (20PGFC).
        '''
        return '20UPGFC{0:07d}'.format(int(chip_sn, 16))

    def _analyze(self, raw_data_file=None, analyzed_data_file=None):
        if not raw_data_file:
            raw_data_file = self.output_filename + '.h5'
        if not analyzed_data_file:
            analyzed_data_file = self.output_filename + '_interpreted.h5'

        QC_passed = True

        with tb.open_file(raw_data_file, 'r') as in_file:
            with tb.open_file(analyzed_data_file, 'w', title=in_file.title) as out_file:
                out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)

                # Get efuse value
                efuse_table = in_file.root.Efuse[:]
                efuse_dict = {}
                for k, v in efuse_table:
                    efuse_dict[k.decode()] = v.decode()

                efuse_read = efuse_dict['efuse_read']

                # Extract efuse information
                probe_loc = efuse_read[2]
                chip_sn = '0x' + efuse_read[3:8]
                chip_sn = chip_sn.upper()
                parity_bit = efuse_read[-2:]

                try:
                    probe_loc = probe_locations[probe_loc]
                except KeyError:
                    self.log.exception('Unknown Probe Location')
                    QC_passed = False

                self.log.info('Probe Location: %s' % probe_loc)
                self.log.info('Parity bits: %s' % parity_bit)

                match = chip_sn == self.chip_settings['chip_sn'].upper()
                if not match:
                    self.log.error('Chip SN does not match!')
                    self.log.info(f'Chip SN: {chip_sn}')
                    self.log.info(f'Chip SN from settings: {self.chip_settings["chip_sn"]}')
                    QC_passed = False
                else:
                    self.log.success('Chip SN: %s' % chip_sn)
                atlas_sn = self._convert_chip_sn(chip_sn)
                self.log.info('ATLAS SN: %s' % atlas_sn)

                self.data.efuse_table = out_file.create_table(out_file.root, name='Efuse', title='Efuse_Data', description=EfuseTable)

                row = self.data.efuse_table.row

                for attribute, value in zip(['Probe_Location', 'Parity_Bits', 'Chip_SN', 'ATLAS_SN', 'SN_Match'],
                                            [str(probe_loc), str(parity_bit), str(chip_sn), str(atlas_sn), str(match)]):
                    row['attribute'] = attribute
                    row['value'] = value
                    row.append()
                    self.data.efuse_table.flush()

        self.chip.QC_passed = QC_passed


if __name__ == "__main__":
    with EFuseScan() as scan:
        scan.start()
