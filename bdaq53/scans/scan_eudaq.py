#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script connects bdaq53 to the EUDAQ 1.x data acquisition system.
'''

import argparse
import os
import time
import sys
import threading

import numpy as np
import tables as tb
from tqdm import tqdm
import yaml

from bdaq53.system import logger
from bdaq53.scans import scan_ext_trigger
from bdaq53.analysis import analysis_utils as au

log = logger.setup_derived_logger('BDAQ EUDAQ Producer')


scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'scan_timeout': False,  # timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,  # number of maximum received triggers after stopping readout, if False no limit on received trigger

    'trigger_latency': 100,  # latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 57,  # trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,  # length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 210,  # length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Should be adjusted for longer trigger length.

    # Trigger configuration
    'bench': {'TLU': {
        'TRIGGER_MODE': 3,             # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 0,            # Selecting trigger input: HitOR (1), disabled (0)
        'TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES': 20   # TLU trigger minimum length in TLU clock cycles. Change default here in order to ignore glitches from buggy original TLU FW.
    }
    }
}


BOARD_ID_TO_IP = {0: '192.168.10.12',
                  1: '192.168.11.13',
                  2: '192.168.12.14',
                  3: '192.168.13.15'}


class EudaqScan(scan_ext_trigger.ExtTriggerScan):
    scan_id = 'eudaq_scan'

    min_spec_occupancy = False  # needed for handle data function of ext. trigger scan

    def _configure(self, callback=None, **_):
        super(EudaqScan, self)._configure(**_)
        # Set callback in configure step since callback is needed for every producer (chip)
        self.callback = callback[self.chip.receiver]
        self.last_readout_data = np.array([], dtype=np.uint32)
        self.last_trigger = 0

    def handle_data(self, data_tuple, receiver=None):
        '''
        Called on every readout (a few Hz)
        Sends data per event by checking for the trigger word that comes first.
        '''
        super(EudaqScan, self).handle_data(data_tuple, receiver)

        raw_data = data_tuple[0]

        if np.any(self.last_readout_data):  # no last readout data for first readout
            actual_data = np.concatenate((self.last_readout_data, raw_data))
        else:
            actual_data = raw_data

        trg_idx = np.where(actual_data & au.TRIGGER_HEADER > 0)[0]
        trigger_data = np.split(actual_data, trg_idx)

        # Send data of each trigger
        for dat in trigger_data[:-1]:
            glitch_detected = False
            # Split can return empty data, thus do not return send empty data
            # Otherwise fragile EUDAQ will fail. It is based on very simple event counting only
            if np.any(dat):
                trigger = dat[0] & au.TRG_MASK
                if self.last_trigger > 0 and trigger != self.last_trigger + 1:    # Trigger number jump
                    if (self.last_trigger + 1) == (trigger >> 1):  # Measured trigger number is exactly shifted by 1 bit, due to glitch
                        glitch_detected = True
                    else:
                        self.log.warning('Expected != Measured trigger number: %d != %d', self.last_trigger + 1, trigger)
                self.last_trigger = trigger if not glitch_detected else (trigger >> 1)
                self.callback(dat)

        self.last_readout_data = trigger_data[-1]

    def _scan(self, start_column=0, stop_column=400, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, fraction=0.99, use_tdc=False, **_):
        super(EudaqScan, self)._scan(start_column, stop_column, scan_timeout, max_triggers, min_spec_occupancy, fraction, use_tdc)
        # Send remaining data after stopped readout
        self.callback(self.last_readout_data)


def replay_triggered_data(data_file, real_time=True):
    '''
    Yield raw data for every trigger.

    Parameters
    ----------
    real_time: boolean
        Delays return if replay is too fast to keep
        replay speed at original data taking speed.
    '''

    with tb.open_file(data_file, mode="r") as in_file_h5:
        meta_data = in_file_h5.root.meta_data[:]
        raw_data = in_file_h5.root.raw_data
        n_readouts = meta_data.shape[0]

        last_readout_time = time.time()

        # Leftover data from last readout
        last_readout_data = np.array([], dtype=np.uint32)
        last_trigger = -1

        for i in tqdm(range(n_readouts)):
            # Raw data indeces of readout
            i_start = meta_data['index_start'][i]
            i_stop = meta_data['index_stop'][i]

            t_start = meta_data[i]['timestamp_start']

            # Determine replay delays
            if i == 0:  # Initialize on first readout
                last_timestamp_start = t_start
            now = time.time()
            delay = now - last_readout_time
            additional_delay = t_start - last_timestamp_start - delay
            if real_time and additional_delay > 0:
                # Wait if send too fast, especially needed when readout was
                # stopped during data taking (e.g. for mask shifting)
                time.sleep(additional_delay)
            last_readout_time = time.time()
            last_timestamp_start = t_start

            actual_data = np.concatenate((last_readout_data, raw_data[i_start:i_stop]))
            trg_idx = np.where(actual_data & au.TRIGGER_HEADER > 0)[0]
            trigger_data = np.split(actual_data, trg_idx)

            # Special case: last readout, do not keep data for next readout
            if i == n_readouts - 1:
                trigger_data += [trigger_data[-1]]

            for dat in trigger_data[:-1]:
                if np.any(dat):
                    trigger = dat[0] & au.TRG_MASK
                    if last_trigger > 0 and trigger != last_trigger + 1:
                        log.warning('Expected != Measured trigger number: %d != %d', last_trigger + 1, trigger)
                    last_trigger = dat[0] & au.TRG_MASK

                yield dat

            last_readout_data = trigger_data[-1]


def main():
    # Scan start procedure
    def eudaq_start():
        scan.init()
        callbacks = {}
        for _ in scan.iterate_chips():  # Set callbacks per producer (chip)
            callbacks[scan.chip.receiver] = pp.SendEvent
        scan.scan_config['callback'] = callbacks
        scan.start()

    # Parse program arguments
    description = "Start EUDAQ producer for bdaq53"
    parser = argparse.ArgumentParser(prog='bdaq53_eudaq',
                                     description=description,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-b', '--board_id', type=int, default=0, help="The board identifier to be used with this producer")
    parser.add_argument('address', metavar='address',
                        help='Destination address',
                        default='tcp://localhost:44000',
                        nargs='?')
    parser.add_argument('--path', type=str,
                        help='Absolute path of your eudaq installation')
    parser.add_argument('--replay', type=str,
                        help='Raw data file to replay for testing')
    parser.add_argument('--delay', type=float,
                        help='Additional delay when replaying data in seconds')
    parser.add_argument('-f', '--testbench_file',
                        type=str,
                        nargs='?',
                        help='Path to scan parameter file. If none, the default configuration is used.',
                        metavar='testbench_file')
    args = parser.parse_args()

    if args.testbench_file:
        testbench_file = args.testbench_file
    else:
        testbench_file = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'testbench.yaml')
    with open(testbench_file) as f:
        bench_config = yaml.full_load(f)

    log.info('Using testbench file: ' + testbench_file + '\n')

    # Import EUDAQ python wrapper with error handling
    try:
        from PyEUDAQWrapper import PyProducer
    except ImportError:
        if not args.path:
            log.error('Cannot find PyEUDAQWrapper! Please specify the path of your EUDAQ installation!')
            return
        else:
            wrapper_path = os.path.join(args.path, 'python/')
            sys.path.append(os.path.join(args.path, 'python/'))
            try:
                from PyEUDAQWrapper import PyProducer
            except ImportError:
                log.error('Cannot find PyEUDAQWrapper in %s', wrapper_path)
                return

    log.info('Connect to %s', args.address)

    if args.replay:
        if os.path.isfile(args.replay):
            log.info('Replay %s', args.replay)
        else:
            log.error('Cannot open %s for replay!', args.replay)
    delay = args.delay if args.delay else 0.

    proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    with open(os.path.join(proj_dir, 'system' + os.sep + 'bdaq53.yaml')) as conf_f:
        conf = yaml.full_load(conf_f)

    conf['transfer_layer'][0]['init']['ip'] = BOARD_ID_TO_IP[args.board_id]

    # Initialize producers (one for each chip defined in testbench.yaml)
    producers = {}
    if args.replay:
        producers['REPLAY_PRODUCER'] = PyProducer('bdaq53a', args.address, args.board_id)
        log.info('Use board id %s', args.board_id)
    else:
        # FIXME: Only working for one chip per module.
        for i, k in enumerate(bench_config['modules'].items()):  # Loop over modules
            module_name = k[0]
            pp = PyProducer('bdaq53a', args.address, args.board_id)  # TODO: bdaq53a-module_0 not working, since converter plugin name does not match
            producers[module_name] = pp
            log.info('Use board id %s', args.board_id)

    # Start state mashine, keep connection until termination of euRun
    while not any([pp.Error for _, pp in producers.items()]) and not any([pp.Terminating for _, pp in producers.items()]):
        # Wait for configure cmd from RunControl
        while not any([pp.Configuring for _, pp in producers.items()]) and not any([pp.Terminating for _, pp in producers.items()]):
            if all([pp.StartingRun for _, pp in producers.items()]):
                break
            time.sleep(0.1)

        # Check if configuration received
        for key, pp in producers.items():
            if pp.Configuring:
                log.info('Configuring... [' + key + ']')
                time.sleep(3)

                if not args.replay:
                    pass
                    # FIXME: use proper configuration step, issue #121
                pp.Configuring = True

        # Check for start of run cmd from RunControl
        for _, pp in producers.items():
            while not pp.StartingRun and not pp.Terminating:
                if pp.Configuring:
                    break
                time.sleep(0.1)

        # Check if we are starting:
        if all([pp.StartingRun for _, pp in producers.items()]):
            # Since buggy TLU/EUDAQ needs very long to start and starting DAQ before
            # TLU has not finished is bad (sometimes introduces artificial trigger) just wait here 5 seconds.
            time.sleep(5)
            log.info('Starting run...')

            if not args.replay:
                # Setup external trigger scan
                scan = EudaqScan(bdaq_conf=conf, bench_config=testbench_file, scan_config=scan_configuration)
                thread = threading.Thread(target=eudaq_start, name='EUDAQThread')
                thread.start()
                for _, pp in producers.items():
                    pp.StartingRun = True   # set status and send BORE
                # Run loop for normal data taking
                while True:
                    if any([(pp.Error or pp.Terminating) for _, pp in producers.items()]):
                        if any([pp.Error for _, pp in producers.items()]):
                            log.warning('Received error')
                        log.info('Stopping run...')
                        scan.stop_scan.set()
                        thread.join()
                        scan.close()
                        break
                    if any([pp.StoppingRun for _, pp in producers.items()]):
                        log.info('Stopping run...')
                        scan.stop_scan.set()
                        thread.join()
                        scan.close()
                        break
                    time.sleep(0.1)
            else:  # Run loop to replay data
                pp = producers['REPLAY_PRODUCER']
                pp.StartingRun = True  # set status and send BORE
                for raw_data in replay_triggered_data(data_file=args.replay):
                    pp.SendEvent(raw_data)
                    if pp.Error or pp.Terminating:
                        break
                    if pp.StoppingRun:
                        break
                    time.sleep(delay)

            for _, pp in producers.items():
                # Abort conditions
                if pp.Error or pp.Terminating:
                    pp.StoppingRun = False  # Set status and send EORE
                # Check if the run is stopping regularly
                if pp.StoppingRun:
                    pp.StoppingRun = True  # Set status and send EORE

        # Back to check for configured + start run state
        time.sleep(0.1)


if __name__ == "__main__":
    # When run in development environment, eudaq path can be added with:
    sys.path.append('/faust/user/mstandke/RD53_env/RD53B_SIM/bdaq53/bdaq53/tests/eudaq/python/')
    main()
