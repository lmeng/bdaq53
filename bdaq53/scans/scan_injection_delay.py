#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script scans over different fine delays at constant injection charge
    to find the effective injection for a given charge of the enabled pixels.
'''
import numba
import numpy as np
import tables as tb
from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject, get_scan_loop_mask_steps
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

import bdaq53.analysis.analysis_utils as au

scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'VCAL_MED': 500,
    'VCAL_HIGH': 4000,
}


class InjDelayScan(ScanBase):
    scan_id = 'injection_delay_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

    def _scan(self, n_injections=100, VCAL_MED=500, VCAL_HIGH=1000, **_):
        '''
        injection delay scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH : int
            VCAL_HIGH DAC value.
        '''
        pbar = tqdm(total=get_scan_loop_mask_steps(scan=self) * 16, unit=' Mask steps')
        for scan_param_id in range(16):
            self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED, fine_delay=scan_param_id)
            self.store_scan_par_values(scan_param_id=scan_param_id, fine_delay=scan_param_id)
            with self.readout(scan_param_id=scan_param_id):
                shift_and_inject(scan=self, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id, cache=True)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        self.configuration['bench']['analysis']['store_hits'] = False
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        with tb.open_file(self.output_filename + '_interpreted.h5', "r+") as io_file:
            scan_config = au.ConfigDict(io_file.root.configuration_in.scan.scan_config[:])
            start_column = scan_config.get('start_column', 0)
            stop_column = scan_config.get('stop_column', 400)
            start_row = scan_config.get('start_row', 0)
            stop_row = scan_config.get('stop_row', 192)

            disable_mask = io_file.root.configuration_in.chip.use_pixel[:]
            enable_mask = np.zeros((400, 192), bool)
            enable_mask[start_column:stop_column, start_row:stop_row] = True
            enable_mask = np.logical_and(enable_mask, disable_mask)

            # Import BCID array
            if start_column in range(0, 128):
                bcid_in = io_file.root.HistTrigID[:, :, :, :]

            else:
                bcid_in = io_file.root.HistRelBCID[:, :, :, :]
            bcid_all = bcid_in[:, :, :, 0].reshape((192 * 400, -1)).T

            # Sort BCID array in plottable order
            for i in range(1, 32, 1):
                bcid_new = bcid_in[:, :, :, i].reshape((192 * 400, -1)).T
                bcid_all = np.concatenate((bcid_new, bcid_all), axis=0)

            # Make a second data set for zoomed in s-curve plot
            scan_params = np.arange(32 * 16 - 12 * 16, 32 * 16 - 9 * 16)
            bcid_small = bcid_all[np.min(scan_params):np.max(scan_params), :]

            delay_map = np.zeros(shape=(400, 192), dtype=np.uint32)
            delay_mask_raw = make_fine_delay_mask(bcid_all.T, delay_map, max_occ=scan_config['n_injections'])
            delay_mask = avg_core_cols(np.ma.masked_array(delay_mask_raw, ~enable_mask), start_column, stop_column)

            self.chip.masks['injection_delay'][start_column:stop_column, start_row:stop_row] = delay_mask[start_column:stop_column, start_row:stop_row]

            io_file.create_carray(io_file.root, name='HistSCurve', title='Scurve Data', obj=bcid_all,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            io_file.create_carray(io_file.root, name='DelayMap', title='Delay Map', obj=delay_mask_raw,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file, save_png=False) as p:
                p.n_failed_scurves = False
                p.create_standard_plots()
                electron_axis = False

                signal_width = get_signal_width(bcid_all.T)
                p._plot_scurves(scurves=bcid_all,
                                scan_parameters=np.arange(bcid_all.shape[0] + 1),
                                electron_axis=electron_axis,
                                scan_parameter_name='Injection Delay [LSB]')

                p._plot_scurves(scurves=bcid_small,
                                scan_parameters=scan_params,
                                electron_axis=electron_axis,
                                scan_parameter_name='Injection Delay [LSB]',
                                suffix='scurve_small')

                p._plot_distribution(delay_mask[delay_mask != 0 & ~p.enable_mask].T,
                                     plot_range=scan_params,
                                     electron_axis=electron_axis,
                                     x_axis_title='Injection Delay [LSB]',
                                     title='Injection Delay Distribution of Enabled Pixels',
                                     print_failed_fits=False,
                                     suffix='delay_distribution')

                p._plot_occupancy(hist=np.ma.masked_array(delay_mask_raw, p.enable_mask).T,
                                  electron_axis=electron_axis,
                                  z_label='Injection Delay [LSB]',
                                  title='Injection Delay Map',
                                  use_electron_offset=False,
                                  show_sum=False,
                                  z_min=np.min(np.ma.masked_array(delay_mask_raw[delay_mask_raw != 0],
                                                                  p.enable_mask[delay_mask_raw != 0])),
                                  z_max=np.max(np.ma.masked_array(delay_mask_raw, p.enable_mask)),
                                  suffix='injection_delay_map')

                p._plot_occupancy(hist=np.ma.masked_array(delay_mask, p.enable_mask).T,
                                  electron_axis=electron_axis,
                                  z_label='Injection Delay [LSB]',
                                  title='Injection Delay Mask',
                                  use_electron_offset=False,
                                  show_sum=False,
                                  z_min=np.min(np.ma.masked_array(delay_mask, p.enable_mask)),
                                  z_max=np.max(np.ma.masked_array(delay_mask, p.enable_mask)),
                                  suffix='injection_delay_mask')

                p._plot_distribution(signal_width[signal_width != 0],
                                     plot_range=np.arange(0, 32),
                                     electron_axis=electron_axis,
                                     x_axis_title='Finedelay [LSB]',
                                     title='Signal Width Distribution of Enabled Pixels',
                                     print_failed_fits=False,
                                     suffix='width_distribution')


# Calculate ideal delay for each pixel individually
@numba.njit
def make_fine_delay_mask(scurves, delay_map, max_occ=100):
    '''
    Generates an injection delay map based on the collected by injection_delay_scan

    Parameters
    ----------
        scurves : array [76800,16]
            Scurves which show the hit occupancy in bcid 10, as a function of the fine delay
        delay_map : array [400,192]
            empty array, to write the delay map data into
        max_occ : int [0:2**16]
            the amount of injections into each pixel

    Returns
    -------
        delay_map : array [400,192]
            array filled with ideal delay values for each pixel
    '''

    for index in range(len(scurves)):
        entries = scurves[index]
        for index2 in range(len(entries)):
            if entries[index2] >= max_occ:
                left_border = index2
                break
        for index2 in range(len(entries), -1, -1):
            if entries[index2] >= max_occ:
                right_border = index2
                break
        delay_map[int(index / 192), index % 192] = int((left_border + right_border) / 2)
    return delay_map


def avg_core_cols(delay_map, start, stop):
    '''
    Average the delay setting over all core columns and choose optimum
    '''
    delay_map_out = np.zeros(shape=(400, 192), dtype=np.uint32)
    for corecols in range(start, stop, 8):
        delay_map_out[corecols:corecols + 8, :] = (int(np.mean(delay_map[corecols:corecols + 8, :])))
    return delay_map_out


def get_signal_width(scurves, n_injections=100):
    '''
    Get left rising edge and right falling edge of plot and calculate width (should always be around 15)
    '''
    signal_width = np.zeros((400 * 192))
    for i in range(400 * 192):
        signal_width[i] = np.count_nonzero(scurves[i] == n_injections)
    return signal_width


if __name__ == "__main__":
    with InjDelayScan(scan_config=scan_configuration) as scan:
        scan.start()
