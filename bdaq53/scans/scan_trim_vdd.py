import time

import tables as tb
from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis.convert_to_json import JSONConverter
from bdaq53.analysis import plotting


scan_configuration = {
    'periphery_device': 'Multimeter',
    'power_settings': {
        'Iin': 5.88,
        'Vlim': 2.0
    },
    'VDDA_range': (0, 15),
    'VDDD_range': (0, 15)
}


class TrimTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=0)
    trim = tb.UInt32Col(pos=1)
    voltage = tb.Float32Col(pos=2)
    voltage_gnd = tb.Float32Col(pos=3)


class TrimTableAnalysed(tb.IsDescription):
    trim = tb.UInt32Col(pos=0)
    voltage = tb.Float32Col(pos=1)


class TrimScan(ScanBase):
    scan_id = 'trim_scan'

    def _configure(self, **_):
        if not self.periphery.enabled:
            raise IOError('Periphery has to be enabled and a multimeter configured correctly!')

    def _scan(self, periphery_device='Multimeter', VDDA_range=None, VDDD_range=None, **_):
        if VDDA_range is not None:
            value_range = range(VDDA_range[0], VDDA_range[1] + 1)

            self.data.trim_data_table_vdda = self.h5_file.create_table(self.h5_file.root, name='trim_vdda', title='Trim_VDDA', description=TrimTable)
            row = self.data.trim_data_table_vdda.row

            for scan_param_id, trim in tqdm(enumerate(value_range), total=len(value_range), unit=' Values'):
                with self.readout(scan_param_id=scan_param_id):
                    vref_a_trim = trim
                    vref_d_trim = self.chip.configuration['trim'].get('VREF_D_TRIM', 15)
                    self.chip.registers['VOLTAGE_TRIM'].write(int('00' + format(vref_a_trim, '04b') + format(vref_d_trim, '04b'), 2), verify=True)
                    time.sleep(0.2)  # Sleep to ensure system has settled to the new voltage.

                    voltage, voltage_gnd = self._get_voltage_with_gnd(MUX='VDDA_HALF', periphery_device=periphery_device)

                    row['trim'] = trim
                    row['voltage'] = voltage
                    row['voltage_gnd'] = voltage_gnd
                    row['scan_param_id'] = scan_param_id
                    row.append()
                    self.data.trim_data_table_vdda.flush()

        if VDDD_range is not None:
            value_range = range(VDDD_range[0], VDDD_range[1] + 1)

            self.data.trim_data_table_vddd = self.h5_file.create_table(self.h5_file.root, name='trim_vddd', title='Trim_VDDD', description=TrimTable)
            row = self.data.trim_data_table_vddd.row

            for scan_param_id, trim in tqdm(enumerate(value_range), total=len(value_range), unit=' Values'):
                with self.readout(scan_param_id=scan_param_id):
                    vref_a_trim = self.chip.configuration['trim'].get('VREF_A_TRIM', 15)
                    vref_d_trim = trim
                    self.chip.registers['VOLTAGE_TRIM'].write(int('00' + format(vref_a_trim, '04b') + format(vref_d_trim, '04b'), 2), verify=False)
                    time.sleep(0.2)  # Sleep to ensure system has settled to the new voltage.

                    voltage, voltage_gnd = self._get_voltage_with_gnd(MUX='VDDD_HALF', periphery_device=periphery_device)

                    row['trim'] = trim
                    row['voltage'] = voltage
                    row['voltage_gnd'] = voltage_gnd
                    row['scan_param_id'] = scan_param_id
                    row.append()
                    self.data.trim_data_table_vdda.flush()

        self.log.success('Scan finished')

    def _get_voltage_with_gnd(self, MUX='VDDA_HALF', measure='voltage', periphery_device='Multimeter'):
        self.chip.set_mux(MUX, measure)
        time.sleep(0.2)
        _voltage = self.periphery.aux_devices[periphery_device].get_voltage()
        self.chip.set_mux('GNDA30', measure)
        time.sleep(0.2)
        _voltage_gnd = self.periphery.aux_devices[periphery_device].get_voltage()

        return _voltage, _voltage_gnd

    def _analyze(self, raw_data_file=None, analyzed_data_file=None, write_trim=True):
        if not raw_data_file:
            raw_data_file = self.output_filename + '.h5'
        if not analyzed_data_file:
            analyzed_data_file = self.output_filename + '_interpreted.h5'

        diffs_vdda = {}
        diffs_vddd = {}
        with tb.open_file(raw_data_file, 'r') as in_file:
            trim_vdda = in_file.root.trim_vdda[:]
            trim_vddd = in_file.root.trim_vddd[:]

            with tb.open_file(analyzed_data_file, 'w', title=in_file.title) as out_file:
                out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)

                output_vdda = out_file.create_table(out_file.root, name='trim_vdda', title='VDDA_Trim', description=TrimTableAnalysed)
                row_vdda = output_vdda.row

                for row in trim_vdda:
                    trim = row[1]
                    vdda = (row[2] - row[3]) * 2

                    diffs_vdda[trim] = abs(vdda - 1.2)

                    row_vdda['trim'] = trim
                    row_vdda['voltage'] = vdda

                    row_vdda.append()
                    output_vdda.flush()

                output_vddd = out_file.create_table(out_file.root, name='trim_vddd', title='VDDD_Trim', description=TrimTableAnalysed)
                row_vddd = output_vddd.row

                for row in trim_vddd:
                    trim = row[1]
                    vddd = (row[2] - row[3]) * 2

                    diffs_vddd[trim] = abs(vddd - 1.2)

                    row_vddd['trim'] = trim
                    row_vddd['voltage'] = vddd

                    row_vddd.append()
                    output_vddd.flush()

                opt_trim_vdda = min(diffs_vdda, key=diffs_vdda.get)
                opt_trim_vddd = min(diffs_vddd, key=diffs_vddd.get)

                self.log.info(self.chip_settings['chip_sn'].upper() + ': optimal trim values VDDA/D: %d / %d' % (opt_trim_vdda, opt_trim_vddd))

                if write_trim:
                    trim_vdda_old = self.chip.configuration['trim']['VREF_A_TRIM']
                    trim_vddd_old = self.chip.configuration['trim']['VREF_D_TRIM']
                    self.chip.configuration['trim']['VREF_A_TRIM'] = opt_trim_vdda
                    self.log.info(self.chip_settings['chip_sn'].upper() + ": VDDA Trim changed: %d -> %d" % (trim_vdda_old, opt_trim_vdda))
                    self.chip.configuration['trim']['VREF_D_TRIM'] = opt_trim_vddd
                    self.log.info(self.chip_settings['chip_sn'].upper() + ": VDDD Trim changed: %d -> %d" % (trim_vddd_old, opt_trim_vddd))

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=analyzed_data_file) as p:
                p.create_standard_plots()

        conv = JSONConverter(data_file=raw_data_file, outpath=self.working_dir)
        conv.convert_h5_to_json()

        return opt_trim_vddd, opt_trim_vdda


if __name__ == "__main__":
    with TrimScan(scan_config=scan_configuration) as scan:
        scan.start()
