#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan injects a digital pulse into enabled pixels to test the digital part of the chip.

    The scan is not aborted when issues occur. Error handling routines are added to recover from bad chip states
    and to re-create the communication with the chip. Checks on RX issues can be enabled/disbaled using scan configuration.
    This makes this scan much more robust for missbehaving chips (e.g. as during SEU measurements).

    Only data that is collected during no link issues are stored, to allow for data analysis.

    Note:
    In case of ITkPixV1, it is observed that after a lost of sync exactly two pixels fire in all events. All other pixels have the expected amount of hits.
    This is mostlikely a raw data interpretation bug or pixel registers are reset, while the others are not.
'''

from tqdm import tqdm
from time import sleep
import numpy as np
import os

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


scan_configuration = {
    'start_column': 0,  # first column to scan
    'stop_column': 400,  # column to stop the scan, this column is excluded from the scan
    'start_row': 0,  # first row to scan
    'stop_row': 384,  # row to stop the scan. This row is excluded from the scan
    'n_injections': 100,  # number of injections

    # Configure checks on RX. If True error is checked, else error is ommited.
    'check_no_rx_sync': True,  # Check RX Sync
    'check_hard_errors': True,  # Check hard errors
    'check_fifo_discard_errors': True,  # Check FIFO discard errors
    'check_soft_errors': False  # Check soft errors
}


class DigitalScanRobust(ScanBase):
    scan_id = 'robust_digital_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=384, **_):
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update(force=True)
        self.chip.setup_digital_injection()
        self.configuration['bench']['general']['abort_on_rx_error'] = False  # do not abort on RX errors
        self.data_storage = []

    def _scan(self, n_injections=100, check_no_rx_sync=True, check_hard_errors=True, check_fifo_discard_errors=True, check_soft_errors=False, **_):
        pbar = tqdm(total=self.chip.masks.get_mask_steps(), unit=' Mask steps')

        with self.readout(callback=self.data_into_memory):
            for fe, _ in self.chip.masks.shift(masks=['enable', 'injection']):
                while True:
                    if not fe == 'skipped':
                        self.chip.inject_digital(repetitions=n_injections)
                    # Wait for data
                    sleep(0.1)
                    while not self.fifo_readout._data_queue.empty():
                        pass
                    sleep(0.1)
                    if self.is_rx_ok(check_no_rx_sync, check_hard_errors, check_fifo_discard_errors, check_soft_errors):
                        self.write_data()
                        break
                pbar.update(1)
        self.write_data()  # Write last data

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

    def write_data(self):  # write to disk
        for data_tuple in self.data_storage:
            self.handle_data(data_tuple, receiver=None)
        self.data_storage = []  # reset storage

    def data_into_memory(self, data_tuple, receiver):  # called at each readout of FIFO with 20 Hz
        self.data_storage.append(data_tuple)

    def reset_receiver(self):  # Receiver reset
        self.bdaq.rx_channels[self.chip.receiver].reset_counters()
        self.bdaq.rx_channels[self.chip.receiver].reset_logic()
        self.bdaq.rx_channels[self.chip.receiver].reset()

    def is_rx_ok(self, check_no_rx_sync, check_hard_errors, check_fifo_discard_errors, check_soft_errors):
        ''' Returns false if RX issues occured and tries to regain communication '''

        sync = self.fifo_readout.get_rx_sync_status(rx_channel=self.chip.receiver)
        hard_error_count = self.fifo_readout.get_rx_hard_error_count(rx_channel=self.chip.receiver)
        fifo_discard_count = self.fifo_readout.get_rx_fifo_discard_count(rx_channel=self.chip.receiver)
        soft_error_count = self.fifo_readout.get_rx_soft_error_count(rx_channel=self.chip.receiver)

        if not (sync and check_no_rx_sync) or (np.any(hard_error_count) and check_hard_errors) or (np.any(fifo_discard_count) and check_fifo_discard_errors) or (np.any(soft_error_count) and check_soft_errors):
            self.log.warning('RX errors\nRX SYNC (%s): %i\nHARD ERRORS (%s): %i\nSOFT ERRORS (%s): %i\nFIFO DISCARDS (%s): %i',
                             'checked' if check_no_rx_sync else 'ignored', sync,
                             'checked' if check_hard_errors else 'ignored', hard_error_count,
                             'checked' if check_soft_errors else 'ignored', soft_error_count,
                             'checked' if check_fifo_discard_errors else 'ignored', fifo_discard_count)

            # Wait and check if RX comes back
            self.log.info('Checking if RX sync comes back...')
            sleep(0.1)
            sync = self.fifo_readout.get_rx_sync_status(rx_channel=self.chip.receiver)

            # Do reset procedure in case RX sync did not come back.
            if not (sync and check_no_rx_sync):
                # First: Try to reset receiver and check if RX sync is back
                self.log.info('Checking if RX sync comes back after receiver reset')
                self.reset_receiver()  # Reset receiver (counters, logic, ...)
                sleep(0.1)  # Wait before checking for RX sync
                sync = self.fifo_readout.get_rx_sync_status(rx_channel=self.chip.receiver)

                # Second: Try to init communication until RX sync is back, in case RX reset did not help.
                while not sync:
                    self.log.info('Trying to establish communication again...')
                    try:  # Do not abort on no AuroraSync
                        self.log.info('Sending ECR...')
                        self.chip.write_ecr()  # Send ECR
                        sleep(0.1)
                        sync = self.fifo_readout.get_rx_sync_status(rx_channel=self.chip.receiver)
                        sleep(0.1)
                        self.log.info('Resetting receiver...')
                        self.reset_receiver()  # Reset receiver (counters, logic, ...)
                        sleep(0.1)
                        sync = self.fifo_readout.get_rx_sync_status(rx_channel=self.chip.receiver)
                        sleep(0.1)
                        if not sync:
                            self.log.info('Init communication...')
                            self.chip.init_communication()  # Init communication
                            sleep(0.1)
                            sync = self.fifo_readout.get_rx_sync_status(rx_channel=self.chip.receiver)
                    except RuntimeError:  # No AuroraSync gives RuntimeError
                        sync = 0

            # Re-configure digital injection
            self.chip.setup_digital_injection()
            self.data_storage = []  # reset storage
            return False
        return True


if __name__ == '__main__':
    iteration = 1
    while True:
        print('==================================================')
        print('========== Starting Iteration number ' + str(iteration) + ' ===========')
        print('==================================================')
        with DigitalScanRobust(scan_config=scan_configuration) as scan:
            scan.start()
            os.system('mv output_data/module_0/chip_0/chip_0.log output_data/module_0/chip_0/' + scan.run_name + '.log')
        iteration = iteration + 1
