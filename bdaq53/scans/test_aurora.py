from bdaq53.scans.scan_digital import DigitalScan
import yaml
import os
import bdaq53
import tables as tb
import numpy as np
import unittest
import logging

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config_file = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))

scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 384,
    'n_injections': 100
}


class TestAuroraLanes(unittest.TestCase):

    def test_aurora_lanes(self):
        logging.info("Please define ITkPixV1 as module 0 and connect the ITkPixV1 to the Display Port closest to the button pad (DP0)")
        with open(bench_config_file) as f:
            bench_config = yaml.full_load(f)
        start_column = scan_configuration.get('start_column', 0)
        stop_column = scan_configuration.get('stop_column', 400)
        start_row = scan_configuration.get('start_row', 0)
        stop_row = scan_configuration.get('stop_row', 384)
        n_injections = scan_configuration.get('n_injections', 100)
        scan_configuration['lane_test'] = True
        expected_hits = (stop_column - start_column) * (stop_row - start_row) * n_injections
        for n in range(4):
            bench_config['modules']['module_0']['chip_0']['receiver'] = 'rx' + str(n)
            with DigitalScan(scan_config=scan_configuration, bench_config=bench_config) as scan:
                scan.start()
                output_filename = scan.output_filename
            with tb.open_file(output_filename + '_interpreted.h5') as in_file:
                hits = np.sum(in_file.root.HistOcc[start_column:stop_column, start_row:stop_row])
            self.assertEqual(int(hits), expected_hits, 'Incorrect number of hits has been recorded on lane: ' + str(n))


if __name__ == '__main__':
    unittest.main()
