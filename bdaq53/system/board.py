#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from basil.HL.RegisterHardwareLayer import RegisterHardwareLayer


class BDAQ53Board(RegisterHardwareLayer):
    ''' BDAQ readout board configuration
    '''

    _registers = {'RESET': {'descr': {'addr': 0, 'size': 8, 'properties': ['writeonly']}},
                  'VERSION': {'descr': {'addr': 0, 'size': 8, 'properties': ['ro']}},
                  'VERSION_MINOR': {'descr': {'addr': 1, 'size': 8, 'properties': ['ro']}},
                  'VERSION_MAJOR': {'descr': {'addr': 2, 'size': 8, 'properties': ['ro']}},
                  'BOARD_VERSION': {'descr': {'addr': 3, 'size': 8, 'properties': ['ro']}},
                  'N_AURORA_RX': {'descr': {'addr': 4, 'size': 8, 'properties': ['ro']}},
                  'OPTIONS': {'descr': {'addr': 5, 'size': 8, 'properties': ['ro']}},
                  'AURORA_RX_640M': {'descr': {'addr': 5, 'size': 1, 'offset': 0, 'properties': ['ro']}},
                  'SI570_IS_CONFIGURED': {'descr': {'addr': 5, 'size': 1, 'offset': 1, 'properties': ['rw']}},
                  'BYPASS_MODE': {'descr': {'addr': 5, 'size': 1, 'offset': 2, 'properties': ['rw']}},
                  'BYPASS_CMD_CLK_EN': {'descr': {'addr': 5, 'size': 1, 'offset': 3, 'properties': ['rw']}},
                  'BYPASS_EXT_SER_CLK_EN': {'descr': {'addr': 5, 'size': 1, 'offset': 4, 'properties': ['rw']}},
                  'CONNECTOR': {'descr': {'addr': 6, 'size': 8, 'properties': ['ro']}}
                  }

    _require_version = "==1"

    ''' Map hardware IDs for board identification '''
    hw_map = {
        0: 'SIMULATION',
        1: 'BDAQ53',
        2: 'USBPix3',
        3: 'KC705',
        4: 'GENESYS 2'
    }

    hw_con_map = {
        0: 'SMA',
        1: 'FMC_LPC',
        2: 'FMC_HPC',
        3: 'Displayport',
        4: 'Cocotb'
    }

    def __init__(self, intf, conf):
        super(BDAQ53Board, self).__init__(intf, conf)

    def init(self):
        super(BDAQ53Board, self).init()

    def reset(self):
        self.RESET = 0

    def enable_bypass_clocks(self):
        self.BYPASS_EXT_SER_CLK_EN = 1
        self.BYPASS_CMD_CLK_EN = 1

    def disable_bypass_clocks(self):
        self.BYPASS_EXT_SER_CLK_EN = 0
        self.BYPASS_CMD_CLK_EN = 0

    def get_daq_version(self):
        fw_version = str('%s.%s' % (self.VERSION_MAJOR, self.VERSION_MINOR))
        board_version = self.hw_map[self.BOARD_VERSION]
        num_rx_channels = self.N_AURORA_RX
        board_options = self.AURORA_RX_640M
        connector_version = self.hw_con_map[self.CONNECTOR]

        return fw_version, board_version, board_options, connector_version, num_rx_channels
