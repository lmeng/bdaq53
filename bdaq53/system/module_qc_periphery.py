import time

from pathlib import Path
import pyvisa

import zmq
import os

from yaml import safe_load
from six import string_types

from bdaq53 import periphery_file
from bdaq53.system import PERIPHERY_PORT

from threading import Thread, Event
from queue import Queue
from basil.dut import Dut
import json
import psutil
import logging
import traceback
import signal


def queryIdentification(rm, resource, baud_rate, read_termination="\n", write_termination="\n", timeout=1000 * 5, verbose=False):
    inst = rm.open_resource(resource)
    inst.timeout = timeout
    inst.baud_rate = baud_rate
    inst.read_termination = read_termination
    inst.write_termination = write_termination

    return inst.query("*IDN?", delay=0.5)


def findUSBBinds(rm, log, instruments, binds_to_skip=[], memorized_binds=[], timeout=1000 * 4, verbose=False):
    """
    Finds the USB bind for each instrument in the given list of instruments.

    Args:
        rm (pyvisa.ResourceManager): The pyvisa resource manager.
        log (logging.Logger): The logger object for logging messages.
        instruments (list): List of dictionaries representing the instruments.
            Each dictionary should contain the following keys:
            - 'baud_rate': The baud rate for the instrument.
            - 'read_termination': The read termination character for the instrument.
            - 'write_termination' (optional): The write termination character for the instrument.
            - 'identification': The identification string for the instrument.
        binds_to_skip (list, optional): List of binds to skip during the search.
            Defaults to an empty list.
        memorized_binds (list, optional): List of memorized binds.
            Defaults to an empty list.
        timeout (int, optional): Timeout value in milliseconds.
            Defaults to 4000.
        verbose (bool, optional): Flag indicating whether to log verbose messages.
            Defaults to False.

    Returns:
        dict: A dictionary mapping the identification strings of the instruments
            to their corresponding USB binds.
    """
    skip_binds = binds_to_skip + [
        str(Path(f"/dev/{bind}").resolve()) for bind in binds_to_skip
    ]

    results = {}

    for instrument in instruments:
        for res in rm.list_resources():
            if "USB" not in res:  # Only search for USB devices
                continue

            if any(bind in res for bind in skip_binds):
                if verbose:
                    log.info(f"Skipping USB bind {res}")
                continue

            try:
                if verbose:
                    log.info(f"Trying {res} with baud rate {instrument['baud_rate']}")

                if any(res in bind for bind in memorized_binds):
                    if verbose:
                        log.info(f"Found memorized bind {res}")
                    result = memorized_binds[res]
                else:
                    result = queryIdentification(rm, res, instrument['baud_rate'], instrument['read_termination'], instrument['write_termination'], timeout=timeout, verbose=verbose)

                    memorized_binds.append({res, result})

                    if verbose:
                        log.info(f"Found {result.strip()}")

                if result.lower().strip() in [inst["identification"].lower().strip() for inst in instruments]:
                    substring = res.split("/")[2].split("::")[0]

                    log.info(f"Matched instrument {instrument['identification']} to /dev/{str(substring)}")
                    skip_binds.append(f"/dev/{str(substring)}")

                    results[result.lower().strip()] = f"/dev/{str(substring)}"

                    if len(results) == len(instruments):
                        return results

            except pyvisa.VisaIOError:
                pass

    return results


def getBaudrate(dictionary):
    """
    Gets the baud rate from the given dictionary.

    Args:
        dict (dict): The dictionary to get the baud rate from.

    Returns:
        int: The baud rate.
    """
    if "baud_rate" in dictionary.keys():
        return dictionary["baud_rate"]
    elif "baudrate" in dictionary.keys():
        return dictionary["baudrate"]
    else:
        return 9600


def modify_basil_config(conf, log, skip_binds=[], verbose=False):
    """
    Modifies the basil configuration file by finding USB binds for devices.

    Args:
        conf (dict): The basil configuration dictionary.
        log: The logger object for logging messages.
        skip_binds (list, optional): List of USB binds to skip. Defaults to [].
        verbose (bool, optional): Flag to enable verbose logging. Defaults to False.

    Returns:
        dict: The modified basil configuration dictionary.
    """
    log.info("Trying to find USB binds for devices in basil configuration file")

    rm = pyvisa.ResourceManager()

    instruments = []
    insts_idx_map = {}

    # Iterate over transfer layers in the configuration
    for i, tf in enumerate(conf["transfer_layer"]):
        if (
            "identification" not in tf["init"].keys()
            or "read_termination" not in tf["init"].keys()
            or not any(e in tf["init"].keys() for e in ["baud_rate", "baudrate"])
        ):
            if verbose:
                log.debug(f"Skipping {tf['type']} transfer layer with name {tf['name']}")
            continue

        instrument = tf["init"]["identification"]
        baud_rate = getBaudrate(tf["init"])
        read_termination = tf["init"]["read_termination"]
        write_termination = tf["init"]["write_termination"] if "write_termination" in tf["init"].keys() else "\n"

        instruments.append({
            "identification": instrument,
            "baud_rate": baud_rate,
            "read_termination": read_termination,
            "write_termination": write_termination,
        })

        insts_idx_map[instrument.lower().strip()] = i

    found_binds = findUSBBinds(rm, log=log, instruments=instruments, binds_to_skip=skip_binds, verbose=verbose)

    for inst in found_binds.keys():
        if found_binds[inst] is None:
            raise LookupError(f"Could not find USB bind for {inst.title().replace('_', '')}")

        if conf["transfer_layer"][insts_idx_map[inst]]["type"].lower() == "serial":
            conf["transfer_layer"][insts_idx_map[inst]]["init"]["port"] = found_binds[inst]
        elif conf["transfer_layer"][insts_idx_map[inst]]["type"].lower() == "visa":
            conf["transfer_layer"][insts_idx_map[inst]]["init"]["resource_name"] = f"ASRL{found_binds[inst]}::INSTR"

        del conf["transfer_layer"][insts_idx_map[inst]]["init"]["identification"]

    return conf


def _open_conf(conf, log):
    def _create_periphery_dict(dut_dict, log):
        periphery = {}
        transfer_layer = {}
        transfer_layer_assignment = {}
        for dev in dut_dict['transfer_layer']:
            transfer_layer[dev['name']] = dev
        for dev in dut_dict['hw_drivers']:
            interface = dev['interface']
            if interface not in transfer_layer_assignment:
                periphery[dev['name']] = {'hw_drivers': [dev], 'transfer_layer': [transfer_layer[dev['interface']]]}
                transfer_layer_assignment[interface] = dev['name']
            else:
                dev_name = transfer_layer_assignment[interface]
                log.info(f'Multiple devices accessing same port: {dev["name"]}, {dev_name}')
                periphery[dev_name]['hw_drivers'].append(dev)

        return periphery

    def isFile(f):
        return hasattr(f, 'read')

    conf_dict = {}
    if not conf:
        pass
    elif isinstance(conf, string_types):  # parse the first YAML document in a stream
        if os.path.isfile(conf):
            with open(conf, 'r') as f:
                conf_dict.update(safe_load(f))
                conf_dict.update(conf_path=f.name)
        else:  # YAML string
            try:
                conf_dict.update(safe_load(conf))
            except ValueError:  # invalid path/filename
                raise IOError("File not found: %s" % conf)
    elif isFile(conf):  # parse the first YAML document in a stream
        conf_dict.update(safe_load(conf))
        conf_dict.update(conf_path=conf.name)
    else:  # conf is already a dict
        conf_dict.update(conf)

    skip_binds = ["arduino", "sensirion", "chiller"]

    conf_dict = modify_basil_config(conf_dict, log=log, skip_binds=skip_binds, verbose=False)

    periphery = _create_periphery_dict(conf_dict, log=log)

    return periphery


class PeripheryDevice(Thread):
    def __init__(self, comm_queue, reply_queue, dut_config, name, *args, **kwargs):
        """
        Initialize the PeripheryDevice. The PeripheryDevice initializes the respective basil.dut.Dut using the dut_config

        Parameters
        ----------
        comm_queue: Queue
            Queue over which the PeripheryDevice receives the commands to execute and returns the results
        dut_config : str, dict
            Configuration of the underlying Dut, either as str, dict or a path
        """

        # Make super call
        super().__init__(name=name, *args, **kwargs)

        self.dut_config = dut_config

        # Store queue for communication as instance attr
        self._comm_queue = comm_queue
        self._reply_queue = reply_queue

        # Create event for stopping the device
        self._stop_flag = Event()
        self._wait = 1e-3

    def stop(self):
        self._stop_flag.set()

    def shutdown(self):
        self.dut.close()

    def _run(self):
        # Initialize dut
        self.dut = Dut(conf=self.dut_config)
        self.dut.init()

        # Loop and get commands for the Dut and return results
        while not self._stop_flag.wait(self._wait):
            start_time = time.time()
            # Init cmd_dict as None; check if it is not None for returning result
            msg = None
            cmd_dict = None

            # Check for incoming command; only try to get something if queue is not empty
            if not self._comm_queue.empty():
                msg = self._comm_queue.get_nowait()
                cmd_dict = msg['cmd']
                msg['time_dev_recv'] = start_time

            # We have received a cmd and want to call the respective Dut method and return a result
            if cmd_dict is not None:
                # Get method to be called as well as args and kwargs to be passed to method
                method = cmd_dict['method']
                args = cmd_dict.get('args', [])
                kwargs = cmd_dict.get('kwargs', {})

                res, error = None, None

                # Execute Dut method; THE DUT METHOD WHICH IS CALLED SHOULD NOT BE BLOCKING! (on a large scale)
                try:
                    # Get the method
                    res = getattr(self.dut[msg['device']], method)
                # The Dut method did not exists
                except ValueError as e:
                    error = repr(e)
                    traceback.print_exc()

                # If *res* is an attribute, just return it, otherwise call *res* and return the result of the call
                if callable(res):
                    try:
                        res = res(*args, **kwargs)
                    except Exception as e:
                        res = None
                        error = repr(e)
                        manager.logger.error(e)
                        traceback.print_exc()  # enhanced debugging output
                if res is not None:
                    msg['result'] = res

                if error is not None:
                    msg['error'] = error

                # Send updated cmd_dict back with result
                stop_time = time.time()
                msg['time_dev_send'] = stop_time
                self._reply_queue.put_nowait(msg)

                # Indicate a task has been completed
                self._comm_queue.task_done()

    def run(self):
        """Main function that is being run on *self.start* call"""
        self._run()
        self.shutdown()


class PeripheryManager(Thread):

    def __init__(self, port=PERIPHERY_PORT, *args, **kwargs):
        """
        Initialize the PeripheryManager by creating the given *periphery*

        Parameters
        ----------
        periphery : dict
            Dictionary containing name, conf pair for the Duts to be created
        port : int, optional
            port to bind to, the default is specified in bdaq.system.__init__.py
            IMPORTANT! If set manually, has match the port specified in periphery.py to work! It is advised to only change this by changing the default in bdaq.system.
        """
        super(PeripheryManager, self).__init__(*args, **kwargs)
        self.port = port
        self.logger = logging.getLogger('HARDWARE')
        signal.signal(signal.SIGINT, self.signal_handler)

        # Stop signal
        self._manager_started = Event()
        self._stop_flag = Event()
        self.interlock = Event()
        self._wait = 1e-2

        # ZMQ context
        self.context = zmq.Context()
        self.clients = {}

        self.thread_respond_to_pings = Thread(target=self.ping_preparing)
        self.thread_respond_to_pings.start()

        # Container for periphery devices and communication queues
        self.periphery = {}
        self.comm_queues = {}
        self.reply_queue = Queue()

        # Load periphery configuration and assign USB ports
        self.devices = _open_conf(periphery_file, self.logger)

        if not self._stop_flag.is_set():  # Allows for KeyboardInterrupts during USB-matching
            # Loop over devices and create all needed objects
            for dev_name, dev_conf in self.devices.items():
                self._start_device(conf=dev_conf, name=dev_name)
            self.logger.info('PERIPHERY INITIALIZED')

    def _start_device(self, conf, name, queue=None):
        self.logger.info(f'INITIALIZING {name}')
        if queue is None:
            queue = Queue()
        dev = PeripheryDevice(comm_queue=queue, reply_queue=self.reply_queue, dut_config=conf, name=name)
        dev.start()
        for dev_driver in conf['hw_drivers']:
            self.comm_queues[dev_driver['name']] = queue
            self.periphery[dev_driver['name']] = dev

    def stop(self):
        self.logger.info('STOPPING PERIPHERY...')
        # Set this Threads stop flag
        self._stop_flag.set()

        # Set all the periphery devices stop flag
        for _, dev in self.periphery.items():
            dev.stop()

        # Wait for the periphery devices to have stopped
        for _, dev in self.periphery.items():
            dev.join()
        self.logger.info('PERIPHERY STOPPED')

    def signal_handler(self, sig, frame):
        self.logger.critical('Intercepted SIGINT!')
        for _ in range(5):
            if self.clients:
                if any(psutil.pid_exists(pid) for pid in self.clients.keys()):
                    self.logger.critical('There are still Applications utilising periphery!')
                    time.sleep(1)
                    continue
            break
        else:
            self.logger.critical('Could not terminate periphery')
            return
        self.stop()

    def _run(self):
        self._manager_started.set()
        self.thread_respond_to_pings.join()

        # Create router socket
        router = self.context.socket(zmq.ROUTER)
        router.bind(f'tcp://*:{self.port}')

        is_dead = {}
        while not self._stop_flag.is_set():
            # Poll the router for incoming things for 1 ms
            if router.poll(timeout=1, flags=zmq.POLLIN):
                start_time = time.time()
                addr, _, cmd_bytes = router.recv_multipart()
                msg = json.loads(cmd_bytes.decode())
                msg['time_recv'] = start_time
                # Get the device name that *addr* wants to address
                dev_name = msg['device']
                if msg.get("interlock"):
                    self.interlock.set()

                if dev_name == "Manager":
                    if msg["cmd"] == "PING":
                        if msg.get('pid') not in self.clients:
                            self.clients[msg.get('pid')] = msg.get('name')
                        self.logger.info('GETTING PINGED')
                        self.logger.info(f'Connected client PIDs: {self.clients}')
                        result = {'init': True}
                        result_bytes = json.dumps(result)
                        router.send_multipart([addr, b'', bytes(result_bytes.encode())])
                    elif msg["cmd"] == "RESET INTERLOCK":
                        self.interlock.clear()
                        result = {"INTERLOCK": self.interlock.is_set()}
                        result_bytes = json.dumps(result)
                        router.send_multipart([addr, b'', bytes(result_bytes.encode())])
                    continue
                else:
                    msg["addr"] = addr
                # Create new device on-demand
                if dev_name not in self.periphery and dev_name in self.devices:
                    self._start_device(name=dev_name, conf=self.devices[dev_name])

                if not self.interlock.is_set() or msg.get("interlock") or 'get' in msg['cmd'].get('method'):
                    self.comm_queues[dev_name].put_nowait(msg)
                else:
                    msg['error'] = 'INTERLOCK'
                    msg['result'] = None
                    self.reply_queue.put_nowait(msg)

            # Loop over all queues and check if there is anything outgoing
            if not self.reply_queue.empty():
                result = self.reply_queue.get_nowait()

                if 'error' in result:
                    if result['error'] == 'INTERLOCK':
                        self.logger.error(f'{result["device"]}: Access blocked by interlock! cmd: {result["cmd"]["method"]}')
                    else:
                        self.logger.error(f'{result["device"]}: An Error occured while processing cmd: {result["cmd"]["method"]}')

                addr = result.pop('addr')
                send_time = time.time()
                result["time_send"] = send_time
                result_bytes = bytes(json.dumps(result).encode())
                router.send_multipart([addr, b'', result_bytes])

            # Loop over periphery devices and check if they are alive
            for name, dev in is_dead.items():
                if not dev.is_alive():
                    self.logger.error(f'{name}: RESTART FAILED!')
                    self.logger.error(f'Removing {name} from periphery...')
                    self.periphery.pop(name)
                    if name in self.devices:
                        self.devices.pop(name)
            is_dead = {}
            for name, dev in self.periphery.items():
                if not dev.is_alive():
                    self.logger.error(f'Oh no, device {name} is dead!')
                    is_dead[name] = dev
            for name, dev in is_dead.items():
                try:
                    if name not in self.devices:
                        pass
                    else:
                        # Create the dead PeripheryDevice again
                        self.logger.info(f'RESTARTING {name}...')
                        try:
                            self._start_device(self.devices[name], name, queue=self.comm_queues[name])
                        except KeyError:
                            self.logger.info(f'Queue for {name} not found. RESTARTING...')
                            self._start_device(self.devices[name], name)

                except Exception:
                    self.logger.error(f'RESTARTING {name} FAILED!')
                    traceback.print_exc()  # enhanced debugging output

            if self.clients:
                if not any(psutil.pid_exists(pid) for pid in self.clients.keys()):
                    self.stop()

    def run(self):
        self._run()

    def ping_preparing(self):
        router = self.context.socket(zmq.ROUTER)
        router.bind(f'tcp://*:{self.port}')

        while not self._manager_started.is_set():
            # Poll the router for incoming things for 1 ms
            if router.poll(timeout=1, flags=zmq.POLLIN):
                addr, _, cmd_bytes = router.recv_multipart()
                msg = json.loads(cmd_bytes.decode())

                # Get the device name that *addr* wants to address
                dev_name = msg['device']
                if msg.get("interlock"):
                    self.interlock.set()

                if dev_name == "Manager":
                    if msg["cmd"] == "PING":
                        if msg.get('pid') not in self.clients:
                            self.clients[msg.get('pid')] = msg.get('name')

                        self.logger.info('GETTING PINGED')
                        self.logger.info(f'Connected client PIDs: {self.clients}')
                        result = {'init': False}
                        result_bytes = json.dumps(result)
                        router.send_multipart([addr, b'', bytes(result_bytes.encode())])

                    elif msg["cmd"] == "RESET INTERLOCK":
                        self.interlock.clear()
                        result = {"INTERLOCK": self.interlock.is_set()}
                        result_bytes = json.dumps(result)
                        router.send_multipart([addr, b'', bytes(result_bytes.encode())])
                    continue
                else:
                    self.logger.error('Unable to perform command - Manager starting up')
                    msg['result'] = None
                    msg['error'] = 'NOT READY'

                    addr = msg.pop('addr')
                    result_bytes = bytes(json.dumps(msg).encode())
                    router.send_multipart([addr, b'', result_bytes])


if __name__ == '__main__':
    # This dict should contain all availbale periphery devices and their configs
    # If all available devices are specified in periphery.yaml, the _open_conf() function can be used to create the dictionary.
    manager = PeripheryManager()
    manager.start()
    manager.join()
