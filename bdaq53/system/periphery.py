#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

"""

This script initialises the BDAQ Periphery Interface. For each device, a class with the required commands based on PeripheryDeviceBase is needed.
The implemented devices can be found in the periphery_device_interfaces folder.

If an instance of the device handler 'module_qc_periphery.py' is running, the interface connects to that instance. If none is found, the script launches the device handler.

"""
import os
import time
import yaml
import zmq
import sys

# from pathlib import Path

from six import string_types
import subprocess

from bdaq53.system import logger
from bdaq53.system import module_qc_periphery_script
from bdaq53.system import PERIPHERY_PORT

# This is needed=!=!=
from bdaq53.system.periphery_device_interfaces.PeripheryDeviceBase import PeripheryDeviceBase
from bdaq53.system.periphery_device_interfaces.PeripheryThermohygrometer import PeripheryThermohygrometer
from bdaq53.system.periphery_device_interfaces.PeripheryArduino import PeripheryArduino
from bdaq53.system.periphery_device_interfaces.PeripheryMultimeter import PeripheryMultimeter
from bdaq53.system.periphery_device_interfaces.PeripherySourcemeter import PeripheryHV, PeripherySourcemeter
from bdaq53.system.periphery_device_interfaces.PeripheryPowersupply import PeripheryLV, PeripheryBDAQ
from bdaq53.system.periphery_device_interfaces.PeripheryChiller import PeripheryChiller


class BDAQ53Periphery(object):
    _aux_device_types = ['BDAQ', 'Multimeter', 'Sourcemeter', 'Arduino', 'Thermohygrometer', 'Chiller']

    def __init__(self, bench_config=None, dut_conf=None, port=PERIPHERY_PORT, name=None):
        self.name = name

        self.is_monitoring = False
        self.closed = True
        self.aux_devices = {}
        self.module_devices = {}
        self.log = logger.setup_derived_logger(f'Periphery {self.name}')
        self.closed = False

        self.context = zmq.Context()
        self.port = port

        proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if bench_config is None or isinstance(bench_config, str):
            if bench_config is None:
                bench_config = os.path.join(proj_dir, 'testbench.yaml')
            with open(bench_config) as f:
                bench_config = yaml.full_load(f)
        if not isinstance(bench_config, dict):
            raise ValueError('bench_config is of unknown type!')

        if not bench_config['periphery'].get('enable_periphery', False):
            self.log.notice('Periphery is disabled.')
            self.enabled = False
            return
        else:
            self.configuration = bench_config['periphery']
            self.configuration['modules'] = bench_config['modules']
            self.enabled = True

        if dut_conf is None:
            self.dut_conf = os.path.join(proj_dir, 'periphery.yaml')
        else:
            self.dut_conf = dut_conf

        self.dut_dict = self._open_conf(self.dut_conf)

        self.module_list = list(bench_config['modules'].keys())

    def __del__(self):
        if not self.closed:
            self.close()

    def init(self):
        '''
            Initialize hardware and start process for each device.
        '''
        if not self.enabled:
            return

        self.closed = False
        if not self.start_periphery():
            return
        try:
            self.log.notice('Initializing periphery...')
            # ADD: Check if periphery is running
            self.enabled = True

            for mod in self.module_list:
                self.module_devices[mod] = {}
                for typ in ['LV', 'HV']:
                    try:
                        name = self.configuration['modules'][mod]['powersupply'][typ.lower() + '_name']
                        if name is None:
                            continue
                        channel = self.configuration['modules'][mod]['powersupply'].get(typ.lower() + '_channel')

                        if mod in self.module_devices.keys():
                            self.module_devices[mod].update({typ: globals()[f"Periphery{typ}"](name=name, context=self.context, port=self.port, channel=channel)})
                        else:
                            self.module_devices[mod] = {typ: globals()[f"Periphery{typ}"](name=name, context=self.context, port=self.port, channel=channel)}
                    except KeyError:
                        self.log.debug('No {0} device defined for module {1}.'.format(typ, mod))

            for dev in self.dut_dict['hw_drivers']:
                name = dev['name']
                for typ in self._aux_device_types:
                    if typ.lower() in name.lower():
                        if typ == 'Sourcemeter':  # FIX ME: Allows only for one Sourcemeter to be used as periphery
                            if name != 'Sourcemeter':
                                continue
                        self.aux_devices[name] = globals()[f"Periphery{typ}"](name=name, context=self.context, port=self.port)

            if self.dut_dict.get('registers') is not None:
                for dev in self.dut_dict['registers']:
                    name = dev['name']
                    dev_name = dev['hw_driver']
                    if name in self._aux_device_types:
                        channel = dev['arg_add'].get('channel', None)
                        self.aux_devices[name] = globals()[f"Periphery{name}"](name=name, context=self.context, port=self.port, channel=channel, dev_name=dev_name)
        except Exception as e:
            self.enabled = False
            self.log.notice('Cannot initialize periphery: %s' % e)
            raise e

        for mod in self.module_devices.keys():
            self.log.notice('Initialized devices for module {0}: {1}'.format(mod, list(self.module_devices[mod].keys())))
        self.log.notice('Initialized auxilliary devices: {}'.format(list(self.aux_devices.keys())))

    def close(self):
        for dev in self.aux_devices.values():
            dev.close()
        for mod in self.module_devices.values():
            for dev in mod.values():
                dev.close()

        self.closed = True

    # Control methods for AUX devices
    def _open_conf(self, conf):
        def isFile(f):
            return hasattr(f, 'read')

        conf_dict = {}
        if not conf:
            pass
        elif isinstance(conf, string_types):  # parse the first YAML document in a stream
            if os.path.isfile(conf):
                with open(conf, 'r') as f:
                    conf_dict.update(yaml.safe_load(f))
                    conf_dict.update(conf_path=f.name)
            else:  # YAML string
                try:
                    conf_dict.update(yaml.safe_load(conf))
                except ValueError:  # invalid path/filename
                    raise IOError("File not found: %s" % conf)
        elif isFile(conf):  # parse the first YAML document in a stream
            conf_dict.update(yaml.safe_load(conf))
            conf_dict.update(conf_path=conf.name)
        else:  # conf is already a dict
            conf_dict.update(conf)

        return conf_dict

    def reset_interlock(self):
        """
        This function sends a reset command to the device handler 'module_qc_periphery.py'.
        BE CAREFULL WITH THIS; IT SHOULD ONLY BE USED FROM THE SCRIPT CONTROLLING THE INTERLOCK CONDITIONS

        Returns
        -------
        returns the boolean value of the interlock flag of the device handler
        """
        socket = self.context.socket(zmq.REQ)
        socket.connect(f'tcp://localhost:{self.port}')
        cmd_dict = {'device': "Manager", 'cmd': "RESET INTERLOCK"}
        socket.send_json(cmd_dict)
        ret_val = socket.recv_json()

        return ret_val.get("INTERLOCK")

    def power_on_BDAQ(self):
        if not self.enabled:
            return

        def ping_bdaq():
            ping = os.system('ping -c 1 -W 1 192.168.10.12 > /dev/null')
            if ping == 0:
                return True
            else:
                return False

        if 'BDAQ' not in self.aux_devices.keys():
            self.log.notice('No BDQ powersupply defined!')
            return

        if ping_bdaq():
            self.aux_devices['BDAQ'].output = True
            self.log.notice('BDAQ board already set up!')
        else:
            self.aux_devices['BDAQ'].on()
            self.log.notice('Powered on BDAQ board!')
            for _ in range(10):
                if ping_bdaq():
                    break
            else:
                self.log.notice('Could not power on BDAQ board!')

    def power_off_BDAQ(self):
        if not self.enabled:
            return

        if 'BDAQ' not in self.aux_devices.keys():
            self.log.notice('No BDAQ powersupply defined!')
            return

        self.aux_devices['BDAQ'].off()
        self.log.notice('Powered off BDAQ board!')

    # Control methods for modules

    def power_on_LV(self, module, interlock=False, verbose=True, **kwargs):
        '''
            Power on LV of the module.
        '''

        if not self.enabled:
            return False

        if module not in self.module_devices.keys():
            if verbose:
                self.log.notice('No LV powersupply defined for module {}'.format(module))
            return False

        config = self.configuration['modules'].get(module, {}).get('powersupply', {})
        config.update(**kwargs)

        if 'lv_voltage' in config.keys():
            self.module_devices[module]['LV'].set_voltage(config['lv_voltage'], interlock=interlock)
        else:
            if verbose:
                self.log.notice('No LV value defined for module {}'.format(module))
            return False
        if 'lv_current_limit' in config.keys():
            self.module_devices[module]['LV'].set_current_limit(config['lv_current_limit'], interlock=interlock)
        self.module_devices[module]['LV'].on(interlock=interlock)
        time.sleep(0.5)
        if verbose:
            self.log.notice('Turned on LV of {0}: {1:1.3f}A @ {2:1.3f}V'.format(module, self.module_devices[module]['LV'].get_current(interlock=interlock), config['lv_voltage']))
        return True

    def power_off_LV(self, module, interlock=False):
        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        if 'LV' in self.module_devices[module]:
            self.module_devices[module]['LV'].off(interlock=interlock)
            self.log.notice('Turned off LV of {0}.'.format(module))
        else:
            self.log.notice('No LV powersupply defined for module {}'.format(module))

    def power_on_module(self, module, interlock=False, **kwargs):
        '''
            Power on LV and HV of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        self.power_on_LV(module, interlock=interlock, **kwargs)
        self.power_on_HV(module, interlock=interlock, **kwargs)

    def power_off_module(self, module, interlock=False):
        '''
            Power off LV and HV of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        self.power_off_HV(module, interlock=interlock)
        self.power_off_LV(module, interlock=interlock)

    def get_module_power(self, module, log=True):
        '''
            Get LV and HV voltage and current of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        ret = {}
        log_string = 'Power consumption of {0}:'.format(module)
        if 'LV' in self.module_devices[module]:
            lv_voltage = self.module_devices[module]['LV'].get_voltage()
            lv_current = self.module_devices[module]['LV'].get_current()
            ret.update({'LV': {'voltage': lv_voltage, 'current': lv_current}})
            log_string += ' LV: {0:1.3f}A @ {1:1.3f}V'.format(lv_current, lv_voltage)
        else:
            self.log.notice('No LV powersupply defined for module {}'.format(module))

        if 'HV' in self.module_devices[module]:
            hv_voltage = self.module_devices[module]['HV'].get_voltage()
            hv_current = self.module_devices[module]['HV'].get_current()
            ret.update({'HV': {'voltage': hv_voltage, 'current': hv_current}})
            log_string += ' HV: {0:1.3e}A @ {1:1.0f}V'.format(hv_current, hv_voltage)
        else:
            self.log.notice('No HV powersupply defined for module {}'.format(module))

        if log:
            self.log.notice(log_string)
        return ret

    def power_on_HV(self, module, interlock=False, verbose=True, **kwargs):
        '''
            Power on HV of the module.
        '''

        if not self.enabled:
            return False

        if module not in self.module_devices.keys():
            if verbose:
                self.log.notice('No devices defined for module {}'.format(module))
            return False

        config = self.configuration['modules'].get(module, {}).get('powersupply', {})
        config.update(kwargs)

        if 'HV' in self.module_devices[module]:
            if 'hv_voltage' in config.keys() and 'hv_current_limit' in config.keys():
                self.module_devices[module]['HV'].set_current_limit(config['hv_current_limit'], interlock=interlock)
                self.module_devices[module]['HV'].ramp_voltage(config['hv_voltage'], interlock=interlock)
            else:
                return False
            time.sleep(0.1)
            if verbose:
                self.log.notice('Turned on HV of {0}: {1:1.3e}A @ {2:1.0f}V'.format(module, self.module_devices[module]['HV'].get_current(interlock=interlock), self.module_devices[module]['HV'].get_voltage(interlock=interlock)))
        else:
            if verbose:
                self.log.notice('No HV powersupply defined for module {}'.format(module))
            return False
        return True

    def power_off_HV(self, module, interlock=False):
        '''
            Power off HV of the module.
        '''

        if not self.enabled:
            return

        if module not in self.module_devices.keys():
            self.log.notice('No devices defined for module {}'.format(module))
            return

        if 'HV' in self.module_devices[module]:
            self.module_devices[module]['HV'].ramp_voltage(0, interlock=interlock)
            self.module_devices[module]['HV'].off(interlock=interlock)
            self.log.notice('Turned off HV of {0}.'.format(module))
        else:
            self.log.notice('No HV powersupply defined for module {}'.format(module))

    def start_periphery(self, trys=0):  # Approach only works if periphery is run on the same computer!
        socket = self.context.socket(zmq.REQ)
        socket.connect(f"tcp://localhost:{self.port}")

        if trys > 10:
            self.log.error('Connecting Periphery Failed!')
            self.enabled = False
            return False

        pid = os.getpid()
        cmd_dict = {'device': "Manager", 'cmd': "PING", 'pid': pid, 'name': self.name}
        socket.send_json(cmd_dict)

        if socket.poll(timeout=2000, flags=zmq.POLLIN):
            rep_dict = socket.recv_json()
            initialized = rep_dict.get('init')
        else:
            self.log.info('NO CONNECTION')

            subprocess.Popen('{} {}'.format(sys.executable, module_qc_periphery_script), shell=True, creationflags=subprocess.CREATE_NEW_PROCESS_GROUP if os.name == 'nt' else 0)
            return self.start_periphery(trys=(trys + 1))

        while not initialized:
            time.sleep(10)  # FIX ME: Optimize this
            self.log.success('WAITING FOR PERIPHERY TO START UP...')
            pid = os.getpid()
            cmd_dict = {'device': "Manager", 'cmd': "PING", 'pid': pid, 'name': self.name}
            socket.send_json(cmd_dict)

            if socket.poll(timeout=2000, flags=zmq.POLLIN):
                rep_dict = socket.recv_json()
                initialized = rep_dict.get('init')

        self.log.success('PERIPHERY READY')
        return True


if __name__ == "__main__":
    periphery = BDAQ53Periphery()
    periphery.init()
