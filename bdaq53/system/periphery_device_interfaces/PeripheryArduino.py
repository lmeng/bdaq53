import numpy as np
from bdaq53.system.periphery_device_interfaces.PeripheryDeviceBase import PeripheryDeviceBase


class PeripheryArduino(PeripheryDeviceBase):
    def get_temperature(self, sensor=0, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_res", 'kwargs': {'sensor': sensor}},
                    "interlock": interlock})
        ret_dict = self.ret.get()
        resistivity_dict = ret_dict.get('result', {})

        def T_max(R):
            A = 3.35401834e-3
            B = 3.00010770e-4
            C = 5.01928334e-6
            D = 3.07803097e-7
            R25 = 10000

            T = 1.0 / (A + B * np.log(R / R25) + C * (np.log(R / R25)) ** 2 + D * (np.log(R / R25)) ** 3) - 273.15

            return T

        return {s: T_max(resistivity_dict[s]) for s in resistivity_dict}

    def get_resistivity(self, sensor=0, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_res", 'kwargs': {'sensor': sensor}},
                    "interlock": interlock})
        ret_dict = self.ret.get()
        return ret_dict.get('result', {})

    def get_pressure(self, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "pressure"}, "interlock": interlock})
        ret_dict = self.ret.get()
        return ret_dict.get('result')
