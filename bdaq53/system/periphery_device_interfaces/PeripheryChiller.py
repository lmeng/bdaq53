import numpy as np
from bdaq53.system.periphery_device_interfaces.PeripheryDeviceBase import PeripheryDeviceBase


class PeripheryChiller(PeripheryDeviceBase):
    def get_status(self):
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_status"}})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')

        return result

    def start_chiller(self, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "start_chiller"}, "interlock": interlock})

        if 'error' not in self.ret.get():
            self.output = True
        return self.output

    def stop_chiller(self, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "stop_chiller"}, "interlock": interlock})
        if 'error' not in self.ret.get():
            self.output = False
        return self.output

    def set_power(self, value, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "set_power", "args": [value]}, "interlock": interlock})
        if 'error' not in self.ret.get():
            return 0
        else:
            return 1
