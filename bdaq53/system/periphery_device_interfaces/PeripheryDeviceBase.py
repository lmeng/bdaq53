"""
Base class for BDAQ Periphery devices. This implements the nessecary data queues and tcp connections.

For each device that is to be used as BDAQ Periphery, create a subclass implementing the required functions.

To send commands to a device, put a dictionary to the command queue self.q:
{"device": self.dev_name, "cmd": {"method": "name_of_basil_command(str)", "kwargs": kwargs(dict, optional)}, "interlock": interlock_state(bool, optional), "no_return": should_the_cmd_exit_without_returning_a_value(bool, optional)}

The return value is passed through the queue self.ret with the result as ret_val['result'] and potential error messages as ret_val['error'].

"""
import time
import zmq

from threading import Thread, Event
from multiprocessing import Value
from queue import Queue

from bdaq53.system import logger


class PeripheryDeviceBase(object):
    def __init__(self, name, context, port, channel=None, dev_name=None):
        self.log = logger.setup_derived_logger("INTERFACE: " + name)

        self.name = name
        self.dev_name = name
        if dev_name is not None:
            self.dev_name = dev_name

        self.context = context
        self.port = port

        self.channel = channel
        self._output = Value('i', False)  # Why does this exist?

        self._stop_flag = Event()
        self._wait = 1e-3
        self.q = Queue()
        self.ret = Queue()
        self.p = Thread(target=self.send_commands, args=(self.q, self.ret), name='INTERFACE: ' + name)
        self.p.start()
        self.log.info('Interface started')

    def send_commands(self, q, ret):
        socket = self.context.socket(zmq.REQ)
        socket.connect(f'tcp://localhost:{self.port}')
        socket.RCVTIMEO = 15000

        while not self._stop_flag.wait(self._wait):
            if not q.empty():
                cmd_dict = q.get_nowait()
            else:
                continue

            if cmd_dict.get("cmd") is not None:
                send_time = time.time()
                socket.send_json(cmd_dict)
                try:
                    ret_val = socket.recv_json()
                except zmq.Again:
                    self.log.error("Request timed out!")
                    ret_val = cmd_dict
                    ret_val["error"] = "TIMEOUT"
                recv_time = time.time()
                ret_val["time_intf_send"] = send_time
                ret_val["time_intf_recv"] = recv_time
                if not cmd_dict.get("no_return", False):
                    ret.put(ret_val)

    def close(self):
        try:
            self._stop_flag.set()
            self.p.join()
        except AssertionError:  # Queue was already closed
            pass
