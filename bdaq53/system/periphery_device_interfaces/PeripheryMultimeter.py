from bdaq53.system.periphery_device_interfaces.PeripheryDeviceBase import PeripheryDeviceBase


class PeripheryMultimeter(PeripheryDeviceBase):
    def get_voltage(self, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_voltage"}, "interlock": interlock})
        ret_dict = self.ret.get()

        result = ret_dict.get('result')
        if result is not None:
            result = float(result)

        return result

    def get_current(self, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_current"}, "interlock": interlock})
        ret_dict = self.ret.get()

        result = ret_dict.get('result')
        if result is not None:
            result = float(result)

        return result
