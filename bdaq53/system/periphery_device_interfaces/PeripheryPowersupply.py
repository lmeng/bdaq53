from bdaq53.system.periphery_device_interfaces.PeripheryDeviceBase import PeripheryDeviceBase


class PeripheryBDAQ(PeripheryDeviceBase):
    def on(self):
        if self.channel is not None:
            kwargs = {"on": True, "channel": self.channel}
        else:
            kwargs = {"on": True}

        self.q.put({"device": self.dev_name, "cmd": {"method": "set_enable", "kwargs": kwargs}})

        if 'error' not in self.ret.get():
            self.output = True
        return self.output

    def off(self):
        if self.channel is not None:
            kwargs = {"on": False, "channel": self.channel}
        else:
            kwargs = {"on": False}

        self.q.put({"device": self.dev_name, "cmd": {"method": "set_enable", "kwargs": kwargs}})

        if 'error' not in self.ret.get():
            self.output = False
        return self.output

    @property
    def output(self):
        return bool(self._output.value)

    @output.setter
    def output(self, value):
        self._output.value = value


class PeripheryLV(PeripheryDeviceBase):
    def on(self, interlock=False):
        if self.channel is not None:
            kwargs = {"on": True, "channel": self.channel}
        else:
            kwargs = {"on": True}

        self.q.put({"device": self.dev_name, "cmd": {"method": "set_enable", "kwargs": kwargs}, "interlock": interlock})

        if 'error' not in self.ret.get():
            self.output = True
        return self.output

    def off(self, interlock=False):
        if self.channel is not None:
            kwargs = {"on": False, "channel": self.channel}
        else:
            kwargs = {"on": False}

        self.q.put({"device": self.dev_name, "cmd": {"method": "set_enable", "kwargs": kwargs}, "interlock": interlock})

        if 'error' not in self.ret.get():
            self.output = False
        return self.output

    @property
    def output(self):
        return bool(self._output.value)

    @output.setter
    def output(self, value):
        self._output.value = value

    def get_current(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "get_current", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = float(result)
        return result

    def get_voltage(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "get_voltage", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = float(result)
        return result

    def set_voltage(self, value, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "set_voltage", "args": [value], "kwargs": kwargs}, "interlock": interlock})
        self.ret.get()

        self.q.put({"device": self.dev_name, "cmd": {"method": "get_set_voltage", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')

        if result is not None:
            result = float(result)
        return result

    def get_set_voltage(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "get_set_voltage", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')

        if result is not None:
            result = float(result)
        return result

    def set_current_limit(self, value, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "set_current_limit", "args": [value], "kwargs": kwargs},
                    "interlock": interlock})
        self.ret.get()

        self.q.put(
            {"device": self.dev_name, "cmd": {"method": "get_current_limit", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = float(result)
        return result

    def get_current_limit(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put(
            {"device": self.dev_name, "cmd": {"method": "get_current_limit", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = float(result)
        return result
