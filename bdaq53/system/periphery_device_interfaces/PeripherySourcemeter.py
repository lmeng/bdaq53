import time
from bdaq53.system.periphery_device_interfaces.PeripheryDeviceBase import PeripheryDeviceBase


class PeripherySourcemeter(PeripheryDeviceBase):
    def on(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "on", "kwargs": kwargs}, "interlock": interlock})
        self.ret.get()
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_on", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = bool(int(float(result.strip())))
        return result

    def off(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "off", "kwargs": kwargs}, "interlock": interlock})
        self.ret.get()
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_on", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = bool(int(float(result.strip())))
        return result

    def get_on(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "get_on", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = bool(int(float(result.strip())))
        return result

    def get_voltage(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "off", "kwargs": kwargs}, "interlock": interlock, "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "source_current", "kwargs": kwargs}, "interlock": interlock, "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "set_current", "args": [0], "kwargs": kwargs}, "interlock": interlock, "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "on", "kwargs": kwargs}, "interlock": interlock, "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_voltage", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = result.strip()
            if ',' in result:
                result = float(result.split(',')[0])
            else:
                result = float(result)
        self.q.put({"device": self.dev_name, "cmd": {"method": "off", "kwargs": kwargs}, "interlock": interlock, "no_return": True})

        return result

    def get_current(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "off", "kwargs": kwargs}, "interlock": interlock,
                    "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "source_volt", "kwargs": kwargs}, "interlock": interlock,
                    "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "set_voltage", "args": [0], "kwargs": kwargs},
                    "interlock": interlock, "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "on", "kwargs": kwargs}, "interlock": interlock,
                    "no_return": True})
        self.q.put(
            {"device": self.dev_name, "cmd": {"method": "get_current", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = result.strip()
            if ',' in result:
                result = float(result.split(',')[0])
            else:
                result = float(result)
        self.q.put({"device": self.dev_name, "cmd": {"method": "off", "kwargs": kwargs}, "interlock": interlock,
                    "no_return": True})

        return result

    def set_voltage(self, value, interlock=False, timeout=0.1):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        if abs(value) < 0.2:
            voltage_range = 0.2
        elif abs(value) < 2:
            voltage_range = 2
        elif abs(value) < 20:
            voltage_range = 20
        elif abs(value) < 200:
            voltage_range = 200
        elif abs(value) < 1000:
            voltage_range = 1000
        self.q.put({"device": self.dev_name, "cmd": {"method": "source_volt", "kwargs": kwargs}, "interlock": interlock,
                    "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "set_voltage_range", "args": [voltage_range], "kwargs": kwargs}, "interlock": interlock,
                    "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "set_voltage", "args": [value], "kwargs": kwargs}, "interlock": interlock,
                    "no_return": True})
        self.q.put({"device": self.dev_name, "cmd": {"method": "on", "kwargs": kwargs}, "interlock": interlock})
        self.ret.get()
        time.sleep(timeout)
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_source_voltage", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = result.strip()
            if ',' in result:
                result = float(result.split(',')[0])
            else:
                result = float(result)
        return result

    def set_current_limit(self, value, interlock=False, timeout=0.1):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "set_current_limit", "args": [value], "kwargs": kwargs}, "interlock": interlock})
        self.ret.get()
        time.sleep(timeout)
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_current_limit", "kwargs": kwargs},
                    "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = result.strip()
            if ',' in result:
                result = float(result.split(',')[0])
            else:
                result = float(result)
        return result

    def set_current(self, value, interlock=False, timeout=0.1):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "source_current", "kwargs": kwargs}, "interlock": interlock,
                    "no_return": True})

        self.q.put({"device": self.dev_name, "cmd": {"method": "set_current", "args": [value], "kwargs": kwargs}, "interlock": interlock})
        self.ret.get()
        time.sleep(timeout)
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_source_current", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = result.strip()
            if ',' in result:
                result = float(result.split(',')[0])
            else:
                result = float(result)
        return result

    def get_source_voltage(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_source_voltage", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = result.strip()
            if ',' in result:
                result = float(result.split(',')[0])
            else:
                result = float(result)
        return result

    def get_source_current(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "get_source_current", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result is not None:
            result = result.strip()
            if ',' in result:
                result = float(result.split(',')[0])
            else:
                result = float(result)
        return result


class PeripheryHV(PeripherySourcemeter):
    @property
    def output(self):
        return self.get_on()

    def get_current(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "get_current", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result:
            result = result.strip()
            if ',' in result:
                result = float(result.split(',')[1])
            else:
                result = float(result)
        return result

    def get_voltage(self, interlock=False):
        if self.channel is not None:
            kwargs = {"channel": self.channel}
        else:
            kwargs = {}

        self.q.put({"device": self.dev_name, "cmd": {"method": "get_voltage", "kwargs": kwargs}, "interlock": interlock})
        ret_dict = self.ret.get()
        result = ret_dict.get('result')
        if result:
            result = result.strip()
            if ',' in result:
                result = float(result.split(',')[0])
            else:
                result = float(result)
        return result

    def ramp_voltage(self, target, interlock=False):
        if not self.output:
            self.set_voltage(0, interlock=interlock)
            self.on(interlock=interlock)

        voltage = int(self.get_voltage(interlock=interlock))
        target = int(target)

        while abs(voltage - target) > 5:
            if abs(voltage - target) > 100:
                voltage += 10 if voltage < target else -10
            else:
                voltage += 5 if voltage < target else -5
            self.set_voltage(voltage, interlock=interlock)
            time.sleep(1)

        return self.set_voltage(target, interlock=interlock)
