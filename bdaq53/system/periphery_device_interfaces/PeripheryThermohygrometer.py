from bdaq53.system.periphery_device_interfaces.PeripheryDeviceBase import PeripheryDeviceBase


class PeripheryThermohygrometer(PeripheryDeviceBase):
    def get_temperature(self, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_temperature"}, "interlock": interlock})
        ret_dict = self.ret.get()

        result = ret_dict.get('result')
        if result is not None:
            result = float(result)

        return result

    def get_humidity(self, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_humidity"}, "interlock": interlock})
        ret_dict = self.ret.get()

        result = ret_dict.get('result')
        if result is not None:
            result = float(result)

        return result

    def get_dew_point(self, interlock=False):
        self.q.put({"device": self.dev_name, "cmd": {"method": "get_dew_point"}, "interlock": interlock})
        ret_dict = self.ret.get()

        result = ret_dict.get('result')
        if result is not None:
            result = float(result)

        return result
