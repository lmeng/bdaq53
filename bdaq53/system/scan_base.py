#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import ast
import collections
import inspect
import multiprocessing
import os
import time
import traceback
import yaml
import zmq
from collections import OrderedDict
from contextlib import contextmanager
from copy import deepcopy
import numpy as np
import tables as tb

from slack import WebClient
from online_monitor.utils import utils as ou

from bdaq53 import utils as bu
from bdaq53 import manage_databases
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import plotting
from bdaq53.analysis import combine_module_files

from bdaq53.chips.rd53a import RD53A
from bdaq53.chips.ITkPixV1 import ITkPixV1
from bdaq53.chips.CROCv1 import CROCv1
from bdaq53.chips.ITkPixV2 import ITkPixV2

from bdaq53.system import logger, fifo_readout
from bdaq53.system.bdaq53_base import BDAQ53
from bdaq53.system.periphery import BDAQ53Periphery
from bdaq53.system.fifo_readout import FifoReadout

from bdaq53.qms.qms_control import QMS_Card

# Compression for data files
FILTER_RAW_DATA = tb.Filters(complib='blosc', complevel=5, fletcher32=False)
FILTER_TABLES = tb.Filters(complib='zlib', complevel=5, fletcher32=False)
# Default locations
PROJECT_FOLDER = os.path.join(os.path.dirname(__file__), '..')
SYSTEM_FOLDER = os.path.join(PROJECT_FOLDER, 'system')
CHIP_FOLDER = os.path.join(PROJECT_FOLDER, 'chips')
RD53A_DEFAULT_CONFIG_FILE = os.path.join(CHIP_FOLDER, 'rd53a_default.cfg.yaml')
ITKPIXV1_DEFAULT_CONFIG_FILE = os.path.join(CHIP_FOLDER, 'ITkPixV1_default.cfg.yaml')
CROCV1_DEFAULT_CONFIG_FILE = os.path.join(CHIP_FOLDER, 'CROCv1_default.cfg.yaml')
ITKPIXV2_DEFAULT_CONFIG_FILE = os.path.join(CHIP_FOLDER, 'ITkPixV2_default.cfg.yaml')
TESTBENCH_DEFAULT_FILE = os.path.join(PROJECT_FOLDER, 'testbench.yaml')


def fill_dict_from_conf_table(table):
    conf = au.ConfigDict()
    for k, v in table[:]:
        conf[k] = v
    return conf


def str_to_slices(slice_str):
    sls = []
    for s in slice_str.replace(' ', '').split(','):
        sls.append(slice(*[{True: lambda n: None, False: int}[x == ''](x) for x in (s.split(':') + ['', '', ''])[:3]]))
    return sls


def send_data(socket, data, scan_par_id, ptot_table=None, name='ReadoutData'):
    '''Sends the data of every read out (raw data and meta data)

        via ZeroMQ to a specified socket.
        Uses a serialization provided by the online_monitor package
    '''

    data_meta_data = dict(
        name=name,
        timestamp_start=data[1],  # float
        timestamp_stop=data[2],  # float
        error=data[3],  # int
        scan_par_id=scan_par_id,
        ptot_data=ptot_table
    )
    try:
        data_ser = ou.simple_enc(data[0], meta=data_meta_data)
        socket.send(data_ser, flags=zmq.NOBLOCK)
    except zmq.Again:
        pass


class MetaTable(tb.IsDescription):
    index_start = tb.Int64Col(pos=0)
    index_stop = tb.Int64Col(pos=1)
    data_length = tb.UInt32Col(pos=2)
    timestamp_start = tb.Float64Col(pos=3)
    timestamp_stop = tb.Float64Col(pos=4)
    scan_param_id = tb.UInt32Col(pos=5)
    error = tb.UInt32Col(pos=6)
    trigger = tb.Float64Col(pos=7)


class MapTable(tb.IsDescription):
    cmd_number_start = tb.UInt32Col(pos=0)
    cmd_number_stop = tb.UInt32Col(pos=1)
    cmd_length = tb.UInt32Col(pos=2)
    scan_param_id = tb.UInt32Col(pos=3)


class PtotTable(tb.IsDescription):
    cmd_number_start = tb.UInt32Col(pos=0)
    cmd_number_stop = tb.UInt32Col(pos=1)
    cmd_length = tb.UInt32Col(pos=2)
    scan_param_id = tb.UInt32Col(pos=3)
    hit_or_1_col = tb.UInt16Col(pos=4)
    hit_or_1_row = tb.UInt16Col(pos=5)
    hit_or_2_col = tb.UInt16Col(pos=6)
    hit_or_2_row = tb.UInt16Col(pos=7)
    hit_or_3_col = tb.UInt16Col(pos=8)
    hit_or_3_row = tb.UInt16Col(pos=9)
    hit_or_4_col = tb.UInt16Col(pos=10)
    hit_or_4_row = tb.UInt16Col(pos=11)


class RunConfigTable(tb.IsDescription):
    attribute = tb.StringCol(64)
    value = tb.StringCol(512)


class ChipStatusTable(tb.IsDescription):
    attribute = tb.StringCol(64, pos=0)
    ADC = tb.UInt16Col(pos=1)
    value = tb.Float64Col(pos=2)


class RegisterTable(tb.IsDescription):
    register = tb.StringCol(64)
    value = tb.UInt16Col()


class PowersupplyTable(tb.IsDescription):
    powersupply = tb.StringCol(128, pos=0)
    voltage = tb.Float64Col(pos=1)
    current = tb.Float64Col(pos=2)


class ScanData:
    ''' Class to store data created in the scan.

        Shared between the configure(), scan() and analyze() steps
    '''
    pass


class ChipContainer:
    '''
    Data class that collects all chip specific objects, data, and configs

    Is created per chip.
    '''

    def __init__(self, name, chip_settings, chip_conf, module_settings, output_filename, output_dir, log_fh, scan_config, suffix=''):
        self.name = name
        self.chip_settings = chip_settings  # chip settings from testbench; not to be confused with self.chip_settings['chip_config_file']
        self.module_settings = module_settings  # module configuration of this chip from testbench
        self.chip_conf = chip_conf  # configuration object for chip
        if suffix != '' and output_filename is not None:
            self.output_filename = output_filename + suffix
        else:
            self.output_filename = output_filename
        self.output_dir = output_dir
        self.log_fh = log_fh
        self.scan_config = scan_config

        # Set later
        self.chip = None  # the RD53 chip object
        self.data = ScanData()
        self.h5_file = None
        self.raw_data_earray = None
        self.meta_data_table = None
        self.environmental_data_table = None
        self.trigger_table = None
        self.ptot_table = None
        self.scan_parameters = OrderedDict()
        self.socket = None

    def __repr__(self):
        return 'ChipContainer for %s (%s) of %s with data at %s' % (self.name,
                                                                    self.chip_settings['chip_sn'] if self.chip_settings else 'undefined',
                                                                    self.module_settings['name'] if self.module_settings else 'undefined',
                                                                    self.output_dir)


class ScanBase(object):
    '''
        Basic run meta class.
        Base class for scan- / tune- / analyze-class.
    '''

    is_parallel_scan = False  # Parallel readout of ExtTrigger-type scans etc.; must be overridden in the derived classes if needed

    def __init__(self, bdaq_conf=None, bench_config=None, scan_config={}, scan_config_per_chip=None, suffix=''):
        '''
            Initializer.

            Parameters:
            ----------
            bdaq_conf : str, dict or file
                    Readout board configuration (configuration as dict or file or its filename as string)

            bench_config : str or dict
                    Testbench configuration (configuration as dict or its filename as string)

            scan_config : dict
                    Dictionary of scan parameters. These can be complemented/overwritten per chip by scan_config_per_chip, see below.
                    The scan parameters will be passed to the _configure() and _scan() functions as expanded kwargs.

                    If the dictionary contains a key named 'chip' then the corresponding value (which should be a dict with format/structure
                    compatible to the 'rd53x_default.cfg.yaml') is used to complement/overwrite the chip configuration(s)
                    loaded from the chip_configuration file(s) specified in the testbench configuration.

                    If the dictionary contains a key named 'bdaq' then the corresponding value (which should be a dict with format/structure
                    compatible to the 'testbench.yaml') is used to complement/overwrite the testbench configuration loaded from 'testbench.yaml'.

            scan_config_per_chip : dict
                    Dictionary (format similar as in 'testbench.yaml') containing additional or changed scan parameters
                    for each chips given. Example: {'module_0': {'chip_0': {'start_column': 128, 'stop_column': 148},
                                                                'chip_1': {'start_column': 0, ...}},
                                                   'module_1': {...}, ...}
                    A 'chip' dict as additional parameter will complement/overwrite a 'chip' dict given in scan_config for the corresponding chip.

            record_chip_status : boolean
                    Add chip statuses to the output files after the scan
        '''
        # Allow changes without changing originals
        if isinstance(bdaq_conf, dict):
            bdaq_conf = deepcopy(bdaq_conf)
        if isinstance(bench_config, dict):
            bench_config = deepcopy(bench_config)
        scan_config = deepcopy(scan_config)
        if isinstance(scan_config_per_chip, dict):
            scan_config_per_chip = deepcopy(scan_config_per_chip)

        self.errors_occured = False
        self.use_default_chip_configuration = scan_config.pop('use_default_chip_configuration', False)

        # Configuration parameters
        self.bdaq_conf_par = bdaq_conf
        self.bench_config_par = bench_config
        self.scan_config_par = scan_config
        self.scan_config_per_chip_par = scan_config_per_chip

        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.ana_proc = None  # analysis process for non-blocking analysis
        self.log = logger.setup_derived_logger(self.__class__.__name__)  # setup logger
        self._log_handlers_per_scan = []  # FIXME: all log handlers of all chips
        self.hardware_initialized = False
        self.initialized = False

        self.bdaq = None  # readout system, defined during scan init if not existing
        self.periphery = None  # readout periphery, defined during scan init if not existing
        self.chips = {}
        self.suffix = suffix
        self.monitor_NTC = multiprocessing.Event()
        self.interlock = multiprocessing.Event()

    def init(self, force=False):
        try:
            self.errors_occured = False
            self._init_environment()
            self._init_hardware(force)
            self._init_files()
            self.initialized = True
        except Exception as e:
            if self.periphery:
                self.periphery.close()
            if self.bdaq:
                self.bdaq.close()
            raise e

    def configure(self):
        ret_values = [None] * self.n_chips()
        try:
            if not self.initialized:
                raise RuntimeError('Cannot call configure() before init() is called!')
            # Deactivate all receiver to prevent recording useless data
            self._set_receiver_enabled(receiver=None, enabled=False)
            for i, _ in enumerate(self.iterate_chips()):
                with self._logging_through_handler(self.log_fh):
                    self.log.info('Configuring chip {0}...'.format(self.chip.get_sn()))
                    # Load masks from config
                    self._set_receiver_enabled(receiver=self.chip.receiver, enabled=True)
                    self._configure_masks()

                    # FIXME: This is a hack. Unfortunately, event building is only possible for ITkPix with external TLU words.
                    # Generate external trigger words every CMD repetition using TLU module. Needed for proper event building.
                    if self.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
                        self.bdaq.trigger_on_cmd_loop_start()
                        if self.chip_settings.get('use_ptot', False):
                            self.chip.enable_ptot()
                        # setting mux to VREF_PRE prevents soft errors?
                        self.chip.set_mux(name='VREF_PRE', measure='voltage')

                    # convert target injection to DVCAL
                    if self.scan_config.get('injection_target', False):
                        dvcal_target = self.chip.calibration._convert_e_to_DVCAL(target=self.scan_config['injection_target'])
                        self.log.info(f'Injection target: {self.scan_config["injection_target"]} in e, {dvcal_target} in DVCAL with {self.chip.calibration.e_conversion}')
                        self.scan_config['VCAL_HIGH'] = dvcal_target + self.scan_config.get('VCAL_MED', 500)

                    # Scan dependent configuration step before actual scan can be started (set enable masks etc.)
                    ret_values[i] = self._configure(**self.scan_config)
                    self.periphery.get_module_power(module=self.module_settings['name'], log=True)
                    self._set_receiver_enabled(receiver=self.chip.receiver, enabled=False)

            # Create general FIFO readout (for all chips/modules)
            self._configure_fifo_readout()

            # Make sure monitor filter is blocking for all receivers before starting scan
            self.bdaq.set_monitor_filter(mode='block')

            return ret_values
        except Exception as e:
            self._on_exception()
            raise e

    def scan(self):
        ret_values = [None] * self.n_chips()
        try:
            if not self.initialized:
                raise RuntimeError('Cannot call scan() before init() is called!')
            # scan() does not the same as start() anymore
            try:
                self.fifo_readout
            except AttributeError:  # configure not called
                raise RuntimeError('Cannot call scan() before configure() is called!')
            if self.is_parallel_scan:
                # Enable all channels of defined chips
                for _ in self.iterate_chips():
                    self.periphery.get_module_power(module=self.module_settings['name'], log=True)
                    self._set_receiver_enabled(receiver=self.chip.receiver, enabled=True)
                self._scan(**self.scan_config)
                for _ in self.iterate_chips():
                    self._set_receiver_enabled(receiver=self.chip.receiver, enabled=False)
            else:
                for i, _ in enumerate(self.iterate_chips()):
                    with self._logging_through_handler(self.log_fh):
                        self.periphery.get_module_power(module=self.module_settings['name'], log=True)
                        if self.configuration['bench']['hardware'].get('use_QMS_card', False):
                            self.module_slot = self.qms_dict[self.module_settings['name']]
                            self.qms.select_mux(self.module_slot, self.name)
                        self._set_receiver_enabled(receiver=self.chip.receiver, enabled=True)
                        ret_values[i] = self._scan(**self.scan_config)
                        self._set_receiver_enabled(receiver=self.chip.receiver, enabled=False)

            # Finalize scan
            # Disable tlu module in case it was enabled.
            if self.bdaq.tlu_module_enabled:
                self.bdaq.disable_tlu_module()
            if self.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
                if self.chip.ptot_enabled:
                    self.chip.disable_ptot()

            # Disable LP mode
            if self.configuration['bench']['hardware'].get('LP_enable', False):
                self.log.info('Disabling LP mode')
                self.bdaq.reset_lpmode_pulse()

            # Add status info
            self._set_readout_status()
            for _ in self.iterate_chips():
                # Add additional after scan data
                self._add_chip_status()
                node = self.h5_file.create_group(self.h5_file.root, 'configuration_out', 'Configuration after scan step')
                self._write_config_h5(self.h5_file, node)
                self._store_scan_par_values(self.h5_file)  # store scan params in out node, since it is defined during scan step
                self.h5_file.close()

            return ret_values
        except Exception as e:
            self._on_exception()
            raise e

    def analyze(self):
        '''
            Loop over all chips in testbench and for each perform the analysis routine of the scan:
            - Analyze raw data, plotting
        '''
        ret_values = [None] * self.n_chips()
        try:
            if self.configuration['bench']['analysis'].get('skip', False):
                return
            if not self.configuration['bench']['analysis'].get('blocking', True):
                self.bdaq.close()  # BDAQ can not be closed before starting the process, since it is needed for error report.
            for i, _ in enumerate(self.iterate_chips()):
                with self._logging_through_handler(self.log_fh):
                    # Perform actual analysis
                    self.log.info('Starting analysis for ' + self.name + ' (' + self.chip_settings['chip_sn'] + ')')

                    ret_values[i] = self._analyze()
                    self._close_h5_file()

            if self.configuration['bench']['analysis'].get('module_plotting', True):
                with self._logging_through_handler(self.log_fh):
                    self._run_module_plotting()
            return ret_values
        except Exception as e:
            self._on_exception()
            raise e

    def analyze_file(self, raw_data_file):
        ''' Allows to re-analyze a chip raw data file without hardware

            filename: str
                path to raw data file. h5 suffix can be ommited
        '''

        raw_data_file = raw_data_file.replace('.h5', '') + '.h5'
        # Since the state machine init -> configure -> scan -> analyze -> close is broken here, set the minimum of required variables
        self.bench_config_par = self._load_testbench_cfg(self.bench_config_par)['bench']  # load std testbench settings

        with tb.open_file(raw_data_file) as in_file:
            chip_type = fill_dict_from_conf_table(in_file.root.configuration_in.scan.run_config)['chip_type']

        # Set from filename
        self.bench_config_par['general']['output_directory'] = os.path.dirname(raw_data_file)

        if self.scan_id not in raw_data_file:
            raise RuntimeError('Cannot run the analysis of %s on %s', self.scan_id, raw_data_file)
        self._init_environment()

        if chip_type.lower() != self.chip_settings['chip_type']:
            raise RuntimeError('Raw data file has different chip type than testbench (%s!=%s)' % (chip_type.lower(), self.chip_settings['chip_type']))

        self.output_filename = raw_data_file[:-3]
        self._analyze()

    def close(self):
        ''' Opposite of init

            Free hardware resources and store final config
        '''
        if self.initialized:
            self.bdaq.close()
            self.periphery.close()
            self._close_sockets()
            self.initialized = False
        if not self.ana_proc:  # h5 files are closed in ana proc
            for _ in self.iterate_chips():
                self._close_h5_file()
        with self._logging_through_handlers():
            if self.errors_occured:
                self.log.error(self.errors_occured)
                self.log.error('Scan failed!')
            elif self.interlock.is_set():
                self.log.error("Scan failed due to interlock")
            else:
                self.log.success('All done!')
        self._close_logfiles()
        if self.chips:
            self._unset_chip_handles()  # allows for gc of chip objects

    def start(self):
        '''
            Calls all required steps exluding the init/close step
        '''
        return_values = None
        self.configure()
        self.scan()
        if not self.interlock.is_set():
            if self.configuration['bench']['analysis'].get('blocking', True):
                return_values = self.analyze()
            else:
                self._close_sockets()

                self.log.info("Starting analysis in separate process...")
                self.ana_proc = multiprocessing.Process(target=self.analyze)
                self.ana_proc.start()

        # Get readout status once, for all receiver channels
        with self._logging_through_handlers():
            self._set_readout_status()

        return return_values

    def enable_hitor(self, enable=True):
        '''
            Configure the hitor display port connectors depending on scan type.
            If enable is False all HitOr ports are disabled.
            If enable is True and the scan is a parallel scan (e.g. external trigger scan, source scan),
            there is parallel data readout and thus all HitOr ports get activated and or-ed in the FPGA.
            If enable is True and the scan is *not* a parallel scan, the available HitOr ports
            are activated one after another, always one during the scan of one chip;
            the order depends on the sorting order in 'testbench.yaml' module section (see wiki...).

            Note: the TDC feature does currently only work with *one* chip.
        '''
        if enable:
            if self.is_parallel_scan:  # Enable all HitOr ports simultaneously
                active_ports_conf = 0b000
                for _ in range(len(self.chips)):
                    active_ports_conf = (active_ports_conf << 1) + 0b001
                self.bdaq.configure_hitor_inputs(active_ports_conf=active_ports_conf)
            else:
                if not hasattr(self, 'hitor_en'):
                    self.hitor_en = 0b001  # Enable first HitOr port (DP)
                else:
                    self.hitor_en = self.hitor_en << 1  # Enable next HitOr port (mDP)
                if self.hitor_en == 0b100:
                    self.hitor_en = 0b000  # Only two HitOr ports available
                self.bdaq.configure_hitor_inputs(active_ports_conf=self.hitor_en)
        else:
            self.bdaq.configure_hitor_inputs(active_ports_conf=0b000)  # Disable all ports

    def store_scan_par_values(self, scan_param_id, **kwargs):
        '''
            Manually store the scan parameter values for the scan parameter id
            This allows to reconstruct the scan parameter values for a given parameter state vector
        '''
        if self.scan_parameters.get(scan_param_id) and self.scan_parameters.get(scan_param_id) != kwargs:
            raise ValueError('You cannot change the scan parameter value of a scan parameter id')
        self.scan_parameters[scan_param_id] = kwargs

    def iterate_chips(self):
        ''' Iterate through the chips and set all chip handles

            Usage for accessing chip attributes:
            for _ in self.iterate_chips():
                self.name = 'my_chip'
        '''
        for c in self.chips.values():
            self._set_chip_handles(c)
            yield c

    def n_chips(self):
        return len(self.chips)

    def wait_for_analysis(self):
        ''' Block exction until analysis is finished '''
        if self.ana_proc:
            self.log.info('Waiting for analysis process to finish...')
            self.ana_proc.join()

    def notify(self, message):
        if self.configuration['bench']['notifications']['enable_notifications']:
            try:
                for user in self.configuration['bench']['notifications']['slack_users']:
                    self.slack.chat_postMessage(channel=user, text=message, username='BDAQ53 Bot', icon_emoji=':robot_face:')
            except Exception as e:
                self.log.error('Notification error: {0}'.format(e))

    def get_module_cfgs(self):
        ''' Returns the module configurations defined in the test bench '''
        return {k: v for k, v in self.configuration['bench']['modules'].items() if 'identifier' in v.keys()}

    def get_n_modules(self):
        return len(self.get_module_cfgs())

    def __enter__(self):
        self.initialized = False
        self.init()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def _configure(self, **kwargs):
        '''
            Place here in derived class: configuration steps that need to be performed before each scan,
            but don't belong to the actual scan routine.
        '''
        self.log.info('No _configure() method implemented in scan. Use std. configuration.')

    def _scan(self, **kwargs):
        raise NotImplementedError('ScanBase._scan() not implemented')

    def _analyze(self, **_):
        self.log.warning('analyze() method not implemented; do not analyze data')

    def _init_environment(self):
        self.timestamp = time.strftime("%Y%m%d_%H%M%S")
        self.run_name = self.timestamp + '_' + self.scan_id
        self.start_timestamp = self.timestamp  # start of scan
        self.stop_timestamp = None  # stop of scan (will be calculatd after scan is done)
        self.ext_trig_num = 0  # reqired for trigger based analysis
        self.context = zmq.Context()  # one context per process to manage sockets
        # Configuration with testbench and scan configuration configs (dict-like object)
        self.configuration = self._load_testbench_cfg(self.bench_config_par)  # fill self.configuration['bench'] with provided testbench config
        bu.recursive_update(self.configuration['bench'], self.scan_config_par.get('bench', {}))  # Update testbench configuration from scan configuration

        # Main working directory
        if self.configuration['bench']['general']['output_directory'] is not None:
            self.working_dir = self.configuration['bench']['general']['output_directory']
        else:
            self.working_dir = os.path.join(os.getcwd(), "output_data")

        self.timestamp = time.strftime("%Y%m%d_%H%M%S")
        self.run_name = self.timestamp + '_' + self.scan_id
        self.use_run_number = self.configuration['bench']['general'].get('use_run_number', False)

        self._create_chip_container(self.scan_config_par, self.scan_config_per_chip_par)  # fill self.chips with chip container objects from testbench and parameters
        self._setup_slack()

        # Instantiate periphery and RO hardware (append log to all chip log files)
        with self._logging_through_handlers():
            self.log.info('Initializing %s...', self.__class__.__name__)
            if self.is_parallel_scan:
                self.log.info('Run scan on all chips in parallel')
            if not self.periphery:  # create periphery object only once
                self.periphery = BDAQ53Periphery(bench_config=self.configuration['bench'], name=self.scan_id)
            if not self.bdaq:  # create bdaq object only once
                self.bdaq = BDAQ53(conf=self.bdaq_conf_par, bench_config=self.configuration['bench'])

        # Number of failed register reads is counted per chip
        max_reg_read_failures = self.configuration['bench']['general'].get("max_reg_read_failures", None)

        # Instantiate RD53 chips
        for _ in self.iterate_chips():
            with self._logging_through_handler(self.log_fh):
                if self.chip:  # create chip object only once
                    continue
                if 'rd53a' in self.chip_settings['chip_type']:
                    self.chip = RD53A(self.bdaq, chip_sn=self.chip_settings['chip_sn'], chip_id=self.chip_settings['chip_id'], receiver=self.chip_settings['receiver'], config=self.chip_conf, max_reg_read_failures=max_reg_read_failures)
                elif 'itkpixv1' in self.chip_settings['chip_type'].lower():
                    self.chip = ITkPixV1(self.bdaq, chip_sn=self.chip_settings['chip_sn'], chip_id=self.chip_settings['chip_id'], receiver=self.chip_settings['receiver'], config=self.chip_conf, max_reg_read_failures=max_reg_read_failures)
                elif 'crocv1' in self.chip_settings['chip_type'].lower():
                    self.chip = CROCv1(self.bdaq, chip_sn=self.chip_settings['chip_sn'], chip_id=self.chip_settings['chip_id'], receiver=self.chip_settings['receiver'], config=self.chip_conf, max_reg_read_failures=max_reg_read_failures)
                elif 'itkpixv2' in self.chip_settings['chip_type'].lower():
                    self.chip = ITkPixV2(self.bdaq, chip_sn=self.chip_settings['chip_sn'], chip_id=self.chip_settings['chip_id'], receiver=self.chip_settings['receiver'], config=self.chip_conf, max_reg_read_failures=max_reg_read_failures)
                else:
                    self.log.error('Chip type %s not supported', self.chip_settings['chip_type'])
                    raise NotImplementedError('Chip type %s not supported', self.chip_settings['chip_type'])
                if self.configuration['bench']['general'].get('use_database', False):
                    manage_databases.check_chip_in_database(self.chip_settings['chip_type'], self.chip_settings['chip_sn'])
        if self.configuration['bench']['general'].get('interlock', False):
            self.monitor_NTC.set()
            self.passive_sub = self.context.socket(zmq.SUB)
            self.passive_sub.connect('tcp://127.0.0.1:7000')
            self.passive_sub.setsockopt(zmq.SUBSCRIBE, b'')

    def _init_files(self):
        for _ in self.iterate_chips():
            self.h5_file = tb.open_file(self.output_filename + '.h5', mode='w', title=self.scan_id)

            # Create config nodes
            self.h5_file.create_group(self.h5_file.root, 'configuration_in', 'Configuration before scan')
            self._write_config_h5(self.h5_file, self.h5_file.root.configuration_in)

            # Create data nodes
            self.raw_data_earray = self.h5_file.create_earray(self.h5_file.root, name='raw_data', atom=tb.UIntAtom(),
                                                              shape=(0,), title='raw_data', filters=FILTER_RAW_DATA)
            self.meta_data_table = self.h5_file.create_table(self.h5_file.root, name='meta_data', description=MetaTable,
                                                             title='meta_data', filters=FILTER_TABLES)
            if self.monitor_NTC.is_set():
                self.environmental_data_table = self.h5_file.create_table(self.h5_file.root, name='environmental_data',
                                                                          description=dict({'timestamp': tb.Float128Col(pos=0), 'NTC_temp': tb.Float64Col(pos=1),
                                                                                            'T_1': tb.Float64Col(pos=2), 'T_2': tb.Float64Col(pos=3), 'Hum_1': tb.Float64Col(pos=4),
                                                                                            'Hum_2': tb.Float64Col(pos=5), 'dew_1': tb.Float64Col(pos=6), 'dew_2': tb.Float64Col(pos=7),
                                                                                            'HV': tb.Float64Col(pos=8), 'I_leak': tb.Float64Col(pos=9), 'Vin': tb.Float64Col(pos=10), 'Iin': tb.Float64Col(pos=11),
                                                                                            "interlock": tb.BoolCol(pos=12)}), title='environmental_data', filters=FILTER_TABLES)

            if self.scan_id not in ['test_efuse', 'dac_linearity_scan', 'sldo_iv_scan', 'meas_inj_cap', 'vcal_calibration', 'analog_readback', 'trim_scan']:
                self.trigger_table = self.h5_file.create_table(self.h5_file.root, name='trigger_table', description=MapTable,
                                                               title='trigger_table', filters=FILTER_TABLES)
                self.ptot_table = self.h5_file.create_table(self.h5_file.root, name='ptot_table', description=PtotTable,
                                                            title='ptot_table', filters=FILTER_TABLES)

            # Setup data sending
            socket_addr = self.chip_settings.get('send_data', None)
            if socket_addr:
                try:
                    self.socket = self.context.socket(zmq.PUB)  # publisher socket
                    self.socket.setsockopt(zmq.LINGER, 0)
                    self.socket.bind(socket_addr)
                    self.log.debug('Sending data to server %s', socket_addr)
                except zmq.error.ZMQError:
                    self.log.exception('Cannot connect to socket for data sending.')
                    self.socket = None
            else:
                self.socket = None

    def _init_hardware(self, force):
        if not self.hardware_initialized or force:
            with self._logging_through_handlers():  # TODO: log power supply logs for chips of same module only
                if self.periphery.enabled:
                    self.log.debug('Initialize hardware')
                    self.periphery.init()

                if self.periphery.enabled:
                    self.periphery.power_on_BDAQ()

                    # FIXME
                    for module_cfg_key, module_cfg in self.get_module_cfgs().items():
                        if 'powersupply' in module_cfg:
                            if module_cfg.get('power_cycle', False):
                                self.periphery.power_off_module(module_cfg_key)
                                time.sleep(1)
                            self.periphery.power_on_module(module_cfg_key, **module_cfg['powersupply'])

                if self.configuration['bench']['hardware'].get('use_QMS_card', False):
                    self.qms = QMS_Card(intf=self.bdaq['i2c'], module_switch_addr=0x90)
                    self.qms.init(bdaq=self.bdaq)

                    if 'qms_dict' in self.configuration['bench']['hardware'].keys():
                        self.qms_dict = {}
                        for slot, module in self.configuration['bench']['hardware']['qms_dict'].items():
                            if module is not None:
                                self.qms_dict[module] = slot
                else:
                    self.bdaq.init()
                self.bdaq.print_powered_dp_connectors()

                # Enable LP mode
                if self.configuration['bench']['hardware'].get('LP_enable', False):
                    self.log.info('Enabling LP mode')
                    # 100 kHz AC signal for low power mode
                    self.bdaq.configure_lpmode_pulse(pulse_length=400, pulse_delay=400)
                    self.bdaq.start_lpmode_pulse()

                self.log.info('Initializing chips...')

            # Keep all Aurora receiver disabled until the scan starts
            self._set_receiver_enabled(enabled=False)

            def configure_loop():
                # Reset CMD state mashine, creates glitch that requires often a new PLL lock and AURORA sync
                # Likely not required at chip init, was done for RD53A
                # self.bdaq['cmd'].reset()
                write_chip_reset = True
                for _ in self.iterate_chips():
                    with self._logging_through_handler(self.log_fh):
                        self._set_receiver_enabled(receiver=self.chip.receiver, enabled=True)

                        # Initialize chip
                        self.chip.init(write_chip_reset=write_chip_reset)  # resets masks to std. config

                        # Set mask settings
                        # Set TDAC mask, only available if previous file exists
                        # Do not set other masks, but use std. config for them
                        # Not really easy to understand logic: https://gitlab.cern.ch/silab/bdaq53/-/issues/401
                        if self.chip_conf['masks']:
                            self.chip.masks['tdac'] = deepcopy(self.chip_conf['masks']['tdac'])
                        if not np.any(self.chip_conf['use_pixel']):  # first scan has no use_pixel mask defined
                            self.chip_conf['use_pixel'] = np.ones_like(self.chip.masks['enable'])
                        # Unset disabled pixels in use_pixel mask, issue #456
                        if self.chip_conf.get('disable_pixel'):
                            for p in self.chip_conf.get('disable_pixel'):
                                try:
                                    p_idx = ast.literal_eval(p)
                                    self.chip_conf['use_pixel'][p_idx] = 0
                                except SyntaxError:  # assume slice notation
                                    slices = str_to_slices(p)
                                    self.chip_conf['use_pixel'][slices[0], slices[1]] = 0
                        self.chip.masks.disable_mask = deepcopy(self.chip_conf['use_pixel'])

                        # Check if chip is configured properly
                        if self.bdaq.board_version != 'SIMULATION':
                            self.chip.registers.check_all()

                        self._set_receiver_enabled(receiver=self.chip.receiver, enabled=False)
                    write_chip_reset = False

            for _ in range(5):
                try:
                    configure_loop()
                except Exception as ex:
                    if 'Timeout while waiting for PLL to lock' in str(ex):
                        self.log.exception(ex)
                    else:
                        raise
                else:
                    break
            else:
                raise RuntimeError('Tried locking the PLL too many times.')

            self.hardware_initialized = True
        else:
            with self._logging_through_handlers():
                self.log.info('Hardware already initialized, skip initialization!')

    def _set_chip_handles(self, chip):
        ''' Add the chip properties that are kept in the chip container
            to this class.

            This allows to access via the class attributes (handels)
            e.g. the h5 file with self.h5_file
        '''
        cls = type(self)

        def set_property_one_chip(name, chip):

            def setter(self, value):
                chip.__dict__[name] = value

            def getter(self):
                return chip.__dict__[name]

            setattr(cls, name, property(fset=setter, fget=getter))

        def set_property_many_chips(name):

            def setter(self, value):
                for chip in self.chips:
                    chip.__dict__[name] = value

            def getter(self):
                raise NotImplementedError('Cannot read from handle that points to many chips')

            setattr(cls, name, property(fset=setter, fget=getter))

        if chip:  # set scan base handles to one chip
            for name in chip.__dict__.keys():  # loop instance attributes
                set_property_one_chip(name, chip)
            self.configuration['scan'] = chip.scan_config  # API compatibility
        else:  # set scan base handles to all chips
            raise NotImplementedError('Multi chip handles are not supported')
            for name in self.chip.__dict__.keys():  # loop instance attributes
                set_property_many_chips(name)

    def _unset_chip_handles(self):
        ''' Remove the handles to a chip object.

            Required, otherwise python gc will not collect the chip objects (#442)
        '''
        chip_dummy = ChipContainer(name=None, chip_settings=None, chip_conf=None, module_settings=None, output_filename=None, output_dir=None, log_fh=None, scan_config=None)
        self._set_chip_handles(chip=chip_dummy)

    def _get_chip_at_rx(self, receiver):
        ''' Activates the handles for the chip at the receiver '''
        for _ in self.iterate_chips():
            if self.chip_settings['receiver'] == receiver:
                return self.name

    def _load_testbench_cfg(self, bench_config):
        ''' Load the bench config into the scan

            Parameters:
            ----------
            bench_config : str or dict
                    Testbench configuration (configuration as dict or its filename as string)
        '''
        conf = au.ConfigDict()
        try:
            if bench_config is None:
                bench_config = TESTBENCH_DEFAULT_FILE
            with open(bench_config) as f:
                conf['bench'] = yaml.full_load(f)
        except TypeError:
            conf['bench'] = bench_config

        return conf

    def _setup_slack(self):
        ''' Setup Slack notifications
        '''
        if not self.configuration['bench']['notifications']['enable_notifications']:
            self.slack = None
        else:
            slack_token = self.configuration['bench']['notifications']['slack_token']
            if os.path.isfile(os.path.expanduser(slack_token)):
                with open(os.path.expanduser(slack_token), 'r') as token_file:
                    token = token_file.read().strip()
            else:
                token = slack_token
            self.slack = WebClient(token)

    def _parse_chip_cfg_file(self, file_name):
        if file_name.endswith('h5'):  # config from data file
            with tb.open_file(file_name, 'r') as in_file_h5:
                try:
                    configuration = in_file_h5.root.configuration_out
                    self.log.info('Use chip configuration at configuration_out node')
                except tb.NoSuchNodeError:  # out config does not exist due to aborted run
                    try:
                        configuration = in_file_h5.root.configuration_in
                        self.log.info('Use chip configuration at configuration_in node')
                    except tb.NoSuchNodeError:  # out config does not exist due to aborted run
                        configuration = None
                if not configuration:
                    raise RuntimeError('No configuration found in ' + file_name)
                chip_conf = {}
                settings = fill_dict_from_conf_table(configuration.chip.settings)
                chip_conf['chip_type'] = settings['chip_type']
                chip_conf['calibration'] = fill_dict_from_conf_table(configuration.chip.calibration)
                chip_conf['trim'] = fill_dict_from_conf_table(configuration.chip.trim)
                chip_conf['registers'] = fill_dict_from_conf_table(configuration.chip.registers)
                chip_conf['use_pixel'] = configuration.chip.use_pixel[:]
                chip_conf['masks'] = {}
                for node in configuration.chip.masks:
                    chip_conf['masks'][node.name] = node[:]
        else:  # std config from yaml file
            with open(file_name) as conf_yaml:
                chip_conf = yaml.full_load(conf_yaml)
                # No pixel config in yaml
                chip_conf['masks'] = {}
                chip_conf['use_pixel'] = {}
        return chip_conf

    def _create_chip_container(self, scan_config, scan_config_per_chip):
        ''' Extract the chip and scan configurations from mulitple sources

            Set all configurations that do not require the init step
        '''
        # Load scan configuration from default arguments of _configure() and _scan() and overwrite with scan_config.
        args = inspect.getfullargspec(self._configure)
        scan_configuration = {key: args[3][i] for i, key in enumerate(args[0][1:])}
        args = inspect.getfullargspec(self._scan)
        scan_configuration.update({key: args[3][i] for i, key in enumerate(args[0][1:])})
        scan_configuration.update(scan_config)

        # Detect modules defined in testbench by the definition of a module serial number
        module_cfgs = self.get_module_cfgs()

        for mod_name, mod_cfg in module_cfgs.items():
            for k, v in mod_cfg.items():
                # Detect chips defined in testbench by the definition of a chip serial number
                if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:
                    output_dir = self.working_dir + os.sep + mod_name + os.sep + k
                    if not os.path.exists(output_dir):
                        os.makedirs(output_dir)
                    name = k  # chip name
                    # Create log file handler for the chip
                    chip_fh = self._create_logfile_handler(output_dir + os.sep + name)
                    self._log_handlers_per_scan.append(chip_fh)
                    # Create module config corresponding to this chip
                    module_settings = deepcopy(mod_cfg)
                    module_settings.pop(k)
                    module_settings['name'] = mod_name
                    # Set chip config file name
                    with self._logging_through_handlers():
                        chip_settings = v
                        chip_settings['name'] = k
                        if self.use_default_chip_configuration:
                            std_cfg = RD53A_DEFAULT_CONFIG_FILE
                            if 'itkpixv1' in chip_settings['chip_type'].lower():
                                std_cfg = ITKPIXV1_DEFAULT_CONFIG_FILE
                            elif 'crocv1' in chip_settings['chip_type'].lower():
                                std_cfg = CROCV1_DEFAULT_CONFIG_FILE
                            chip_settings['chip_config_file'] = std_cfg
                        elif not chip_settings['chip_config_file']:  # take chip cfg from latest scan
                            chip_settings['chip_config_file'] = bu.get_latest_config_node_from_files(directory=output_dir)
                            if not chip_settings['chip_config_file']:  # fallback to yaml
                                chip_settings['chip_config_file'] = bu.get_latest_chip_configuration_file(directory=output_dir)
                            if not chip_settings['chip_config_file']:  # fallback to std. config
                                std_cfg = RD53A_DEFAULT_CONFIG_FILE
                                if 'itkpixv1' in chip_settings['chip_type'].lower():
                                    std_cfg = ITKPIXV1_DEFAULT_CONFIG_FILE
                                elif 'crocv1' in chip_settings['chip_type'].lower():
                                    std_cfg = CROCV1_DEFAULT_CONFIG_FILE
                                elif 'itkpixv2' in chip_settings['chip_type'].lower():
                                    std_cfg = ITKPIXV2_DEFAULT_CONFIG_FILE
                                self.log.warning("No explicit configuration supplied for chip {0}. Using '{1}'!".format(chip_settings['chip_sn'], std_cfg))
                                chip_settings['chip_config_file'] = std_cfg
                        self.log.info('Loading chip configuration for chip {0} from {1}'.format(chip_settings['chip_sn'], chip_settings['chip_config_file']))
                        chip_conf = self._parse_chip_cfg_file(chip_settings['chip_config_file'])
                        if chip_conf['chip_type'].lower() != chip_settings['chip_type'].lower():
                            raise TypeError('Chip configuration file is for the ' + chip_conf['chip_type'] + '. But chip ' + chip_settings['chip_type'] + ' defined in the testbench!')
                    # Set scan and chip configuration
                    scan_config = deepcopy(scan_configuration)  # scan config from scan definition
                    # Update chip config from test bench
                    chip_conf_overwrites = {'registers': deepcopy(chip_settings.get('registers', {})),
                                            'trim': deepcopy(chip_settings.get('trim', {})),
                                            'calibration': deepcopy(chip_settings.get('calibration', {})),
                                            'masks': deepcopy(chip_settings.get('masks', {})),
                                            'disable_pixel': deepcopy(chip_settings.get('disable_pixel', {}))
                                            }
                    chip_conf = bu.recursive_update(chip_conf, chip_conf_overwrites)
                    # Update chip config from scan definition
                    chip_conf_overwrites = deepcopy(scan_configuration.get('chip', {}))
                    chip_conf = bu.recursive_update(chip_conf, chip_conf_overwrites)
                    if scan_config_per_chip:
                        chip_specific_scan_config = scan_config_per_chip.get(mod_name, {}).get(name, {})  # scan config for this chip
                        chip_conf = bu.recursive_update(chip_conf, chip_specific_scan_config.get('chip', {}))
                    if self.use_run_number:
                        # check for filename that is not in use
                        self.run_number = 1
                        run_list = []
                        for f in os.listdir(output_dir):
                            if os.path.isfile(os.path.join(output_dir, f)) and 'run_' in f:
                                run_list.append(f.split('_')[1])
                        while True:
                            filename = 'run_' + str(self.run_number).zfill(4) + '_' + self.scan_id
                            if str(self.run_number).zfill(4) in run_list:
                                self.run_number += 1  # increase run number and try again
                                continue
                            else:
                                self.run_name = filename
                                break

                    output_filename = os.path.join(output_dir, self.run_name)
                    # Chek if file name exists already, can happen since file names are not guaranteed to be unique, issue # 445
                    i = 2
                    while os.path.isfile(output_filename + '.h5'):
                        output_filename = os.path.join(output_dir, self.run_name + '_%d' % i)  # append an index
                        i += 1

                    self.chips[mod_name + '_' + k] = ChipContainer(name=name, chip_settings=chip_settings, chip_conf=chip_conf, module_settings=module_settings,
                                                                   output_filename=output_filename, output_dir=output_dir, log_fh=chip_fh, scan_config=scan_config, suffix=self.suffix)
        with self._logging_through_handlers():
            self.log.info('Found %d chip(s) of %d module(s) defined in the testbench', len(self.chips), self.get_n_modules())

    def _write_config_h5(self, h5_file, node):
        ''' Write complete configuration to the provided node of a h5 file '''

        def write_dict_to_table(dictionary, node):
            for attr, val in dictionary.items():
                row = node.row
                row['attribute'] = attr
                try:
                    row['value'] = val
                except TypeError:  # value cannot be implicitly converted to string
                    row['value'] = str(val)
                row.append()
            node.flush()

        scan_node = h5_file.create_group(node, 'scan', 'Scan configuration')
        # Run configuration
        run_config_table = h5_file.create_table(scan_node, name='run_config', title='Run config', description=RunConfigTable)
        row = run_config_table.row
        row['attribute'] = 'scan_id'
        row['value'] = self.scan_id
        row.append()
        row = run_config_table.row
        row['attribute'] = 'run_name'
        row['value'] = self.run_name
        row.append()
        row = run_config_table.row
        row['attribute'] = 'software_version'
        row['value'] = bu.get_software_version()
        row.append()
        row = run_config_table.row
        row['attribute'] = 'module'
        row['value'] = self.module_settings['name']
        row.append()
        row = run_config_table.row
        row['attribute'] = 'chip_sn'
        row['value'] = self.chip.get_sn()
        row.append()
        row = run_config_table.row
        row['attribute'] = 'chip_type'
        row['value'] = self.chip.get_type()
        row.append()
        row = run_config_table.row
        row['attribute'] = 'receiver'
        row['value'] = self.chip.receiver
        row.append()
        row = run_config_table.row
        row['attribute'] = 'start_timestamp'
        row['value'] = self.start_timestamp
        row.append()
        row = run_config_table.row
        row['attribute'] = 'stop_timestamp'
        row['value'] = self.stop_timestamp
        row.append()

        # Scan configuration as provided during scan __init__
        scan_cfg_table = h5_file.create_table(scan_node, name='scan_config', title='Scan configuration', description=RunConfigTable)
        write_dict_to_table(self.scan_config, scan_cfg_table)

        chip_node = h5_file.create_group(node, 'chip', 'Chip configuration')
        # Chip register table
        register_table = h5_file.create_table(chip_node, name='registers', title='Registers', description=RegisterTable)
        for name, reg in self.chip.registers.items():
            row = register_table.row
            row['register'] = name
            row['value'] = reg.get()
            row.append()
        register_table.flush()
        # Chip calibration table
        calibration_table = h5_file.create_table(chip_node, name='calibration', title='Calibration', description=RunConfigTable)
        write_dict_to_table(self.chip.calibration.return_all_values(), calibration_table)
        # Chip trim table
        trim_table = h5_file.create_table(chip_node, name='trim', title='Trim values', description=RunConfigTable)
        write_dict_to_table(self.chip.configuration['trim'], trim_table)

        # Chip settings table
        settings_table = h5_file.create_table(chip_node, name='settings', title='Chip settings from test bench', description=RunConfigTable)
        write_dict_to_table(self.chip_settings, settings_table)

        # Settings of the module where this chip belongs to
        module_settings_table = h5_file.create_table(chip_node, name='module', title='Module settings from test bench', description=RunConfigTable)
        write_dict_to_table(self.module_settings, module_settings_table)

        # Chip masks
        mask_node = h5_file.create_group(chip_node, 'masks', 'Pixel masks (configuration per pixel and virtual disable mask)')
        for name, value in self.chip.masks.items():
            h5_file.create_carray(mask_node, name=name, title=name.capitalize(), obj=value, filters=FILTER_RAW_DATA)

        # Virtual enable mask
        h5_file.create_carray(chip_node, name='use_pixel', title='Select pixels to be used in scans', obj=self.chip.masks.disable_mask, filters=FILTER_RAW_DATA)

        bench_node = h5_file.create_group(node, 'bench', 'Test bench settings')

        for setting, values in self.configuration['bench'].items():
            if setting in ['modules']:  # settings parsed into chip container and stored seperately
                continue
            table = h5_file.create_table(bench_node, name=setting, title=setting.capitalize(), description=RunConfigTable)
            write_dict_to_table(values, table)

    def _set_readout_status(self):
        self.readout_status = self.fifo_readout.print_readout_status()

    def _get_readout_status(self, receiver):
        discard_counts, soft_error_counts, hard_error_counts = self.readout_status
        discard_count = discard_counts[int(receiver[2])]
        soft_error_count = soft_error_counts[int(receiver[2])]
        hard_error_count = hard_error_counts[int(receiver[2])]
        return discard_count, soft_error_count, hard_error_count

    def _add_chip_status(self):
        '''
            Read all important chip values and dump to raw data file
        '''
        self.h5_file.create_group(self.h5_file.root, 'chip_status', 'Chip status')

        # Add periphery monitoring data if any
        if self.periphery.enabled and self.configuration['bench']['periphery'].get('monitoring', False):
            monitoring_data = {}
            for group_name, group in self.periphery.monitoring_data.items():
                monitoring_data[group_name] = {}
                for table_name, table in group.items():
                    t = table[:]
                    for rid, row in enumerate(t):
                        if row['timestamp'] < time.mktime(time.strptime(self.timestamp, "%Y%m%d_%H%M%S")):
                            t = np.delete(t, rid, axis=0)

                    monitoring_data[group_name][table_name] = t

            self.h5_file.create_group(self.h5_file.root.chip_status, name='periphery_monitoring', title='Periphery Monitoring')
            for grp in monitoring_data.keys():
                group = self.h5_file.create_group(self.h5_file.root.chip_status.periphery_monitoring, grp)
                for name, table in monitoring_data[grp].items():
                    self.h5_file.create_table(group, name, table)

        if self.chip_settings['record_chip_status'] and self.bdaq.board_version != 'SIMULATION':
            discard_count, soft_error_count, hard_error_count = self._get_readout_status(self.chip.receiver)
            aurora_table = self.h5_file.create_table(self.h5_file.root.chip_status, name='aurora_link', title='Aurora link status', description=RunConfigTable)
            row = aurora_table.row
            row['attribute'] = 'discard_counter'
            row['value'] = discard_count
            row.append()
            row['attribute'] = 'soft_error_counter'
            row['value'] = soft_error_count
            row.append()
            row['attribute'] = 'hard_error_counter'
            row['value'] = hard_error_count
            row.append()
            aurora_table.flush()

            self._set_receiver_enabled(receiver=self.chip.receiver, enabled=True)
            voltages, currents = self.chip.get_chip_status()
            temperature = self.chip.get_temperature(log=False)
            self._set_receiver_enabled(receiver=self.chip.receiver, enabled=False)

            dac_currents_table = self.h5_file.create_table(self.h5_file.root.chip_status, name='ADCCurrents', title='ADC Currents', description=ChipStatusTable)
            dac_voltages_table = self.h5_file.create_table(self.h5_file.root.chip_status, name='ADCVoltages', title='ADC Voltages', description=ChipStatusTable)

            for name, value in currents.items():
                row = dac_currents_table.row
                row['attribute'] = name
                row['ADC'] = value[0]
                row['value'] = value[1]
                row.append()
            dac_currents_table.flush()
            for name, value in voltages.items():
                row = dac_voltages_table.row
                row['attribute'] = name
                row['ADC'] = value[0]
                row['value'] = value[1]
                row.append()
            dac_voltages_table.flush()

            # Temperature measurements
            other_table = self.h5_file.create_table(self.h5_file.root.chip_status, name='other', title='Other', description=RunConfigTable)
            row = other_table.row
            row['attribute'] = 'Chip temperature'
            row['value'] = temperature
            row.append()
            other_table.flush()

    def _store_scan_par_values(self, h5_file):
        '''
            Create scan_params table after a scan
        '''
        # Create parameter description
        keys = set()  # find all keys to make the table column names
        for par_values in self.scan_parameters.values():
            keys.update(par_values.keys())
        fields = [('scan_param_id', np.uint32)]
        # FIXME only float32 supported so far
        fields.extend([(name, np.float32) for name in keys])

        scan_par_table = h5_file.create_table(h5_file.root.configuration_out.scan, name='scan_params', title='Scan parameter values per scan parameter id', description=np.dtype(fields))
        for par_id, par_values in self.scan_parameters.items():
            a = np.full(shape=(1,), fill_value=np.NaN).astype(np.dtype(fields))
            for key, val in par_values.items():
                a['scan_param_id'] = par_id
                a[key] = np.float32(val)
            scan_par_table.append(a)

    def _configure_masks(self):
        '''
            Masks configuring steps always needed after chip reset and before scan configure
        '''

        if self.chip_settings['use_good_pixels_diff']:
            self.chip.masks.apply_good_pixel_mask_diff()
        if self.chip_settings['chip_type'].lower() in ['itkpixv1', 'crocv1', 'itkpixv2']:
            if self.chip_settings.get('use_ptot', False):
                # Automatically enable hitbus if PToT mode is enabled
                self.chip.masks['hitbus'] = self.chip.masks['enable']

        self.chip.masks.update(force=True)  # write all masks to chip

    def _configure_fifo_readout(self):
        self.fifo_readout = FifoReadout(self.bdaq)
        for receiver in self.bdaq.receivers:
            if self.bdaq.board_version != 'SIMULATION':  # Causes a timing issue in simulation
                self.bdaq.rx_channels[receiver].reset_logic()  # TODO: was done before at readout start, maybe not needed here
            self.bdaq.rx_channels[receiver].reset_counters()

    def _set_receiver_enabled(self, receiver=None, enabled=True):
        if receiver is not None:
            self.bdaq.rx_channels[receiver].set_en(enabled)
        else:
            for rx_channel in self.bdaq.rx_channels.values():
                rx_channel.set_en(enabled)

    def _run_module_plotting(self):
        data_files = []
        for _ in self.iterate_chips():
            if os.path.isfile(self.output_filename + '_interpreted.h5'):
                data_files.append(self.output_filename + '_interpreted.h5')
        if len(data_files) > 0 and self.module_settings.get('module_type', None) not in [None, '', 'single'] \
                and self.scan_id not in ['meas_inj_cap', 'vcal_calibration', 'dac_linearity_scan', 'analog_readback', 'sldo_iv_scan', 'trim_scan', 'NTC_readout', 'LOW_POWER_Scan', 'OVP_Scan', 'test_efuse']:
            with combine_module_files.CombineModuleFiles(data_files) as cmf:
                cmf.combine_standard_tables()
            with plotting.Plotting(analyzed_data_file=cmf.target_file) as p:
                p.create_standard_plots()

    def _close_h5_file(self):
        # Must be closed if already opened, otherwise access to file handle is only
        # possible using tb.file._open_files.close_all() (--> memory leak + file cannot be closed anymore)
        try:
            self.h5_file.close()
        except tb.exceptions.ClosedFileError:  # if scan was called the file is already closed
            pass
        # Reopen interpreted file to append final config after analysis
        if not self.errors_occured and os.path.isfile(self.output_filename + '_interpreted.h5'):
            with tb.open_file(self.output_filename + '_interpreted.h5', 'a') as h5_file:
                if '/configuration_out' not in h5_file:
                    self.log.info('Adding configuration after analysis')
                    node = h5_file.create_group(h5_file.root, 'configuration_out', 'Configuration after scan analysis')
                    self.stop_timestamp = time.strftime("%Y%m%d_%H%M%S")
                    self._write_config_h5(h5_file, node)
                    self._store_scan_par_values(h5_file)

    def _on_exception(self):
        ''' Called when exception occurs in main process '''
        self.errors_occured = traceback.format_exc()
        self.close()

    def _create_logfile_handler(self, output_filename):
        return logger.setup_logfile(output_filename + '.log')

    def _open_logfile(self, handler):
        logger.add_logfile_to_loggers(handler)

    def _close_logfile(self, handler):
        logger.close_logfile(handler)

    def _close_logfiles(self):
        for handler in self._log_handlers_per_scan:
            self._close_logfile(handler)

    def _close_sockets(self):
        if self.context:
            if self.monitor_NTC.is_set():
                time.sleep(1)
                self.passive_sub.close()
            for _ in self.iterate_chips():
                try:
                    if self.socket:
                        self.log.debug('Closing socket connection')
                        self.socket.close()
                        self.socket = None
                except AttributeError:
                    pass
            self.context.term()
            self.context = None

    @contextmanager
    def _logging_through_handler(self, handler):
        self._open_logfile(handler)
        try:
            yield
        finally:
            self._close_logfile(handler)

    @contextmanager
    def _logging_through_handlers(self):
        for handler in self._log_handlers_per_scan:
            self._open_logfile(handler)
        try:
            yield
        finally:
            for handler in self._log_handlers_per_scan:
                self._close_logfile(handler)

    # Readout methods

    @contextmanager
    def readout(self, scan_param_id=0, timeout=10.0, *args, **kwargs):

        self.scan_param_id = scan_param_id

        callback = kwargs.pop('callback', self.handle_data)

        if kwargs:
            self.store_scan_par_values(scan_param_id, **kwargs)

        self.fifo_readout.reset_channels()
        if not self.is_parallel_scan:
            self.fifo_readout.attach_channel(self.chip.receiver)
        else:
            for _ in self.iterate_chips():
                self.fifo_readout.attach_channel(self.chip.receiver)
        self.fifo_readout.set_callback(callback=callback)

        self.start_readout(*args, **kwargs)
        try:
            yield
        finally:
            if self.bdaq.board_version == 'SIMULATION':
                for _ in range(100):
                    self.bdaq.rx_channels[self.chip.receiver].get_rx_ready()
            self.stop_readout(timeout=timeout)

    def start_readout(self, *args, **kwargs):
        errback = kwargs.pop('errback', self.handle_err)
        reset_rx = kwargs.pop('reset_rx', False)
        reset_sram_fifo = kwargs.pop('reset_sram_fifo', True)
        no_data_timeout = kwargs.pop('no_data_timeout', None)
        fill_buffer = kwargs.pop('fill_buffer', False)

        self.fifo_readout.start(errback=errback, reset_rx=reset_rx, reset_sram_fifo=reset_sram_fifo, no_data_timeout=no_data_timeout, fill_buffer=fill_buffer)

    def stop_readout(self, timeout=10.0):
        self.fifo_readout.stop(timeout=timeout)

    def add_trigger_table_data(self, cmd_repetitions, scan_param_id):
        '''
            Mapping data to scan_param_id.
        '''
        self.trigger_table.row['cmd_number_start'] = self.ext_trig_num
        self.trigger_table.row['cmd_number_stop'] = self.ext_trig_num + cmd_repetitions
        self.trigger_table.row['cmd_length'] = cmd_repetitions
        self.trigger_table.row['scan_param_id'] = scan_param_id

        self.ext_trig_num += cmd_repetitions

        self.trigger_table.row.append()
        self.trigger_table.flush()

    def add_ptot_table_data(self, cmd_repetitions, scan_param_id, active_pixels):
        '''
            Map Ptot masking and scan parameter id to data.
        '''
        hitor_col = np.full(shape=(4,), fill_value=0xffff)
        hitor_row = np.full(shape=(4,), fill_value=0xffff)

        # Row and position inside core column is the same for the same hitor line within on mask step. Thus, store only last one.
        for col, row in zip(active_pixels[0], active_pixels[1]):
            # Its sufficient to store column position inside core column.
            # Exact column will be extracted using ptot word from which hitor lane and core column can be extracted
            col = np.mod(int(col), 8)  # Map column into core column.
            row = int(row)
            lane = self.chip.masks._get_hitor_lane(row, col)
            hitor_col[lane] = col
            hitor_row[lane] = row

        self.ptot_table.row['cmd_number_start'] = self.ext_trig_num
        self.ptot_table.row['cmd_number_stop'] = self.ext_trig_num + cmd_repetitions
        self.ptot_table.row['cmd_length'] = cmd_repetitions
        self.ptot_table.row['scan_param_id'] = scan_param_id
        for lane in range(4):
            self.ptot_table.row['hit_or_%i_col' % (lane + 1)] = hitor_col[lane]
            self.ptot_table.row['hit_or_%i_row' % (lane + 1)] = hitor_row[lane]
        self.ext_trig_num += cmd_repetitions

        self.ptot_table.row.append()
        self.ptot_table.flush()

    def handle_data(self, data_tuple, receiver):
        '''
            Handling of the raw data.

            Called from fifo readout per readout channel
        '''

        # Set handles to the chip at the channel
        self._get_chip_at_rx(receiver)

        total_words = self.raw_data_earray.nrows
        self.raw_data_earray.append(data_tuple[0])
        self.raw_data_earray.flush()

        len_raw_data = data_tuple[0].shape[0]
        self.meta_data_table.row['timestamp_start'] = data_tuple[1]
        self.meta_data_table.row['timestamp_stop'] = data_tuple[2]
        self.meta_data_table.row['error'] = data_tuple[3]
        self.meta_data_table.row['data_length'] = len_raw_data
        self.meta_data_table.row['index_start'] = total_words
        total_words += len_raw_data
        self.meta_data_table.row['index_stop'] = total_words
        self.meta_data_table.row['scan_param_id'] = self.scan_param_id

        self.meta_data_table.row.append()
        self.meta_data_table.flush()
        if self.monitor_NTC.is_set():
            try:
                if not self.passive_sub.poll(timeout=10, flags=zmq.POLLIN):
                    pass
                data = self.passive_sub.recv(flags=zmq.NOBLOCK)
                data, meta_data = ou.simple_dec(data)
                self.environmental_data_table.row['timestamp'] = meta_data["timestamp"]
                self.environmental_data_table.row['interlock'] = meta_data["interlock"]["interlock"]
                self.environmental_data_table.row['T_1'] = data[0]
                self.environmental_data_table.row['T_2'] = data[1]
                self.environmental_data_table.row['Hum_1'] = data[2]
                self.environmental_data_table.row['Hum_2'] = data[3]
                self.environmental_data_table.row['dew_1'] = data[4]
                self.environmental_data_table.row['dew_2'] = data[5]
                self.environmental_data_table.row['NTC_temp'] = data[6]
                self.environmental_data_table.row['HV'] = data[7]
                self.environmental_data_table.row['I_leak'] = data[8]
                self.environmental_data_table.row["Vin"] = data[9]
                self.environmental_data_table.row["Iin"] = data[10]
                self.environmental_data_table.row.append()
                self.environmental_data_table.flush()
                if meta_data["interlock"]["interlock"]:
                    self.log.error('interlock was raised! Aborting run...')
                    self.fifo_readout.get_interlock_status(interlock=True)
                    self.interlock.set()
                    self.fifo_readout.force_stop.set()
            except zmq.Again:
                pass
        if self.socket:
            if self.ptot_table is not None:
                p = self.ptot_table[:]
            else:
                p = None
            send_data(self.socket, data=data_tuple, scan_par_id=self.scan_param_id, ptot_table=p)

    def handle_err(self, exc):
        ''' Handle errors when readout is started '''
        msg = '%s' % exc[1]
        if msg:
            self.log.error('%s', msg)
        if self.configuration['bench']['general'].get('abort_on_rx_error', True):
            if issubclass(exc[0], fifo_readout.FifoError):  # only abort on fifo errors (not soft error)
                self.log.error('Aborting run...')
                self.fifo_readout.force_stop.set()

    @property
    def data_buffer(self):
        '''
            Access data buffer of current receiver
        '''
        return self.fifo_readout.get_data_buffer(self.chip.receiver)


if __name__ == '__main__':
    pass
