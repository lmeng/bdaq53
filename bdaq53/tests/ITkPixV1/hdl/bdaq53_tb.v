/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ns / 1ps

`include "glbl.v"

`define BDAQ53

`ifdef INCLUDE_DUT
    `include "rtl/top/RD53B.sv"
    `include "rtl/common/defines.sv"
    `include "rtl/models/models.sv"      // **NOTE: contains also `include directive for PADFRAME sources under OpenAccess
    `include "rtl/top/PixelArray.sv"
    `include "rtl/top/ChipBottom.sv"
`endif

`default_nettype wire
//    `include "RD53_DM/Libs/PDK_2017/digital/Front_End/verilog/tcbn65lpbwp12tlvt_200a/tcbn65lpbwp12tlvt_pwr.v"

`include "bram_fifo/bram_fifo.v"
`include "bram_fifo/bram_fifo_core.v"
`include "utils/clock_multiplier.v"
`include "utils/clock_divider.v"


module tb #(
    // FIRMWARE VERSION
    parameter VERSION_MAJOR = 8'd0,
    parameter VERSION_MINOR = 8'd0,
    parameter VERSION_PATCH = 8'd0
)(
    output reg          BUS_CLK,
    input wire          BUS_RST,
    input wire  [31:0]  BUS_ADD,
    inout wire  [31:0]  BUS_DATA,
    input wire          BUS_RD,
    input wire          BUS_WR,
    output wire         BUS_BYTE_ACCESS,

    output wire         CLK_BX,
    input wire [384*400-1:0] HIT,
    input wire          TRIGGER,
    output wire         RESET_TB
);


// -------  MODULE ADREESSES  ------- //
localparam FIFO_BASEADDR = 32'h8000;
localparam FIFO_HIGHADDR = 32'h9000-1;

localparam FIFO_BASEADDR_DATA = 32'h8000_0000;
localparam FIFO_HIGHADDR_DATA = 32'h9000_0000;

localparam ABUSWIDTH = 32;
assign BUS_BYTE_ACCESS = BUS_ADD < 32'h8000_0000 ? 1'b1 : 1'b0;

initial BUS_CLK = 1'b0;
always #(5000 / 2) BUS_CLK = !BUS_CLK;


// ----- Clock (mimics a PLL) -----
localparam PLL_MUL            = 5;
localparam PLL_DIV_CLK_CMD    = 25;
localparam PLL_DIV_BX_CLK     = 4;
localparam PLL_LOCK_DELAY     = 1000*1000;

wire PLL_VCO, CMD_CLK, BX_CLK;
clock_multiplier #( .MULTIPLIER(PLL_MUL)  ) i_clock_multiplier( .CLK(BUS_CLK),                      .CLOCK(PLL_VCO)  );
clock_divider #(.DIVISOR(PLL_DIV_BX_CLK)  ) i_clock_divisor_bx( .CLK(CMD_CLK), .RESET(1'b0), .CE(), .CLOCK(BX_CLK));

// DRP_CLK: 100 MHz
localparam DRP_CLK_PERIOD  = 10*1000;
reg DRP_CLK=0;
initial DRP_CLK = 1'b0;
always #(DRP_CLK_PERIOD / 2) DRP_CLK = !DRP_CLK;

// GTX reference clock: 160 MHz for 1.28 Gbit/s. Has to match IP core configuration!
localparam CLK_MGT_REF_PERIOD  = 6.25*1000;
reg CLK_MGT_REF=0;
initial CLK_MGT_REF = 1'b0;
always #(CLK_MGT_REF_PERIOD / 2) CLK_MGT_REF = !CLK_MGT_REF;

// INIT clock for the Aurora core
localparam INIT_CLK_PERIOD = 5*1000;
reg INIT_CLK=0;
initial INIT_CLK = 1'b0;
always #(INIT_CLK_PERIOD / 2) INIT_CLK = !INIT_CLK;

// Aurora TX clock for data merging: 320 MHz
localparam CLK_AURORA_TX_PERIOD  = 3.128*1000;

//reg CLK_AURORA_TX=0;
//initial CLK_AURORA_TX = 1'b0;
//always #(CLK_AURORA_TX_PERIOD / 2) CLK_AURORA_TX = !CLK_AURORA_TX;

//wire CLK_AURORA_TX;
//assign CLK_AURORA_TX = tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.DATA_MERGE.DATA_MERGE_CLK640;

reg CLK_AURORA_TX;
initial begin
    CLK_AURORA_TX = 1'b0;
    #(CLK_AURORA_TX_PERIOD/10);
    forever CLK_AURORA_TX = #(CLK_AURORA_TX_PERIOD/2.0) ~CLK_AURORA_TX;
end



wire REFCLK_OUT;

reg LOCKED;
initial begin
    LOCKED = 1'b0;
    #(PLL_LOCK_DELAY) LOCKED = 1'b1;
end
// -------------------------

`ifdef INCLUDE_DUT
    // Get the clock from the chip
    wire CMD_BCR;
    assign CMD_CLK = dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.CMD_CLK;
    // assign CMD_CLK = CLK_MGT_REF; // Added for quick bypass simulation testing
    assign CMD_BCR = tb.dut.ChipBottom.DigitalChipBottom.GlobalConfiguration.RingOscBClear;
    assign RESET_TB = CMD_BCR;
    ODDR bx_clk_gate(.D1(1), .D2(1'b0), .C(BX_CLK), .CE(1'b1), .R(1'b0), .S(1'b0), .Q(CLK_BX) );
`else
    assign CMD_CLK = CLK_MGT_REF;
`endif


// Aurora / MGT clocks
wire MGT_REFCLK0_P, MGT_REFCLK0_N;
wire MGT_INITCLK_P, MGT_INITCLK_N;
assign MGT_REFCLK0_P    = CLK_MGT_REF;
assign MGT_REFCLK0_N    = ~CLK_MGT_REF;
assign MGT_INITCLK_P = INIT_CLK;
assign MGT_INITCLK_N = ~INIT_CLK;


// -------  TB SIGNALS  ------- //
wire RESET, DUT_RESET;
assign RESET = DUT_RESET;


// -------  USER MODULES  ------- //
wire FIFO_FULL, FIFO_READ, FIFO_NOT_EMPTY, FIFO_WRITE;
wire [31:0] AURORA_RX_FIFO_DATA;

bram_fifo
#(
    .BASEADDR(FIFO_BASEADDR),
    .HIGHADDR(FIFO_HIGHADDR),
    .BASEADDR_DATA(FIFO_BASEADDR_DATA),
    .HIGHADDR_DATA(FIFO_HIGHADDR_DATA),
    .ABUSWIDTH(32)
) i_out_fifo (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .FIFO_READ_NEXT_OUT(FIFO_READ),
    .FIFO_EMPTY_IN(!FIFO_WRITE),
    .FIFO_DATA(AURORA_RX_FIFO_DATA),

    .FIFO_NOT_EMPTY(FIFO_NOT_EMPTY),
    .FIFO_FULL(FIFO_FULL),
    .FIFO_NEAR_FULL(),
    .FIFO_READ_ERROR()
    );


// ----- BDAQ53A CORE ----- //
wire AURORA_MMCM_NOT_LOCKED;
wire AURORA_CLK_OUT;
wire BYPASS_MODE;
wire BYPASS_MODE_RESET;
wire BYPASS_CDR;
wire BYPASS_CMD_CLK_EN;
wire BYPASS_EXT_SER_CLK_EN;
wire CMD_OUT, CMD_DATA;
wire CKL_SEL;
wire [3:0] OUT_DATA_P, OUT_DATA_N;
wire LEMO_TX0, LEMO_TX1;
wire EEPROM_CS, EEPROM_SK, EEPROM_DI, EEPROM_DO;
wire I2C_SDA, I2C_SCL;
wire [3:0] DATA_MERGING_OUTPUT;
wire [3:0] DATA_MERGING_ENABLE;

bdaq53_core #(
    .VERSION_MAJOR(VERSION_MAJOR),
    .VERSION_MINOR(VERSION_MINOR)
   // .VERSION_PATCH(VERSION_PATCH)
) fpga (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),    // full 32 bit bus
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_BYTE_ACCESS(BUS_BYTE_ACCESS),

    // Clocks from oscillators and mux select
	.INIT_CLK(BUS_CLK),
    .DRP_CLK(DRP_CLK),
    .RX_CLK_IN_P(MGT_REFCLK0_P),
	.RX_CLK_IN_N(MGT_REFCLK0_N),

    // PLL
    .CLK_CMD(CMD_CLK),
    .AURORA_CLK_OUT(AURORA_CLK_OUT),
    .PLL_LOCKED(),
    .RX_CHANNEL_UP(),
    .RX_LANE_UP(),

    // Aurora lanes
    .MGT_RX_P(OUT_DATA_P),
	.MGT_RX_N(OUT_DATA_N),
    .TX_P(), .TX_N(),

    // CMD encoder
    .EXT_TRIGGER(TRIGGER),
    .CMD_DATA(CMD_DATA),
    .CMD_OUT(CMD_OUT),
    .CMD_WRITING(),
    .BYPASS_MODE(BYPASS_MODE),
    .BYPASS_MODE_RESET(BYPASS_MODE_RESET),
    .BYPASS_CDR(BYPASS_CDR),
    .BYPASS_CMD_CLK_EN(BYPASS_CMD_CLK_EN),
    .BYPASS_EXT_SER_CLK_EN(BYPASS_EXT_SER_CLK_EN),

    `ifdef BDAQ53 .GPIO_SENSE(4'b1110),
                  .GPIO_RESET(), `endif

    // Debug signals
    .DEBUG_TX0(LEMO_TX0),
	.DEBUG_TX1(LEMO_TX1),    //debug signal copy of CMD

    // I2C bus
    .I2C_SDA(I2C_SDA),
	.I2C_SCL(I2C_SCL),

    // FIFO cpontrol signals (TX FIFO of DAQ)
    .FIFO_DATA(AURORA_RX_FIFO_DATA),
    .FIFO_WRITE(FIFO_WRITE),
    .FIFO_EMPTY(!FIFO_NOT_EMPTY),
    .FIFO_FULL(FIFO_FULL),

    .AURORA_RESET(1'b0),
    .RX_HARD_ERROR(),
	.RX_SOFT_ERROR(),

    .CLK320(CLK_AURORA_TX),     // Aurora TX clock for data merging tests

    .DATA_MERGING_OUTPUT(DATA_MERGING_OUTPUT),  // Aurora data to chip inputs
    .DATA_MERGING_ENABLE(DATA_MERGING_ENABLE)
);


`ifdef INCLUDE_DUT

    wire [3:0] SER_DATA1G_P, SER_DATA1G_N;

  // change lane order
    assign OUT_DATA_P = {SER_DATA1G_P[0], SER_DATA1G_P[1], SER_DATA1G_P[2], SER_DATA1G_P[3]};
    assign OUT_DATA_N = {SER_DATA1G_N[0], SER_DATA1G_N[1], SER_DATA1G_N[2], SER_DATA1G_N[3]};



    //--------------- Begin INSTANTIATION Template ---------------//

    wire por_out_b ;             // use this to monitor internally-generated POR signal
    wire [3:0] hit_or ;          // use this to monitor Hit-ORs


    wire VDD_PLL = 1'b1 ;        // **NOTE: power connectivity is modeled for CDR/PLL block !
    wire GND_PLL = 1'b0 ;

    wire VDD_CML = 1'b1 ;        // **NOTE: power connectivity is modeled for CML drivers !
    wire GND_CML = 1'b0 ;


    RD53B dut (

    //------------------------------   DIGITAL INTERFACE   ------------------------------//

    //
    // Power-On Resets (POR)
    //
    .EXT_POR_CAP_PAD     ( RESET                        ),
//    .POR_RESET_B       ( por_out_b                  ),
//    .CMD_RESET_B    (                            ),

       //
       // Clock Data Recovery (CDR) input command/data stream [SLVS]
       //
       .CMD_P_PAD           (    CMD_DATA /*m_cmd_if.serial_in  */),
       .CMD_N_PAD           (   ~CMD_DATA /*~m_cmd_if.serial_in */),


    //
    // 4x general-purpose SLVS outputs, including Hit-ORs
    //
    .GPO_LVDS0_OR_SCAN_OUT0_P_PAD       ( hit_or[0]                  ),
    .GPO_LVDS0_OR_SCAN_OUT0_N_PAD       (                            ),

    .GPO_LVDS1_OR_SCAN_OUT1_P_PAD       ( hit_or[1]                  ),
    .GPO_LVDS1_OR_SCAN_OUT1_N_PAD       (                            ),

    .GPO_LVDS2_P_PAD       ( hit_or[2]                  ),
    .GPO_LVDS2_N_PAD       (                            ),

    .GPO_LVDS3_P_PAD       ( hit_or[3]                  ),
    .GPO_LVDS3_N_PAD       (                            ),

    //
    // general purpose monitor output [CMOS]
    //
    .GPO_CMOS_PAD          (                            ),

       //
       // 4x serial output data links @ 1.28 Gb/s [CML]
       //
       .GTX0_P_PAD          ( SER_DATA1G_P[3] /*DUT_OUT_DATA_P */),
       .GTX0_N_PAD          ( SER_DATA1G_N[3] /*DUT_OUT_DATA_N */),

       .GTX1_P_PAD          ( SER_DATA1G_P[2]       ),
       .GTX1_N_PAD          ( SER_DATA1G_N[2]       ),

       .GTX2_P_PAD          ( SER_DATA1G_P[1]       ),
       .GTX2_N_PAD          ( SER_DATA1G_N[1]       ),

       .GTX3_P_PAD          ( SER_DATA1G_P[0]       ),
       .GTX3_N_PAD          ( SER_DATA1G_N[0]       ),

    //
    // single serial output data link @ 5 Gb/s [GWT]
    //
    // .GWTX_P_PAD          (                       ),
    // .GWTX_N_PAD          (                       ),

    //
    // external 3-bit hard-wired local chip address [CMOS]
    //
    .BYPASS_MODE_PAD(BYPASS_MODE),
    .CHIP_ID0_OR_PIXEL_MATRIX_TEST_MODE_PAD         ( /*              1'b0 */    ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
    .CHIP_ID1_PAD         ( /*              1'b0 */    ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
    .CHIP_ID2_PAD         ( /*              1'b0 */    ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
    .CHIP_ID3_PAD         ( /*              1'b0 */    ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
    .DATA_IN0_OR_SCAN_IN0_P_PAD(    BYPASS_CMD_CLK_EN ? 1'b1 : DATA_MERGING_OUTPUT),
    .DATA_IN0_OR_SCAN_IN0_N_PAD(    BYPASS_CMD_CLK_EN ? 1'b0 : ~DATA_MERGING_OUTPUT),
    .DATA_IN1_OR_SCAN_IN1_P_PAD(    BYPASS_CMD_CLK_EN ? CMD_CLK : 1'b0),
    .DATA_IN1_OR_SCAN_IN1_N_PAD(    BYPASS_CMD_CLK_EN ? ~CMD_CLK : 1'b1),
    .DATA_IN2_OR_TEST_CLK40_P_PAD(  BYPASS_EXT_SER_CLK_EN ? AURORA_CLK_OUT : 1'b0),
    .DATA_IN2_OR_TEST_CLK40_N_PAD(  BYPASS_EXT_SER_CLK_EN ? ~AURORA_CLK_OUT : 1'b1),
    .DATA_IN3_OR_TEST_CLK160_P_PAD( BYPASS_MODE_RESET),
    .DATA_IN3_OR_TEST_CLK160_N_PAD( ~BYPASS_MODE_RESET),
    //
    // **BACKUP: single 1.28 Gb/s output line [SLVS]
    //
//    .GPO_LVDS0_OR_SCAN_OUT0_P_PAD      (                            ),
//    .GPO_LVDS0_OR_SCAN_OUT0_N_PAD      (                            ),
//
//    .GPO_LVDS1_OR_SCAN_OUT0_P_PAD      (                            ),
//    .GPO_LVDS1_OR_SCAN_OUT0_N_PAD      (                            ),
//
//    .GPO_LVDS2_P_PAD      (                            ),
//    .GPO_LVDS2_N_PAD      (                            ),
//
//    .GPO_LVDS3_P_PAD      (                            ),
//    .GPO_LVDS3_N_PAD      (                            ),

    //
    // **BACKUP: bypass/debug MUX controls [CMOS]
    //
//    .BYPASS_CMD_PAD      ( /*        bypass_cmd */    ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
//    .BYPASS_CDR_PAD      ( /*        bypass_cdr */    ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
//    .DEBUG_EN_PAD        ( /*          debug_en */    ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

    //
    // **BACKUP: external clocks [SLVS]
    //
//    .EXT_CMD_CLK_P_PAD   ( /*     ext_cmd_clk_p */    ),
//    .EXT_CMD_CLK_N_PAD   ( /*     ext_cmd_clk_n */    ),
//
//    .EXT_SER_CLK_P_PAD   ( /*     ext_ser_clk_p */    ),
//    .EXT_SER_CLK_N_PAD   ( /*     ext_ser_clk_n */    ),

    //
    // **OBSOLETE: no more external trigger [CMOS]
    //
    // .EXT_TRIGGER_PAD     ( m_ext_trig_if.trigger      ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

    //
    // **OBSOLETE: no more auxiliary external CalEdge/CalDly injection signals [CMOS]
    //
    //.INJ_STRB0_PAD       (                            ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
    //.INJ_STRB1_PAD       (                            ), // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

    .TEST_MODE_PAD       (                       1'b0 ), // **WARN: Temporary until PADFRAME is updated for DFT.
//    .TEST_RST_B_PAD    (                       1'b1 ), // **WARN: Temporary until PADFRAME is updated for DFT.
//    .TEST_CLK160_PAD     (                       1'b0 ), // **WARN: Temporary until PADFRAME is updated for DFT.
//    .TEST_CLK40_CDC_PAD  (                       1'b0 ), // **WARN: Temporary until PADFRAME is updated for DFT.
    .SCAN_EN_PAD         (                       1'b0 ), // **WARN: Temporary until PADFRAME is updated for DFT.

    //
    // spares [CMOS]
    //
    //.SPARE0_PAD          (                       ),
    //.SPARE1_PAD          (                       ),


    //------------------------------   ANALOG INTERFACE   ------------------------------//

    //
    // pixel inputs
    //
    .BUMP             ( HIT               ),

    //
    // external calibration
    //
    .IREF_TRIM0_PAD      ( 1'b0                       ),
    .IREF_TRIM1_PAD      ( 1'b1                       ),
    .IREF_TRIM2_PAD      ( 1'b1                       ),
    .IREF_TRIM3_PAD      ( 1'b0                       ),

    //
    // **OBSOLETE: no more external DC calibration levels
    //
    /////////.VINJ_HI_PAD         (                            ),
    /////////.VINJ_MID_PAD        (                            ),

    //
    // monitoring
    //
    .IMUX_OUT_PAD        (                            ),
    .VMUX_OUT_PAD        (                            )

    //
    // **BACKUP: external ADC reference voltages
    //
//    .VREF_ADC_IN_PAD     (                            ),
//    .VREF_ADC_OUT_PAD    (                            ),

    //
    // **BACKUP: bias currents
    //
//    .IREF_IN_PAD         (                            ),
//    .IREF_OUT_PAD        (                            ),


    //---------------------------   POWER/GROUND INTERFACE   ---------------------------//
    //
    // ANALOG Shunt-LDO
    //
//    .SLDO_REXT_A_PAD     (                            ),
//    .SLDO_RINT_A_PAD     (                            ),
//    .SLDO_VREF_A_PAD     (                            ),
//    .SLDO_VOFFSET_A_PAD  (                            ),
//    .SLDO_VDDSHUNT_A_PAD (                            ),
    //
    // DIGITAL Shunt-LDO
    //
//    .SLDO_REXT_D_PAD     (                            ),
//    .SLDO_RINT_D_PAD     (                            ),
//    .SLDO_VREF_D_PAD     (                            ),
//    .SLDO_VOFFSET_D_PAD  (                            ),
//    .SLDO_VDDSHUNT_D_PAD (                            )

//    .VDD_CML         ( VDD_CML                    ), // **NOTE: power connectivity is modeled for CML drivers !
//    .GND_CML         ( GND_CML                    )
) ;

    //---------------- End INSTANTIATION Template ---------------
`endif

// ---- aurora core for simulation -----//
localparam   CLOCKPERIOD_1 = 6.25;

reg     gsr_r;
reg     gts_r;

assign  glbl.GSR = gsr_r;
assign  glbl.GTS = gts_r;

initial
    begin
     gts_r      = 1'b0;
     gsr_r      = 1'b1;
     #(130*CLOCKPERIOD_1);
     gsr_r      = 1'b0;
     #(1600*CLOCKPERIOD_1);
end

initial begin
      $assertoff(0, tb.dut.ChipBottom.DigitalChipBottom ); // Disable assertions: avoid failing ERR_40MHz_long_down at start-up
      //force tb.dut.ChipBottom.DigitalChipBottom.NoCmdResetB     = 1'b0; //**TODO: should become part of CDR
      //force tb.dut.ChipBottom.DigitalChipBottom.NoCmdSyncResetB = 1'b0; //**TODO: should become part of CDR
      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.NoCmdResetB, 1'b0); // can get reset also after (sync_load_ff), but more realistic if free running clock
      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.NoCmdSyncResetB, 1'b0); // can get reset also after (sync_load_ff), but more realistic if free running clock

      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.ClkDiv4.div_notmr, $random ); // can get reset also after (sync_load_ff), but more realistic if free running clock
      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.phase_cnt, $random ); // phase_cnt<=phase_cnt+1 (and w/o lock cannot process reset)
      repeat (2) @( posedge tb.dut.ChipBottom.DigitalChipBottom.Clk40 ); // For sync reset of GCR_DEFAULT registers (magin number) to take place
      //force tb.dut.ChipBottom.DigitalChipBottom.NoCmdResetB     = 1'b1;
      //force tb.dut.ChipBottom.DigitalChipBottom.NoCmdSyncResetB = 1'b1;
      $asserton(0, tb.dut.ChipBottom.DigitalChipBottom );

      force tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.DATA_MERGE.div2_inst_ClkDserDiv2.Out = 1'b0;
      force tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.DATA_MERGE.div2_inst_ClkDserDiv4.Out = 1'b0;
      force tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.DATA_MERGE.div2_inst_ClkDserDiv8.Out = 1'b0;
      force tb.dut.ChipBottom.DigitalChipBottom.DeserAurora.Reset_b = 1'b0;

      #(10*CLOCKPERIOD_1)
      release tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.DATA_MERGE.div2_inst_ClkDserDiv2.Out;
      release tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.DATA_MERGE.div2_inst_ClkDserDiv4.Out;
      release tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.DATA_MERGE.div2_inst_ClkDserDiv8.Out;
      release tb.dut.ChipBottom.DigitalChipBottom.DeserAurora.Reset_b;


////      $deposit(tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.Clk40MHz, 2'b00);
//      $deposit ( tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.SEL_CAL_EDGE_DELAY[5:0], 6'h00);
//      $deposit ( tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.SEL_CMD_CLK_DELAY[5:0] , 6'h00);
//      $deposit ( tb.dut.ChipBottom.AnalogChipBottom.CDR_BLOCK.SEL_CMD_DATA_DELAY[5:0], 6'h00);
//      // Also initialize GCR_DEFAULT to random values for reset to have reset at startup
//      // $deposit to internal reg (not OutData) works in RTL (will need the path for GL )
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.GlobalConfiguration.GCR_6.gc_value_tmr[15:0], $random );
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.GlobalConfiguration.GCR_7.gc_value_tmr[15:0], $random );
////      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.GlobalConfiguration.SelDataMergeClk , 1'b1);
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.GlobalConfiguration.GCR_DEFAULT_CONFIG , 16'hac75);
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.GlobalConfiguration.GCR_DEFAULT_CONFIG_B , 16'h538a);
//
//// Initilize all ChSync registers to random value to get channel locked before able to send reset through commands
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.ClkDiv4.div_notmr, $random );
//
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.phase_cnt, $random ); // phase_cnt<=phase_cnt+1 (and w/o lock cannot process reset)
//      // Additional reset of Channel Sync not striclty required for RTL initialization
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.lock_cnt, $random );
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.sync_load_ff, $random );
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.no_sync_cnt, $random );
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.phase_cnt_last_sync, $random ); // init by phase_cnt
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.unlock_cnt, $random ); // Gets reset with clock
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.phase_cnt_thrhigh, $random ); // init by phase_cnt
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.int_locked, $random );
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.Locked, $random );
//      $deposit ( tb.dut.ChipBottom.DigitalChipBottom.ChannelSync.lock_loss_cnt, $random );
end

//initial begin
//    `ifdef INCLUDE_DUT
//        `ifdef INCA
//            $shm_open("/tmp/rd53a.shm");
//            //$shm_probe("ASM");
//            //$shm_probe(tb.dut.DCB.DataConcentrator, "A");
//            $shm_probe(tb.dut, "A");
//            $shm_probe(tb.dut.DCB, "ASM");
//            $shm_probe(tb, "A");
//            //$shm_probe(tb.dut.DCB.Aurora64b66b_Frame_Multilane_top, "A");
//            $shm_probe(tb.dut.PixelArray, "A");
//            $shm_probe(tb.fpga, "AS");
//            $shm_probe(tb.fpga.i_aurora_rx, "A");
//            $shm_probe(tb.fpga.i_aurora_rx.i_aurora_rx_core, "A");
//            $shm_probe(tb.fpga.i_aurora_rx.i_aurora_rx_core.aurora_frame, "AS");
//            //$shm_probe(tb.dut.DCB.CommandDecoder, "A");
//            //$shm_probe(tb.dut.DCB.ChannelSync, "A");
//            //$shm_probe(tb.dut.DCB.GlobalConfiguration, "A");
//            $shm_probe(tb.fpga.i_cmd_rd53.i_cmd_rd53_core, "A");
//            //$shm_probe(tb.dut.DCB.OutputCdcFifo, "A");
//            //$shm_probe(tb.dut.DCB.AlignData, "A");
//            //$shm_probe(tb.dut.PixelArray.chip_gen[20].column.iColumnReadControl, "A");
//        `endif
//    `endif
//
////    `ifdef KC705
////        $dumpfile("/tmp/rd53daq_k.vcd");
////    `elsif BDAQ53
////        $dumpfile("/tmp/rd53daq_b.vcd");
////    `endif
//  //  $dumpvars(22); //6
//end


endmodule
