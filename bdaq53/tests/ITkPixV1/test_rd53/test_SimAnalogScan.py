#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import logging
import yaml
import shutil
import os
import numpy as np
import tables as tb

from os.path import dirname, join, abspath

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

from bdaq53.tests import utils

configuration = {
    'n_injections': 2,

    'mask_step': 1,
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 384,
    'VCAL_MED': 500,
    'VCAL_HIGH': 1300
}


class AnalogScan(ScanBase):
    scan_id = 'analog_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=384, **_):
        '''_bdaq
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True

    def _scan(self, n_injections=3, VCAL_MED=500, VCAL_HIGH=1300, **_):
        '''
        Digital scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        '''

        self.log.info('Starting scan...')
        self.chip.enable_core_col_clock(range(0, 50))
        self.chip.enable_macro_col_cal(range(0, 50))
        self.chip.setup_analog_injection(VCAL_HIGH, VCAL_MED)
        with self.readout():
            for i in range((n_injections)):
                self.chip.inject_analog_single(repetitions=1)
        self.log.success('Scan finished')

    def _analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=True) as a:
            a.analyze_data()

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


class TestDigitalScan(unittest.TestCase):
    def test_scan_digital(self):
        ''' Enable ITkPixV1 settings'''
        with open(join(abspath(join(dirname(__file__), '..', '..', '..')), 'testbench.yaml'), 'r') as tbf:
            tb_dict = yaml.full_load(tbf)
        tb_dict['modules']['module_0']['fe_0']['chip_type'] = '"ITkPixV1"'
        tb_dict['modules']['module_0']['fe_0']['chip_id'] = 15
        tb_dict['general']['abort_on_rx_error'] = False  # simulation too slow for RX sync check
        tb_dict['general']['use_database'] = False  # data base feature not working for this chip

        with open(join(abspath(join(dirname(__file__), '..', '..', '..')), 'testbench.yaml'), 'w') as tbf:
            yaml.dump(tb_dict, tbf)

        logging.info('Starting digital scan test')
        self.sim_dc = 21
        self.scan = AnalogScan(bdaq_conf=utils.setup_cocotb(self.sim_dc, chip='ITkPixV1', rx_lanes=1), scan_config=configuration)
        self.scan.scan()
        self.scan.analyze()
        self.scan.close()

        ''' Assert raw data '''
        output_filename = os.path.join(self.scan._output_directories_per_scan[0], self.scan.run_name)

        with tb.open_file(str(output_filename + '_interpreted.h5'), 'r+') as in_file_h5:
            hist_occ = in_file_h5.root.HistOcc[:]

        self.assertEqual(np.sum(hist_occ[self.sim_dc * 8:(self.sim_dc + 1) * 8, :]), configuration['n_injections'] * 768, 'Incorrect number of hits has been recorded!')

    def tearDown(self):
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
