#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
from os.path import dirname, join, abspath
import yaml
import unittest
# import copy

import tables as tb
import numpy as np

from bdaq53.scans import test_registers

logger = logging.getLogger(__file__)


class TestScanScripts(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestScanScripts, cls).setUpClass()

        ''' Enable periphery and powercycle SCC '''
        with open(join(abspath(join(dirname(__file__), '..', '..')), 'testbench.yaml'), 'r') as tbf:
            tb = yaml.full_load(tbf)
        tb['periphery']['enable_periphery'] = True
        tb['modules']['module_0']['powersupply'] = {'lv_name': 'LV-0', 'lv_voltage': 1.7, 'lv_current_limit': 2.0}
        tb['modules']['module_0']['power_cycle'] = True
        tb['modules']['module_0']['chip_0']['chip_config_file'] = join(abspath(join(dirname(__file__), '..', '..', 'chips')), 'rd53a_default.cfg.yaml')
        tb['modules']['module_0']['chip_0']['chip_sn'] = '0x0001'
        tb['modules']['module_0']['chip_0']['chip_id'] = 0
        tb['modules']['module_0']['chip_0']['chip_type'] = "rd53a"
        tb['modules']['module_0']['chip_0']['receiver'] = "rx4"

        with open(join(abspath(join(dirname(__file__), '..', '..')), 'testbench.yaml'), 'w') as tbf:
            yaml.dump(tb, tbf)

        with open(join(abspath(join(dirname(__file__), '..', '..')), 'periphery.yaml'), 'r') as tbf:
            tb = yaml.full_load(tbf)
        tb['transfer_layer'][0]['init']['port'] = '/dev/power_supply'
        tb['registers'][-1]['name'] = 'LV-0'
        tb['registers'][-1]['arg_add']['channel'] = 1
        with open(join(abspath(join(dirname(__file__), '..', '..')), 'periphery.yaml'), 'w') as tbf:
            yaml.dump(tb, tbf)

        ''' Run this test first and do not assert anything, because for the first test after flashing a new firmware, there will never be aurora sync '''
        try:
            with test_registers.RegisterTest(scan_config=test_registers.scan_configuration) as test:
                test.configure()
                test.scan()
        except RuntimeError:
            pass

    def test_digital_scan(self):
        ''' Test digital scan '''
        from bdaq53.scans import scan_digital
        with scan_digital.DigitalScan(scan_config=scan_digital.scan_configuration) as scan:
            scan.start()
            filename = scan.output_filename + '_interpreted.h5'

        with tb.open_file(filename) as in_file:
            enable_mask = in_file.root.configuration_out.chip.use_pixel[:]
            # logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2)[enable_mask] != 100)))
            # logger.info('Integrated occupancy: %s' % np.sum(in_file.root.HistOcc[:].sum(axis=2)[enable_mask]))
            self.assertTrue(np.all(in_file.root.HistOcc[:].sum(axis=2)[enable_mask] == 100))
            # FIXME: Skip these two for now until shift in BCID and ToT is unterstood
            # self.assertTrue(np.all(in_file.root.HistRelBCID[:] == 15))
            # self.assertTrue(np.all(in_file.root.HistTot[:] == 2))
            self.assertTrue(np.all(in_file.root.HistBCIDError[:] == 0))
            self.assertTrue(np.all(in_file.root.HistEventStatus[:] == 0))

    def test_analog_scan(self):
        ''' Test analog scan '''
        from bdaq53.scans import scan_analog
        with scan_analog.AnalogScan(scan_config=scan_analog.scan_configuration) as scan:
            scan.start()
            filename = scan.output_filename + '_interpreted.h5'

        with tb.open_file(filename) as in_file:
            # logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2) != 100)))
            self.assertTrue(np.count_nonzero(in_file.root.HistOcc[:].sum(axis=2) == 100) > 0.99 * 400 * 192)
            self.assertTrue(np.any(in_file.root.HistRelBCID[:]))
            self.assertTrue(np.any(in_file.root.HistTot[:]))
            # We expect BCID errors from SYNC
            # self.assertTrue(np.all(in_file.root.HistBCIDError[:] == 0))
            # self.assertTrue(np.all(in_file.root.HistEventStatus[:] == 0))

    def test_threshold_scan(self):
        ''' Test threshold scan and results '''
        from bdaq53.scans import scan_threshold
        with scan_threshold.ThresholdScan(scan_config=scan_threshold.scan_configuration) as scan:
            scan.start()

#         with tb.open_file(scan.output_filename + '_interpreted.h5') as in_file:
#             logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2) != 100)))
#             self.assertTrue(np.count_nonzero(in_file.root.HistOcc[:].sum(axis=2) == 100) > 0.99 * 400 * 192)
#             self.assertTrue(np.any(in_file.root.HistRelBCID[:]))
#             self.assertTrue(np.any(in_file.root.HistTot[:]))
#             # We expect BCID errors from SYNC
#             # self.assertTrue(np.all(in_file.root.HistBCIDError[:] == 0))
#             # self.assertTrue(np.all(in_file.root.HistEventStatus[:] == 0))

    def test_fast_threshold_scan(self):
        ''' Test fast threshold scan and results '''
        from bdaq53.scans import scan_threshold_autorange
        with scan_threshold_autorange.AutorangeThresholdScan(scan_config=scan_threshold_autorange.scan_configuration) as scan:
            scan.start()

    def test_register_test(self):
        from bdaq53.scans import test_registers
        with test_registers.RegisterTest(scan_config=test_registers.scan_configuration) as test:
            test.start()

    def test_source_scan(self):
        ''' Test source scan and results. Do not use actual SourceScan instead
            use source scan with analog injections.
        '''
        from bdaq53.scans import scan_source_injection
        with scan_source_injection.SourceScanInj(scan_config=scan_source_injection.scan_configuration) as scan:
            scan.start()
            filename = scan.output_filename + '_interpreted.h5'

        with tb.open_file(filename) as in_file:
            n_inj = scan_source_injection.scan_configuration['n_injections']
            start_column = scan_source_injection.scan_configuration['start_column']
            stop_column = scan_source_injection.scan_configuration['stop_column']
            start_row = scan_source_injection.scan_configuration['start_row']
            stop_row = scan_source_injection.scan_configuration['stop_row']
            n_cols = stop_column - start_column
            n_rows = stop_row - start_row
            # Only the working part of the matrix is tested (LIN, DIFF)
            self.assertTrue(np.count_nonzero(in_file.root.HistOcc[start_column:stop_column, :, :].sum(axis=2) == n_inj) > 0.95 * n_cols * n_rows)
            self.assertTrue(np.any(in_file.root.HistRelBCID[:]))
            self.assertTrue(np.any(in_file.root.HistTot[:]))
            # We expect ext. triggers
            self.assertTrue(np.any(in_file.root.HistEventStatus[:][1]))

#
#     def test_threshold_noise_tuning(self):
#         from bdaq53.scans import tune_local_threshold_noise
#         with tune_local_threshold_noise.NoiseTuning(scan_config=tune_local_threshold_noise.scan_configuration) as tuning:
#           tuning.scan()


if __name__ == '__main__':
    unittest.main()
