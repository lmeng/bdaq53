#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import os
import unittest
import pytest

import numpy as np
import matplotlib

import bdaq53
from bdaq53.tests import utils  # noqa: E731
from bdaq53.analysis.analyze_disconnected_bumbs_from_source_scan import _min_occ_thr, analyze_chip, plot_module

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))

matplotlib.use('Agg')  # Allow headless plotting


class TestDisconnectedBumpAnalysis(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestDisconnectedBumpAnalysis, cls).setUpClass()
        plt_logger = logging.getLogger('Plotting')
        cls._plt_log_handler = utils.MockLoggingHandler(level='DEBUG')
        plt_logger.addHandler(cls._plt_log_handler)
        cls.plt_log_messages = cls._plt_log_handler.messages

        ana_logger = logging.getLogger('SrcScanBumpConAna')
        cls._ana_log_handler = utils.MockLoggingHandler(level='DEBUG')
        ana_logger.addHandler(cls._ana_log_handler)
        cls.ana_log_messages = cls._ana_log_handler.messages

    @classmethod
    def tearDownClass(cls):
        scm_dir = os.path.join(data_folder, 'modules', 'SCM0', '0x0000')
        utils.try_remove(os.path.join(scm_dir, '20210101_000001_source_scan_bump_connectivity.h5'))
        utils.try_remove(os.path.join(scm_dir, '20210101_000001_source_scan_bump_connectivity.pdf'))
        utils.try_remove(os.path.join(scm_dir, 'last_scan.pdf'))

        dcm_dir = os.path.join(data_folder, 'modules', 'DCM0')
        utils.try_remove(os.path.join(dcm_dir, '20210101_000001_source_scan_bump_connectivity.pdf'))
        utils.try_remove(os.path.join(dcm_dir, 'last_scan.pdf'))
        utils.try_remove(os.path.join(dcm_dir, '20210101_000001_source_scan_bump_connectivity.h5'))
        utils.try_remove(os.path.join(dcm_dir, '20210101_000001_source_scan_bump_connectivity.pdf'))
        utils.try_remove(os.path.join(dcm_dir, '0x0000', '20210101_000001_source_scan_bump_connectivity.h5'))
        utils.try_remove(os.path.join(dcm_dir, '0x0000', '20210101_000001_source_scan_bump_connectivity.pdf'))
        utils.try_remove(os.path.join(dcm_dir, '0x0000', 'last_scan.pdf'))
        utils.try_remove(os.path.join(dcm_dir, '0x0001', '20210101_000001_source_scan_bump_connectivity.h5'))
        utils.try_remove(os.path.join(dcm_dir, '0x0001', '20210101_000001_source_scan_bump_connectivity.pdf'))
        utils.try_remove(os.path.join(dcm_dir, '0x0001', 'last_scan.pdf'))

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls._plt_log_handler.reset()
        cls._ana_log_handler.reset()

    def test_source_scan_bump_ana(self):
        ''' Test analysis of source scan data for disconencted bumps '''

        prefix = os.path.join(data_folder, 'modules')

        module_dtype = np.dtype([("module", "U10"), ('chips', 'O'), ('date_source', 'U16'), ('date_analog', 'U16'), ('params', 'O')])
        modules = np.array([
            ("SCM0", ["0x0000"], "20210101_000001", '20210101_000000', {"customize_connectivity_map": _min_occ_thr, "min_occ_thr": 2, "module_type_name": "single"}),
            ("DCM0", ["0x0000", "0x0001"], "20210101_000001", "20210101_000000", {"allowed_diviation_of_itensity_up": 1.5, "module_type_name": "dual"}),
        ], dtype=module_dtype)

        for module in modules:
            module_folder = os.path.join(prefix, module["module"])
            interpreted_data_filenames = [None] * len(module["chips"])
            for chip_i, chip in enumerate(module["chips"]):
                source_file = os.path.join(module_folder, chip, module["date_source"] + "_source_scan")
                interpreted_data_filenames[chip_i] = source_file + '_bump_connectivity.h5'
                analog_file = None if module["date_analog"] == "None" else os.path.join(module_folder, chip, module["date_analog"] + "_analog_scan")
                analyze_chip(source_file, analog_file, create_plots=True, **module["params"])
            if len(module["chips"]) > 1:
                plot_module(interpreted_data_filenames, **module["params"])

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'modules', 'SCM0', '0x0000', '20210101_000001_source_scan_bump_connectivity.h5'),
            os.path.join(data_folder, 'modules', 'SCM0', '0x0000', '20210101_000001_source_scan_bump_connectivity_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'modules', 'DCM0', '0x0000', '20210101_000001_source_scan_bump_connectivity.h5'),
            os.path.join(data_folder, 'modules', 'DCM0', '0x0000', '20210101_000001_source_scan_bump_connectivity_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'modules', 'DCM0', '0x0001', '20210101_000001_source_scan_bump_connectivity.h5'),
            os.path.join(data_folder, 'modules', 'DCM0', '0x0001', '20210101_000001_source_scan_bump_connectivity_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'modules', 'DCM0', '20210101_000001_source_scan_bump_connectivity.h5'),
            os.path.join(data_folder, 'modules', 'DCM0', '20210101_000001_source_scan_bump_connectivity_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)


if __name__ == '__main__':
    pytest.main([__file__])
