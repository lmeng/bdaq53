#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import os
import threading
import time

from unittest.mock import patch

import pytest
import tables as tb
import numpy as np
import matplotlib
matplotlib.use('Agg')  # noqa: E402 Allow headless plotting

import bdaq53  # noqa: E731 E402
from bdaq53.analysis import online as oa  # noqa: E402
from bdaq53.tests import utils  # noqa: E40


@pytest.fixture()
def data_folder():
    bdaq53_path = os.path.dirname(bdaq53.__file__)
    return os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


@pytest.fixture()
def ana_log_messages():
    ana_logger = logging.getLogger('OnlineAnalysis')
    _ana_log_handler = utils.MockLoggingHandler(level='DEBUG')
    ana_logger.addHandler(_ana_log_handler)
    ana_log_messages = _ana_log_handler.messages
    yield ana_log_messages
    ana_logger.removeHandler(_ana_log_handler)  # cleanup


@pytest.fixture()
def occ_hist_oa():
    h = oa.OccupancyHistogramming(chip_type='rd53a')
    yield h
    h.close()
    del h


@pytest.fixture()
def tot_hist_oa():
    h = oa.TotHistogramming(chip_type='rd53a')
    yield h
    h.close()
    del h


@pytest.fixture()
def occ_hist_oa_b():
    h = oa.OccupancyHistogramming(chip_type='itkpixv1')
    yield h
    h.close()
    del h


@pytest.fixture()
def occ_hist_oa_b_no_jit():
    with patch('bdaq53.analysis.online.histogram_rd53b', side_effect=oa.histogram_rd53b.py_func):
        h = oa.OccupancyHistogramming(chip_type='itkpixv1')
        yield h
        h.close()
        del h


def get_raw_data(raw_data_file):
    ''' Yield data of one readout

        Delay return if replay is too fast
    '''
    with tb.open_file(raw_data_file, mode="r") as in_file_h5:
        meta_data = in_file_h5.root.meta_data[:]
        raw_data = in_file_h5.root.raw_data
        n_readouts = meta_data.shape[0]

        for i in range(n_readouts):
            # Raw data indeces of readout
            i_start = meta_data['index_start'][i]
            i_stop = meta_data['index_stop'][i]

            yield raw_data[i_start:i_stop]


def get_raw_data_ptot(raw_data_file):
    ''' Yield data and ptot table of one readout

        Delay return if replay is too fast
    '''
    with tb.open_file(raw_data_file, mode="r") as in_file_h5:
        meta_data = in_file_h5.root.meta_data[:]
        raw_data = in_file_h5.root.raw_data
        n_readouts = meta_data.shape[0]
        ptot_table = in_file_h5.root.ptot_table[:]

        for i in range(n_readouts):
            # Raw data indeces of readout
            i_start = meta_data['index_start'][i]
            i_stop = meta_data['index_stop'][i]

            yield raw_data[i_start:i_stop], ptot_table


def test_occupancy_histogramming(data_folder, occ_hist_oa):
    ''' Test online occupancy histogramming '''

    raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
    raw_data_file_result = os.path.join(data_folder, 'analog_scan_interpreted_result.h5')
    for words in get_raw_data(raw_data_file):
        occ_hist_oa.add(words)

    occ_hist = occ_hist_oa.get()

    with tb.open_file(raw_data_file_result) as in_file:
        occ_hist_exptected = in_file.root.HistOcc[:, :, 0]
        assert(np.array_equal(occ_hist_exptected, occ_hist))


def test_occupancy_histogramming_rd53b(data_folder, occ_hist_oa_b):
    ''' Test online occupancy histogramming for RD53B '''

    raw_data_file = os.path.join(data_folder, 'analog_scan_ptot.h5')
    raw_data_file_result = os.path.join(data_folder, 'analog_scan_ptot_interpreted_result.h5')
    for words, ptot_table in get_raw_data_ptot(raw_data_file):
        occ_hist_oa_b.add(words, ptot_table)

    occ_hist = occ_hist_oa_b.get()
    with tb.open_file(raw_data_file_result) as in_file:
        occ_hist_exptected = in_file.root.HistOcc[:, :, 0]
        assert(np.array_equal(occ_hist_exptected, occ_hist))


def test_occupancy_histogramming_rd53b_no_jit(data_folder, occ_hist_oa_b_no_jit):
    ''' Test online occupancy histogramming for RD53B '''

    raw_data_file = os.path.join(data_folder, 'analog_ptot_few_hits.h5')
    raw_data_file_result = os.path.join(data_folder, 'analog_ptot_few_hits_interpreted_result.h5')
    for words, ptot_table in get_raw_data_ptot(raw_data_file):
        occ_hist_oa_b_no_jit.add(words, ptot_table)

    occ_hist = occ_hist_oa_b_no_jit.get(timeout=10)

    with tb.open_file(raw_data_file_result) as in_file:
        occ_hist_exptected = in_file.root.HistOcc[:, :, 0]
        assert(np.array_equal(occ_hist_exptected, occ_hist))


def test_occupancy_histogramming_errors(data_folder, occ_hist_oa, ana_log_messages):
    # Check error message when requesting histogram before analysis finished

    def add_data():
        for words in get_raw_data(raw_data_file):
            for _ in range(100):
                occ_hist_oa.add(words)

    raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
    occ_hist_oa.reset()
    thread = threading.Thread(target=add_data)
    thread.start()
    time.sleep(0.5)  # wait for first data to be added to queue
    occ_hist_oa.get(wait=False)
    thread.join()
    # Check that warning was given
    assert('Getting histogram while analyzing data' in ana_log_messages['warning'])


def test_tot_histogramming(data_folder, tot_hist_oa):
    ''' Test online tot histogramming '''

    raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
    raw_data_file_result = os.path.join(data_folder, 'analog_scan_interpreted_result.h5')

    for words in get_raw_data(raw_data_file):
        tot_hist_oa.add(words)
    tot_hist = tot_hist_oa.get()
    with tb.open_file(raw_data_file_result) as in_file:
        tot_hist_exptected = in_file.root.HistTot[:, :, 0, :]
        assert(np.array_equal(tot_hist_exptected, tot_hist))


if __name__ == '__main__':
    import pytest
    pytest.main(['-s', __file__])
