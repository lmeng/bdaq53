#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest

import bdaq53.analysis.rd53b_analysis as anb
import bdaq53.analysis.analysis_utils as au


class TestAnalysis(unittest.TestCase):
    def test_hitmap_decompression(self):
        ''' Test decompression of RD53B hitmap '''
        result_expected = [77848, 65538]  # 16 bit decompressed hitmaps
        result = []
        hmap = [62418, 34]  # 4 - 30 bit compressed hitmaps
        hmap_is_compressed = True

        for w in hmap:
            le = au.get_length(w)
            r = anb.get_hitmap(w, le, hmap_is_compressed)[0]
            result.append(r)

        # Check result
        self.assertListEqual(result, result_expected)


if __name__ == '__main__':
    unittest.main()
