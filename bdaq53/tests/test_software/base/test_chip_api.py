#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import unittest
import inspect
import bdaq53  # noqa: E731

from bdaq53.chips.rd53a import RD53A
from bdaq53.chips.ITkPixV1 import ITkPixV1
from bdaq53.analysis import rd53a_analysis as ana
from bdaq53.analysis import rd53b_analysis as anb

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))

ignore_list = ['inject_analog_double', 'setup_aurora']


class TestChipApi(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestChipApi, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        pass

    @classmethod
    def tearDown(cls):
        pass

    def get_chip_api(self, input_class):
        definitions = {}
        functions_inclass = inspect.getmembers(input_class, inspect.isfunction)
        for functions in functions_inclass:
            in_params = str(inspect.signature(functions[1])).count(', ') + 1
            function_code = inspect.getsource(functions[1])
            if function_code.find('return ') > 0:
                out_values = function_code[function_code.find('return '):].count(', ') + 1
            else:
                out_values = 0
            if functions[0][0] != '_':
                definitions[functions[0]] = [in_params, out_values]
        return definitions

    @unittest.skip('Not intended')
    def test_compare_chip_apis(self):
        # Get analysis API
        rd53a_api = self.get_chip_api(RD53A)
        itkpixv1_api = self.get_chip_api(ITkPixV1)

        # Smarter implementation of code below
        # Check for same function names, do not care about function arguments
        # rd53a_api_func_names = list(set([func[0] for func in rd53a_api.items()]) - set(ignore_list))
        # itkpixv1_api_func_names = list(set([func[0] for func in itkpixv1_api.items()]) - set(ignore_list))
        # difference_names = list(set(rd53a_api_func_names).symmetric_difference(set(itkpixv1_api_func_names)))
        # self.assertListEqual(difference_names, [])

        function_differences = []
        success = False
        for functions in rd53a_api.items():
            try:
                if functions[1] != itkpixv1_api[functions[0]]:
                    function_differences.append([functions[0], functions[1], itkpixv1_api[functions[0]]])
            except KeyError:
                function_differences.append(functions[0])
                continue

        for functions in itkpixv1_api.items():
            try:
                if functions[1] != rd53a_api[functions[0]]:
                    function_differences.append([functions[0], functions[1], rd53a_api[functions[0]]])
            except KeyError:
                function_differences.append(functions[0])
                continue
        for ignore_function in ignore_list:
            try:
                function_differences.remove(ignore_function)
            except ValueError:
                pass

        if len(function_differences) == 0:
            success = True

        self.assertTrue(success, msg="There are differences in the following chip functions" + str(function_differences))

    @unittest.skip('Makes no sense')
    def test_compare_chip_analysis(self):
        rd53a_api = self.get_chip_api(ana)
        itkpixv1_api = self.get_chip_api(anb)
        function_differences = []
        success = False
        for functions in rd53a_api.items():
            try:
                if functions[1] != itkpixv1_api[functions[0]]:
                    function_differences.append([functions[0], functions[1], itkpixv1_api[functions[0]]])
            except KeyError:
                function_differences.append(functions[0])
                continue

        for functions in itkpixv1_api.items():
            try:
                if functions[1] != rd53a_api[functions[0]]:
                    function_differences.append([functions[0], functions[1], rd53a_api[functions[0]]])
            except KeyError:
                function_differences.append(functions[0])
                continue
        for ignore_function in ignore_list:
            try:
                function_differences.remove(ignore_function)
            except ValueError:
                pass

        if len(function_differences) == 0:
            success = True

        self.assertTrue(success, msg="There are differences in the following analysis functions" + str(function_differences))


if __name__ == '__main__':
    unittest.main()
