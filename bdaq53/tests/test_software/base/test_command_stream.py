#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import copy
import os
import shutil
import time
import unittest
import yaml

from unittest import mock

import tables as tb

import bdaq53
from bdaq53.tests import bdaq_mock
from bdaq53.tests import utils

from bdaq53.scans.scan_digital import DigitalScan

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192
}


class TestCommandStream(unittest.TestCase):
    ''' Check that the commands send to chip during a scan are constant.

        This allows to spot many unintended changes. However, intended changes
        as changing the std. config and mask loop will lead to failures here and require
        new fixtures
    '''

    @classmethod
    def setUpClass(cls):
        super(TestCommandStream, cls).setUpClass()

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)
        cls.bench_config['general']['use_database'] = False  # deactivate failing feature

        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock()
        cls.bhm.start()

    @classmethod
    def tearDownClass(cls):
        cls.bhm.stop()
        utils.try_remove('scan_digital_rd53a_commands.h5')
        utils.try_remove('scan_digital_rd53b_commands.h5')

    @classmethod
    def tearDown(cls):
        shutil.rmtree('output_data', ignore_errors=True)  # always delete output from previous test
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024
        cls.bhm.reset()

    def test_digital_commands_rd53a(self):
        ''' Test that the command stream for a rd53a digital scan is bit constant '''

        h5_file = tb.open_file('scan_digital_rd53a_commands.h5', 'w')
        commands = h5_file.create_earray(h5_file.root, name='commands', atom=tb.UIntAtom(), shape=(0,),
                                         title='commands', filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        def store_cmd(cmd, repetitions=1, wait_for_ready=True, wait_for_done=False):
            commands.append(cmd)

        with mock.patch('bdaq53.chips.rd53a.RD53A.write_command', side_effect=store_cmd):
            with DigitalScan(scan_config=scan_configuration, bench_config=self.bench_config) as scan:
                scan.start()
            commands.flush()
            h5_file.close()

            data_equal, error_msg = utils.compare_h5_files('scan_digital_rd53a_commands.h5', os.path.join(data_folder, 'scan_digital_rd53a_commands.h5'))
            self.assertTrue(data_equal, msg=error_msg)

    def test_digital_commands_rd53b_ptot(self):
        ''' Test that the command stream for a rd53b digital scan with ptot is bit constant '''

        bench_config = copy.deepcopy(self.bench_config)
        bench_config['modules']['module_0']['chip_0']['chip_type'] = 'itkpixv1'
        bench_config['modules']['module_0']['chip_0']['use_ptot'] = True
        bench_config['modules']['module_0']['chip_0']['chip_config_file'] = os.path.join(bdaq53_path, 'chips', 'ITkPixV1_default.cfg.yaml')

        h5_file = tb.open_file('scan_digital_rd53b_commands.h5', 'w')
        commands = h5_file.create_earray(h5_file.root, name='commands', atom=tb.UIntAtom(), shape=(0,),
                                         title='commands', filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        def store_cmd(cmd, repetitions=1, wait_for_ready=True, wait_for_done=False):
            commands.append(cmd)

        with mock.patch('bdaq53.chips.ITkPixV1.ITkPixV1.write_command', side_effect=store_cmd):
            with DigitalScan(scan_config=scan_configuration, bench_config=bench_config) as scan:
                scan.start()
            commands.flush()
            h5_file.close()

            data_equal, error_msg = utils.compare_h5_files('scan_digital_rd53b_commands.h5', os.path.join(data_folder, 'scan_digital_rd53b_commands.h5'))
            self.assertTrue(data_equal, msg=error_msg)


if __name__ == '__main__':
    unittest.main()
