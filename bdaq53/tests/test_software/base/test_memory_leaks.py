import copy
import gc
import os
import shutil
import time

import pytest
import yaml
from pympler import muppy, summary

import bdaq53
from bdaq53.scans import scan_digital
from bdaq53.system.scan_base import ScanBase
from bdaq53.tests import bdaq_mock


bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))
rd53_cfg_file = os.path.join(bdaq53_path, 'chips', 'rd53a_default.cfg.yaml')
itkpixv1_cfg_file = os.path.join(bdaq53_path, 'chips', 'ITkPixV1_default.cfg.yaml')


class TestScan(ScanBase):
    scan_id = 'test_scan_paralel'

    is_parallel_scan = True

    def _configure(self, **_):
        pass

    def _scan(self, **_):
        with self.readout(timeout=0.5):
            pass

    def _analyze(self):
        pass


class TestMemoryLeaks():
    ''' Test for memory leaks in bdaq scans
    '''

    @classmethod
    def setup_class(cls):
        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)
        cls.bench_config['general']['use_database'] = False  # deactivate failing feature
        cls.bench_config['modules']['module_0']['chip_0']['chip_config_file'] = rd53_cfg_file  # always use std. config

        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock(n_chips=2)
        cls.bhm.start()

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)

        # Use the second chip of a dual module to make setup more complex
        cls.bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(cls.bench_config['modules']['module_0']['chip_0'])
        cls.bench_config['modules']['module_0']['chip_1']['chip_sn'] = '0x0002'
        cls.bench_config['modules']['module_0']['chip_1']['chip_id'] = 1
        cls.bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        cls.bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

    @classmethod
    def teardown_class(cls):
        cls.bhm.stop()

    @classmethod
    def teardown_method(self, method):
        # Reset messages after each test
        shutil.rmtree('output_data', ignore_errors=True)  # always delete output from previous test
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

    @pytest.mark.skip(reason="Not clear if test procedure works as expected")
    def test_scan_cleanup_rd53a(self):
        ''' Check for memory leaks in bdaq digital scan

            Repeat a digital scan and check if number of objects in RAM stays constant after < 10 iterations.
            The number of objects is not constant from the beginning.
            Likely due to mocking and memory/time optimizations during runtime of (external) python packages (numba, matplotlib).
        '''
        n_objects = None
        self.bhm.reset()

        for _ in range(10):
            with scan_digital.DigitalScan(bench_config=self.bench_config, scan_config=scan_digital.scan_configuration) as scan:
                scan.start()
            del scan
            self.bhm.reset()
            gc.collect()
            n_objects_now = sum([i[1] for i in summary.summarize(muppy.get_objects())])
            if n_objects and n_objects_now - n_objects == 0:
                break  # number of objects in RAM constant
            else:  # number of objects increased --> re-run scan
                n_objects = n_objects_now
        else:
            raise RuntimeError('Number of objects in RAM increase, check for memory leaks needed.')

    @pytest.mark.skip(reason="Not clear if test procedure works as expected")
    def test_scan_cleanup_rd53b(self):
        ''' Check for memory leaks in bdaq digital scan

            Repeat a digital scan and check if number of objects in RAM stays constant after < 10 iterations.
            The number of objects is not constant at the beginning.
            Likely due to mocking and memory/time optimizations during runtime of (external) python packages (numba, matplotlib).
        '''
        n_objects = None
        self.bhm.reset()

        bench_config = copy.deepcopy(self.bench_config)
        bench_config['modules']['module_0']['chip_0']['chip_type'] = 'itkpixv1'
        bench_config['modules']['module_0']['chip_0']['chip_config_file'] = itkpixv1_cfg_file  # always use std. config
        bench_config['modules']['module_0']['chip_0']['use_ptot'] = True
        bench_config['modules']['module_0']['chip_1']['chip_type'] = 'itkpixv1'
        bench_config['modules']['module_0']['chip_1']['chip_config_file'] = itkpixv1_cfg_file  # always use std. config
        bench_config['modules']['module_0']['chip_1']['use_ptot'] = True

        for _ in range(10):
            with scan_digital.DigitalScan(bench_config=bench_config, scan_config=scan_digital.scan_configuration) as scan:
                scan.start()
            del scan
            self.bhm.reset()
            gc.collect()
            n_objects_now = sum([i[1] for i in summary.summarize(muppy.get_objects())])
            if n_objects and n_objects_now - n_objects == 0:
                break  # number of objects in RAM constant
            else:  # number of objects increased --> re-run scan
                n_objects = n_objects_now
        else:
            raise RuntimeError('Number of objects in RAM increase, check for memory leaks needed.')

    @pytest.mark.skip(reason="Not clear if test procedure works as expected")
    def test_parallel_scan_cleanup(self):
        ''' Check for memory leaks in bdaq digital scan

            Repeat a digital scan and check if number of objects in RAM stays constant after < 10 iterations.
            The number of objects is not constant at the beginning.
            Likely due to mocking and memory/time optimizations during runtime of (external) python packages (numba, matplotlib).
        '''
        n_objects = None
        self.bhm.reset()

        for _ in range(10):
            with TestScan(bench_config=self.bench_config, scan_config=scan_digital.scan_configuration) as scan:
                scan.start()
            del scan
            self.bhm.reset()
            gc.collect()
            n_objects_now = sum([i[1] for i in summary.summarize(muppy.get_objects())])
            if n_objects and n_objects_now - n_objects == 0:
                break  # number of objects in RAM constant
            else:  # number of objects increased --> re-run scan
                n_objects = n_objects_now
        else:
            raise RuntimeError('Number of objects in RAM increase, check for memory leaks needed.')


if __name__ == '__main__':
    pytest.main(['-s', __file__])
