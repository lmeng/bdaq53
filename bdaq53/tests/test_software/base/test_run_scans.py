''' Script to test how scans are run: vias the command line interface, via __main__ entry point or via meta scans

    Independent of how they are run on how many chips the command stream and the configuration shoud be the same.
    This is checked here.
'''

import copy
import importlib
import os
import shutil
import sys
import tempfile
import time
import types
import unittest

from subprocess import call

import yaml
import tables as tb

import bdaq53
from bdaq53.tests.bdaq_mock import BdaqMock
from bdaq53 import utils
from bdaq53.tests import utils as tu, bdaq_mock
from bdaq53.scans import scan_digital

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


def _run_scan_script(script, send_commands_file='commands_script.h5'):
    ''' Run a script from __main__ with bdaq mock

        Running __main__ entry points for testing is complicated.

        This solution is inspired by:
        https://stackoverflow.com/questions/19009932/import-arbitrary-python-source-file-python-3-3
        https://stackoverflow.com/questions/5850268/how-to-test-or-mock-if-name-main-contents
    '''
    loader = importlib.machinery.SourceFileLoader('__main__', script)
    mod = types.ModuleType(loader.name)

    with BdaqMock(n_chips=2, send_commands_file=send_commands_file):
        loader.exec_module(mod)


class TestCommandLineInterface(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestCommandLineInterface, cls).setUpClass()

        shutil.copyfile(bench_config, bench_config + '~')  # store original

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)
            cls.bench_config['general']['use_database'] = False  # deactivate failing feature

        # Use the second chip of a dual module to make setup more complex
        bench_config_2_chip = copy.deepcopy(cls.bench_config)
        bench_config_2_chip['modules']['module_0']['chip_0']['chip_type'] = 'itkpixv1'
        bench_config_2_chip['modules']['module_0']['chip_1'] = copy.deepcopy(bench_config_2_chip['modules']['module_0']['chip_0'])
        bench_config_2_chip['modules']['module_0']['chip_1']['chip_type'] = 'itkpixv1'
        bench_config_2_chip['modules']['module_0']['chip_1']['chip_sn'] = '0x0002'
        bench_config_2_chip['modules']['module_0']['chip_1']['chip_id'] = 1
        bench_config_2_chip['modules']['module_0']['chip_1']['receiver'] = "rx1"
        bench_config_2_chip['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

        with open(bench_config, 'w') as outfile:
            outfile.write(yaml.dump(bench_config_2_chip, default_flow_style=False))

    @classmethod
    def tearDownClass(cls):
        shutil.move(bench_config + '~', bench_config)  # restore original
        tu.try_remove('combined_module_cmds.h5')
        tu.try_remove('DC_second_cmds.h5')
        tu.try_remove('DC_first_cmds.h5')
        tu.try_remove('DC_module_cmds.h5')

    @classmethod
    def tearDown(cls):
        # return
        # always delete output from previous scan
        tu.try_remove('output_data')
        tu.try_remove('output_data_1')
        tu.try_remove('commands_script.h5')
        tu.try_remove('commands_meta.h5')
        tu.try_remove('commands_cli.h5')
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

    def test_module_chip_inits(self):
        ''' Check that the chip initialization is the same independent if using a single chip or a multi-chip module.

            Compares all commands send to chip for the init step for two chips of a module when run independently vs run as a module.
        '''

        # 1. Run scan on first chip of a DC module
        with bdaq_mock.BdaqMock(n_chips=2, send_commands_file='DC_first_cmds.h5'):
            with scan_digital.DigitalScan(bench_config=self.bench_config, scan_config=scan_digital.scan_configuration):
                pass

        # 2. Run scan on second chip of a DC module
        bench_config = copy.deepcopy(self.bench_config)
        bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(bench_config['modules']['module_0']['chip_0'])
        del bench_config['modules']['module_0']['chip_0']
        bench_config['modules']['module_0']['chip_1']['chip_sn'] = "0x0002"
        bench_config['modules']['module_0']['chip_1']['chip_id'] = 1
        bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

        with bdaq_mock.BdaqMock(n_chips=2, send_commands_file='DC_second_cmds.h5'):
            with scan_digital.DigitalScan(bench_config=bench_config, scan_config=scan_digital.scan_configuration):
                pass

        # 3. Run scan on the two chips of a DC module
        bench_config = copy.deepcopy(self.bench_config)
        bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(bench_config['modules']['module_0']['chip_0'])
        bench_config['modules']['module_0']['chip_1']['chip_sn'] = "0x0002"
        bench_config['modules']['module_0']['chip_1']['chip_id'] = 1
        bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

        shutil.move('output_data', 'output_data_1')
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

        with bdaq_mock.BdaqMock(n_chips=2, send_commands_file='DC_module_cmds.h5'):
            with scan_digital.DigitalScan(bench_config=bench_config, scan_config=scan_digital.scan_configuration):
                pass

        # 4. Combined SC data stream should correspond to module data stream
        with tb.open_file('combined_module_cmds.h5', 'w') as out_file:
            with tb.open_file('DC_first_cmds.h5') as in_file_1:
                with tb.open_file('DC_second_cmds.h5') as in_file_2:
                    out_file.copy_node(in_file_1.root.commands, out_file.root)
                    cmds = out_file.root.commands
                    cmds.append(in_file_2.root.commands[:])

        data_equal, error_msg = tu.compare_h5_files('combined_module_cmds.h5', 'DC_module_cmds.h5')
        self.assertTrue(data_equal, msg=error_msg)

        chip_0_file = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data_1', 'module_0', 'chip_0'))
        chip_1_file = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data_1', 'module_0', 'chip_1'))
        mod_0_chip_0_file = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_0'))
        mod_0_chip_1_file = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_1'))

        # Check that chip config is the same in the noise occupancy scans
        data_equal, error_msg = tu.compare_h5_files(chip_0_file, mod_0_chip_0_file,
                                                    ignore_nodes=['/configuration_in/scan/run_config',  # contains name of scan
                                                                  '/configuration_out/scan/run_config',  # contains name of scan
                                                                  '/configuration_in/chip/settings',  # contains name of previous scan
                                                                  '/configuration_out/chip/settings',  # contains name of previous scan
                                                                  '/configuration_in/chip/module',  # 1 vs 2 chip module
                                                                  '/configuration_out/chip/module'])    # 1 vs 2 chip module
        self.assertTrue(data_equal, msg=error_msg)

        # Check that chip config is the same in the noise occupancy scans
        data_equal, error_msg = tu.compare_h5_files(chip_1_file, mod_0_chip_1_file,
                                                    ignore_nodes=['/configuration_in/scan/run_config',  # contains name of scan
                                                                  '/configuration_out/scan/run_config',  # contains name of scan
                                                                  '/configuration_in/chip/settings',  # contains name of previous scan
                                                                  '/configuration_out/chip/settings',  # contains name of previous scan
                                                                  '/configuration_in/chip/module',  # 1 vs 2 chip module
                                                                  '/configuration_out/chip/module'])    # 1 vs 2 chip module
        self.assertTrue(data_equal, msg=error_msg)

    def test_main_call_vs_cli(self):
        ''' Check that calling one script after another using the __main__ entry point is the same as calling them using the CLI '''
        # 1. Run the scripts from __main__
        script = os.path.join(bdaq53_path, 'scans', 'scan_noise_occupancy.py')
        _run_scan_script(script)
        script = os.path.join(bdaq53_path, 'scans', 'scan_stuck_pixel.py')
        _run_scan_script(script)
        noise_occ_raw_data_file_meta = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_0'), scan_pattern='noise_occupancy_scan')
        stuck_raw_data_file_meta = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_0'))
        shutil.move('output_data', 'output_data_1')
        stuck_raw_data_file_meta = stuck_raw_data_file_meta.replace('output_data', 'output_data_1')
        noise_occ_raw_data_file_meta = noise_occ_raw_data_file_meta.replace('output_data', 'output_data_1')
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

        # 2. Run a Noise Occupancy Scan followed by a Stuck Pixel Scan using the CLI
        try:
            # Load bdaq mock in bdaq.__init__ to be available in CLI
            shutil.copyfile(bdaq53_path + '/__init__.py', bdaq53_path + '/__init__~')  # store original
            with open(bdaq53_path + '/__init__.py', "a") as f:
                f.write("from bdaq53.tests.bdaq_mock import BdaqMock\n")
                f.write("BdaqMock(n_chips=2, send_commands_file='commands_cli.h5').start()\n")
            call(["bdaq", "scan_noise_occupancy"])
            call(["bdaq", "scan_stuck_pixel"])
            noise_occ_raw_data_file_cli = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_0'), scan_pattern='noise_occupancy_scan')
            stuck_raw_data_file_cli = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_0'))
        finally:
            shutil.move(bdaq53_path + '/__init__~', bdaq53_path + '/__init__.py')  # re-store original

        # Check that chip config is the same in the noise occupancy scans
        data_equal, error_msg = tu.compare_h5_files(noise_occ_raw_data_file_meta, noise_occ_raw_data_file_cli,
                                                    ignore_nodes=['/raw_data', '/meta_data',  # fake chip data is created non deterministically
                                                                  '/configuration_in/scan/run_config',  # contains name of scan
                                                                  '/configuration_out/scan/run_config',  # contains name of scan
                                                                  '/configuration_in/chip/settings',  # contains name of previous scan
                                                                  '/configuration_out/chip/settings'])  # contains name of previous scan
        self.assertTrue(data_equal, msg=error_msg)

        # Check that chip config is the same in the stuck pixel scans
        data_equal, error_msg = tu.compare_h5_files(stuck_raw_data_file_meta, stuck_raw_data_file_cli,
                                                    ignore_nodes=['/raw_data', '/meta_data',  # fake chip data is created non deterministically
                                                                  '/configuration_in/scan/run_config',  # contains name of scan
                                                                  '/configuration_out/scan/run_config',  # contains name of scan
                                                                  '/configuration_in/chip/settings',  # contains name of previous scan
                                                                  '/configuration_out/chip/settings'])  # contains name of previous scan
        self.assertTrue(data_equal, msg=error_msg)

        # Check that command stream is the same
        data_equal, error_msg = tu.compare_h5_files('commands_script.h5', 'commands_cli.h5')
        self.assertTrue(data_equal, msg=error_msg)

    def test_cli_vs_meta_scan(self):
        ''' Check that calling two scan consecutively via the CLI is the same as running both scans in a meta_scan

            Configuration and command stream is checked for similarity. Needed to prove no underlying bug related to issue #416
        '''

        # 1. Run a meta script with a Noise Occupancy Scan followed by a Stuck Pixel Scan
        meta_script = '''from bdaq53.scans import scan_noise_occupancy, scan_stuck_pixel
if __name__ == '__main__':
    from bdaq53.tests.bdaq_mock import BdaqMock

    with BdaqMock(n_chips=2, send_commands_file='commands_meta.h5'):
        with scan_noise_occupancy.NoiseOccScan(scan_config=scan_noise_occupancy.scan_configuration) as noise_occ_scan:
            noise_occ_scan.start()
        with scan_stuck_pixel.StuckPixelScan(scan_config=scan_stuck_pixel.scan_configuration) as stuck_pix_scan:
            stuck_pix_scan.start()
'''
        try:
            with tempfile.NamedTemporaryFile(suffix='.py', delete=False) as tmp:
                tmp.write(meta_script.encode())
            call([sys.executable, os.path.join(os.getcwd(), tmp.name)])
            noise_occ_raw_data_file_meta = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_0'), scan_pattern='noise_occupancy_scan')
            stuck_raw_data_file_meta = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_0'))
        finally:
            os.remove(tmp.name)

        shutil.move('output_data', 'output_data_1')
        stuck_raw_data_file_meta = stuck_raw_data_file_meta.replace('output_data', 'output_data_1')
        noise_occ_raw_data_file_meta = noise_occ_raw_data_file_meta.replace('output_data', 'output_data_1')
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

        # 2. Run a Noise Occupancy Scan followed by a Stuck Pixel Scan using the CLI
        try:
            # Load bdaq mock in bdaq.__init__ to be available in CLI
            shutil.copyfile(bdaq53_path + '/__init__.py', bdaq53_path + '/__init__~')  # store original
            with open(bdaq53_path + '/__init__.py', "a") as f:
                f.write("from bdaq53.tests.bdaq_mock import BdaqMock\n")
                f.write("BdaqMock(n_chips=2, send_commands_file='commands_cli.h5').start()\n")
            call(["bdaq", "scan_noise_occupancy"])
            call(["bdaq", "scan_stuck_pixel"])
            noise_occ_raw_data_file_cli = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_0'), scan_pattern='noise_occupancy_scan')
            stuck_raw_data_file_cli = utils.get_latest_h5file(directory=os.path.join(os.getcwd(), 'output_data', 'module_0', 'chip_0'))
        finally:
            shutil.move(bdaq53_path + '/__init__~', bdaq53_path + '/__init__.py')  # re-store original

        # Check that chip config is the same in the noise occupancy scans
        data_equal, error_msg = tu.compare_h5_files(noise_occ_raw_data_file_meta, noise_occ_raw_data_file_cli,
                                                    ignore_nodes=['/raw_data', '/meta_data',  # fake chip data is created non deterministically
                                                                  '/configuration_in/scan/run_config',  # contains name of scan
                                                                  '/configuration_out/scan/run_config',  # contains name of scan
                                                                  '/configuration_in/chip/settings',  # contains name of previous scan
                                                                  '/configuration_out/chip/settings'])  # contains name of previous scan
        self.assertTrue(data_equal, msg=error_msg)

        # Check that chip config is the same in the stuck pixel scans
        data_equal, error_msg = tu.compare_h5_files(stuck_raw_data_file_meta, stuck_raw_data_file_cli,
                                                    ignore_nodes=['/raw_data', '/meta_data',  # fake chip data is created non deterministically
                                                                  '/configuration_in/scan/run_config',  # contains name of scan
                                                                  '/configuration_out/scan/run_config',  # contains name of scan
                                                                  '/configuration_in/chip/settings',  # contains name of previous scan
                                                                  '/configuration_out/chip/settings'])  # contains name of previous scan
        self.assertTrue(data_equal, msg=error_msg)

        # Check that command stream is the same
        data_equal, error_msg = tu.compare_h5_files('commands_meta.h5', 'commands_cli.h5')
        self.assertTrue(data_equal, msg=error_msg)


if __name__ == '__main__':
    unittest.main()
