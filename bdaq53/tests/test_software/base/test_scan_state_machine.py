''' Script to check that the scan base state mashine follows the intended behavior.
'''

import copy
import os
import shutil
import time
import unittest
import yaml

from unittest.mock import MagicMock

import numpy as np
import tables as tb

import bdaq53
from bdaq53.tests import bdaq_mock

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))

scan_configuration = {
    'start_column': 100,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,
    'VCAL_MED': 500,
    'VCAL_HIGH': 1300
}


def create_test_scan(parallel=False, mask_shifts=False):
    ''' Must be in method to allow delayed import of ScanBase '''
    from bdaq53.system.scan_base import ScanBase

    class TestScan(ScanBase):
        scan_id = 'test_scan'

        is_parallel_scan = parallel

        if is_parallel_scan:
            scan_id += '_parallel'

        def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
            self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
            self.chip.masks.apply_disable_mask()

        def _scan(self, **_):
            with self.readout(timeout=0.1):
                if mask_shifts:
                    for _ in self.chip.masks.shift(masks=['enable', 'injection'], cache=True):
                        pass

        def _analyze(self):
            pass

    return TestScan


class TestScanStateMachine(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ''' Do the minimum necessary to run bdaq53 without hardware '''
        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock(n_chips=4)
        cls.bhm.start()

        cls.TestScan = create_test_scan()
        cls.TestScanParallel = create_test_scan(parallel=True)
        cls.TestScanMasking = create_test_scan(mask_shifts=True)

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)
            cls.bench_config['general']['use_database'] = False  # deactivate failing feature

    @classmethod
    def tearDownClass(cls):
        cls.bhm.stop()

    @classmethod
    def tearDown(cls):
        # always delete output from previous scan
        shutil.rmtree('output_data', ignore_errors=True)
        cls.bhm.reset()  # clear mock call storage to save RAM
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

    def test_no_init(self):
        ''' Without calls to any scan function
            this should not raise an exception
        '''
        with self.TestScan() as _:
            pass

    def test_std_usage(self):
        ''' Test with std. scan API call
        '''
        with self.TestScan(bench_config=self.bench_config, scan_config=scan_configuration) as scan:
            scan.start()

    def test_explicit_usage(self):
        scan = self.TestScan(bench_config=self.bench_config, scan_config=scan_configuration)
        scan.init()
        scan.configure()
        scan.scan()
        scan.analyze()
        scan.close()

    def test_missing_init_1(self):
        ''' Check for exceptions if init is not called before configure '''
        with self.assertRaises(RuntimeError):
            scan = self.TestScan(bench_config=self.bench_config, scan_config=scan_configuration)
            scan.configure()

    def test_missing_init_2(self):
        ''' Check for exceptions if init is not called before scan'''
        with self.assertRaises(RuntimeError):
            scan = self.TestScan(bench_config=self.bench_config, scan_config=scan_configuration)
            scan.scan()

    def test_repeating_scan(self):
        ''' Check that the scan routinge can be called several times '''
        scan = self.TestScan(bench_config=self.bench_config, scan_config=scan_configuration)
        for _ in range(3):
            scan.init()
            scan.start()
            scan.close()

    def test_loading_from_previous_run(self):
        this_scan_config = scan_configuration.copy()
        this_scan_config['start_column'] = 10
        this_scan_config['stop_row'] = 100
        n_enabled = (this_scan_config['stop_column'] - this_scan_config['start_column']) * (this_scan_config['stop_row'] - this_scan_config['start_row'])

        # Create folder with data
        with self.TestScanMasking(bench_config=self.bench_config, scan_config=this_scan_config) as scan:
            self.assertEqual(np.count_nonzero(scan.chip.masks['enable']), 0)  # all pixels disabled after init with std. settings
            scan.configure()
            self.assertEqual(np.count_nonzero(scan.chip.masks['enable']), n_enabled)
            scan.scan()
            self.assertEqual(np.count_nonzero(scan.chip.masks['enable']), n_enabled)  # after mask shifting is should be the same
            scan.analyze()

        this_scan_config = scan_configuration.copy()
        this_scan_config['start_column'] = 100
        this_scan_config['stop_column'] = 200
        this_scan_config['start_row'] = 10
        this_scan_config['stop_row'] = 110
        n_enabled_new = (this_scan_config['stop_column'] - this_scan_config['start_column']) * (this_scan_config['stop_row'] - this_scan_config['start_row'])

        # Load from previous run
        with self.TestScanMasking(bench_config=self.bench_config, scan_config=this_scan_config) as scan:
            self.assertEqual(np.count_nonzero(scan.chip.masks['enable']), 0)  # all pixels disabled after init
            scan.configure()
            self.assertEqual(np.count_nonzero(scan.chip.masks['enable']), n_enabled_new)  # should keep setting, https://gitlab.cern.ch/silab/bdaq53/-/issues/400
            scan.scan()
            self.assertEqual(np.count_nonzero(scan.chip.masks['enable']), n_enabled_new)  # after mask shifting is should be the same
            scan.analyze()

    def test_parallel_scan(self):
        ''' Check that parallel scans work '''
        with self.TestScanParallel(bench_config=self.bench_config, scan_config=scan_configuration) as scan:
            scan.start()

    def test_missing_scan_steps(self):
        ''' Check that scan without configure and analysis step '''

        from bdaq53.system.scan_base import ScanBase

        class TestScan(ScanBase):
            scan_id = 'test_scan'

            def _scan(self, **_):
                with self.readout(timeout=0.1):
                    pass

        with TestScan(bench_config=self.bench_config, scan_config=scan_configuration) as scan:
            scan.start()

    def test_exception_handling(self):
        ''' Test that exceptions during scan steps are raised but always call close()

            https://gitlab.cern.ch/silab/bdaq53/-/issues/391

            Use double chip module.
        '''

        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(bench_config['modules']['module_0']['chip_0'])
        bench_config['modules']['module_0']['chip_1']['chip_sn'] = '0x0002'
        bench_config['modules']['module_0']['chip_1']['chip_id'] = 1
        bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

        class CustomError(Exception):
            pass

        def raise_exception(*args, **kwargs):
            raise CustomError

        # Test exception handling during configure step
        with self.assertRaises(CustomError):
            scan = self.TestScan(bench_config=bench_config, scan_config=scan_configuration)
            scan.close_orig = scan.close  # store original close
            scan.close = MagicMock(wraps=scan.close_orig)
            scan._configure = raise_exception  # create exception during configure
            scan.init()
            scan.configure()
        scan.close.assert_called_once()

        # Test exception handling during scan step
        with self.assertRaises(CustomError):
            scan = self.TestScan(bench_config=bench_config, scan_config=scan_configuration)
            scan.close_orig = scan.close  # store original close
            scan.close = MagicMock(wraps=scan.close_orig)
            scan._scan = raise_exception  # create exception during scan
            scan.init()
            scan.configure()
            scan.scan()
        scan.close.assert_called_once()

        # Test exception handling during analyze step
        with self.assertRaises(CustomError):
            scan = self.TestScan(bench_config=bench_config, scan_config=scan_configuration)
            scan.close_orig = scan.close  # store original close
            scan.close = MagicMock(wraps=scan.close_orig)
            scan._analyze = raise_exception  # create exception during analysis
            scan.init()
            output_filename = scan.output_filename
            scan.configure()
            scan.scan()
            scan.analyze()
        scan.close.assert_called_once()

        # Check that output file is not written on file close when error occured
        # https://gitlab.cern.ch/silab/bdaq53/-/issues/396
        with self.assertRaises(OSError):
            tb.open_file(output_filename + '_interpreted.h5')


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestScanStateMachine)
    unittest.TextTestRunner(verbosity=2).run(suite)
