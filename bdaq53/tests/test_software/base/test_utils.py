#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import unittest

import bdaq53
from bdaq53 import utils

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


class TestUtils(unittest.TestCase):

    def test_get_h5_file(self):
        ''' Checks the latest h5 file loading

            https://gitlab.cern.ch/silab/bdaq53/-/issues/399
        '''
        self.assertEqual(os.path.join(data_folder, 'timewalk_scan.h5'),
                         utils.get_latest_h5file(data_folder,
                                                 scan_pattern='scan',
                                                 interpreted=False,
                                                 file_timestamps=False))
        self.assertEqual(os.path.join(data_folder, 'tot_calibration.h5'),
                         utils.get_latest_h5file(data_folder,
                                                 scan_pattern='calibration',
                                                 interpreted=False,
                                                 file_timestamps=False))
        self.assertEqual(os.path.join(data_folder, 'tot_tuning.h5'),
                         utils.get_latest_h5file(data_folder,
                                                 scan_pattern='tuning',
                                                 interpreted=False,
                                                 file_timestamps=False))
        self.assertFalse(utils.get_latest_h5file(data_folder, interpreted=True))


if __name__ == '__main__':
    unittest.main()
