import os
from glob import glob
import pandas as pd
import xlrd
from collections import OrderedDict
import numpy as np
import xlsxwriter
from bdaq53.waferprobing.analysis.ITkPix.analyze_wafer_ITkPixV1 import TITLE_COLOR
from matplotlib.figure import Figure
import matplotlib.patches as mpatches
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.collections import PatchCollection
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.ticker import MaxNLocator
from matplotlib import colors, pyplot, colorbar
from matplotlib.table import Table
import matplotlib as mpl

v2 = True
if v2:
    nchips = 131
    valid_chips = [(1, 7), (1, 8), (1, 9),
                   (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9), (2, 10), (2, 11),
                   (3, 3), (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10), (3, 11), (3, 12),
                   (4, 2), (4, 3), (4, 4), (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10), (4, 11), (4, 12), (4, 13),
                   (5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7), (5, 8), (5, 9), (5, 10), (5, 11), (5, 12), (5, 13),
                   (6, 2), (6, 3), (6, 4), (6, 5), (6, 6), (6, 7), (6, 8), (6, 9), (6, 10), (6, 11), (6, 12), (6, 13), (6, 14),
                   (7, 1), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8), (7, 9), (7, 10), (7, 11), (7, 12), (7, 13), (7, 14),
                   (8, 2), (8, 3), (8, 4), (8, 5), (8, 6), (8, 7), (8, 8), (8, 9), (8, 10), (8, 11), (8, 12), (8, 13), (8, 14),
                   (9, 2), (9, 3), (9, 4), (9, 5), (9, 6), (9, 7), (9, 8), (9, 9), (9, 10), (9, 11), (9, 12), (9, 13),
                   (10, 2), (10, 3), (10, 4), (10, 5), (10, 6), (10, 7), (10, 8), (10, 9), (10, 10), (10, 11), (10, 12), (10, 13),
                   (11, 3), (11, 4), (11, 5), (11, 6), (11, 7), (11, 8), (11, 9), (11, 10), (11, 11), (11, 12),
                   (12, 4), (12, 5), (12, 6), (12, 7), (12, 8), (12, 9), (12, 10), (12, 11),
                   (13, 6), (13, 7), (13, 8), (13, 9)]
else:
    nchips = 132
    valid_chips = [(1, 6), (1, 7), (1, 8), (1, 9),
                   (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9), (2, 10), (2, 11),
                   (3, 3), (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9), (3, 10), (3, 11), (3, 12),
                   (4, 2), (4, 3), (4, 4), (4, 5), (4, 6), (4, 7), (4, 8), (4, 9), (4, 10), (4, 11), (4, 12), (4, 13),
                   (5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7), (5, 8), (5, 9), (5, 10), (5, 11), (5, 12), (5, 13),
                   (6, 2), (6, 3), (6, 4), (6, 5), (6, 6), (6, 7), (6, 8), (6, 9), (6, 10), (6, 11), (6, 12), (6, 13), (6, 14),
                   (7, 1), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8), (7, 9), (7, 10), (7, 11), (7, 12), (7, 13), (7, 14),
                   (8, 2), (8, 3), (8, 4), (8, 5), (8, 6), (8, 7), (8, 8), (8, 9), (8, 10), (8, 11), (8, 12), (8, 13), (8, 14),
                   (9, 2), (9, 3), (9, 4), (9, 5), (9, 6), (9, 7), (9, 8), (9, 9), (9, 10), (9, 11), (9, 12), (9, 13),
                   (10, 2), (10, 3), (10, 4), (10, 5), (10, 6), (10, 7), (10, 8), (10, 9), (10, 10), (10, 11), (10, 12), (10, 13),
                   (11, 3), (11, 4), (11, 5), (11, 6), (11, 7), (11, 8), (11, 9), (11, 10), (11, 11), (11, 12),
                   (12, 4), (12, 5), (12, 6), (12, 7), (12, 8), (12, 9), (12, 10), (12, 11),
                   (13, 6), (13, 7), (13, 8), (13, 9)]


def combine_xlsx(files, outfile_name=None):
    data_files = glob(files)
    relevant_xlsx = []
    for elem in data_files:
        if '0x' in elem and '0x192' not in elem:
            relevant_xlsx.extend(glob(os.path.join(elem, 'wafer_summary_?????.xlsx')))

    print("Combining the data of %d wafers" % len(relevant_xlsx))

    if outfile_name is None:
        outfile_name = os.path.join(os.sep.join(files.split(os.sep)[:-1]), 'wafers_summary.xlsx')
    out_wb = xlsxwriter.Workbook(outfile_name)

    combine_single_sheet(relevant_xlsx, out_wb, wb_name='Test Results', wb_number=0)
    combine_single_sheet(relevant_xlsx, out_wb, wb_name='Test Times', wb_number=1)
    combine_total_yield(relevant_xlsx, out_wb, wb_name='Total Yields', wb_number=2)
    copy_full_sheet(relevant_xlsx, out_wb, wb_name='Cuts', wb_number=3)
    combine_single_sheet(relevant_xlsx, out_wb, wb_name='Meta', wb_number=4)
    combine_single_sheet(relevant_xlsx, out_wb, wb_name='Ana Shunt Current', wb_number=5)
    combine_single_sheet(relevant_xlsx, out_wb, wb_name='Ana Shunt Voltage', wb_number=6)
    combine_single_sheet(relevant_xlsx, out_wb, wb_name='Digi Shunt Current', wb_number=7)
    combine_single_sheet(relevant_xlsx, out_wb, wb_name='Digi Shunt Voltage', wb_number=8)

    out_wb.close()
    return outfile_name


def copy_full_sheet(data_files, out_wb, wb_name='Tests', wb_number=3):
    out_ws_test = out_wb.add_worksheet(wb_name)
    cuts = []
    new_row = 0
    for df in data_files[::-1]:
        workbook = xlrd.open_workbook(df, on_demand=True)
        worksheet = workbook.sheet_by_index(wb_number)
        for row in range(worksheet.nrows):
            test = worksheet.cell_value(row, 0)
            if test not in cuts:
                cuts.append(test)
                for col in range(worksheet.ncols):
                    if wb_name == 'Cuts' and row > 0 and col > 0 and col <= 10:
                        if (col % 2) and (worksheet.cell_value(row, col) > worksheet.cell_value(row, col + 1)):
                            print(worksheet.cell_value(row, col - 1), worksheet.cell_value(row, col), worksheet.cell_value(row, col + 1))
                            out_ws_test.write(new_row, col, worksheet.cell_value(row, col + 1))
                        elif (not col % 2) and (worksheet.cell_value(row, col) < worksheet.cell_value(row, col - 1)):
                            print(worksheet.cell_value(row, col - 1), worksheet.cell_value(row, col), worksheet.cell_value(row, col + 1))
                            out_ws_test.write(new_row, col, worksheet.cell_value(row, col - 1))
                        else:
                            out_ws_test.write(new_row, col, worksheet.cell_value(row, col))
                    else:
                        out_ws_test.write(new_row, col, worksheet.cell_value(row, col))
                new_row += 1


def combine_total_yield(data_files, out_wb, wb_name='Tests', wb_number=0):
    scans = []
    out_ws_test = out_wb.add_worksheet(wb_name)
    for elem in data_files:
        workbook = xlrd.open_workbook(elem, on_demand=True)
        worksheet = workbook.sheet_by_index(wb_number)
        for n in range(worksheet.ncols):
            scans.append(worksheet.cell_value(0, n))
    scans = list(set(scans))
    scans_dict = {}
    cnt = 1
    for elem in scans:
        scans_dict[elem] = {}
        scans_dict[elem]['idx'] = cnt
        scans_dict[elem]['green'] = 0
        scans_dict[elem]['yellow'] = 0
        scans_dict[elem]['red'] = 0
        scans_dict[elem]['blue'] = 0
        cnt += 1

    for elem in data_files:
        workbook = xlrd.open_workbook(elem, on_demand=True)
        worksheet = workbook.sheet_by_index(wb_number)
        for row in range(1, worksheet.nrows):
            color = worksheet.cell_value(row, 0)
            for col in range(1, worksheet.ncols):
                scans_dict[worksheet.cell_value(0, col)][color] += int(worksheet.cell_value(row, col))

    out_ws_test.write(0, 0, 'Test Names')
    out_ws_test.write(1, 0, 'green')
    out_ws_test.write(2, 0, 'yellow')
    out_ws_test.write(3, 0, 'red')
    out_ws_test.write(4, 0, 'blue')
    for name, values in scans_dict.items():
        if name != 'Test name':
            out_ws_test.write(0, values['idx'], name)
            for cnt, color in enumerate(values.keys()):
                if color != 'idx':
                    out_ws_test.write(cnt, scans_dict[name]['idx'], scans_dict[name][color])


def combine_single_sheet(data_files, out_wb, wb_name='Tests', wb_number=0):
    scans = []
    out_ws_test = out_wb.add_worksheet(wb_name)
    out_ws_row = 0
    for elem in data_files:
        workbook = xlrd.open_workbook(elem, on_demand=True)
        worksheet = workbook.sheet_by_index(wb_number)
        for n in range(worksheet.ncols):
            scans.append(worksheet.cell_value(0, n))
    scans = list(dict.fromkeys(scans))
    # scans = sorted(set(scans), key=str)
    scans_dict = {}
    scans_dict['CHIP ID'] = 0
    scans_dict['chip_sn'] = 0
    out_ws_test.write(0, 0, 'CHIP ID')
    cnt = 1
    for _, x in enumerate(scans):
        if x != 'CHIP ID' and x != 'Yield' and x != 'chip_sn':
            scans_dict[x] = cnt
            out_ws_test.write(out_ws_row, cnt, x)
            cnt += 1
    if 'Yield' in scans:
        scans_dict['Yield'] = cnt
        out_ws_test.write(out_ws_row, cnt, 'Yield')
    out_ws_row += 1
    for elem in data_files:
        workbook = xlrd.open_workbook(elem, on_demand=True)
        worksheet = workbook.sheet_by_index(wb_number)
        for row in range(1, worksheet.nrows):
            if '0x' in worksheet.cell_value(row, 0):
                for col in range(worksheet.ncols):
                    out_ws_test.write(out_ws_row, scans_dict[worksheet.cell_value(0, col)], worksheet.cell_value(row, col))
                out_ws_row += 1


def check_path_else_create(dirpath):
    if not os.path.exists(dirpath):
        os.mkdir(dirpath)
    return dirpath


def load_cut_dict(worksheet):
    out_dict = {}
    for row in range(worksheet.nrows):
        if row == 0:
            continue
        for col in range(worksheet.ncols):
            if col == 0:
                out_dict[worksheet.cell_value(row, col).lower()] = {}
            else:
                cur_val = worksheet.cell_value(row, col)
                out_dict[worksheet.cell_value(row, 0).lower()][worksheet.cell_value(0, col)] = cur_val
    return out_dict


def load_yield_dict(worksheet):
    out_dict = {}
    for col in range(worksheet.ncols):
        if col == 0:
            continue
        for row in range(worksheet.nrows):
            if row == 0:
                out_dict[worksheet.cell_value(row, col).lower()] = {}
            else:
                cur_val = worksheet.cell_value(row, col)
                out_dict[worksheet.cell_value(0, col).lower()][worksheet.cell_value(row, 0)] = cur_val
    return out_dict


def plot_xlsx_summary_hist(filename, save_png=True, summary_map=True, success_map=True):
    workbook = xlrd.open_workbook(filename, on_demand=True)
    worksheet = workbook.sheet_by_index(0)
    worksheet_yield = load_yield_dict(workbook.sheet_by_index(2))
    cut_dict = load_cut_dict(workbook.sheet_by_index(3))
    outfile_pdf = PdfPages(os.path.join(os.sep.join(filename.split(os.sep)[:-1]), 'wafers_summary.pdf'))
    out_folder = check_path_else_create(os.path.join(os.sep.join(filename.split(os.sep)[:-1]), 'summary_pngs'))
    round_to = 2
    for col in range(1, worksheet.ncols):
        plot_name_normal = worksheet.cell_value(0, col)
        plot_name = plot_name_normal.lower()
        print("Generating summary plots for %s" % plot_name)
        hist_data = []
        for row in range(1, worksheet.nrows):
            cur_val = worksheet.cell_value(row, col)
            if cur_val != '':
                hist_data.append(float(cur_val))
        if len(hist_data) == 0:
            print("No data available")
            continue
        try:
            multiplier = float(cut_dict[plot_name]['multiplier'])
            cuts_arr = OrderedDict([('green', [(np.round(cut_dict[plot_name]['green_low'] * multiplier, round_to), np.round(cut_dict[plot_name]['green_high'] * multiplier, round_to))])])
            yields = worksheet_yield[plot_name]
            for colors_local in ['red', 'yellow']:
                color_arr = []
                for elem in ['_low_', '_high_']:
                    idx = colors_local + elem
                    if cut_dict[plot_name][idx + 'low'] != '':
                        cur_border = (np.round(cut_dict[plot_name][idx + 'low'] * multiplier, round_to), np.round(cut_dict[plot_name][idx + 'high'] * multiplier, round_to))
                        color_arr.append(cur_border)
                if len(color_arr) > 0:
                    cuts_arr[colors_local] = color_arr
            las_fig = plot_1d_hist(hist_data, ignore=cut_dict[plot_name]['ignore'], n_bins=int(cut_dict[plot_name]['n_bins']), title=plot_name_normal, x_axis_title=cut_dict[plot_name]['unit'], plot_range=[cut_dict[plot_name]['plot_range_low'], cut_dict[plot_name]['plot_range_high']], cuts=cuts_arr, yields=yields)
            if save_png:
                las_fig.savefig(os.path.join(out_folder, plot_name + '.png'), dpi=300)
            outfile_pdf.savefig(las_fig)
            if summary_map:
                las_map = generate_heatmap(filename, test_name=plot_name_normal, save_fig=False)
                if save_png:
                    las_map.savefig(os.path.join(out_folder, plot_name + '_map.png'), dpi=300)
                outfile_pdf.savefig(las_map)
            if success_map:
                las_map = generate_failure_heatmap(filename, test_name=plot_name_normal, save_fig=False)
                if save_png:
                    las_map.savefig(os.path.join(out_folder, plot_name + '_map_success.png'), dpi=300)
                outfile_pdf.savefig(las_map)

        except KeyError:
            print("key error", plot_name)
    outfile_pdf.close()


def plot_1d_hist(hist, cuts=None, stage=None, multiplier=1.0, plot_range=None, n_bins=75, ignore=False,
                 log_y=None, int_x_tics=False, title=None, x_axis_title=None, y_axis_title=None, yields=None,
                 use_plot_range_for_mean=True):
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    if ignore:
        ax.text(0.75, 0.75, 'Ignored!', fontsize=16, color='red',
                bbox=dict(boxstyle='round', facecolor='white', edgecolor='red'), rotation=-45,
                transform=ax.transAxes, zorder=999)
    hist = np.array([h * multiplier for h in hist if h != ''])
    if plot_range is None:
        plot_range = (np.amin(hist), np.amax(hist))
    ax.hist(hist, bins=np.linspace(plot_range[0], plot_range[1], num=n_bins))
    ax.set_xlim(plot_range)
    if int_x_tics:
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))

    if stage is not None:
        ax.set_title('Stage {0}: {1}'.format(stage, title), fontsize=12, color=TITLE_COLOR)
    else:
        ax.set_title(title, fontsize=12, color=TITLE_COLOR)

    if x_axis_title is not None:
        ax.set_xlabel(x_axis_title)
    if y_axis_title is not None:
        ax.set_ylabel(y_axis_title)
    else:
        ax.set_ylabel('# Chips')
    if log_y:
        ax.set_yscale('log')
        ax.set_ylim((1e-1, len(hist) * 2))
    ax.grid(True)

    if yields is not None:
        textleft = 'Yield'
        sums = sum(yields.values())
        for color in yields.keys():
            textleft += '\n' + color + ': %i (%1.1f%%)' % (yields[color], float(yields[color]) / sums * 100.)
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax.text(0.05, 0.75, textleft, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

    if cuts is not None:
        handles = []
        highest, lowest = plot_range[0], plot_range[1]
        mean_lower = plot_range[0]
        mean_upper = plot_range[1]
        for color in cuts.keys():
            for i in range(len(cuts[color])):
                (lower, upper) = cuts[color][i]
                lower = lower * multiplier
                upper = upper * multiplier
                if color == 'green' and not use_plot_range_for_mean:
                    mean_lower = lower
                    mean_upper = upper
                ax.axvspan(lower, upper, alpha=0.25, color=color, zorder=0)
                handles.append(mpatches.Patch(color=color, alpha=0.3,
                                              label=str(cuts[color][i][0]) + ' to ' + str(cuts[color][i][1])))
                if lower < lowest:
                    lowest = lower
                if upper > highest:
                    highest = upper
        mean_hist = hist[np.where(hist > mean_lower)]
        mean_hist = mean_hist[np.where(mean_hist < mean_upper)]
        if len(mean_hist) == 0:
            mean_hist = hist[np.where(hist > lowest)]
            mean_hist = mean_hist[np.where(mean_hist < highest)]
        if len(mean_hist) == 0:
            textleft = 'No valid data'
        else:
            textleft = 'Mean:  ' + str(np.round(np.mean(mean_hist), 3)) + \
                       '\n Std:  ' + str(np.round(np.std(mean_hist), 3))

        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax.text(0.45, 0.5, textleft, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

        ax.axvspan(plot_range[0], lowest, alpha=0.25, color='red', zorder=0)
        ax.axvspan(highest, plot_range[1], alpha=0.25, color='red', zorder=0)

        ax.legend(handles=handles, loc='upper left', fontsize=7)

    return fig


def generate_limits_all(out_file):
    out_wb = xlsxwriter.Workbook(out_file)
    out_ws_test = out_wb.add_worksheet('Cuts')
    file1 = open('analyze_all_wafers_ITkPix.py', 'r')
    lines = file1.readlines()
    search_string = 'fig = plot_1d_hist('
    row = 0
    y_values = ['Test name', 'red_low_low', 'red_low_high', 'yellow_low_low', 'yellow_low_high', 'green_low', 'green_high', 'yellow_high_low', 'yellow_high_high', 'red_high_low', 'red_high_high']
    for col, elem in enumerate(y_values):
        out_ws_test.write(row, col, elem)
    row += 1
    out_dict = {}
    for cnt, n in enumerate(lines):
        if search_string in n and "total_tested_registers" not in n and cnt < 208:
            scan_name = lines[cnt - 1].split("'")[1].lower()
            out_dict[scan_name] = {}
            out_ws_test.write(row, 0, scan_name)
            value_dict = OrderedDict(eval(n.split('OrderedDict')[-1][:-2]))
            try:
                out_ws_test.write(row, 1, value_dict['red'][0][0])
                out_dict[scan_name]['red_low_low'] = value_dict['red'][0][0]
            except Exception:
                pass
            try:
                out_ws_test.write(row, 2, value_dict['red'][0][1])
                out_dict[scan_name]['red_low_high'] = value_dict['red'][0][1]
            except Exception:
                pass
            try:
                out_ws_test.write(row, 3, value_dict['yellow'][0][0])
                out_dict[scan_name]['yellow_low_low'] = value_dict['yellow'][0][0]
            except Exception:
                pass
            try:
                out_ws_test.write(row, 4, value_dict['yellow'][0][1])
                out_dict[scan_name]['yellow_low_high'] = value_dict['yellow'][0][1]
            except Exception:
                pass
            try:
                out_ws_test.write(row, 5, value_dict['green'][0][0])
                out_dict[scan_name]['green_low'] = value_dict['green'][0][0]
            except Exception:
                pass
            try:
                out_ws_test.write(row, 6, value_dict['green'][0][1])
                out_dict[scan_name]['green_high'] = value_dict['green'][0][0]
            except Exception:
                pass
            try:
                out_ws_test.write(row, 7, value_dict['yellow'][1][0])
                out_dict[scan_name]['yellow_high_low'] = value_dict['yellow'][1][0]
            except Exception:
                pass
            try:
                out_ws_test.write(row, 8, value_dict['yellow'][1][1])
                out_dict[scan_name]['yellow_high_high'] = value_dict['yellow'][1][1]
            except Exception:
                pass
            try:
                out_ws_test.write(row, 9, value_dict['red'][1][0])
                out_dict[scan_name]['red_high_low'] = value_dict['red'][1][0]
            except Exception:
                pass
            try:
                out_ws_test.write(row, 10, value_dict['red'][1][1])
                out_dict[scan_name]['red_high_high'] = value_dict['red'][1][1]
            except Exception:
                pass
            row += 1
    out_wb.close()
    return out_dict


def generate_limits_single(out_file):
    out_wb = xlsxwriter.Workbook(out_file)
    out_ws_test = out_wb.add_worksheet('Cuts')
    file1 = open('analyze_wafer_ITkPixV1.py')
    lines = file1.readlines()
    search_string = 'self.create_map_and_hist('
    row = 0
    y_values = ['Test name', 'red_low_low', 'red_low_high', 'yellow_low_low', 'yellow_low_high', 'green_low', 'green_high', 'yellow_high_low', 'yellow_high_high', 'red_high_low', 'red_high_high']
    for col, elem in enumerate(y_values):
        out_ws_test.write(row, col, elem)
    row += 1
    out_dict = {}
    for cnt, n in enumerate(lines):
        if search_string in n and "def" not in n and cnt > 979:
            scan_name = lines[cnt].split("'")[1].lower()
            out_ws_test.write(row, 0, scan_name)
            multiplier = eval(lines[cnt].split("multiplier=")[1].split(',')[0])
            out_dict[scan_name] = {}

            try:
                value_dict = OrderedDict(eval(n.split('OrderedDict')[-1][:-2]))
            except Exception:
                try:
                    value_dict = OrderedDict(eval((n + lines[cnt + 1]).split('OrderedDict')[-1][:-2]))
                except Exception:
                    continue
            try:
                out_ws_test.write(row, 1, value_dict['red'][0][0] * multiplier)
                out_dict[scan_name]['red_low_low'] = value_dict['red'][0][0] * multiplier
            except Exception:
                pass
            try:
                out_ws_test.write(row, 2, value_dict['red'][0][1] * multiplier)
                out_dict[scan_name]['red_low_high'] = value_dict['red'][0][1] * multiplier
            except Exception:
                pass
            try:
                out_ws_test.write(row, 3, value_dict['yellow'][0][0] * multiplier)
                out_dict[scan_name]['yellow_low_low'] = value_dict['yellow'][0][0] * multiplier
            except Exception:
                pass
            try:
                out_ws_test.write(row, 4, value_dict['yellow'][0][1] * multiplier)
                out_dict[scan_name]['yellow_low_high'] = value_dict['yellow'][0][1] * multiplier
            except Exception:
                pass
            try:
                out_ws_test.write(row, 5, value_dict['green'][0][0] * multiplier)
                out_dict[scan_name]['green_low'] = value_dict['green'][0][0] * multiplier
            except Exception:
                pass
            try:
                out_ws_test.write(row, 6, value_dict['green'][0][1] * multiplier)
                out_dict[scan_name]['green_high'] = value_dict['green'][0][0] * multiplier
            except Exception:
                pass
            try:
                out_ws_test.write(row, 7, value_dict['yellow'][1][0] * multiplier)
                out_dict[scan_name]['yellow_high_low'] = value_dict['yellow'][1][0] * multiplier
            except Exception:
                pass
            try:
                out_ws_test.write(row, 8, value_dict['yellow'][1][1] * multiplier)
                out_dict[scan_name]['yellow_high_high'] = value_dict['yellow'][1][1] * multiplier
            except Exception:
                pass
            try:
                out_ws_test.write(row, 9, value_dict['red'][1][0] * multiplier)
                out_dict[scan_name]['red_high_low'] = value_dict['red'][1][0] * multiplier
            except Exception:
                pass
            try:
                out_ws_test.write(row, 10, value_dict['red'][1][1] * multiplier)
                out_dict[scan_name]['red_high_high'] = value_dict['red'][1][1] * multiplier
            except Exception:
                pass
            row += 1
    out_wb.close()
    return out_dict


def get_cuts(wb, test_name):
    df = pd.read_excel(wb, sheet_name="Cuts")
    test = df[df["Test name"] == test_name].iloc[0]
    m = test.multiplier
    cuts = OrderedDict([
        ('green', [(test.green_low * m, test.green_high * m)]),
        ('yellow', [(test.yellow_low_low * m, test.yellow_low_high * m), (test.yellow_high_low * m, test.yellow_high_high * m)]),
        ('red', [(test.red_low_low * m, test.red_low_high * m), (test.red_high_low * m, test.red_high_high * m)])])
    return cuts


def get_range(wb, test_name):
    df = pd.read_excel(wb, sheet_name="Cuts")
    test = df[df["Test name"] == test_name].iloc[0]
    return test.plot_range_low, test.plot_range_high, test.unit


def generate_failure_heatmap(combined_xlsx, test_name='Yield', z_axis_title="Succes Probability [%]", only_green=True, save_fig=True):
    plot_title = test_name + " Probability Map"
    if only_green:
        weight_dict = {'blue': 0, 'red': 0, 'yellow': 0, 'green': 100}
    else:
        weight_dict = {'blue': 0, 'red': 0, 'yellow': 50, 'green': 100}
    cuts = get_cuts(combined_xlsx, test_name)
    workbook = xlrd.open_workbook(combined_xlsx, on_demand=True)
    worksheet = workbook.sheet_by_index(0)
    col_dict = {}
    for col_no in range(worksheet.ncols):
        col_dict[worksheet.cell_value(0, col_no)] = col_no
    map_dict = {}
    n_chips = 0
    for row_no in range(1, worksheet.nrows):
        chip_id = worksheet.cell_value(row_no, col_dict['CHIP ID'])
        cell_string = worksheet.cell_value(row_no, col_dict[test_name])
        if cell_string == '':
            continue
        chip_yield = 'blue'
        chip_value = float(cell_string)
        for col_key, ranges in cuts.items():
            for n in ranges:
                if chip_value >= n[0] and chip_value < n[1]:
                    chip_yield = col_key
                    break
        if chip_value != -1000:
            cur_val = weight_dict[chip_yield]
            try:
                map_dict[chip_id[-2:]].append(cur_val)
            except Exception:
                map_dict[chip_id[-2:]] = [cur_val]
        n_chips += 1
    output_pdf_name = combined_xlsx.split('.xlsx')[0] + '_' + test_name + '.pdf'

    return plot_wafermap(map_dict, n_chips, output_path=output_pdf_name, title=plot_title, colorscale_title=z_axis_title, save_fig=save_fig)


def generate_heatmap(combined_xlsx, test_name='Yield', save_fig=True):
    rang = get_range(combined_xlsx, test_name)
    plot_title = test_name + " Map"
    colorscale_title = "%s [%s]" % (test_name, rang[2])
    colorrange = rang[:2]
    workbook = xlrd.open_workbook(combined_xlsx, on_demand=True)
    worksheet = workbook.sheet_by_index(0)
    col_dict = {}
    for col_no in range(worksheet.ncols):
        col_dict[worksheet.cell_value(0, col_no)] = col_no
    map_dict = {}
    n_chips = 0
    for row_no in range(1, worksheet.nrows):
        chip_id = worksheet.cell_value(row_no, col_dict['CHIP ID'])
        cell_string = worksheet.cell_value(row_no, col_dict[test_name])
        if cell_string == '' or cell_string == -1:
            continue
        chip_value = float(cell_string)
        try:
            map_dict[chip_id[-2:]].append(chip_value)
        except Exception:
            map_dict[chip_id[-2:]] = [chip_value]
        n_chips += 1
    output_pdf_name = combined_xlsx.split('.xlsx')[0] + '_' + test_name + '.pdf'

    return plot_wafermap(map_dict, n_chips, output_path=output_pdf_name, title=plot_title, colorscale_title=colorscale_title, colorrange=colorrange, save_fig=save_fig, unit=rang[2], form='%.2f', print_yield=False)


def plot_wafermap(data, number_of_chips, output_path='test.pdf', title='', stage=None, colorscale_title="", colorrange=(0, 100), save_fig=True, unit='%', form='%d', print_yield=True):
    hist = {}
    for coordinates, dat in data.items():
        hist[coordinates] = float(np.mean(dat))
    avg = np.mean(list(np.mean(list(dat)) for dat in data.values()))
    cmap = pyplot.get_cmap('plasma')
    if colorrange:
        norm = colors.Normalize(vmin=colorrange[0], vmax=colorrange[1])
    else:
        norm = colors.Normalize(vmin=min(hist.values()), vmax=max(hist.values()))

    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)

    ax.axis('off')

    if stage is not None:
        ax.set_title('Stage {0}: {1}'.format(stage, title), fontsize=12, color=TITLE_COLOR)
    else:
        ax.set_title(title, fontsize=12, color=TITLE_COLOR)

    tb_list = [['' for _ in range(14)] for _ in range(14)]
    widths = [0.05 for _ in range(14)]
    table = ax.table(cellText=tb_list, colWidths=widths, edges='closed', cellLoc='center', loc='center')

    for key, cell in table.get_celld().items():
        row, col = key
        row = 13 - row
        chip_sn = '0x' + str(hex(row + 1))[2:].upper() + str(hex(col + 1))[2:].upper()
        cell.set_height(0.06)

        if tuple((row + 1, col + 1)) not in valid_chips:
            cell.set_facecolor('white')
            cell.set_edgecolor('white')
            cell._text.set_color('white')
            continue

        cell._text.set_fontsize(4)

        celltext = str(row + 1) + '-' + str(col + 1) + '\n'
        celltext = chip_sn + '\n'
        cell._text.set_text(celltext)
        cell._text.set_text(celltext)
        text = '{0} Wafers\n{1} Chips'.format(int(number_of_chips / nchips), int(number_of_chips))
        if print_yield:
            text += "\n{0:.1f} % avg. yield".format(avg)
        props = dict(boxstyle='round', facecolor='white', alpha=0.5)
        ax.text(0, 1, text, transform=ax.transAxes,
                fontsize=8, verticalalignment='top', bbox=props)
        try:
            if data is not None:
                celltext += form % hist[chip_sn[2:]] + ' %s' % unit
            cell.set_facecolor(cmap(norm(hist[chip_sn[2:]])))
        except KeyError:
            celltext += 'No Data'
            cell.set_facecolor('red')
        cell._text.set_text(celltext)

    ax2 = fig.add_axes([0.88, 0.1, 0.02, 0.8])
    cb = colorbar.ColorbarBase(ax2, cmap=cmap, norm=norm, orientation='vertical')
    cb.set_label(colorscale_title)

    # Notch
    notch = mpatches.Polygon([[0.49, 0.045], [0.5, 0.06], [0.51, 0.045]], alpha=0.5, zorder=999)
    p = PatchCollection([notch])
    p.set_edgecolor('black')
    p.set_facecolor('white')
    ax.add_collection(p)

    if save_fig:
        fig.savefig(output_path)
    else:
        return fig


def plot_correlattion(combined_xlsx, not_ignored=True):
    workbook = xlrd.open_workbook(combined_xlsx, on_demand=True)
    worksheet = workbook.sheet_by_index(0)
    cut_dict = load_cut_dict(workbook.sheet_by_index(3))
    tests = {cut: idx for idx, cut in enumerate([cut for cut, val in cut_dict.items()
                                                 if (not val['ignore'] or not not_ignored)
                                                 and not cut.startswith('ring_osc')
                                                 or cut.startswith('ring_osc_a_0')])}
    # tests = {'iref current': 0, 'vddd after trimming': 1}
    ntests = len(tests)
    means = {}
    ring_osc = None
    for col in range(1, worksheet.ncols):
        col_Name = worksheet.cell_value(0, col)
        col_name = col_Name.lower()
        if col_name == 'test time':
            test_time_col = col
    for col in range(1, worksheet.ncols):
        col_Name = worksheet.cell_value(0, col)
        col_name = col_Name.lower()
        if col_name.startswith('ring_osc'):
            cuts = get_cuts(combined_xlsx, col_Name)
            if ring_osc is None:
                ring_osc = {}
                for row_no in range(1, worksheet.nrows):
                    cell_string = worksheet.cell_value(row_no, col)
                    chip_sn = worksheet.cell_value(row_no, 0)
                    time_val = float(worksheet.cell_value(row_no, test_time_col))
                    if cell_string == '' or time_val == -1:
                        continue
                    chip_value = float(cell_string)
                    green = chip_value >= cuts['green'][0][0] and chip_value < cuts['green'][0][1]
                    ring_osc[chip_sn] = green
            else:
                for row_no in range(1, worksheet.nrows):
                    cell_string = worksheet.cell_value(row_no, col)
                    chip_sn = worksheet.cell_value(row_no, 0)
                    time_val = float(worksheet.cell_value(row_no, test_time_col))
                    if cell_string == '' or time_val == -1:
                        continue
                    chip_value = float(cell_string)
                    green = chip_value >= cuts['green'][0][0] and chip_value < cuts['green'][0][1]
                    ring_osc[chip_sn] &= green
        if col_name not in tests:
            continue
        sum = 0
        n = 0
        for row_no in range(1, worksheet.nrows):
            cell_string = worksheet.cell_value(row_no, col)
            if cell_string == '':
                continue
            sum += float(cell_string)
            n += 1
        means[col_name] = sum / n
    hist_occ, hist_cor = np.zeros((ntests + 1, ntests + 1)), np.zeros((ntests + 1, ntests + 1))
    corr_coeff = np.zeros((ntests, ntests))
    for i in range(len(tests)):
        hist_occ[i, i] = worksheet.nrows
        hist_cor[i, i] = worksheet.nrows
        corr_coeff[i, i] = 1
    hist_occ[-1, -1] = worksheet.nrows
    hist_cor[-1, -1] = worksheet.nrows
    for col1 in range(1, worksheet.ncols):
        col_Name1 = worksheet.cell_value(0, col1)
        col_name1 = col_Name1.lower()
        if col_name1 not in tests:
            continue
        cuts1 = get_cuts(combined_xlsx, col_Name1)
        for col2 in range(col1 + 1, worksheet.ncols):
            col_Name2 = worksheet.cell_value(0, col2)
            col_name2 = col_Name2.lower()
            if col_name2 not in tests:
                continue
            print(col_Name1, col_Name2)
            cuts2 = get_cuts(combined_xlsx, col_Name2)
            xys, xss, yss, ns = 0, 0, 0, 0
            for row_no in range(1, worksheet.nrows):
                cell_string1 = worksheet.cell_value(row_no, col1)
                cell_string2 = worksheet.cell_value(row_no, col2)
                time_val = float(worksheet.cell_value(row_no, test_time_col))
                if cell_string1 == '' or cell_string2 == '' or time_val == -1:
                    continue
                chip_yield1 = 'blue'
                chip_value1 = float(cell_string1)
                for col_key, ranges in cuts1.items():
                    for n in ranges:
                        if chip_value1 >= n[0] and chip_value1 < n[1]:
                            chip_yield1 = col_key
                            break
                chip_yield2 = 'blue'
                chip_value2 = float(cell_string2)
                for col_key, ranges in cuts2.items():
                    for n in ranges:
                        if chip_value2 >= n[0] and chip_value2 < n[1]:
                            chip_yield2 = col_key
                            break
                xys += (chip_value1 - means[col_name1]) * (chip_value2 - means[col_name2])
                xss += (chip_value1 - means[col_name1])**2
                yss += (chip_value2 - means[col_name2])**2
                ns += 1
                if chip_yield1 != 'green' or chip_yield2 != 'green':
                    hist_occ[tests[col_name1], tests[col_name2]] += 1
                    hist_occ[tests[col_name2], tests[col_name1]] += 1
                if chip_yield1 != 'green' and chip_yield2 != 'green':
                    hist_cor[tests[col_name1], tests[col_name2]] += 1
                    hist_cor[tests[col_name2], tests[col_name1]] += 1
            cc = xys / ns / np.sqrt(xss / ns) / np.sqrt(yss / ns)
            corr_coeff[tests[col_name1], tests[col_name2]] = cc
            corr_coeff[tests[col_name2], tests[col_name1]] = cc
        for row_no in range(1, worksheet.nrows):
            cell_string1 = worksheet.cell_value(row_no, col1)
            time_val = float(worksheet.cell_value(row_no, test_time_col))
            chip_sn = worksheet.cell_value(row_no, 0)
            if cell_string1 == '' or time_val == -1:
                continue
            chip_yield1 = 'blue'
            chip_value1 = float(cell_string1)
            for col_key, ranges in cuts1.items():
                for n in ranges:
                    if chip_value1 >= n[0] and chip_value1 < n[1]:
                        chip_yield1 = col_key
                        break
            if chip_yield1 != 'green' or ring_osc[chip_sn]:
                hist_occ[tests[col_name1], -1] += 1
                hist_occ[-1, tests[col_name1]] += 1
            if chip_yield1 != 'green' and ring_osc[chip_sn]:
                hist_cor[tests[col_name1], -1] += 1
                hist_cor[-1, tests[col_name1]] += 1
    correlation = hist_cor / hist_occ

    outfile_pdf = PdfPages(os.path.join(os.sep.join(combined_xlsx.split(os.sep)[:-1]), 'correlation.pdf'))

    for title, map, cmap_n, ct in (("Failure correlation", correlation, 'viridis', lambda x: x), ("Correlation coefficients", corr_coeff, 'PRGn', lambda x: x / 2 + 0.5)):
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.set_axis_off()
        tb = Table(ax, bbox=[0, 0, 1, 1])

        nrows, ncols = ntests, ntests
        width, height = 1.0 / ncols, 1.0 / nrows
        cmap = mpl.colormaps[cmap_n]

        for row, data in enumerate(map):
            for col, val in enumerate(data):
                label = f"{val:.2f}"
                color = cmap(ct(val))
                cell = tb.add_cell(row, col + 1, width, height, text=label, facecolor=color)
                cell.set_text_props(size=3.5)
                cell.set(lw=0.1)

        labels = tests.keys() if map.shape[0] == len(tests) else [*tests.keys(), "Ring Oscillators"]
        for i, label in enumerate(labels):
            cell = tb.add_cell(i, 0, width * 5, height, text=label, loc='right',
                               edgecolor='none', facecolor='none')
            cell.set_text_props(size='xx-small')
            cell = tb.add_cell(nrows + 1, i + 1, width, height, text=label, loc='right',
                               edgecolor='none', facecolor='none')
            cell.set_text_props(size=2, rotation=90, verticalalignment="bottom", rotation_mode="anchor")
            cell.set_text_props(size='xx-small')
        cell = tb.add_cell(nrows + 2, 0, width, width * 8, text="", loc='right',
                           edgecolor='none', facecolor='none')

        tb.auto_set_font_size(False)
        ax.add_table(tb)
        ax.set_title(title, fontsize=12, color=TITLE_COLOR)
        outfile_pdf.savefig(fig)
    outfile_pdf.close()
    return


if __name__ == '__main__':
    path_to_analysed_data = '/mnt/drive/Analysis230429'
    batches = {
        "*": "Summary_1-5",
        # "1_N6W780.01": "1_N6W780.01",
        # "2_N6W755.01": "2_N6W755.01",
        # "3_N4KX64": "3_N4KX64",
        # "4_N4KX65": "4_N4KX65",
        # "5_N60P97": "5_N60P97",
    }
    for batch, out_folder in batches.items():
        path_to_wafers = path_to_analysed_data + '/%s/0x*' % batch
        path_to_summary = path_to_analysed_data + "/%s/wafers_summary.xlsx" % out_folder

        combine_xlsx(path_to_wafers, outfile_name=path_to_summary)
        plot_xlsx_summary_hist(path_to_summary, summary_map=True)
        # generate_failure_heatmap(path_to_summary, test_name='Yield')
