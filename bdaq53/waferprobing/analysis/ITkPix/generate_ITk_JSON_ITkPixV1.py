import xlrd
import json
from datetime import datetime
from tqdm import tqdm
import yaml
import os
import tables as tb


class ITkPixV1ProdDBGenerator():
    test_mapping_dict = {
        'VINA Shunt Delta k factor': 'VINA_SHUNT_DELTAKFACTOR',
        'Temperature A Sensor': 'TEMPERATURE_A',
        'VCAL MED Small Range Slope': 'VCAL_MED_SMALL_RANGE_SLOPE',
        'VIND Shunt Delta k factor': 'VIND_SHUNT_DELTAKFACTOR',
        'VDDD after trimming': 'VDDD_TRIM_VALUE',
        'Mean Threshold Distribution': 'THRESHOLDSCAN_VALUE',
        'VIND Shunt k factor': 'VIND_SHUNT_KFACTOR',
        'Aurora Lane Test': 'LINK_VALUE',
        'VCAL HIGH Small Range Slope': 'VCAL_HIGH_SMALL_RANGE_SLOPE',
        'Temperature C Sensor': 'TEMPERATURE_C',
        'Iref Trim Bit': 'IREF_TRIM',
        'Pixel Register Test': 'PIXEL_REGISTER_TEST',
        'VDDD Max': 'VDDD_16',
        'VDDA Max': 'VDDA_16',
        'VDDD Trim Bit': 'VDDD_TRIM',
        'ADC Offset': 'ADC_OFFSET',
        'VDDA Trim Bit': 'VDDA_TRIM',
        'Temperature B Sensor': 'TEMPERATURE_B',
        'Temperature T Sensor': 'TEMPERATURE_T',
        'VDDA after trimming': 'VDDA_TRIM_VALUE',
        'VCAL HIGH Small Range Offset': 'VCAL_HIGH_SMALL_RANGE_OFFSET',
        'VCAL HIGH Large Range Offset': 'VCAL_HIGH_LARGE_RANGE_OFFSET',
        'VCAL MED Large Range Offset': 'VCAL_MED_LARGE_RANGE_OFFSET',
        'Temperature D Sensor': 'TEMPERATURE_D',
        'VCAL HIGH Large Range Slope': 'VCAL_HIGH_LARGE_RANGE_SLOPE',
        'VINA Shunt k factor': 'VINA_SHUNT_KFACTOR',
        'Iref Current': 'IREF_TRIM_VALUE',
        'ADC Slope': 'ADC_SLOPE',
        'VCAL MED Large Range Slope': 'VCAL_MED_LARGE_RANGE_SLOPE',
        'Analog Occupancy': 'ANALOGSCAN_VALUE',
        'VCAL MED Small Range Offset': 'VCAL_MED_SMALL_RANGE_OFFSET',
        'Digital Occupancy': 'DIGITALSCAN_VALUE',
        'Injection capacitance': 'InjectionCapacitance',
        'Register Test': 'REGISTER_TEST',
        'Chip ID Test': 'CHIP_ID_TEST',
        'Data Merging': 'DATA_MERGING',
        'Yield': 'OVERALL',
        'Current Multiplicator A': 'CURR_MULT_FAC_A',
        'Current Multiplicator D': 'CURR_MULT_FAC_D',
        'PC NTC Temperature': 'PC_NTC',
        'IINA in LDO mode': 'ANALOG_POWER_CONSUMPTION',
        'IIND in LDO mode': 'DIGITAL_POWER_CONSUMPTION',
        'Analog OVP - 3.3 x OVP Ref': 'ANALOG_OVER_VOLTAGE_PROTECTION',
        'Digital OVP - 3.3 x OVP Ref': 'DIGITAL_OVER_VOLTAGE_PROTECTION',
        'Efuses Write Success': 'EFUSES_WRITE_RESULT'
    }

    def __init__(self, inxlsx, example_json_file, institution='BONN'):
        self.yaml_folder = os.path.join(os.sep.join(inxlsx.split(os.sep)[:-1]), 'db_attachments')
        self.h5_file_path = inxlsx.replace('.xlsx', '.h5')
        self.ring_osc_dict = self.load_ring_osc_dict(self.h5_file_path)
        self.std_json = self.load_example_json(example_json_file)
        self.chips_all = self.load_xlsx_to_dict(inxlsx, wb_number=0)
        self.cuts = self.load_xlsx_to_dict(inxlsx, wb_number=3)
        self.meta_all = self.load_xlsx_to_dict(inxlsx, wb_number=4)
        self.ana_curr = self.load_xlsx_to_dict(inxlsx, wb_number=5)
        self.ana_volt = self.load_xlsx_to_dict(inxlsx, wb_number=6)
        self.digi_curr = self.load_xlsx_to_dict(inxlsx, wb_number=7)
        self.digi_volt = self.load_xlsx_to_dict(inxlsx, wb_number=8)
        self.test_mapping_dict_inv = {v: k for k, v in self.test_mapping_dict.items()}
        self.wafer_number = list(self.chips_all.keys())[0][:5]
        self.institution = institution

    def load_ring_osc_dict(self, h5_file_path):
        ring_osc_dict = {}
        with tb.open_file(h5_file_path, 'r') as in_file:
            for node in in_file:
                if 'RingOsc' in str(node):
                    ring_osc_values = node[:]
                    cur_out_dict = {}
                    for n in ring_osc_values:
                        cur_out_dict[n[0].decode("utf-8")] = {}
                        cur_out_dict[n[0].decode("utf-8")]['raw_data'] = int(n[1])
                        cur_out_dict[n[0].decode("utf-8")]['count'] = int(n[2])
                        cur_out_dict[n[0].decode("utf-8")]['frequency'] = float(n[3])
                        cur_out_dict[n[0].decode("utf-8")]['pulse_length'] = int(n[4])
                    chip_id = str(node).split('/')[2]
                    ring_osc_dict[chip_id] = cur_out_dict
        return ring_osc_dict

    def get_results(self, chip_id, test_name):
        test_passed = 'red'
        if self.cuts[test_name]['green_low'] * self.cuts[test_name]['multiplier'] <= self.chips_all[chip_id][test_name] and self.cuts[test_name]['green_high'] * self.cuts[test_name]['multiplier'] >= self.chips_all[chip_id][test_name]:
            test_passed = 'green'
        try:
            if test_passed != 'green':
                if self.cuts[test_name]['yellow_low_low'] * self.cuts[test_name]['multiplier'] <= self.chips_all[chip_id][test_name] and self.cuts[test_name]['yellow_low_high'] * self.cuts[test_name]['multiplier'] >= self.chips_all[chip_id][test_name] or self.cuts[test_name]['yellow_high_low'] * self.cuts[test_name]['multiplier'] <= self.chips_all[chip_id][test_name] and self.cuts[test_name]['yellow_high_high'] * self.cuts[test_name]['multiplier'] >= self.chips_all[chip_id][test_name]:
                    test_passed = 'yellow'
        except TypeError:
            pass
        return test_passed

    def load_example_json(self, in_file):
        with open(in_file) as json_file:
            data = json.load(json_file)
        return data

    def load_xlsx_to_dict(self, input_file, wb_number=0):
        workbook = xlrd.open_workbook(input_file, on_demand=True)
        worksheet = workbook.sheet_by_index(wb_number)
        out_dict = {}
        for row in range(1, worksheet.nrows):
            chip_id = worksheet.cell_value(row, 0)
            out_dict[chip_id] = {}
            for col in range(1, worksheet.ncols):
                cur_test = worksheet.cell_value(0, col)
                out_dict[chip_id][cur_test] = worksheet.cell_value(row, col)
        return out_dict

    def dict_to_proddb_json(self):
        out_dict = {}
        out_dict['testType'] = "FECHIP_TEST"
        out_dict['institution'] = self.institution
        chip_id = list(self.meta_all.keys())[0]
        out_dict['runNumber'] = self.wafer_number + '-' + datetime.fromtimestamp(
            self.meta_all[chip_id]['start_time']).strftime('%d%m%Y')
        out_dict['date'] = datetime.fromtimestamp(self.meta_all[chip_id]['start_time']).strftime('%Y-%m-%dT%H:%M:%S.000Z')
        out_dict['Wafer_ID'] = self.meta_all[chip_id]['wafer_no']
        out_dict['DAQ_VERSION'] = 'sw: ' + str(
            self.meta_all[chip_id]['sw_version']) + '; fw: ' + str(
            self.meta_all[chip_id]['fw_version']) + '; lanes: ' + str(
            self.meta_all[chip_id]['rx_lanes']) + '; speed: ' + str(self.meta_all[chip_id]['rx_speed'])
        out_dict['chips'] = {}
        failed_chips = 0
        for chip_id, test_dict in tqdm(self.chips_all.items()):
            out_dict['chips'][chip_id] = {}
            out_dict['chips'][chip_id]['testType'] = "FECHIP_TEST"
            out_dict['chips'][chip_id]['institution'] = self.institution
            out_dict['chips'][chip_id]['runNumber'] = self.wafer_number + '-' + datetime.fromtimestamp(self.meta_all[chip_id]['start_time']).strftime('%d%m%Y')
            out_dict['chips'][chip_id]['date'] = datetime.fromtimestamp(self.meta_all[chip_id]['start_time']).strftime('%Y-%m-%dT%H:%M:%S.000Z')
            out_dict['chips'][chip_id]['Wafer_ID'] = self.meta_all[chip_id]['wafer_no']
            if bool(self.chips_all[chip_id]['Yield']):
                result_passed = True
            else:
                result_passed = False
            out_dict['chips'][chip_id]['passed'] = result_passed
            out_dict['chips'][chip_id]['problems'] = not result_passed
            out_dict['chips'][chip_id]['properties'] = {}
            out_dict['chips'][chip_id]['properties']['DAQ_VERSION'] = 'sw: ' + str(self.meta_all[chip_id]['sw_version']) + '; fw: ' + str(self.meta_all[chip_id]['fw_version']) + '; lanes: ' + str(self.meta_all[chip_id]['rx_lanes']) + '; speed: ' + str(self.meta_all[chip_id]['rx_speed'])
            out_dict['chips'][chip_id]['properties']['LOCAL_OBJECT_NAME'] = chip_id
            out_dict['chips'][chip_id]['properties']['DATABASE_LINK'] = 'https://itkpix.web.cern.ch/' + self.wafer_number + '/raw/' + chip_id
            failed_tests = 0
            out_dict['chips'][chip_id]['results'] = {}
            for db_tests in self.std_json['results']:
                if db_tests in self.test_mapping_dict_inv.keys():
                    if db_tests == "EFUSES_WRITE_RESULT":
                        cur_value = self.test_mapping_dict_inv[db_tests]
                        out_dict['chips'][chip_id]['results'][db_tests] = self.get_results(chip_id, cur_value)
                        if out_dict['chips'][chip_id]['results'][db_tests] != 'green' and str(
                                self.cuts[cur_value]['ignore']) == '0':
                            failed_tests += 1
                    elif 'TRIM' in db_tests and not ('VALUE' in db_tests):
                        out_dict['chips'][chip_id]['results'][db_tests] = int(self.chips_all[chip_id][self.test_mapping_dict_inv[db_tests]] * 1 / self.cuts[self.test_mapping_dict_inv[db_tests]]['multiplier'])
                    else:
                        out_dict['chips'][chip_id]['results'][db_tests] = self.chips_all[chip_id][self.test_mapping_dict_inv[db_tests]] * 1 / self.cuts[self.test_mapping_dict_inv[db_tests]]['multiplier']
                elif 'VREF_ADC_TRIM' in db_tests or 'CONF' in db_tests or 'VINA_SHUNT' in db_tests or 'VIND_SHUNT' in db_tests or 'NOCCSCAN' in db_tests or 'RESISTOR' in db_tests:
                    if 'RESULT' in db_tests:
                        out_dict['chips'][chip_id]['results'][db_tests] = 'black'
                    else:
                        out_dict['chips'][chip_id]['results'][db_tests] = -1
                    continue
                elif 'RESULT' in db_tests:
                    cur_value = db_tests.replace('_RESULT', '')
                    try:
                        cur_value = self.test_mapping_dict_inv[cur_value]
                    except Exception:
                        cur_value = db_tests.replace('_RESULT', '_VALUE')
                        cur_value = self.test_mapping_dict_inv[cur_value]
                    out_dict['chips'][chip_id]['results'][db_tests] = self.get_results(chip_id, cur_value)
                    if out_dict['chips'][chip_id]['results'][db_tests] != 'green' and str(self.cuts[cur_value]['ignore']) == '0':
                        failed_tests += 1
                elif 'IV_CURVE' in db_tests:
                    iv_dict = {}
                    iv_dict["IINA"] = [i for i in self.ana_curr[chip_id].values()]
                    iv_dict["VINA"] = [i for i in self.ana_volt[chip_id].values()]
                    iv_dict["IIND"] = [i for i in self.digi_curr[chip_id].values()]
                    iv_dict["VIND"] = [i for i in self.digi_volt[chip_id].values()]
                    out_dict['chips'][chip_id]['results'][db_tests] = iv_dict
                elif 'RING_OSC_0_VALUE' in db_tests:
                    try:
                        out_dict['chips'][chip_id]['results'][db_tests] = self.ring_osc_dict[str(chip_id)]
                    except KeyError:
                        pass
                elif 'ANALOG_DATA' in db_tests:
                    try:
                        with open(os.path.join(self.yaml_folder, str(chip_id) + '_ana.yaml'), 'r') as f:
                            analog_data = yaml.safe_load(f)
                            analog_copy = dict(analog_data)
                            analog_copy_iv = analog_copy.pop('IV curves')
                            analog_copy['IV curves'] = {}
                            for elem, cur_data in analog_copy_iv.items():
                                analog_copy['IV curves'][str(elem).replace('.', '')] = cur_data
                            analog_copy_ovp = analog_copy.pop('OVP')
                            analog_copy['OVP curves'] = {}
                            for elem, cur_data in analog_copy_ovp.items():
                                analog_copy['OVP curves'][str(elem).replace('.', '')] = cur_data
                        out_dict['chips'][chip_id]['results'][db_tests] = analog_copy
                    except FileNotFoundError:
                        print("analog file not found")
                elif 'META_DATA' in db_tests:
                    try:
                        with open(os.path.join(self.yaml_folder, str(chip_id) + '_meta.yaml'), 'r') as f:
                            meta_data = yaml.safe_load(f)
                        out_dict['chips'][chip_id]['results'][db_tests] = meta_data
                    except FileNotFoundError:
                        print("meta file not found")
                else:
                    pass
                    # print("else", db_tests)
            out_dict['chips'][chip_id]['FAILED_TESTS'] = failed_tests
            out_dict['chips'][chip_id]['FAILED_TESTS'] = failed_tests
            if failed_tests > 0:
                failed_chips += 1
        out_dict['yield'] = float((132 - failed_chips) / 132) * 100
        return out_dict


if __name__ == '__main__':
    from glob import glob
    for folders in glob('/bdaq53/waferprobing/new_analysis/0x17F'):
        in_file = glob(os.path.join(folders, '*.xlsx'))[0]
        example_json_file = 'ITkPixV1_proddb_example.json'
        gen = ITkPixV1ProdDBGenerator(in_file, example_json_file)
        out_file = os.path.join(folders, 'wafer_{0}_probing_results.json'.format(gen.wafer_number))
        out_dict = gen.dict_to_proddb_json()
        with open(out_file, 'w') as f:
            j = json.dumps(out_dict, indent=4)
            print(j, file=f)
