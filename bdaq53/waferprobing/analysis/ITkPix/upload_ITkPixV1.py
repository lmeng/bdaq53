import os
import datetime
import pysftp
import json
from tqdm import tqdm
from glob import glob
from bdaq53.waferprobing.analysis.ITkPix.analyze_wafer_ITkPixV1 import WaferAnalysis, PlotItkPixWafer
from bdaq53.waferprobing.analysis.ITkPix.generate_ITk_JSON_ITkPixV1 import ITkPixV1ProdDBGenerator
from bdaq53.waferprobing.analysis.ITkPix.production_database import WPUploader

'''
This is the main scripts for analyzing raw wafer probing results, generating the helper files for the production
database and uploading raw and analyzed results to the ATLAS project storage as well as to the production database.
For this script it is necessary to define the following environment variables:
To get access to the production database please contact: Monika.Wielers(at)cern.ch
TOKEN1=XXXXXXX
TOKEN2=XXXXXXX
To get access to the ATLAS project space please contact: Timon.Heim(at)cern.ch
CERNUSER=XXXXX
CERNPW=XXXXXXX
(These are your regular CERN single sign on credentials)
'''


def upload_analyzed_data(in_folder, wafer_number):
    with pysftp.Connection('lxplus.cern.ch', username=os.environ.get('CERNUSER'), password=os.environ.get('CERNPW')) as sftp:
        with sftp.cd('/eos/atlas/atlascerngroupdisk/det-itk/general/pixels/front_end'):  # temporarily chdir to public
            if not sftp.exists(wafer_number):
                sftp.mkdir(wafer_number)
            print("Starting upload", datetime.datetime.now())
            sftp.put_r(os.path.join(in_folder, wafer_number), wafer_number)
            print("Done uploading", datetime.datetime.now())


def recursive_sftp_list(in_list, sftp, folder):
    local_paths = sftp.listdir(folder)
    for elem in local_paths:
        in_list.append(folder + '/' + elem)
        if not ('.' in elem):
            recursive_sftp_list(in_list, sftp, folder + '/' + elem)


def upload_raw_data(in_data):
    sftp_files = []
    wafer_number = in_data.split('probing_wafer_')[1].split('/')[0]
    abs_files = glob(in_data + '/*/*')
    files_to_be_transfered = [x.split(os.sep)[-1] for x in abs_files]
    with pysftp.Connection('lxplus.cern.ch', username=os.environ.get('CERNUSER'), password=os.environ.get('CERNPW')) as sftp:
        with sftp.cd('/eos/atlas/atlascerngroupdisk/det-itk/general/pixels/front_end'):
            if not sftp.exists(wafer_number):
                sftp.mkdir(wafer_number)
            if not sftp.exists(wafer_number + '/raw/'):
                sftp.mkdir(wafer_number + '/raw/')
            recursive_sftp_list(sftp_files, sftp, wafer_number)
            to_be_copied = list(set(files_to_be_transfered).difference([x.split(os.sep)[-1] for x in sftp_files]))
            files_to_be_copied = [x for x in to_be_copied if '.' in x]
            for folder_elem in sftp_files:
                if not sftp.exists(folder_elem):
                    sftp.mkdir()
            for elem0 in tqdm(files_to_be_copied):
                for elem1 in abs_files:
                    if elem0 in elem1:
                        chip_number = elem1.split(wafer_number + '/')[1].split('/')[0]
                        if str(wafer_number + '/raw/' + chip_number) not in sftp_files:
                            sftp.mkdir(wafer_number + '/raw/' + chip_number)
                            sftp_files.append(wafer_number + '/raw/' + chip_number)
                        sftp.put(elem1, remotepath=wafer_number + '/raw/' + chip_number + '/' + elem1.split(os.sep)[-1])


def main(IN_PATH, OUT_PATH):
    print("Starting raw data analysis.")
    EXISTING_COMPONENT_MODE = 'skip'  # 'skip', 'delete'
    with WaferAnalysis(IN_PATH, out_dir=OUT_PATH) as wpa:
        PLOT_DATA = wpa.analyze()

    print("Start plotting data from h5 file.")
    plotter = PlotItkPixWafer(summary_h5_filename=PLOT_DATA)
    xlsx_file = plotter.plot()
    example_json_file = 'ITkPixV1_proddb_example.json'
    print("Start generating json file for database upload.")
    gen = ITkPixV1ProdDBGenerator(xlsx_file, example_json_file)
    out_dict = gen.dict_to_proddb_json()
    wafer_no = gen.wafer_number
    out_file = os.path.join(os.sep.join(xlsx_file.split(os.sep)[:-1]), 'wafer_{0}_probing_results.json'.format(gen.wafer_number))

    with open(out_file, 'w') as f:
        j = json.dumps(out_dict, indent=4)
        print(j, file=f)

    print("Uploading raw data to ATLAS project space. This step can take easily 40-60 minutes.")
    upload_raw_data(IN_PATH)

    print("Uploading analyzed data to production database")
    with WPUploader(existing_component_mode=EXISTING_COMPONENT_MODE) as uploader:
        upload_folder = os.sep.join(xlsx_file.split(os.sep)[:-1])
        uploader.load_data(upload_folder)
        uploader.run()

    print("Uploading analyzed data to ATLAS project space")
    upload_analyzed_data(OUT_PATH, wafer_no)


if __name__ == '__main__':
    RAW_DATA_PATH = 'path to a folder ending with /probing_wafer_0x???'
    OUT_DIR = 'path to a folder where all analyzed data files will be stored e.g. /wp_analysis'
    main(RAW_DATA_PATH, OUT_DIR)
