#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script generates statistics over several waferprobing result directories
'''

import os
import numpy as np
import tables as tb

from matplotlib.figure import Figure
import matplotlib.patches as mpatches
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

input_path = ''
set_name = 'all'   # e.g. 'LOT1'
TITLE_COLOR = '#07529a'


def plot_1d_hist(hist, cuts, results, filename, multiplier=1.0, plot_range=None, n_bins=75, log_y=False, title=None, x_axis_title=None, y_axis_title=None, color='#07529a', yellow_statistics=False):
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)

    hist = np.array([h * multiplier for h in hist])

    if plot_range is None:
        plot_range = (np.amin(hist), np.amax(hist))

    ax.hist(hist, bins=np.linspace(plot_range[0], plot_range[1], num=n_bins))

    ax.set_xlim(plot_range)

    ax.set_title(title, color=TITLE_COLOR)
    if x_axis_title is not None:
        ax.set_xlabel(x_axis_title)
    if y_axis_title is not None:
        ax.set_ylabel(y_axis_title)
    if log_y:
        ax.set_yscale('log')
        ax.set_ylim((1e-1, len(hist) * 2))
    ax.grid(True)

    if cuts is not None:
        handles = []
        highest, lowest = plot_range[0], plot_range[1]
        for color in cuts.keys():
            for i in range(len(cuts[color])):
                (lower, upper) = cuts[color][i]
                lower = lower * multiplier
                upper = upper * multiplier
                ax.axvspan(lower, upper, alpha=0.25, color=color, zorder=0)
                handles.append(mpatches.Patch(color=color, alpha=0.3, label=str(cuts[color][i][0]) + ' to ' + str(cuts[color][i][1])))
                if lower < lowest:
                    lowest = lower
                if upper > highest:
                    highest = upper

        ax.axvspan(plot_range[0], lowest, alpha=0.25, color='red', zorder=0)
        ax.axvspan(highest, plot_range[1], alpha=0.25, color='red', zorder=0)

        ax.legend(handles=handles, loc='upper left', fontsize=7)

    green = results.count('green')
    yellow = results.count('yellow')
    red = results.count('red')
    total = green + yellow + red

    green_rel = green / total * 100.
    yellow_rel = yellow / total * 100.
    red_rel = red / total * 100.

    text = '%i chips total\n\n%1.2f%% in green\n%1.2f%% in yellow\n%1.2f%% in red' % (total, green_rel, yellow_rel, red_rel)

    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(0.8, 0.9, text, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

    # Detailed statistics
    if yellow_statistics:
        if len(cuts['yellow']) == 2:
            sel = np.logical_and((hist >= cuts['yellow'][0][0] * multiplier), (hist <= cuts['yellow'][1][1] * multiplier))
        else:
            sel = hist <= cuts['yellow'][0][1] * multiplier
        text = 'Green and yellow chips:\n'
        x_coordinate = 0.715
    else:
        sel = np.logical_and((hist >= cuts['green'][0][0] * multiplier), (hist <= cuts['green'][0][1] * multiplier))
        text = 'Green chips:\n'
        x_coordinate = 0.8
    data = hist[sel]
    text += '$\mu = {0:1.5f}$\n$\sigma = {1:1.5f}$'.format(np.nanmean(data), np.nanstd(data))

    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(x_coordinate, 0.7, text, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

    output_dir = os.path.join(input_path, 'output')
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    outfile = os.path.join(output_dir, filename + '.pdf')
    fig.savefig(outfile)


if __name__ == '__main__':
    tests = []
    data = {}
    data['tests'], data['cuts'] = {}, {}

    n_file = 0
    for f in os.listdir(input_path):
        if f.split('.')[-1] != 'h5':
            continue
        print(f)
        n_file += 1
        wafer = 'wafer_' + f.split('_')[1]
        with tb.open_file(os.path.join(input_path, f), 'r') as in_file:

            for test in in_file.root.tests._f_walknodes():
                if n_file == 1:
                    data['tests'][test.name] = {}
                for chip, result, value in test[:]:
                    if str(chip)[-3:-1] in ['23', 'A9']:    # Ignore always broken chips
                        continue
                    data['tests'][test.name][str(chip)[2:-1]] = {}
                    data['tests'][test.name][str(chip)[2:-1]]['value'] = float(value)
                    data['tests'][test.name][str(chip)[2:-1]]['result'] = result.decode('utf-8')

            for test in in_file.root.cuts._f_walknodes():
                name = '_'.join(test.name.split('_')[:-1])
                if n_file == 1:
                    data['cuts'][name] = {}
                for rang, result in test[:]:
                    data['cuts'][name][str(result)[2:-1]] = list(eval(rang.decode('utf-8')))

    for test in data['tests'].keys():
        hist = [value['value'] for value in data['tests'][test].values()]
        results = [value['result'] for value in data['tests'][test].values()]
        cuts = data['cuts'][test]

        plot_range = None
        multiplier = 1.0
        if test == 'ThresholdScan':
            plot_range = (0, 2000)
        if test == 'IREF':
            plot_range = (0, 8)
            multiplier = 1e6

        log_y = True

        plot_1d_hist(hist, cuts, results, filename=set_name + '_' + test, multiplier=multiplier, log_y=log_y, plot_range=plot_range, title=test, y_axis_title='Entries', x_axis_title='Result')
