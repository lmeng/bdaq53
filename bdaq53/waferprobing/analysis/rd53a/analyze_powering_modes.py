import os
import tables as tb
import numpy as np

from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.ticker import MaxNLocator


RESULT_FILES = ['/mnt/RD53_Data/Wafer 113/Run 2/probing_wafer_113_results.h5',
                '/mnt/RD53_Data/Wafer 112/Run 2/probing_wafer_112_results.h5', ]

OUT_DIR = '/home/michael/cernbox/Projects/Waferprobing/special_statistics'


TITLE_COLOR = '#07529a'
OVERTEXT_COLOR = '#07529a'
OUT_OF_RANGE_COLOR = 'magenta'


def extract_global_results(f):
    global DATA

    for row in f.root.global_results[:]:
        if len(row) != 3:
            continue

        chip_sn = row[0].decode('utf-8')
        if chip_sn not in DATA.keys():
            DATA[chip_sn] = {}
        DATA[chip_sn]['global_result'] = row[1].decode('utf-8')


def extract_trimbits(f):
    global DATA

    for row in f.root.trims[:]:
        trim_data = {}
        if row['IREF_LDO'] != 0 and row['IREF_SHUNT'] != 0 and row['IREF_LDO'] != 15 and row['IREF_SHUNT'] != 15:
            trim_data['IREF'] = {}
            trim_data['IREF']['LDO'] = row['IREF_LDO']
            trim_data['IREF']['SHUNT'] = row['IREF_SHUNT']
        if row['VREF_A_LDO'] != 0 and row['VREF_A_SHUNT'] != 0 and row['VREF_A_LDO'] != 31 and row['VREF_A_SHUNT'] != 31:
            trim_data['VREF_A'] = {}
            trim_data['VREF_A']['LDO'] = row['VREF_A_LDO']
            trim_data['VREF_A']['SHUNT'] = row['VREF_A_SHUNT']
        if row['VREF_D_LDO'] != 0 and row['VREF_D_SHUNT'] != 0 and row['VREF_D_LDO'] != 31 and row['VREF_D_SHUNT'] != 31:
            trim_data['VREF_D'] = {}
            trim_data['VREF_D']['LDO'] = row['VREF_D_LDO']
            trim_data['VREF_D']['SHUNT'] = row['VREF_D_SHUNT']
        if row['VREF_ADC_LDO'] != 0 and row['VREF_ADC_SHUNT'] != 0 and row['VREF_ADC_LDO'] != 31 and row['VREF_ADC_SHUNT'] != 31:
            trim_data['VREF_ADC'] = {}
            trim_data['VREF_ADC']['LDO'] = row['VREF_ADC_LDO']
            trim_data['VREF_ADC']['SHUNT'] = row['VREF_ADC_SHUNT']

        if len(trim_data.keys()) > 0:
            chip_sn = row[0].decode('utf-8')
            if chip_sn not in DATA.keys():
                DATA[row[0].decode('utf-8')] = {}
            DATA[row[0].decode('utf-8')]['trims'] = trim_data


def analyze_trimbits(ignore_red=False):
    global DATA

    diffs = {'IREF': [],
             'VREF_A': [],
             'VREF_D': [],
             'VREF_ADC': []}

    for chip in DATA.values():
        if 'trims' not in chip.keys():
            continue
        if ignore_red and chip['global_result'] == 'red':
            continue
        data = chip['trims']
        for key in diffs.keys():
            try:
                diff = data[key]['LDO'] - data[key]['SHUNT']
                diffs[key].append(diff)
            except KeyError:
                pass

    for key in diffs.keys():
        fig = plot_1d_hist(diffs[key], title='Difference in {0}_TRIM (LDO - SHUNT)'.format(key), x_axis_title='Difference [Trimbits]', y_axis_title='# of chips')
        fig.savefig(os.path.join(OUT_DIR, '{0}_TRIM_diffs.pdf'.format(key)))


def plot_1d_hist(hist, multiplier=1.0, plot_range=None, n_bins='match', log_y=None, int_x_tics=False, title=None, x_axis_title=None, y_axis_title=None, color='#07529a', fig_pos=None):
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)

    hist = np.array([h * multiplier for h in hist])

    text = '{0} entries\nMean = {1:1.2f}\nMedian = {2:1.2f}'.format(len(hist), np.mean(hist), np.median(hist))
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    ax.text(0.05, 0.88, text, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

    if plot_range is None:
        plot_range = (np.amin(hist), np.amax(hist))

    ax.set_xlim(plot_range)
    if n_bins == 'match':
        n_bins = int(plot_range[1] - plot_range[0]) + 1

    ax.hist(hist, bins=np.arange(n_bins) - 0.5, align='mid')

    if int_x_tics:
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))

    ax.set_title(title, fontsize=12, color=TITLE_COLOR)

    if x_axis_title is not None:
        ax.set_xlabel(x_axis_title)
    if y_axis_title is not None:
        ax.set_ylabel(y_axis_title)
    if log_y:
        ax.set_yscale('log')
        ax.set_ylim((1e-1, len(hist) * 2))
    ax.grid(True)

    return fig


if __name__ == '__main__':
    DATA = {}

    for path in RESULT_FILES:
        with tb.open_file(path, 'r') as f:
            extract_global_results(f)
            extract_trimbits(f)

    analyze_trimbits(ignore_red=True)
