#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script generates special statistical plots for many wafers. E.g. untrimmed VDD values, etc.
'''

import os
import yaml
import logging
import coloredlogs
import fnmatch
import numpy as np
import tables as tb
from datetime import datetime

from scipy.optimize import curve_fit
from scipy.stats import pearsonr

from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib import colors, cm, colorbar
import matplotlib.pyplot as plt

INPUT_PATH = '/mnt/RD53_Data'
OUT_PATH = '/home/michael/Desktop/special_statistics'
WAFER_WHITELIST = [112, 113, 114]

target_VDDA = 1.2
target_VDDD = 1.2
target_VREF_ADC = 0.9
target_IREF = 4.0e-6

TITLE_COLOR = '#07529a'

coloredlogs.DEFAULT_FIELD_STYLES = {'asctime': {},
                                    'hostname': {},
                                    'levelname': {'bold': True},
                                    'name': {},
                                    'programname': {}}
coloredlogs.DEFAULT_LEVEL_STYLES = {'critical': {'color': 'red', 'bold': True},
                                    'debug': {'color': 'magenta'},
                                    'error': {'color': 'red', 'bold': True},
                                    'info': {},
                                    'success': {'color': 'green'},
                                    'warning': {'color': 'yellow'}}

coloredlogs.install(fmt="%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s", milliseconds=True)


def linear(x, m, b):
    return m * x + b


def IV_fit(data, start_value=0.8, stop_value=0.95):
    x, VIN_A, VIN_D = [], [], []
    for key, meas in data.items():
        x.append(key)
        VIN_A.append(meas['VIN_A'])
        VIN_D.append(meas['VIN_D'])

    x = np.array(x)
    VIN_A = np.array(VIN_A)
    VIN_D = np.array(VIN_D)
    (m_A, b_A), _ = curve_fit(linear, x[(x >= start_value) & (x <= stop_value)], VIN_A[np.where((x >= start_value) & (x <= stop_value))])
    (m_D, b_D), _ = curve_fit(linear, x[(x >= start_value) & (x <= stop_value)], VIN_D[np.where((x >= start_value) & (x <= stop_value))])

    return (float(m_A), float(b_A)), (float(m_D), float(b_D))


def evaluate(data, cuts):
    color = 'red'
    for c, ranges in cuts.items():
        for (lower, upper) in ranges:
            if data >= lower and data < upper:
                color = c
    return color


def gather_data(dir, wafer_sn):
    logging.info('Gathering data for ' + dir)
    global code_version

    if code_version == 2018:
        for root, _, _ in os.walk(dir):
            if 'probing_wafer' in root:
                analog_dir = root
                wafer_id = os.path.basename(root).split('_')[2]
                # load cuts from result h5 file
                cuts = {}
                with tb.open_file(os.path.join(os.path.dirname(root), 'wafer_{0}_probing_results.h5'.format(wafer_id)), 'r') as rf:
                    for test in rf.root.cuts._f_walknodes():
                        cuts['_'.join(test.name.split('_')[:-1])] = {}
                        for rang, result in test[:]:
                            cuts['_'.join(test.name.split('_')[:-1])][str(result)[2:-1]] = list(eval(rang.decode('utf-8')))
                break
        else:
            raise RuntimeError('No analog data directory found in specified data directory!')

        chips = {}

        for analog_file in os.listdir(analog_dir):
            if analog_file.split('.')[1] == 'yaml':
                chip_sn = analog_file.split('.')[0].split('_')[2]
                chip = {}

                with open(os.path.join(analog_dir, analog_file), 'r') as in_file:
                    analog_results = yaml.safe_load(in_file)

                try:
                    iv_curve = analog_results['IV curves']

                    chip['link'] = analog_results['Initial Link']['Init counter']

                    chip['IREF'] = analog_results['IREF Trims'][8]['IREF']
                    chip['VDDD'] = analog_results['VREF_D Trim'][16]['VDDD']
                    chip['VDDA'] = analog_results['VREF_A Trim'][16]['VDDA']
                    chip['VREF_D'] = analog_results['VREF_D Trim'][16]['VREF_D']
                    chip['VREF_A'] = analog_results['VREF_A Trim'][16]['VREF_A']
                    chip['VREF_ADC'] = analog_results['VREF_ADC Trim'][0]['VREF_ADC_OUT']

                    diffs_a, diffs_d, diffs_adc = {}, {}, {}
                    for trimbit in range(0, 31):
                        diffs_a[trimbit] = abs(analog_results['VREF_A Trim'][trimbit]['VDDA'] - target_VDDA)
                        diffs_d[trimbit] = abs(analog_results['VREF_D Trim'][trimbit]['VDDD'] - target_VDDD)
                        diffs_adc[trimbit] = abs(analog_results['VREF_ADC Trim'][trimbit]['VREF_ADC_OUT'] - target_VREF_ADC)

                    chip['VDDA_TRIM'] = min(diffs_a, key=diffs_a.get)
                    chip['VDDD_TRIM'] = min(diffs_d, key=diffs_d.get)
                    chip['VREF_ADC_TRIM'] = min(diffs_adc, key=diffs_adc.get)

                    chip['trimmed_VDDA'] = analog_results['VREF_A Trim'][chip['VDDA_TRIM']]['VDDA']
                    chip['trimmed_VDDD'] = analog_results['VREF_D Trim'][chip['VDDD_TRIM']]['VDDD']
                except KeyError as e:
                    logging.warning('Chip %s is missing some analog data: %s' % (chip_sn, e))
                    continue

                chips[chip_sn] = chip

                # fit IV curve
                (m_A, b_A), (m_D, b_D) = IV_fit(iv_curve, start_value=0.85)
                chip['Slope_A'] = m_A
                chip['Offset_A'] = b_A
                chip['Slope_D'] = m_D
                chip['Offset_D'] = b_D

            elif analog_file.split('.')[1] == 'log':
                times = extract_times(os.path.join(analog_dir, analog_file))

    elif code_version == 2020:
        cuts = {}
        with tb.open_file(os.path.join(dir, 'probing_wafer_{0}_results.h5'.format(wafer_sn)), 'r') as rf:
            for test in rf.root.cuts._f_walknodes():
                cuts['_'.join(test.name.split('_')[:-1])] = {}
                for rang, result in test[:]:
                    cuts['_'.join(test.name.split('_')[:-1])][str(result)[2:-1]] = list(eval(rang.decode('utf-8')))

        chips = {}
        for subdir in os.listdir(dir):
            if '0x' in subdir:    # Chip dir
                chip = {}
                chip_sn = subdir

                with open(os.path.join(dir, os.path.join(subdir, 'analog_data_{0}.yaml'.format(chip_sn))), 'r') as in_file:
                    analog_results = yaml.safe_load(in_file)

                try:
                    iv_curve = analog_results['IV curves']

                    chip['link'] = analog_results['Initial Link']['Init counter']

                    chip['IREF'] = analog_results['IREF Trims (LDO)'][8]['IREF']
                    chip['VDDD'] = analog_results['VREF_D Trim (LDO)'][16]['VDDD']
                    chip['VDDA'] = analog_results['VREF_A Trim (LDO)'][16]['VDDA']
                    chip['VREF_D'] = analog_results['VREF_D Trim (LDO)'][16]['VREF_D']
                    chip['VREF_A'] = analog_results['VREF_A Trim (LDO)'][16]['VREF_A']
                    chip['VREF_ADC'] = analog_results['VREF_ADC Trim (LDO)'][0]['VREF_ADC_OUT']

                    diffs_a, diffs_d, diffs_adc = {}, {}, {}
                    for trimbit in range(0, 31):
                        diffs_a[trimbit] = abs(analog_results['VREF_A Trim (LDO)'][trimbit]['VDDA'] - target_VDDA)
                        diffs_d[trimbit] = abs(analog_results['VREF_D Trim (LDO)'][trimbit]['VDDD'] - target_VDDD)
                        diffs_adc[trimbit] = abs(analog_results['VREF_ADC Trim (LDO)'][trimbit]['VREF_ADC_OUT'] - target_VREF_ADC)

                    chip['VDDA_TRIM'] = min(diffs_a, key=diffs_a.get)
                    chip['VDDD_TRIM'] = min(diffs_d, key=diffs_d.get)
                    chip['VREF_ADC_TRIM'] = min(diffs_adc, key=diffs_adc.get)

                    chip['trimmed_VDDA'] = analog_results['VREF_A Trim (LDO)'][chip['VDDA_TRIM']]['VDDA']
                    chip['trimmed_VDDD'] = analog_results['VREF_D Trim (LDO)'][chip['VDDD_TRIM']]['VDDD']
                except KeyError as e:
                    logging.warning('Chip %s is missing some analog data: %s' % (chip_sn, e))
                    continue

                # fit IV curve
                (m_A, b_A), (m_D, b_D) = IV_fit(iv_curve, start_value=0.85)
                chip['Slope_A'] = m_A
                chip['Offset_A'] = b_A
                chip['Slope_D'] = m_D
                chip['Offset_D'] = b_D

                chips[chip_sn] = chip

            elif subdir == 'probing_wafer_{0}.log'.format(wafer_sn):
                times = extract_times(os.path.join(dir, subdir))

    else:
        raise AttributeError('Code version {0} not implemented!'.format(code_version))

    return chips, cuts, times


def plot_1d_hist(data, key, filename, multiplier=1, plot_range=None, n_bins=75, log_y=False, title=None, x_axis_title=None, y_axis_title=None, color='#07529a'):
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)

    hist = [e[key] for e in data.values()]
    hist = np.array([h * multiplier for h in hist])

    if plot_range is None:
        plot_range = (np.amin(hist), np.amax(hist))

    ax.hist(hist, bins=np.linspace(plot_range[0], plot_range[1], num=n_bins))

    ax.set_xlim(plot_range)

    ax.set_title(title, color=TITLE_COLOR)
    if x_axis_title is not None:
        ax.set_xlabel(x_axis_title)
    if y_axis_title is not None:
        ax.set_ylabel(y_axis_title)
    if log_y:
        ax.set_yscale('log')
        ax.set_ylim((1e-1, len(hist) * 2))
    ax.grid(True)

    outliers = 0
    for c in hist:
        if c < min(plot_range) or c > max(plot_range):
            outliers += 1

    text = '{0} Wafers\n{1} Chips\n{2} out of range'.format(n_wafers, n_chips, outliers)
    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(0.05, 0.9, text, transform=ax.transAxes,
            fontsize=8, verticalalignment='top', bbox=props)

    # Detailed statistics
    text = 'All chips:\n'
    x_coordinate = 0.8
    text += '$\mu = {0:1.5f}$\n$\sigma = {1:1.5f}$'.format(np.nanmean(hist), np.nanstd(hist))

    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(x_coordinate, 0.7, text, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

    outfile = os.path.join(OUT_PATH, filename)
    fig.savefig(outfile)


def plot_waferhist(all_chips, key, title, filename, multiplier=1, colorscale_title=''):
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)

    data = {}
    for chip_sn, chip in all_chips.items():
        coordinates = chip_sn[-2:]
        if coordinates not in data.keys():
            data[coordinates] = []
        data[coordinates].append(chip[key] * multiplier)

    hist = {}
    for coordinates, dat in data.items():
        hist[coordinates] = np.median(dat)

    ax.axis('off')
    ax.set_title(title, fontsize=12, color=TITLE_COLOR)

    empty_cells = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 6), (0, 7), (0, 8), (0, 9),
                   (1, 0), (1, 1), (1, 2), (1, 8), (1, 9),
                   (2, 0), (2, 9),
                   (3, 0), (3, 9),
                   (4, 0),
                   (8, 0),
                   (9, 0), (9, 9),
                   (10, 0), (10, 1), (10, 8), (10, 9),
                   (11, 0), (11, 1), (11, 2), (11, 7), (11, 8), (11, 9)]

    tb_list = [['' for _ in range(10)] for _ in range(12)]
    widths = [0.09 for _ in range(10)]
    table = ax.table(cellText=tb_list, colWidths=widths, edges='closed', cellLoc='center', loc='center')

    cmap = cm.get_cmap('plasma')
    norm = colors.Normalize(vmin=min(hist.values()), vmax=max(hist.values()))

    for k, cell in table.get_celld().items():
        row, col = k
        chip_sn = (str(hex(col + 1)[2:]) + str(hex(row + 1)[2:])).upper()

        if k in empty_cells:
            cell.set_facecolor('white')
            cell.set_edgecolor('white')
            cell._text.set_color('white')
            continue

        cell.set_height(0.08)
        cell._text.set_fontsize(4)

        celltext = str(col + 1) + '-' + str(row + 1) + '\n'
        try:
            if data is not None:
                celltext += str(hist[chip_sn])
            cell.set_facecolor(cmap(norm(hist[chip_sn])))
        except KeyError:
            celltext += 'No Data'
            cell.set_facecolor('red')
        cell._text.set_text(celltext)

    text = '{0} Wafers\n{1} Chips'.format(n_wafers, n_chips)
    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(0, 1, text, transform=ax.transAxes,
            fontsize=8, verticalalignment='top', bbox=props)

    ax2 = fig.add_axes([0.88, 0.1, 0.02, 0.8])
    cb = colorbar.ColorbarBase(ax2, cmap=cmap, norm=norm, orientation='vertical')
    cb.set_label(colorscale_title)

    outfile = os.path.join(OUT_PATH, filename)
    fig.savefig(outfile)


def plot_time_pie(all_times, n_wafers, n_chips):
    total_chip_times = []           # in hours
    total_wafer_times = []          # in minutes
    iv_curve_times = []             # in seconds
    iref_trim_times = []            # in seconds
    init_times = []                 # in seconds
    vref_trim_times = []            # in seconds
    vref_adc_trim_times = []        # in seconds
    analog_measurements_times = []  # in seconds
    nocc_scan_times = []            # in seconds
    digital_scan_times = []         # in seconds
    analog_scan_times = []          # in seconds
    threshold_scan_times = []       # in seconds
    register_scan_times = []        # in seconds
    tuning_times = []

    for times in all_times:
        try:
            total_chip_times.append(times['total_chip_times'])
        except KeyError:
            pass
        try:
            total_wafer_times.append(times['total_wafer_times'])
        except KeyError:
            pass
        try:
            iv_curve_times.append(times['iv_curve_times'])
        except KeyError:
            pass
        try:
            iref_trim_times.append(times['iref_trim_times'])
        except KeyError:
            pass
        try:
            init_times.append(times['init_times'])
        except KeyError:
            pass
        try:
            vref_trim_times.append(times['vref_trim_times'])
        except KeyError:
            pass
        try:
            vref_adc_trim_times.append(times['vref_adc_trim_times'])
        except KeyError:
            pass
        try:
            analog_measurements_times.append(times['analog_measurements_times'])
        except KeyError:
            pass
        try:
            nocc_scan_times.append(times['nocc_scan_times'])
        except KeyError:
            pass
        try:
            digital_scan_times.append(times['digital_scan_times'])
        except KeyError:
            pass
        try:
            analog_scan_times.append(times['analog_scan_times'])
        except KeyError:
            pass
        try:
            threshold_scan_times.append(times['threshold_scan_times'])
        except KeyError:
            pass
        try:
            register_scan_times.append(times['register_scan_times'])
        except KeyError:
            pass
        try:
            tuning_times.append(times['tuning_times'])
        except KeyError:
            pass

    code_version = 2020 if len(tuning_times) > 0 else 2018

    if code_version == 2018:
        arr = [np.mean(iv_curve_times),
               np.mean(iref_trim_times),
               np.mean(init_times),
               np.mean(vref_trim_times),
               np.mean(vref_adc_trim_times),
               np.mean(analog_measurements_times),
               np.mean(nocc_scan_times),
               np.mean(digital_scan_times),
               np.mean(analog_scan_times),
               np.mean(threshold_scan_times),
               np.mean(register_scan_times)]
        labels = ['IV Curves', 'IREF Trim', 'AURORA Sync', 'VREF Trim', 'VREF_ADC Trim', 'Analog Measurements', 'Noise Occupancy Scan', 'Digital Scan', 'Analog Scan', 'Threshold Scan', 'Register Test']
        explode = [0.05, 0, 0, 0.05, 0, 0, 0, 0, 0, 0, 0]

    elif code_version == 2020:
        arr = [np.mean(iv_curve_times),
               np.mean(iref_trim_times),
               np.mean(init_times),
               np.mean(vref_trim_times),
               np.mean(vref_adc_trim_times),
               np.mean(analog_measurements_times),
               np.mean(register_scan_times),
               np.mean(digital_scan_times),
               np.mean(analog_scan_times),
               np.mean(tuning_times),
               np.mean(threshold_scan_times)]
        labels = ['IV Curves', 'IREF Trim', 'AURORA Sync', 'VREF Trim', 'VREF_ADC Trim', 'Analog Measurements', 'Register Test', 'Digital Scan', 'Analog Scan', 'Threshold Tuning', 'Threshold Scan']
        explode = [0.05, 0, 0, 0.05, 0, 0, 0, 0, 0, 0, 0]

    else:
        raise AttributeError('Code version {0} not implemented!'.format(code_version))

    def lbl(pct, allvals):
        absolute = int(pct / 100. * np.sum(allvals))
        return "{:.0f}%\n({:d}s)".format(pct, absolute)

    cs = cm.tab20c(np.arange(len(arr)) / len(arr))
    plt.pie(arr, explode=explode, labels=labels, autopct=lambda pct: lbl(pct, arr), pctdistance=0.8, startangle=90, counterclock=False, colors=cs, textprops={'size': 'x-small'})
    plt.title('Time per scan step')
    txt = 'Total number of\n..wafers: {0}\n..chips: {1}\n'.format(n_wafers, n_chips)
    txt += '\nMean time spent\n..per wafer: {0: 1.2f} h\n..per chip: {1: 1.2f} min'.format(np.mean(total_wafer_times), np.mean(total_chip_times))
    plt.text(-2, 0.9, txt, size='small', bbox={'facecolor': 'grey', 'alpha': 0.3})

    outfile = os.path.join(OUT_PATH, 'times.pdf')
    plt.savefig(outfile)


def plot_correlation(data, data1_key, data2_key, data1_cuts=None, data2_cuts=None, plot_range=None, symmetric=False, data1_title=None, data2_title=None, filename=None):
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)

    if data1_title is None:
        data1_title = data1_key
    if data2_title is None:
        data2_title = data2_key
    title = 'Correlation of {0} and {1}'.format(data2_title, data1_title)

    ax.set_title(title, color=TITLE_COLOR)
    ax.set_xlabel(data1_title)
    ax.set_ylabel(data2_title)

    combined_data, data1, data2 = [], [], []
    if data1_cuts is not None:
        combined_data_green, combined_data_yellow, combined_data_red = [], [], []
        data1_green, data2_green = [], []
        data1_yellow, data2_yellow = [], []
        data1_red, data2_red = [], []
    for chip in data.values():
        combined_data.append((chip[data1_key], chip[data2_key]))
        data1.append(chip[data1_key])
        data2.append(chip[data2_key])

        if data1_cuts is not None:
            color1 = evaluate(chip[data1_key], data1_cuts)
            color2 = evaluate(chip[data2_key], data2_cuts)
            if color1 == 'red' or color2 == 'red':
                combined_data_red.append((chip[data1_key], chip[data2_key]))
                data1_red.append(chip[data1_key])
                data2_red.append(chip[data2_key])
            elif color1 == 'yellow' or color2 == 'yellow':
                combined_data_yellow.append((chip[data1_key], chip[data2_key]))
                data1_yellow.append(chip[data1_key])
                data2_yellow.append(chip[data2_key])
            else:
                combined_data_green.append((chip[data1_key], chip[data2_key]))
                data1_green.append(chip[data1_key])
                data2_green.append(chip[data2_key])

    if plot_range is None:
        if symmetric:
            plot_range_x = (min(min(data1), min(data2)), max(max(data1), max(data2)))
            plot_range_y = plot_range_x
        else:
            plot_range_x = (min(data1), max(data1))
            plot_range_y = (min(data2), max(data2))

        ax.set_xlim(plot_range_x)
        ax.set_ylim(plot_range_y)

    ax.grid(True)

    if data1_cuts is None:
        ax.plot(data1, data2, '.')
    else:
        ax.plot(data1_green, data2_green, 'g.')
        ax.plot(data1_yellow, data2_yellow, 'y.')
        ax.plot(data1_red, data2_red, 'r.')

    r, _ = pearsonr(np.array(data1), np.array(data2))
    if data1_cuts is not None:
        r_yellow, _ = pearsonr(np.array(data1_green + data1_yellow), np.array(data2_green + data2_yellow))
        r_green, _ = pearsonr(np.array(data1_green), np.array(data2_green))

    outliers = 0
    for c in data1:
        if c < min(plot_range_x) or c > max(plot_range_x):
            outliers += 1
    for c in data2:
        if c < min(plot_range_y) or c > max(plot_range_y):
            outliers += 1

    if data1_cuts is None:
        text = '{0} Wafers\n{1} Chips\n{2} out of range'.format(n_wafers, n_chips, outliers)
    else:
        text = '{0} Wafers\n{1} Chips\n{2} out of range\n\n{3} green\n{4} yellow\n{5} red'.format(n_wafers, n_chips, outliers, len(combined_data_green), len(combined_data_yellow), len(combined_data_red))
    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(0.05, 0.9, text, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

    if data1_cuts is None:
        text = 'Correlation:\n$\\rho ={0:1.5f}$'.format(r)
    else:
        text = 'Correlation:\nAll chips:\n$\\rho ={0:1.5f}$\nGreen and yellow chips:\n$\\rho ={1:1.5f}$\nOnly green chips:\n$\\rho ={2:1.5f}$'.format(r, r_yellow, r_green)
    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(0.05, 0.6, text, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)

    if filename is None:
        filename = 'all_chips_{0}_vs_{1}_correlation.pdf'.format(data2_title, data1_title)
    outfile = os.path.join(OUT_PATH, filename)
    fig.savefig(outfile)


# Extract scan time information from logfiles
def extract_times(f):
    times = {}
    global code_version
    with open(f, 'r') as logfile:
        init_running = False
        scan_running = False
        current_scan = None
        for line in logfile:
            try:
                timestamp, module, message = line.split(' - ')
                module = module[1:-1].strip()
                ts = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S,%f')
            except ValueError:
                continue

            if 'SUCCESS Done! Probing this chip took' in message:
                for word in message.split(' '):
                    try:
                        times['total_chip_times'] = float(word)
                    except Exception:
                        continue

            if 'SUCCESS All done! Probing this wafer took' in message:
                for word in message.split(' '):
                    try:
                        times['total_wafer_times'] = float(word)
                    except Exception:
                        continue

            if 'INFO    Start probing chip' in message:  # New chip
                init_running = False
                current_scan = None
                scan_running = False

            if 'INFO    Start regulator IV measurement routine...' in message:
                iv_start_ts = ts
            if 'SUCCESS Optimal regulator working point' in message or 'SUCCESS IV curve measurement done!' in message:
                times['iv_curve_times'] = (ts - iv_start_ts).total_seconds()

            if 'INFO    Start IREF trimming routine...' in message:
                iref_trim_ts = ts
            if 'SUCCESS Optimal IREF TRIM is' in message or 'SUCCESS Optimal IREF_TRIM is' in message:
                times['iref_trim_times'] = (ts - iref_trim_ts).total_seconds()

            if ('INFO    Initializing communication with chip...' in message or 'INFO    Start communication test routine...' in message) and not init_running:
                init_ts = ts
                init_running = True
            if 'INFO    Start VREF trimming routine...' in message and init_running:
                times['init_times'] = (ts - init_ts).total_seconds()
                init_running = False

            if 'INFO    Start VREF trimming routine...' in message:
                vref_trim_ts = ts
            if 'SUCCESS Optimal VREFA_TRIM is' in message:
                times['vref_trim_times'] = (ts - vref_trim_ts).total_seconds()

            if 'INFO    Start VREF_ADC trimming routine...' in message:
                vref_adc_trim_ts = ts
            if 'SUCCESS Optimal MON_BG_TRIM is' in message:
                times['vref_adc_trim_times'] = (ts - vref_adc_trim_ts).total_seconds()

            if 'INFO    Start analog measurement routine...' in message or 'INFO    Start external MUX measurement routine...':
                analog_measurements_ts = ts
            if 'INFO    Start injection DAC calibration routine...' in message:
                times['analog_measurements_times'] = (ts - analog_measurements_ts).total_seconds() + 3.5

            # TODO: ADC trimming times

            if code_version == 2018:
                if current_scan == 'nocc' and not scan_running and 'INFO    Periphery is disabled.' in message:
                    nocc_ts = ts
                    scan_running = True
                elif current_scan == 'nocc' and scan_running and 'INFO    Periphery is disabled.' in message:
                    times['nocc_scan_times'] = (ts - nocc_ts).total_seconds()
                    current_scan = 'digital'
                    digital_ts = ts
                elif current_scan == 'digital' and scan_running and 'INFO    Periphery is disabled.' in message:
                    times['digital_scan_times'] = (ts - digital_ts).total_seconds()
                    current_scan = 'analog'
                    analog_ts = ts
                elif current_scan == 'analog' and scan_running and 'INFO    Periphery is disabled.' in message:
                    times['analog_scan_times'] = (ts - analog_ts).total_seconds()
                    current_scan = 'threshold'
                    threshold_ts = ts
                elif current_scan == 'threshold' and scan_running and 'INFO    Periphery is disabled.' in message:
                    times['threshold_scan_times'] = (ts - threshold_ts).total_seconds()
                    current_scan = 'register'
                    register_ts = ts
                elif current_scan == 'register' and scan_running and 'SUCCESS Done! Probing this chip took' in message:
                    times['register_scan_times'] = (ts - register_ts).total_seconds()
                    current_scan = 'nocc'
                    scan_running = False
            elif code_version == 2020:
                if current_scan is None and scan_running is False and 'INFO    Initializing RegisterTest...' in message:
                    current_scan = 'register'
                    register_ts = ts
                    scan_running = True
                elif current_scan == 'register' and scan_running and 'INFO    Initializing DigitalScan...' in message:
                    times['register_scan_times'] = (ts - register_ts).total_seconds()
                    current_scan = 'digital'
                    digital_ts = ts
                elif current_scan == 'digital' and scan_running and 'INFO    Initializing AnalogScan...' in message:
                    times['digital_scan_times'] = (ts - digital_ts).total_seconds()
                    current_scan = 'analog'
                    analog_ts = ts
                elif current_scan == 'analog' and scan_running and 'INFO    Initializing GDACTuning...' in message:
                    times['analog_scan_times'] = (ts - analog_ts).total_seconds()
                    current_scan = 'tuning'
                    tuning_ts = ts
                elif current_scan == 'tuning' and scan_running and 'INFO    Initializing ThresholdScan...' in message:
                    times['tuning_times'] = (ts - tuning_ts).total_seconds()
                    current_scan = 'threshold'
                    threshold_ts = ts
                elif current_scan == 'threshold' and scan_running and 'SUCCESS Done! Probing this chip took' in message:
                    times['threshold_scan_times'] = (ts - threshold_ts).total_seconds()
                    current_scan = None
                    scan_running = False

    return times


if __name__ == '__main__':
    all_chips = {}
    all_times = []
    n_wafers = 0
    n_chips = 0

    for waferdir in os.listdir(INPUT_PATH):
        if not os.path.isdir(os.path.join(INPUT_PATH, waferdir)):
            continue

        wafer_sn = int(waferdir.replace('Wafer ', ''))
        code_version = 2020 if wafer_sn >= 112 else 2018

        if WAFER_WHITELIST is not None:
            if wafer_sn not in WAFER_WHITELIST:
                continue

        try:
            dirs = os.listdir(os.path.join(INPUT_PATH, waferdir))
            if 'Run 1' in dirs:
                logging.info(waferdir + ' has multiple runs')
                data, cuts, times = gather_data(os.path.join(os.path.join(INPUT_PATH, waferdir), max(dirs)), wafer_sn)
                all_chips.update(data)
                all_times.append(times)
            elif fnmatch.filter(dirs, 'probing_wafer_*'):
                logging.info(waferdir + ' is a waferprobing dir')
                data, cuts, times = gather_data(os.path.join(INPUT_PATH, waferdir), wafer_sn)
                all_chips.update(data)
                all_times.append(times)
            else:
                continue
        except RuntimeError:
            continue
        n_wafers += 1

    with open(os.path.join(OUT_PATH, 'gathered_data.yaml'), 'w') as of:
        data = {'all_chips': all_chips,
                'cuts': cuts,
                'times': all_times}
        yaml.dump(data, of)

    # with open(os.path.join(OUT_PATH, 'gathered_data.yaml'), 'r') as inf:
    #     data = yaml.load(inf)
    #     all_chips = data['all_chips']
    #     cuts = data['cuts']
    #     all_times = data['times']
    #     n_wafers = 58

    n_chips = len(all_chips.keys())

    plot_1d_hist(data=all_chips, key='IREF', multiplier=1e6, plot_range=(3.5, 5), title='Default IREF values', x_axis_title='IREF [uA]', y_axis_title='#', filename='all_chips_default_IREF_histogram.pdf')
    plot_waferhist(all_chips, key='IREF', title='Default IREF values (median)', multiplier=1e6, colorscale_title='IREF [uA]', filename='all_chips_default_IREF_waferhist.pdf')
    plot_1d_hist(data=all_chips, key='VDDD', plot_range=(1.0, 1.3), title='Default VDDD values', x_axis_title='VDDD [V]', y_axis_title='#', filename='all_chips_default_VDDD_histogram.pdf')
    plot_waferhist(all_chips, key='VDDD', title='Default VDDD values (median)', colorscale_title='VDDD [V]', filename='all_chips_default_VDDD_waferhist.pdf')
    plot_1d_hist(data=all_chips, key='VDDA', plot_range=(1.0, 1.3), title='Default VDDA values', x_axis_title='VDDA [V]', y_axis_title='#', filename='all_chips_default_VDDA_histogram.pdf')
    plot_waferhist(all_chips, key='VDDA', title='Default VDDA values (median)', colorscale_title='VDDA [V]', filename='all_chips_default_VDDA_waferhist.pdf')
    plot_1d_hist(data=all_chips, key='VREF_D', plot_range=(0, 0.8), title='Default VREF_D values', x_axis_title='VREF_D [V]', y_axis_title='#', filename='all_chips_default_VREF_D_histogram.pdf')
    plot_waferhist(all_chips, key='VREF_D', title='Default VREF_D values (median)', colorscale_title='VREF_D [V]', filename='all_chips_default_VREF_D_waferhist.pdf')
    plot_1d_hist(data=all_chips, key='VREF_A', plot_range=(0, 0.8), title='Default VREF_A values', x_axis_title='VREF_A [V]', y_axis_title='#', filename='all_chips_default_VREF_A_histogram.pdf')
    plot_waferhist(all_chips, key='VREF_A', title='Default VREF_A values (median)', colorscale_title='VREF_A [V]', filename='all_chips_default_VREF_A_waferhist.pdf')
    plot_1d_hist(data=all_chips, key='VREF_ADC', plot_range=(0.8, 1.0), title='Default VREF_ADC values', x_axis_title='VREF_ADC [V]', y_axis_title='#', filename='all_chips_default_IVREF_ADC_histogram.pdf')
    plot_waferhist(all_chips, key='VREF_ADC', title='Default VREF_ADC values (median)', colorscale_title='VREF_ADC [V]', filename='all_chips_default_VREF_ADC_waferhist.pdf')
    plot_1d_hist(all_chips, key='VDDA_TRIM', plot_range=(15, 32), title='Optimal VREF_A trimbit setting', x_axis_title='Optimal trimbit setting', y_axis_title='#', filename='all_chips_optimal_VREF_A_trimbit_histogram.pdf')
    plot_1d_hist(all_chips, key='VDDD_TRIM', plot_range=(15, 32), title='Optimal VREF_D trimbit setting', x_axis_title='Optimal trimbit setting', y_axis_title='#', filename='all_chips_optimal_VREF_D_trimbit_histogram.pdf')
    plot_1d_hist(all_chips, key='VREF_ADC_TRIM', title='Optimal VREF_ADC trimbit setting', x_axis_title='Optimal trimbit setting', y_axis_title='#', filename='all_chips_optimal_VREF_ADC_trimbit_histogram.pdf')
    plot_1d_hist(all_chips, key='Slope_A', log_y=True, title='Analog regulator slope', x_axis_title='Slope [Ohm]', y_axis_title='#', filename='all_chips_analog_slope_histogram.pdf')
    plot_1d_hist(all_chips, key='Offset_A', log_y=True, title='Analog regulator offset', x_axis_title='Offset [mV]', y_axis_title='#', filename='all_chips_analog_offset_histogram.pdf')
    plot_1d_hist(all_chips, key='Slope_D', log_y=True, title='Digital regulator slope', x_axis_title='Slope', y_axis_title='#', filename='all_chips_digital_slope_histogram.pdf')
    plot_1d_hist(all_chips, key='Offset_D', log_y=True, title='Digital regulator offset', x_axis_title='Offset [mV]', y_axis_title='#', filename='all_chips_digital_offset_histogram.pdf')

    plot_time_pie(all_times, n_wafers, n_chips)

    plot_correlation(data=all_chips, data1_key='trimmed_VDDA', data2_key='trimmed_VDDD', data1_cuts=cuts['VDDA'], data2_cuts=cuts['VDDD'], symmetric=True)
    plot_correlation(data=all_chips, data1_key='VDDA', data2_key='VDDD', symmetric=True)
    plot_correlation(data=all_chips, data1_key='Slope_A', data2_key='Slope_D', data1_cuts=cuts['IV_curve_A_slope'], data2_cuts=cuts['IV_curve_D_slope'], symmetric=True)
    plot_correlation(data=all_chips, data1_key='Offset_A', data2_key='Offset_D', data1_cuts=cuts['IV_curve_A_offset'], data2_cuts=cuts['IV_curve_D_offset'], symmetric=True)
    plot_correlation(data=all_chips, data1_key='Slope_A', data2_key='Offset_A', data1_cuts=cuts['IV_curve_A_slope'], data2_cuts=cuts['IV_curve_A_offset'], symmetric=False)
    plot_correlation(data=all_chips, data1_key='Slope_D', data2_key='Offset_D', data1_cuts=cuts['IV_curve_D_slope'], data2_cuts=cuts['IV_curve_D_offset'], symmetric=False)
    plot_correlation(data=all_chips, data1_key='VREF_A', data2_key='VDDA', symmetric=True)
    plot_correlation(data=all_chips, data1_key='VREF_D', data2_key='VDDD', symmetric=True)
    plot_correlation(data=all_chips, data1_key='VDDA_TRIM', data2_key='trimmed_VDDA', symmetric=False)
    plot_correlation(data=all_chips, data1_key='VDDD_TRIM', data2_key='trimmed_VDDD', symmetric=False)

    plot_correlation(data=all_chips, data1_key='link', data2_key='VDDA', symmetric=False)
    plot_correlation(data=all_chips, data1_key='link', data2_key='trimmed_VDDA', symmetric=False)

    print('Available Keys:')
    keys = []
    for c in all_chips.values():
        for k in c.keys():
            if k not in keys:
                keys.append(k)
                print(k)
