#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script generates a wafermap in IZM format
'''

import os
import tables as tb

infile = ''
lot_id = 'N25A67'

with tb.open_file(infile, 'r') as f:
    data = f.root.global_results[:]

chips = {}
n_green, n_yellow, n_red = 0, 0, 0
for chip in data:
    chip_sn = chip[0].decode('utf-8')
    wafer_id = int(chip_sn[2:4], 16)
    col = int(chip_sn[4:5], 16)
    row = int(chip_sn[5:6], 16)
    result = chip[1].decode('utf-8')
    chips[(col, row)] = result
    if result == 'green':
        n_green += 1
    if result == 'yellow':
        n_yellow += 1
    if result == 'red':
        n_red += 1

if (n_green + n_yellow + n_red) != 89:
    raise ValueError('Total amount of results is not 89')

meta_lookup = {'green': '1', 'yellow': '0', 'red': 'X'}

# # Notch right
# notch = 'Right'
# rows = 12
# cols = 10
# map = 'Map Start{\n'
# for row in range(1, 13):
#     for col in range(1, 11):
#         try:
#             map += meta_lookup[chips[(col, row)]]
#         except KeyError:
#             map += '.'
#     map += '\n'
# map += 'Map End}'

# Notch down
notch = 'Down'
rows = 10
cols = 12
map = 'Map Start{\n'
for row in range(1, 11):
    for col in range(12, 0, -1):
        try:
            map += meta_lookup[chips[(row, col)]]
        except KeyError:
            map += '.'
    map += '\n'
map += 'Map End}'


filename = 'wafer_' + str(wafer_id) + '_IZM_wafermap.txt'
outfile = os.path.join(os.path.dirname(infile), filename)


meta = """SST
Device = "RD53A Wafer"
Lot ID = "%s"
Wafer ID = %i
BIN = "." = "Nicht vorhanden"
BIN = "0" = "yellow" = %i
BIN = "1" = "green" = %i
BIN = "X" = "red" = %i
Notch = %s
ROWs = %i
COLs = %i
File Name = "%s"
""" % (lot_id, wafer_id, n_yellow, n_green, n_red, notch, rows, cols, filename)

print(meta + "\n\n")
print(map)

with open(outfile, 'w') as f:
    f.writelines(meta)
    f.writelines(map)
