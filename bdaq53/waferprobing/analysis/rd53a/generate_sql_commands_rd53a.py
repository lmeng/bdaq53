#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script generates an SQL command file to export waferprobing data to the RD53 database
'''

import tables as tb
from os import path

in_file = ''
update = False      # Wether to use UPDATE or INSERT commands
thickness = 750
location = 'BN'
mon_adc_trim = 5    # Default value since ADC is not yet trimmed in waferprobing


codes = {
    'red': 10,
    'yellow': 1,
    'green': 2
}

with tb.open_file(in_file, 'r') as f:
    chips = f.root.global_results[:]
    vcal_high_slope_table = f.root.tests.VCAL_HIGH_slope[:]
    vcal_high_offset_table = f.root.tests.VCAL_HIGH_offset[:]
    trims = f.root.trims[:]
    configuration = f.root.configuration[:]

wafer_no = int(chips[0][0].decode('utf-8')[2:4], 16)
out_file = path.join(path.dirname(in_file), 'wafer_{0}_sql_commands.txt'.format(wafer_no))

for param, val in configuration:
    parameter = param.decode('utf-8')
    value = val.decode('utf-8')
    if parameter == 'Test date':
        test_date = value

with open(out_file, 'w') as f:
    for chip in chips:
        chip_sn = chip[0].decode('utf-8')
        col = int(chip_sn[4:5], 16)
        row = int(chip_sn[5:6], 16)
        result = chip[1].decode('utf-8')
        result_code = codes[result]
        for r in vcal_high_slope_table:
            if r['Chip_SN'].decode('utf-8') == chip_sn:
                vcal_high_slope = round(float(r['Value'].decode('utf-8')), 15)

        for r in vcal_high_offset_table:
            if r['Chip_SN'].decode('utf-8') == chip_sn:
                vcal_high_offset = round(float(r['Value'].decode('utf-8')), 15)

        for r in trims:
            if r['Chip'].decode('utf-8') == chip_sn:
                iref_trim = int(r['IREF'])
                vref_a_trim = int(r['VREF_A'])
                vref_d_trim = int(r['VREF_D'])
                mon_bg_trim = int(r['VREF_ADC'])

        if update:  # Use UPDATE commands per value
            data = {'IREF_TRIM': iref_trim,
                    'VREF_A_TRIM': vref_a_trim,
                    'VREF_D_TRIM': vref_d_trim,
                    'MON_BG_TRIM': mon_bg_trim,
                    'MON_ADC_TRIM': mon_adc_trim,
                    'VCAL_SLOPE': vcal_high_slope,
                    'VCAL_OFFSET': vcal_high_offset}
            for key, var in data.items():
                if type(var) == float:
                    command = "UPDATE CHIP SET {0}={1:1.15f} WHERE SN='{chip_sn}';".format(key, var, chip_sn=chip_sn)
                else:
                    command = "UPDATE CHIP SET {0}={1} WHERE SN='{chip_sn}';".format(key, var, chip_sn=chip_sn)
                f.write(command + '\n')
            f.write('\n')
        else:   # Use INSERT commands per chip
            command = "INSERT INTO CHIP (SN, wafer,col,row,thickness,status,location," \
                      "IREF_TRIM,VREF_A_TRIM,VREF_D_TRIM,MON_BG_TRIM,MON_ADC_TRIM,VCAL_SLOPE,VCAL_OFFSET) " \
                      "VALUES('{chip_sn}',{wafer_no},{col},{row},{thickness},'${result_code}@{date}$','${location}@{date}$'," \
                      "{IREF_TRIM},{VREF_A_TRIM},{VREF_D_TRIM},{MON_BG_TRIM},{MON_ADC_TRIM},{VCAL_SLOPE},{VCAL_OFFSET});".format(chip_sn=chip_sn,
                                                                                                                                 wafer_no=wafer_no,
                                                                                                                                 col=col,
                                                                                                                                 row=row,
                                                                                                                                 thickness=thickness,
                                                                                                                                 result_code=result_code,
                                                                                                                                 date=test_date,
                                                                                                                                 location=location,
                                                                                                                                 IREF_TRIM=iref_trim,
                                                                                                                                 VREF_A_TRIM=vref_a_trim,
                                                                                                                                 VREF_D_TRIM=vref_d_trim,
                                                                                                                                 MON_BG_TRIM=mon_bg_trim,
                                                                                                                                 MON_ADC_TRIM=mon_adc_trim,
                                                                                                                                 VCAL_SLOPE=vcal_high_slope,
                                                                                                                                 VCAL_OFFSET=vcal_high_offset)
            f.write(command + '\n')
