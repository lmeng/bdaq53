#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.
'''

import tables as tb
from bdaq53.system.scan_base import ScanBase


scan_configuration = {
    'pulse_width': 8
}


class OscTable(tb.IsDescription):
    osc_name = tb.StringCol(64, pos=0)
    raw_data = tb.UInt32Col(pos=1)
    counter = tb.UInt32Col(pos=2)
    frequency = tb.Float32Col(pos=3)
    pulse_length = tb.UInt32Col(pos=4)


class OSCScan(ScanBase):
    scan_id = 'ring_osc_scan'

    def _scan(self, pulse_width=30, **_):
        ring_osc = self.chip.get_ring_oscillators(pulse_width=pulse_width)

        self.data.dac_data_table = self.h5_file.create_table(self.h5_file.root, name='osc_data', title='osc_data', description=OscTable)
        row = self.data.dac_data_table.row
        for ring_osc_name, ring_osc_data in ring_osc.items():
            row['osc_name'] = ring_osc_name
            row['raw_data'] = ring_osc_data['raw_data']
            row['counter'] = ring_osc_data['counter']
            row['frequency'] = ring_osc_data['frequency']
            row['pulse_length'] = pulse_width
            row.append()
            self.data.dac_data_table.flush()


if __name__ == "__main__":
    with OSCScan(scan_config=scan_configuration) as scan:
        scan.start()
