import numpy as np
import tables as tb
from tqdm import tqdm


def load_spf(in_filename):
    with open(in_filename) as fp:
        all_lines = fp.read()
    split1 = all_lines.split('"multiclock_capture"')
    all_multiclock_capture_in = []
    all_multiclock_capture_in_pulse = []
    all_multiclock_capture_out = []
    all_multiclock_capture_out_mask = []
    chain_len = 0
    inter_str_0_all = []
    inter_str_1_all = []
    sc_out0_data_all = []
    sc_out1_data_all = []
    sc_out0_mask_all = []
    sc_out1_mask_all = []
    all_scan_in0 = []
    all_scan_in1 = []
    cnt = 0
    for elem in tqdm(split1[2:]):
        multiclock_capture_in = elem.split('"_pi"=')[1].split(';')[0]
        mc_in_data, mc_in_pulse = out_str_to_num(multiclock_capture_in)
        print(multiclock_capture_in, mc_in_data, mc_in_pulse)
        if '"_po"=' in elem:
            multiclock_capture_out = elem.split('"_po"=')[1].split(';')[0]
            mc_out_data, mc_out_mask = out_str_to_num(multiclock_capture_out)
        else:
            mc_out_mask = '00'
            mc_out_data = '00'
        cnt += 1
        if "SCAN_OUT_" in elem:
            SCAN_OUT_0 = (elem.split('"SCAN_OUT_0"=')[1].split(';')[0].replace('\n', ''))  # [::-1]
            SCAN_OUT_1 = (elem.split('"SCAN_OUT_1"=')[1].split(';')[0].replace('\n', ''))  # [::-1]
            sc_out0_data, sc_out0_mask = out_str_to_num(SCAN_OUT_0)
            sc_out1_data, sc_out1_mask = out_str_to_num(SCAN_OUT_1)
            sc_out0_data = spf_str_to_bytearray(sc_out0_data)
            sc_out0_mask = spf_str_to_bytearray(sc_out0_mask)
            sc_out1_data = spf_str_to_bytearray(sc_out1_data)
            sc_out1_mask = spf_str_to_bytearray(sc_out1_mask)
        else:
            sc_out0_data = [0] * len(sc_out0_data_all[-1])
            sc_out0_mask = [0] * len(sc_out0_data_all[-1])
            sc_out1_data = [0] * len(sc_out1_data_all[-1])
            sc_out1_mask = [0] * len(sc_out1_data_all[-1])
        if "SCAN_IN_" in elem:
            SCAN_IN_0 = (elem.split('"SCAN_IN_0"=')[1].split(';')[0].replace('\n', ''))  # [::-1]
            SCAN_IN_1 = (elem.split('"SCAN_IN_1"=')[1].split(';')[0].replace('\n', ''))  # [::-1]
            chain_len = len(SCAN_IN_0)
            scan_in_0_list = spf_str_to_bytearray(SCAN_IN_0)
            scan_in_1_list = spf_str_to_bytearray(SCAN_IN_1)
        else:
            scan_in_0_list = [0] * len(all_scan_in0[-1])
            scan_in_1_list = [0] * len(all_scan_in1[-1])
        sc_out0_data_all.append(sc_out0_data)
        sc_out1_data_all.append(sc_out1_data)
        sc_out0_mask_all.append(sc_out0_mask)
        sc_out1_mask_all.append(sc_out1_mask)
        inter_str_0_all.append("h")
        inter_str_1_all.append("h")
        all_scan_in0.append(scan_in_0_list)
        all_scan_in1.append(scan_in_1_list)
        all_multiclock_capture_in.append(mc_in_data)
        all_multiclock_capture_out.append(mc_out_data)
        all_multiclock_capture_in_pulse.append(mc_in_pulse)
        all_multiclock_capture_out_mask.append(mc_out_mask)
    return all_scan_in0, all_scan_in1, all_multiclock_capture_in, chain_len, inter_str_0_all, inter_str_1_all, sc_out0_data_all, sc_out0_mask_all, sc_out1_data_all, sc_out1_mask_all, all_multiclock_capture_in, all_multiclock_capture_in_pulse, all_multiclock_capture_out, all_multiclock_capture_out_mask


def spf_arrays_to_h5(in_filename):
    all_scan_in0, all_scan_in1, all_multiclock_capture_in, chain_len, inter_str_0_all, inter_str_1_all, sc_out0_data_all, sc_out0_mask_all, sc_out1_data_all, sc_out1_mask_all, all_multiclock_capture_in, all_multiclock_capture_in_pulse, all_multiclock_capture_out, all_multiclock_capture_out_mask = load_spf(
        in_filename)

    sc_in0 = np.array(all_scan_in0, dtype=np.uint8)
    sc_in1 = np.array(all_scan_in1, dtype=np.uint8)
    sc_out0_data_all = np.array(sc_out0_data_all, dtype=np.uint8)
    sc_out1_data_all = np.array(sc_out1_data_all, dtype=np.uint8)
    sc_out0_mask_all = np.array(sc_out0_mask_all, dtype=np.uint8)
    sc_out1_mask_all = np.array(sc_out1_mask_all, dtype=np.uint8)

    all_multiclock_capture_in = np.array([int(n, 2) for n in all_multiclock_capture_in], dtype=np.uint16)
    all_multiclock_capture_in_pulse = np.array([int(n, 2) for n in all_multiclock_capture_in_pulse], dtype=np.uint8)
    all_multiclock_capture_out = np.array([int(n, 2) for n in all_multiclock_capture_out], dtype=np.uint8)
    all_multiclock_capture_out_mask = np.array([int(n, 2) for n in all_multiclock_capture_out_mask], dtype=np.uint8)
    print(in_filename[:-4])
    with tb.open_file(in_filename[:-4] + '_0end.h5', 'w') as out_file:
        out_file.create_carray(out_file.root, name='SC_IN0', title='SC_IN0', obj=sc_in0,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        out_file.root.SC_IN0.attrs.chain_len = chain_len
        out_file.create_carray(out_file.root, name='SC_IN1', title='SC_IN1', obj=sc_in1,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        out_file.create_carray(out_file.root, name='SC_OUT0', title='SC_OUT0', obj=sc_out0_data_all,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        out_file.create_carray(out_file.root, name='SC_OUT1', title='SC_OUT1', obj=sc_out1_data_all,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        out_file.create_carray(out_file.root, name='SC_OUT_MASK0', title='SC_OUT_MASK0', obj=sc_out0_mask_all,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        out_file.create_carray(out_file.root, name='SC_OUT_MASK1', title='SC_OUT_MASK1', obj=sc_out1_mask_all,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        out_file.create_carray(out_file.root, name='MC_IN', title='MC_IN', obj=all_multiclock_capture_in,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        out_file.create_carray(out_file.root, name='MC_IN_P', title='MC_IN_P', obj=all_multiclock_capture_in_pulse,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        out_file.create_carray(out_file.root, name='MC_OUT', title='MC_OUT', obj=all_multiclock_capture_out,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        out_file.create_carray(out_file.root, name='MC_OUT_MASK', title='MC_OUT_MASK',
                               obj=all_multiclock_capture_out_mask,
                               filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))


def out_str_to_num(instr):
    out_str = ''
    mask = ''
    in_out_dict = {'L': '0', 'H': '1', 'X': '0'}
    for elem in instr:
        if elem == 'L' or elem == 'H':
            out_str += in_out_dict[elem]
            mask += '1'
        elif elem == '1':
            out_str += '1'
            mask += '0'
        elif elem == 'P':
            out_str += '0'
            mask += '1'
        else:
            out_str += '0'
            mask += '0'
    return out_str, mask


def spf_str_to_bytearray(SCAN_IN_0):
    scan_in_0_list = []
    inter_str_0 = ''
    for scan_in_cnt in range(0, len(SCAN_IN_0), 8):
        scan_in_0_list.append(int(SCAN_IN_0[scan_in_cnt:scan_in_cnt + 8], 2))
        inter_str_0 += SCAN_IN_0[scan_in_cnt:scan_in_cnt + 8]
    return scan_in_0_list


if __name__ == '__main__':
    spf_file = '/faust/user/mstandke/Downloads/rd53b_patterns.spf'
    spf_arrays_to_h5(spf_file)
