#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic "scan" writes the efuses to the ITkPixV1s
'''

from bdaq53.system.scan_base import ScanBase


scan_configuration = {
    'pulse_width': 8
}


class WriteEfuse(ScanBase):
    scan_id = 'write_efuse'

    def _scan(self, pulse_width=8, **_):

        def calcRedundantBits(m):
            for i in range(m):
                if (2 ** i >= m + i + 1):
                    return i

        def posRedundantBits(data_in):
            data = bin(data_in)[2:]
            r = calcRedundantBits(len(data))
            j = 0
            k = 1
            m = len(data)
            res = ''
            for i in range(1, m + r + 1):
                if (i == 2 ** j):
                    res = res + '0'
                    j += 1
                else:
                    res = res + data[-1 * k]
                    k += 1
            return res[::-1]

        def calcParityBits(data_in):
            data = bin(data_in)[2:]
            arr = posRedundantBits(data_in)
            r = calcRedundantBits(len(data))
            n = len(arr)
            arr_out = ''
            for i in range(r):
                val = 0
                for j in range(1, n + 1):
                    if (j & (2 ** i) == (2 ** i)):
                        val = val ^ int(arr[-1 * j])
                arr_out += str(val)
            return int(arr_out, 2)

        def read_efuses():
            self.chip.registers['EfusesConfig'].write(0x0f0f)
            self.chip.registers['EfusesReadData0'].read()
            self.chip.registers['EfusesReadData1'].read()
            self.chip.send_global_pulse(bitnames=['reset_efuses'], pulse_width=0xff)
            efuse0 = self.chip.registers['EfusesReadData0'].read()
            efuse1 = self.chip.registers['EfusesReadData1'].read()
            return (efuse1 << 16) + efuse0

        def write_efuses(value):
            self.chip.registers['EfusesWriteData0'].write(value & 0xffff)
            self.chip.registers['EfusesWriteData1'].write(value >> 16)
            self.chip.registers['EfusesConfig'].write(0xf0f0)
            self.chip.send_global_pulse(bitnames=['reset_efuses'], pulse_width=0xff)
            self.chip.send_global_pulse(bitnames=['start_efuses_programmer'], pulse_width=21)

        data = int(self.chip_settings['chip_sn'], 16)
        probelocation = 1  # Code for Bonn
        data = data + (probelocation << 20)
        print("hi", hex(data))
        arr = calcParityBits(data)
        print("hoho", hex(arr))
        data = (data << 8) + arr
        print("fin", hex(data))
        print(hex(data & 0xffff))
        print(hex(data >> 16))
        self.bdaq.needle_card.write_gpio_expander('EN_VDD_EFUSE', 0)
        print("efuses", hex(read_efuses()))
        # write_efuses(data)
        self.bdaq.needle_card.write_gpio_expander('EN_VDD_EFUSE', 1)
        print("efuses", hex(read_efuses()))
        self.bdaq.needle_card.write_gpio_expander('EN_VDD_EFUSE', 0)


if __name__ == "__main__":
    with WriteEfuse(scan_config=scan_configuration) as scan:
        scan.start()
