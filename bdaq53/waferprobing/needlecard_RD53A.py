#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Implementation of the RD53A needlecard I2C interface
'''

import logging
import time

from basil.HL.GPAC import GpioPca9554

loglevel = logging.INFO


class NC_GPIO(GpioPca9554):
    ''' GPIO expander '''

    def __init__(self, intf, conf, NC_CHIP_ADDR=0x00, NC_GPIO_CFG=0x00):
        super(NC_GPIO, self).__init__(intf, conf)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
        self.PCA9554_ADD = NC_CHIP_ADDR
        self.GPIO_CFG = NC_GPIO_CFG

    def _modify_register(self, mask, value):
        # read
        reg_read = self._read_output_port()
        self.logger.debug('register read: %s' % format(reg_read, '#010b'))
        # modify
        reg_mod = (reg_read & ~mask) | (value & mask)
        self.logger.debug('register modify: %s' % format(reg_mod, '#010b'))
        # write
        self._write_output_port(reg_mod)

    def _read_port(self, mask):
        return self._read_input_port() & mask


class Needlecard(GpioPca9554):
    _adc_mux_map = {
        'VINA': 0x0,
        'VIND': 0x1,
        'VDDA': 0x2,
        'VDDD': 0x3,
        'GND': 0x4,
        'IMUX_OUT': 0x5,
        'VMUX_OUT': 0x6,
        'IREF_OUT': 0x7,
        'IREF_IN': 0x8,
        'VREF_ADC_OUT': 0x9,
        'VREF_ADC_IN': 0xa,
        'VINJ_HI': 0xc,
        'VINJ_MID': 0xd,
        'SLDO_VREFA': 0xe,
        'SLDO_VREFD': 0xf
    }

    _gpio_expander_map = {
        # Register name: [chip no, lsb offset, bits]
        # GPIO expander 0 @ I2C address 0x20 (0x40)
        'ES': [0x00, 0, 2],               # 0b00000011
        'EN_VDD_SHUNT': [0x00, 2, 1],     # 0b00000100
        'IREF_EN': [0x00, 3, 1],          # 0b00001000
        'MUX_SEL': [0x00, 4, 4],          # 0b11110000
        # GPIO expander 1 @ I2C address 0x21 (0x42)
        'CHIP_ID': [0x01, 0, 3],          # 0b00000111
        'EXT_POR_CAP': [0x01, 3, 1],      # 0b00001000
        # GPIO expander @ I2C address 0x22 (0x44)
        'IREF_TRIM': [0x02, 0, 4],        # 0b00001111
        'PLL_VCTRL_RST_B': [0x02, 4, 1],  # 0b00010000
        'SLDO_COMP_EN_B': [0x02, 5, 1],   # 0b00100000
        'SLDO_POR_BG': [0x02, 6, 1]       # 0b01000000
    }

    def __init__(self, intf, conf={'name': 'GpioPca9554', 'type': 'GpioPca9554', 'interface': 'intf', 'base_addr': 0x40}):
        super().__init__(intf, conf)
        self._base_addr = conf['base_addr']

        self.logger = logging.getLogger('NeedleCard')
        self.logger.setLevel(loglevel)

        # GPIO expander 0 @ I2C address 0x20 (0x40)
        self.gpio_0 = NC_GPIO(intf, conf, NC_CHIP_ADDR=0x00, NC_GPIO_CFG=0x03)
        # GPIO expander 1 @ I2C address 0x21 (0x42)
        self.gpio_1 = NC_GPIO(intf, conf, NC_CHIP_ADDR=0x02, NC_GPIO_CFG=0x00)
        # GPIO expander @ I2C address 0x22 (0x44)
        self.gpio_2 = NC_GPIO(intf, conf, NC_CHIP_ADDR=0x04, NC_GPIO_CFG=0x00)

    def init(self):
        self.gpio_0.init()
        self.gpio_1.init()
        self.gpio_2.init()

        self.set_adc_mux('GND')
        self.write_gpio_expander('IREF_TRIM', 7)
        self.write_gpio_expander('IREF_EN', 1)
        self.write_gpio_expander('EN_VDD_SHUNT', 0)

    def _calculate_mask(self, regname):
        mask = (pow(2, self._gpio_expander_map[regname][2]) - 1) << self._gpio_expander_map[regname][1]
        return mask

    def write_gpio_expander(self, regname, value):
        time.sleep(0.1)
        val = (value << self._gpio_expander_map[regname][1])
        mask = self._calculate_mask(regname)
        self.logger.debug('write_gpio_expander: mask: %s' % format(mask, '#010b'))
        self.logger.debug('write_gpio_expander: value: %s, shifted value: %s (%s)' % (hex(value), hex(val), format(val, '#010b')))

        # select the chip
        which_gpio_chip = self._gpio_expander_map[regname][0]
        if which_gpio_chip == 0:
            self.gpio_0._modify_register(mask, val)
        elif which_gpio_chip == 1:
            self.gpio_1._modify_register(mask, val)
        elif which_gpio_chip == 2:
            self.gpio_2._modify_register(mask, val)
        else:
            self.logger.error('Invalid GPIO expander chip selected')
        time.sleep(0.1)

    def read_gpio_expander(self, regname):
        # select the chip
        which_gpio_chip = self._gpio_expander_map[regname][0]
        mask = self._calculate_mask(regname)
        if which_gpio_chip == 0:
            value = self.gpio_0._read_port(mask)
        elif which_gpio_chip == 1:
            value = self.gpio_1._read_port(mask)
        elif which_gpio_chip == 2:
            value = self.gpio_2._read_port(mask)
        else:
            self.logger.error('Invalid GPIO expander chip selected')
        self.logger.debug('read_gpio_expander: mask: %s' % format(mask, '#010b'))
        self.logger.debug('read_gpio_expander: value: %s' % format(value, '#010b'))

        # realign bits
        value = value >> self._gpio_expander_map[regname][1]
        self.logger.debug('read_gpio_expander: value: %s (%s)' % (hex(value), format(value, '#010b')))
        return value

    def set_adc_mux(self, value):
        hexvalue = self._adc_mux_map[value]
        self.write_gpio_expander('MUX_SEL', hexvalue)
        self.logger.debug('ADC MUX set to: %s' % value + " (" + hex(hexvalue) + ")")

    def get_edge_sensors(self):
        value = self.read_gpio_expander('ES')
        es1 = bool(int('{0:02b}'.format(value)[1]))
        es2 = bool(int('{0:02b}'.format(value)[0]))

        return (es1, es2)
