#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Implementation of the RD53B needlecard I2C interface
'''

import logging
import time
import openpyxl
from openpyxl.utils import get_column_letter
from array import array
from struct import pack, unpack_from

from basil.HL.GPAC import GpioPca9554
from basil.HL.FEI4AdapterCard import Eeprom24Lc128

loglevel = logging.INFO


class NC_GPIO(GpioPca9554, Eeprom24Lc128):
    ''' GPIO expander '''

    def __init__(self, intf, conf, NC_CHIP_ADDR=0x00, NC_GPIO_CFG=0x00, CAL_EEPROM_ADD=0x00):
        super(NC_GPIO, self).__init__(intf, conf)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
        self.PCA9554_ADD = NC_CHIP_ADDR
        self.GPIO_CFG = NC_GPIO_CFG
        self.CAL_EEPROM_ADD = CAL_EEPROM_ADD

    def _modify_register(self, mask, value):
        # read
        reg_read = self._read_output_port()
        self.logger.debug('register read: %s' % format(reg_read, '#010b'))
        # modify
        reg_mod = (reg_read & ~mask) | (value & mask)
        self.logger.debug('register modify: %s' % format(reg_mod, '#010b'))
        # write
        self._write_output_port(reg_mod)

    def _modify_register_open_drain(self, mask, value):
        ''' Read and write the registers configured as open drain '''

        # read
        self._intf.write(self._base_addr + self.PCA9554_ADD, array('B', pack('B', self.PCA9554_CFG)))
        reg_read = unpack_from('B', self._intf.read(self._base_addr + self.PCA9554_ADD | 1, size=1))[0]
        self.logger.debug('register read: %s' % format(reg_read, '#010b'))

        # modify
        reg_mod = (reg_read & ~mask) | (value & mask)
        self.logger.debug('register modify: %s' % format(reg_mod, '#010b'))

        # write
        self._write_output_port_select(reg_mod)

    def _read_port(self, mask):
        return self._read_input_port() & mask

    def _write_quad_byte_eeprom(self, address, data_double_byte):
        address_high = (address & 0x1f00) >> 8
        address_low = (address & 0xff)
        data0 = data_double_byte & 0x000000ff
        data1 = (data_double_byte & 0x0000ff00) >> 8
        data2 = (data_double_byte & 0x00ff0000) >> 16
        data3 = (data_double_byte & 0xff000000) >> 24
        self._intf.write(self._base_addr + self.CAL_EEPROM_ADD | 1, array('B', pack('BBBBBB', address_high, address_low, data0, data1, data2, data3)))  # 14-bit address, 16384 bytes


class Needlecard(GpioPca9554):
    _adc_mux_map = {
        'VINA': 0x00,
        'VIND': 0x01,
        'VDDA': 0x02,
        'VDDD': 0x03,
        'VDD_CML': 0x04,
        'VDD_PLL': 0x05,
        'VDD_PRE': 0x06,
        'IMUX_OUT': 0x07,
        'VMUX_OUT': 0x08,
        'R_IREF': 0x09,
        'NTC': 0x0a,
        'VREF_ADC': 0x0b,
        'VOFS': 0x0c,
        'VOFS_LP': 0x0d,
        'REXTA': 0x0e,
        'REXTD': 0x0f,
        'VREFA': 0x10,
        'VREFD': 0x11,
        'VREF_OVP': 0x12,
        'GNDA_REF': 0x13,
        'GNDD_REF': 0x14,
        'GND': 0x15,
        'VDD_EFUSE': 0x16,
        'GNDA_REF2': 0x17,
        'VIN_REXTD': 0x18,
        'VIN_REXTA': 0x19
    }

    _gpio_expander_map = {
        # Register name: [chip no, lsb offset, bits]
        # GPIO expander 0 @ I2C address 0x20 (0x40)
        'EN_R_IREF': [0, 0, 1],        # 0b00000001    RW
        'EN_R_IMUX': [0, 1, 1],        # 0b00000010    RW
        'MUX_SEL': [0, 2, 5],          # 0b01111100    RW
        # GPIO expander 1 @ I2C address 0x21 (0x42)
        'CHIP_ID': [1, 0, 4],          # 0b00001111    RW
        'BYPASS': [1, 4, 1],           # 0b00010000    RW
        'TEST': [1, 5, 1],             # 0b00100000    RW
        'EN_VDD_SHUNT': [1, 6, 1],     # 0b01000000    RW
        'EN_VDD_EFUSE': [1, 7, 1],     # 0b10000000    RW
        # GPIO expander @ I2C address 0x22 (0x44)
        'IREF_TRIM': [2, 0, 4],        # 0b00001111    RW
        'PLL_VCTRL_RST_B': [2, 4, 1],  # 0b00010000    RW
        'P_SCAN_EN': [2, 5, 1],  # 0b00010000    RW
        'ES': [2, 6, 2],               # 0b11000000    RO
    }

    _eeprom_storage_map = {
        # Eeprom @ I2C Default_val start_reg stop_reg (0xa0)
        'PC_VERSION': [0, 0, 4],
        'DATAFORMAT_VERSION': [0, 4, 8],
        'LAST_CHANGE': [0, 8, 12],
        'RIREF': [22600, 12, 16],
        'RIMUX': [5000, 16, 20],
        'RVREF_ADC': [84500, 20, 24],
        'REXTA': [604, 24, 28],
        'REXTD': [604, 28, 32],
        'RVREFA': [30000, 32, 36],
        'RVREFD': [30000, 36, 40],
        'RIOFS': [24900, 40, 44],
        'RIOFS_LP': [10000, 44, 48],
        'PC_ID': [0, 48, 52]
    }

    def __init__(self, intf,
                 conf={'GPIO': {'name': 'GpioPca9554', 'type': 'GpioPca9554', 'interface': 'intf', 'base_addr': 0x40},
                       'EEPROM': {'name': 'Eeprom24Lc128', 'type': 'Eeprom24Lc128', 'interface': 'intf', 'base_addr': 0xa0}}, pc_id=2):
        super().__init__(intf, conf['GPIO'])
        self._base_addr = conf['GPIO']['base_addr']

        self.logger = logging.getLogger('NeedleCard')
        self.logger.setLevel(loglevel)

        # GPIO expander 0 @ I2C address 0x20 (0x40)
        self.gpio_0 = NC_GPIO(intf, conf['GPIO'], NC_CHIP_ADDR=0x00, NC_GPIO_CFG=0x00)
        # GPIO expander 1 @ I2C address 0x21 (0x42)
        self.gpio_1 = NC_GPIO(intf, conf['GPIO'], NC_CHIP_ADDR=0x02, NC_GPIO_CFG=0x00)
        # GPIO expander @ I2C address 0x22 (0x44)
        self.gpio_2 = NC_GPIO(intf, conf['GPIO'], NC_CHIP_ADDR=0x04, NC_GPIO_CFG=0xC0)
        # Eeprom @ I2C address 0xa0 (0xa0)
        try:
            self.eeprom = NC_GPIO(intf, conf['EEPROM'], CAL_EEPROM_ADD=0x00)
            # self.write_config_to_eeprom()
            self.load_eeprom_config()
            self.logger.info('Found probe card with ID: %i ' % self._eeprom_storage_map['PC_ID'][0])
        except OSError:
            self.load_from_xlsx('2021_06_02-probecard_resistances.xlsx', pc_id)
            self.logger.info('Old probe card with version: %i found' % self._eeprom_storage_map['PC_VERSION'][0])
            self.logger.info('Loading Config from xlsx file.')
        self.enable_scan_chain_mode()

    def init(self, fast=True):
        self.gpio_0.init()
        self.gpio_1.init()
        self.gpio_2.init()

        self.write_gpio_expander('EN_R_IREF', 1, fast=fast)
        self.write_gpio_expander('EN_R_IMUX', 0, fast=fast)
        self.set_adc_mux('GNDA_REF', fast=fast)

        self.write_gpio_expander('CHIP_ID', 15, fast=fast)
        self.write_gpio_expander('BYPASS', 0, fast=fast)
        self.write_gpio_expander('TEST', 0, fast=fast)
        self.write_gpio_expander('EN_VDD_SHUNT', 0, fast=fast)
        self.write_gpio_expander('EN_VDD_EFUSE', 0, fast=fast)

        self.write_gpio_expander('IREF_TRIM', 7, fast=fast)
        self.write_gpio_expander('PLL_VCTRL_RST_B', 1, fast=fast)

    def _calculate_mask(self, regname):
        mask = (pow(2, self._gpio_expander_map[regname][2]) - 1) << self._gpio_expander_map[regname][1]
        return mask

    def write_gpio_expander(self, regname, value, fast=False):
        if not fast:
            time.sleep(0.1)
        val = (value << self._gpio_expander_map[regname][1])
        mask = self._calculate_mask(regname)
        self.logger.debug('write_gpio_expander: mask: %s' % format(mask, '#010b'))
        self.logger.debug('write_gpio_expander: value: %s, shifted value: %s (%s)' % (hex(value), hex(val), format(val, '#010b')))

        # select the chip
        which_gpio_chip = self._gpio_expander_map[regname][0]
        if which_gpio_chip == 0:
            self.gpio_0._modify_register(mask, val)
        elif which_gpio_chip == 1:
            self.gpio_1._modify_register(mask, val)
        elif which_gpio_chip == 2:
            self.gpio_2._modify_register(mask, val)
        else:
            self.logger.error('Invalid GPIO expander chip selected')
        if not fast:
            time.sleep(0.1)

    def enable_scan_chain_mode(self):
        self.gpio_1._write_output_port_select(0b00111111)  # open drain chip_id,
        self.gpio_2._write_output_port_select(0b00100000)  # open drain chip_id, 0b00100000

    def disable_scan_chain_mode(self):
        self.gpio_1._write_output_port_select(0)  # open drain chip_id,
        self.gpio_2._write_output_port_select(0)  # open drain chip_id,

    def read_gpio_expander(self, regname):
        # select the chip
        which_gpio_chip = self._gpio_expander_map[regname][0]
        mask = self._calculate_mask(regname)
        if which_gpio_chip == 0:
            value = self.gpio_0._read_port(mask)
        elif which_gpio_chip == 1:
            value = self.gpio_1._read_port(mask)
        elif which_gpio_chip == 2:
            value = self.gpio_2._read_port(mask)
        else:
            self.logger.error('Invalid GPIO expander chip selected')
        self.logger.debug('read_gpio_expander: mask: %s' % format(mask, '#010b'))
        self.logger.debug('read_gpio_expander: value: %s' % format(value, '#010b'))

        # realign bits
        value = value >> self._gpio_expander_map[regname][1]
        self.logger.debug('read_gpio_expander: value: %s (%s)' % (hex(value), format(value, '#010b')))
        return value

    def set_adc_mux(self, value, fast=False):
        hexvalue = self._adc_mux_map[value]
        if (hexvalue > 0x19) and (self._eeprom_storage_map['PC_VERSION'][0] == 0):
            raise NotImplementedError("Probe card V0 has no connection to those pins.")
        self.write_gpio_expander('MUX_SEL', hexvalue, fast=fast)
        self.logger.debug('ADC MUX set to: %s' % value + " (" + hex(hexvalue) + ")")

    def get_edge_sensors(self):
        value = self.read_gpio_expander('ES')
        es1 = bool(int('{0:02b}'.format(value)[1]))
        es2 = bool(int('{0:02b}'.format(value)[0]))

        return (es1, es2)

    def read_eeprom_register(self, eeprom_reg_name):
        eeprom_start_add = self._eeprom_storage_map[eeprom_reg_name][1]
        eeprom_read_len = self._eeprom_storage_map[eeprom_reg_name][2] - self._eeprom_storage_map[eeprom_reg_name][1]
        byte_arr = self.eeprom._read_eeprom(eeprom_start_add, eeprom_read_len)
        return (((byte_arr[3] & 0xff) << 24) + ((byte_arr[2] & 0xff) << 16) + ((byte_arr[1] & 0xff) << 8) + (byte_arr[0] & 0xff))

    def write_config_to_eeprom(self, RIREF, RIMUX, RVREF_ADC, REXTA, REXTD, RVREFA, RVREFD, RIOFS, RIOFS_LP, PC_ID):
        """
        PC_VERSION 10 16-12-2020: Design https://twiki.cern.ch/twiki/pub/RD53/RD53BTesting/RD53B_PC_SCH_1.0.pdf
        PC_VERSION 12 20-04-2021: Design https://twiki.cern.ch/twiki/pub/RD53/RD53BTesting/RD53B_PC_1.2.pdf
        DATAFORMAT_VERSION 1 07-06-2021: https://gitlab.cern.ch/silab/bdaq53/-/commit/84fe4c09e732b85ab71a716370d2bfba34b4f8f8
        """
        sleep_time = 0.1
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['PC_VERSION'][1], 12)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['DATAFORMAT_VERSION'][1], 1)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['LAST_CHANGE'][1], int(time.time()))
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['RIREF'][1], RIREF)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['RIMUX'][1], RIMUX)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['RVREF_ADC'][1], RVREF_ADC)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['REXTA'][1], REXTA)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['REXTD'][1], REXTD)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['RVREFA'][1], RVREFA)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['RVREFD'][1], RVREFD)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['RIOFS'][1], RIOFS)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['RIOFS_LP'][1], RIOFS_LP)
        time.sleep(sleep_time)
        self.eeprom._write_quad_byte_eeprom(self._eeprom_storage_map['PC_ID'][1], PC_ID)

    def load_eeprom_config(self):
        self._eeprom_storage_map['PC_VERSION'][0] = self.read_eeprom_register('PC_VERSION')
        self._eeprom_storage_map['DATAFORMAT_VERSION'][0] = self.read_eeprom_register('DATAFORMAT_VERSION')
        self._eeprom_storage_map['LAST_CHANGE'][0] = self.read_eeprom_register('LAST_CHANGE')
        self._eeprom_storage_map['RIREF'][0] = self.read_eeprom_register('RIREF')
        self._eeprom_storage_map['RIMUX'][0] = self.read_eeprom_register('RIMUX')
        self._eeprom_storage_map['RVREF_ADC'][0] = self.read_eeprom_register('RVREF_ADC')
        self._eeprom_storage_map['REXTA'][0] = self.read_eeprom_register('REXTA')
        self._eeprom_storage_map['REXTD'][0] = self.read_eeprom_register('REXTD')
        self._eeprom_storage_map['RVREFA'][0] = self.read_eeprom_register('RVREFA')
        self._eeprom_storage_map['RVREFD'][0] = self.read_eeprom_register('RVREFD')
        self._eeprom_storage_map['RIOFS'][0] = self.read_eeprom_register('RIOFS')
        self._eeprom_storage_map['RIOFS_LP'][0] = self.read_eeprom_register('RIOFS_LP')
        self._eeprom_storage_map['PC_ID'][0] = self.read_eeprom_register('PC_ID')

    def write_from_xlsx(self, filename, pc_no):
        wb_obj = openpyxl.load_workbook(filename)
        sheet = wb_obj.active
        column_letter = get_column_letter(1 + pc_no)
        RIREF = sheet[column_letter + str(2)].value
        RIMUX = sheet[column_letter + str(3)].value
        RVREF_ADC = sheet[column_letter + str(4)].value
        REXTA = sheet[column_letter + str(6)].value
        REXTD = sheet[column_letter + str(7)].value
        RVREFA = sheet[column_letter + str(8)].value
        RVREFD = sheet[column_letter + str(9)].value
        RIOFS = sheet[column_letter + str(10)].value
        RIOFS_LP = sheet[column_letter + str(11)].value
        self.write_config_to_eeprom(RIREF, RIMUX, RVREF_ADC, REXTA, REXTD, RVREFA, RVREFD, RIOFS, RIOFS_LP, pc_no)

    def load_from_xlsx(self, filename, pc_no):
        wb_obj = openpyxl.load_workbook(filename)
        sheet = wb_obj.active
        column_letter = get_column_letter(1 + pc_no)
        self._eeprom_storage_map['RIREF'][0] = sheet[column_letter + str(2)].value
        self._eeprom_storage_map['RIMUX'][0] = sheet[column_letter + str(3)].value
        self._eeprom_storage_map['RVREF_ADC'][0] = sheet[column_letter + str(4)].value
        self._eeprom_storage_map['REXTA'][0] = sheet[column_letter + str(6)].value
        self._eeprom_storage_map['REXTD'][0] = sheet[column_letter + str(7)].value
        self._eeprom_storage_map['RVREFA'][0] = sheet[column_letter + str(8)].value
        self._eeprom_storage_map['RVREFD'][0] = sheet[column_letter + str(9)].value
        self._eeprom_storage_map['RIOFS'][0] = sheet[column_letter + str(10)].value
        self._eeprom_storage_map['RIOFS_LP'][0] = sheet[column_letter + str(11)].value
        self._eeprom_storage_map['PC_ID'][0] = pc_no
