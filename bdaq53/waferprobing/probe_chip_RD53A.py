#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Probing script to probe a single RD53A chip
'''

import os
import time
import yaml
import logging
import copy
import numpy as np

from basil.dut import Dut

from bdaq53.system import bdaq53_base
from bdaq53.chips import rd53a
from bdaq53.waferprobing.needlecard_RD53A import Needlecard as NeedlecardA

from bdaq53.scans.test_registers import RegisterTest
from bdaq53.scans.scan_digital import DigitalScan
from bdaq53.scans.scan_analog import AnalogScan
from bdaq53.scans.tune_global_threshold import GDACTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.scans.scan_noise_occupancy import NoiseOccScan
from bdaq53.scans.scan_stuck_pixel import StuckPixelScan
from bdaq53.scans.scan_threshold import ThresholdScan


CHIP_SN = '0x0000'  # Chip SN for calling this script standalone

LOGLEVEL = logging.INFO

configuration = {
    # Powering settings
    'powering': {
        'VAUX1': 1.2,       # Default VDD_PLL
        'VAUX2': 1.2,       # Default VDD_CML
        'LDO_VINA': 1.7,    # Default analog voltage in LDO mode
        'LDO_VIND': 1.7,    # Default digital voltage in LDO mode
        'LDO_IINA': 1.1,    # Analog current limit in LDO mode
        'LDO_IIND': 1.1,    # Digital current limit in LDO mode
        'SHUNT_VINA': 1.7,  # Analog voltage limit in Shunt mode
        'SHUNT_VIND': 1.7,  # Digital voltage limit in Shunt mode
        'SHUNT_IINA': 1.1,  # Default analog working point in Shunt mode
        'SHUNT_IIND': 1.1   # Default digital working point in Shunt mode
    },

    # Analog measurements to be conducted using the external MUX
    'external_mux_measurements': [
        'VINA',
        'VIND',
        'VDDA',
        'VDDD',
        'VREF_ADC_OUT',
        'SLDO_VREFA',
        'SLDO_VREFD'
    ],

    # Analog measurements to be conducted using the internal MUX
    'internal_mux_measurements': [
        'VIN_Ana_SLDO',
        'VOUT_Ana_SLDO',
        'VREF_Ana_SLDO',
        'VOFF_Ana_SLDO',
        'VIN_Dig_SLDO',
        'VOUT_Dig_SLDO',
        'VREF_Dig_SLDO',
        'VOFF_Dig_SLDO'
    ],

    # High level scan configurations
    'register_test': {
        'ignore': ['PIX_PORTAL',
                   'GLOBAL_PULSE_ROUTE'
                   ]
    },

    'noise_occupancy_scan': {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 192,
        'maskfile': None,
        'mask_diff': False,
        'n_triggers': 1e7,
        'min_occupancy': 10
    },

    'digital_scan': {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 192,
        'maskfile': None,
        'mask_diff': False
    },

    'analog_scan': {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 192,
        'maskfile': None,
        'mask_diff': False,
        'VCAL_MED': 500,
        'VCAL_HIGH': 3500
    },

    'tuning': {
        'start_column': 0,
        'stop_column': 400,
        'start_row': 0,
        'stop_row': 192,
        'maskfile': None,
        'use_good_pixels_diff': False,

        # Target threshold
        'VCAL_MED': 500,
        'VCAL_HIGH': 680,   # 180 DVCAL corresponds to about 2000 e

        # Noise occupancy scan settings
        'n_triggers': 1e7,
        'min_occupancy': 10
    },

    'tot_tuning': {
        'target_tot': {'SYNC': 5.3, 'LIN': 5.3, 'DIFF': 5.3},  # target value for each FE to which ToT is tuned
        'delta_tot': 0.2,  # max. difference between target ToT and tuned ToT value for each FE
        'lower_limit': {'SYNC': 20, 'LIN': 10, 'DIFF': 40},  # lower bound for the feedback register value for each FE
        'upper_limit': {'SYNC': 50, 'LIN': 40, 'DIFF': 70},  # upper bound for the feedback register value for each FE
        'max_iterations': 10,  # max. number of iterations after which tuning is aborted

        'VCAL_MED': 500,
        'VCAL_HIGH': 1100
    }
}


class ChipException(Exception):
    pass


class DiscardException(Exception):
    pass


class BDAQException(Exception):
    pass


class NeedleCardException(Exception):
    pass


class WaferprobingPeriphery(object):
    def __init__(self, configuration, restart_bdaq=False, conf=None, Needlecard=NeedlecardA):
        self._defaults = copy.deepcopy(configuration)
        self.settings = copy.deepcopy(configuration)

        self.logger = logging.getLogger('Periphery')
        self.logger.success = lambda msg, *args, **kwargs: self.logger.log(logging.SUCCESS, msg, *args, **kwargs)
        self.logger.notice = lambda msg, *args, **kwargs: self.logger.log(logging.NOTICE, msg, *args, **kwargs)

        # Create Periphery devices object from waferprobing yaml file
        if conf is None:
            conf = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'waferprobing' + os.sep + 'waferprobing.yaml')

        self._devices = Dut(conf)
        self._devices.init()

        # Make sure all powersupplies are off and reset
        # self._devices['VINA'].set_enable(on=False)
        # self._devices['VIND'].set_enable(on=False)
        # self._devices['AUX1'].set_enable(on=False)
        # self._devices['AUX2'].set_enable(on=False)
        self._devices['SCC_powersupply'].set_enable(channel="ALL", on=False)
        self._devices['AUX_powersupply'].set_enable(channel="ALL", on=False)
        if restart_bdaq:
            self._devices['BDAQ'].set_enable(on=False)
        self._devices['AUX_powersupply'].reset_trip()
        self._devices['SCC_powersupply'].reset_trip()

        self._devices['AUX1'].set_current_limit(0.7)
        self._devices['AUX2'].set_current_limit(0.7)

        # Create bdaq object to piggyback on its I2C interface
        self.bdaq = bdaq53_base.BDAQ53()
        try:
            self.power_on_BDAQ()
        except BDAQException:
            self.power_off_BDAQ()
            self.power_on_BDAQ()

        # Needle card uses bdaq object for I2C communication
        self.needle_card = Needlecard(self.bdaq['i2c'])
        self.needle_card.init()

        # Default state
        self._powering_mode = 'LDO'
        self.needle_card.write_gpio_expander('EN_VDD_SHUNT', 0)

    def close(self):
        self._devices['smu'].off()
        self.power_off_chip()
        self.bdaq.close()
        self._devices.close()
        self.logger.handlers = []

    def get_chip_voltage(self):
        analog_voltage = self._devices['VINA'].get_voltage()
        digital_voltage = self._devices['VIND'].get_voltage()

        return analog_voltage, digital_voltage

    def get_chip_current(self):
        analog_current = self._devices['VINA'].get_current()
        digital_current = self._devices['VIND'].get_current()

        return analog_current, digital_current

    def get_chip_power(self):
        power = {}
        power['Mode'] = self._powering_mode
        power['VINA'], power['VIND'] = self.get_chip_voltage()
        power['I_VINA'], power['I_VIND'] = self.get_chip_current()
        return power

    def reset_SCC(self, powering_mode=None):
        ''' Reset all powering values to defaults '''

        # If powering mode is provided, switch mode
        if powering_mode is not None:
            if powering_mode == 'LDO':
                self.enable_LDO_mode()
            elif powering_mode == 'SHUNT':
                self.enable_SHUNT_mode()
            else:
                self.logger.error('Ignoring unknown powering mode {0}'.format(powering_mode))

        # self._devices['VINA'].set_enable(on=False)
        # self._devices['VINA'].set_enable(on=False)
        # self._devices['AUX1'].set_enable(on=False)
        # self._devices['AUX2'].set_enable(on=False)
        self._devices['SCC_powersupply'].set_enable(channel="ALL", on=False)
        self._devices['AUX_powersupply'].set_enable(channel="ALL", on=False)

        self.settings.update(self._defaults)

    def set_AUX_voltage(self, VAUX1=None, VAUX2=None):
        if VAUX1 is None:
            VAUX1 = self._defaults['VAUX1']
        if VAUX2 is None:
            VAUX2 = self._defaults['VAUX2']
        self.settings['VAUX1'] = VAUX1
        self._devices['AUX1'].set_voltage(VAUX1)
        self.settings['VAUX2'] = VAUX2
        self._devices['AUX2'].set_voltage(VAUX2)

    def get_AUX_voltage(self):
        aux1_voltage = self._devices['AUX1'].get_voltage()
        aux2_voltage = self._devices['AUX2'].get_voltage()

        return aux1_voltage, aux2_voltage

    def get_AUX_current(self):
        aux1_current = self._devices['AUX1'].get_current()
        aux2_current = self._devices['AUX2'].get_current()

        return aux1_current, aux2_current

    def enable_LDO_mode(self, VINA=None, VIND=None, IINA=None, IIND=None, fast=False):
        self._powering_mode = 'LDO'
        self.needle_card.write_gpio_expander('EN_VDD_SHUNT', 0, fast=fast)

        if VINA is None:
            self.settings['LDO_VINA'] = self._defaults['LDO_VINA']
        else:
            self.settings['LDO_VINA'] = VINA
        if VIND is None:
            self.settings['LDO_VIND'] = self._defaults['LDO_VIND']
        else:
            self.settings['LDO_VIND'] = VIND
        if IINA is None:
            self.settings['LDO_IINA'] = self._defaults['LDO_IINA']
        else:
            self.settings['LDO_IINA'] = IINA
        if IIND is None:
            self.settings['LDO_IIND'] = self._defaults['LDO_IIND']
        else:
            self.settings['LDO_IIND'] = IIND

    def enable_SHUNT_mode(self, IINA=None, IIND=None, VINA=None, VIND=None, fast=False):
        self._powering_mode = 'SHUNT'
        self.needle_card.write_gpio_expander('EN_VDD_SHUNT', 1, fast=fast)

        if VINA is None:
            self.settings['SHUNT_VINA'] = self._defaults['SHUNT_VINA']
        else:
            self.settings['SHUNT_VINA'] = VINA
        if VIND is None:
            self.settings['SHUNT_VIND'] = self._defaults['SHUNT_VIND']
        else:
            self.settings['SHUNT_VIND'] = VIND
        if IINA is None:
            self.settings['SHUNT_IINA'] = self._defaults['SHUNT_IINA']
        else:
            self.settings['SHUNT_IINA'] = IINA
        if IIND is None:
            self.settings['SHUNT_IIND'] = self._defaults['SHUNT_IIND']
        else:
            self.settings['SHUNT_IIND'] = IIND

    def power_on_chip(self, check=True, fast=False):
        # Check for contact. Never power on chip if contact is not perfect
        if check:
            if not all(self.needle_card.get_edge_sensors()):
                self.logger.error('Bad contact!')
                raise NeedleCardException('Bad contact!')

        self._devices['AUX_powersupply'].reset_trip()
        self._devices['SCC_powersupply'].reset_trip()

        self._devices['AUX1'].set_voltage(self.settings['VAUX1'])
        self._devices['AUX2'].set_voltage(self.settings['VAUX2'])

        if self._powering_mode == 'LDO':
            if check:
                self.needle_card.write_gpio_expander('EN_VDD_SHUNT', 0, fast=fast)
            self._devices['VINA'].set_voltage(self.settings['LDO_VINA'])
            self._devices['VIND'].set_voltage(self.settings['LDO_VIND'])
            self._devices['VINA'].set_current_limit(self.settings['LDO_IINA'])
            self._devices['VIND'].set_current_limit(self.settings['LDO_IIND'])
        elif self._powering_mode == 'SHUNT':
            if check:
                self.needle_card.write_gpio_expander('EN_VDD_SHUNT', 1, fast=fast)
            self._devices['VINA'].set_voltage(self.settings['SHUNT_VINA'])
            self._devices['VIND'].set_voltage(self.settings['SHUNT_VIND'])
            self._devices['VINA'].set_current_limit(self.settings['SHUNT_IINA'])
            self._devices['VIND'].set_current_limit(self.settings['SHUNT_IIND'])
        else:
            raise AttributeError('Unknown powering mode {}'.format(self._powering_mode))

        self.logger.notice('Powering on chip in {0} mode:'.format(self._powering_mode))
        # self._devices['AUX1'].set_enable(on=True)
        # self._devices['AUX2'].set_enable(on=True)
        self._devices['AUX_powersupply'].set_enable(channel="ALL", on=True)
        # time.sleep(0.1)
        # self._devices['VINA'].set_enable(on=True)
        # self._devices['VIND'].set_enable(on=True)
        self._devices['SCC_powersupply'].set_enable(channel="ALL", on=True)
        time.sleep(3)
        VINA, VIND = self.get_chip_voltage()
        IINA, IIND = self.get_chip_current()

        self.logger.notice('VDD_PLL: {0:1.3f}A @ {2:1.3f}V, VDD_CML: {1:1.3f}A @ {3:1.3f}V'.format(*self.get_AUX_current(), *self.get_AUX_voltage()))
        self.logger.notice('VINA: {0:1.3f}A @ {2:1.3f}V, VIND: {1:1.3f}A @ {3:1.3f}V'.format(IINA, IIND, VINA, VIND))

        if self._powering_mode == 'LDO':
            if (IINA > self.settings['LDO_IINA'] - 0.01 or IIND > self.settings['LDO_IIND'] - 0.01) or (VINA < self.settings['LDO_VINA'] - 0.1 or VIND < self.settings['LDO_VIND'] - 0.1):
                # self._devices['VINA'].set_enable(on=False)
                # self._devices['VIND'].set_enable(on=False)
                self._devices['SCC_powersupply'].set_enable(channel="ALL", on=False)
                time.sleep(0.3)
                # self._devices['VINA'].set_enable(on=True)
                # self._devices['VIND'].set_enable(on=True)
                self._devices['SCC_powersupply'].set_enable(channel="ALL", on=True)
                time.sleep(1)

                VINA, VIND = self.get_chip_voltage()
                IINA, IIND = self.get_chip_current()

                self.logger.notice('VDD_PLL: {0:1.3f}A @ {2:1.3f}V, VDD_CML: {1:1.3f}A @ {3:1.3f}V'.format(*self.get_AUX_current(), *self.get_AUX_voltage()))
                self.logger.notice('VINA: {0:1.3f}A @ {2:1.3f}V, VIND: {1:1.3f}A @ {3:1.3f}V'.format(IINA, IIND, VINA, VIND))
                if (IINA > self.settings['LDO_IINA'] - 0.01 or IIND > self.settings['LDO_IIND'] - 0.01) or (VINA < self.settings['LDO_VINA'] * 0.9 or VIND < self.settings['LDO_VIND'] * 0.9):
                    self.power_off_chip()
                    raise DiscardException('Too much power drawn!')
            elif (IINA < 0.001 or IIND < 0.001):
                self.power_off_chip()
                raise DiscardException('Chip is open!')

        elif self._powering_mode == 'SHUNT':
            if (VIND < 0.5 or VINA < 0.5):
                self.power_off_chip()
                raise DiscardException('Too much power drawn!')
            elif (IINA < 0.001 or IIND < 0.001):
                self.power_off_chip()
                raise DiscardException('Chip is open!')
        if check:
            VDDD = self.get_voltage_from_external_mux('VDDD', fast=fast)
            VDDA = self.get_voltage_from_external_mux('VDDA', fast=fast)
            self.logger.notice('VDDA = {0:1.3f}V, VDDD = {1:1.3f}V (without GND shift)'.format(VDDA, VDDD))

    def power_off_chip(self):
        self.logger.notice('Powering chip off...')
        # self._devices['VINA'].set_enable(on=False)
        # self._devices['VIND'].set_enable(on=False)
        # self._devices['AUX1'].set_enable(on=False)
        # self._devices['AUX2'].set_enable(on=False)
        self._devices['SCC_powersupply'].set_enable(channel="ALL", on=False)
        self._devices['AUX_powersupply'].set_enable(channel="ALL", on=False)

    def power_on_BDAQ(self):
        self.bdaq.close()
        self._devices['BDAQ'].set_enable(on=True)
        for _ in range(5):
            try:
                self.bdaq.init()
                return
            except Exception:
                continue
        else:
            time.sleep(10)
            os.system("ping -c 1 192.168.10.12")
            time.sleep(5)

            for _ in range(5):
                try:
                    self.bdaq.init()
                    return
                except Exception:
                    continue
            try:
                self.bdaq.init()
            except Exception as e:
                self.logger.exception('Cannot communicate with BDAQ board. Stop!')
                raise BDAQException(e)

    def power_off_BDAQ(self):
        self.bdaq.close()
        self._devices['BDAQ'].set_enable(on=False)

    def powercycle(self, chip=True, bdaq=False, check=True, fast=False):
        if chip:
            self.power_off_chip()
        if bdaq:
            self.power_off_BDAQ()
            time.sleep(2)
            self.power_on_BDAQ()
        else:
            time.sleep(0.5)
        time.sleep(1)
        if chip:
            self.power_on_chip(check=check, fast=fast)

    def get_voltage_from_external_mux(self, mux, samples=1, offset=0.):
        self.needle_card.set_adc_mux(mux)
        time.sleep(0.1)
        self._devices['smu'].set_current(0)
        self._devices['smu'].source_current()
        try:
            self._devices['smu'].on()
            time.sleep(0.1)
            vals = []
            for _ in range(samples):
                vals.append(float(self._devices['smu'].get_voltage().split(',')[0]))
            value = float(np.mean(vals)) + offset
        except ValueError as e:
            self.logger.exception(e)
            value = 0.0
        self._devices['smu'].off()
        self.needle_card.set_adc_mux('GND')
        return value

    def measure_iref(self, iref_trim, offset=0.):
        # Measure reference voltage at centered IREF_TRIM
        self.needle_card.write_gpio_expander('IREF_TRIM', 7)
        reference_voltage = self.get_voltage_from_external_mux('IREF_OUT', offset=offset)
        if reference_voltage < 0.5 or reference_voltage > 0.7:
            reference_voltage = 0.6

        # Source reference voltage
        self._devices['smu'].source_volt()
        self._devices['smu'].set_voltage_range(2)
        self._devices['smu'].set_voltage(reference_voltage)

        self.needle_card.write_gpio_expander('IREF_TRIM', iref_trim)
        self.needle_card.set_adc_mux('IREF_OUT')
        self.needle_card.write_gpio_expander('IREF_EN', 0)
        self._devices['smu'].on()
        try:
            iref = float(self._devices['smu'].get_current().split(',')[1])
        except ValueError as e:
            self.logger.exception(e)
            iref = 0.0
        self._devices['smu'].off()
        self.needle_card.write_gpio_expander('IREF_EN', 1)

        return iref, reference_voltage


class ChipProber(object):
    def __init__(self, chip_sn, configuration, working_dir, external_logfile_handlers=[], restart_bdaq=False, chip_config_file=None, **_):
        self.chip_sn = chip_sn
        self.config = configuration
        self.working_dir = working_dir
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.external_logfile_handlers = external_logfile_handlers

        self.meta_data = {'duration': {}}
        self.general_information = {'configuration': configuration}

        if chip_config_file is None:
            chip_config_file = os.path.join(self.proj_dir, 'rd53a_default.cfg.yaml')

        with open(chip_config_file) as f:
            self.chip_config = yaml.safe_load(f)
            self.chip_config['chip_sn'] = self.chip_sn

        logfile = os.path.join(working_dir, 'probing_chip_' + self.chip_sn + '.log')
        self.fh = logging.FileHandler(logfile)
        self.fh.setLevel(logging.INFO)
        self.fh.setFormatter(logging.Formatter("%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s"))

        self.logger = logging.getLogger('Prober_{0}'.format(self.chip_sn))
        self.logger.setLevel(LOGLEVEL)
        self.logger.success = lambda msg, *args, **kwargs: self.logger.log(logging.SUCCESS, msg, *args, **kwargs)
        self.logger.notice = lambda msg, *args, **kwargs: self.logger.log(logging.NOTICE, msg, *args, **kwargs)
        self.logger.addHandler(self.fh)
        for handler in self.external_logfile_handlers:
            self.logger.addHandler(handler)

        logging.getLogger('Waferprobing').addHandler(self.fh)

        self.outfile_name = os.path.join(working_dir, '_'.join(('analog_data', self.chip_sn)) + '.yaml')

        self.periphery = WaferprobingPeriphery(configuration=self.config['powering'], restart_bdaq=restart_bdaq)
        self.periphery.logger.addHandler(self.fh)
        for handler in self.external_logfile_handlers:
            self.periphery.logger.addHandler(handler)

        # Define some general information
        self.general_information['DAQ_fw_version'] = self.periphery.bdaq.fw_version
        self.general_information['DAQ_aurora_lanes'] = self.periphery.bdaq.rx_lanes
        self.general_information['DAQ_rx_speed'] = '640Mb/s' if (self.periphery.bdaq.board_options & self.periphery.bdaq.board_options_map['640Mbps']) else '1.28Gb/s'

        self.chip = rd53a.RD53A(self.periphery.bdaq, chip_sn=self.chip_sn, config=self.chip_config)
        logging.getLogger('RD53A').addHandler(self.fh)
        for handler in self.external_logfile_handlers:
            logging.getLogger('RD53A').addHandler(handler)

    def close(self):
        self.periphery.close()
        del self.periphery

        self.fh.close()
        for lg in logging.Logger.manager.loggerDict.values():
            if isinstance(lg, logging.Logger):
                lg.removeHandler(self.fh)
        self.logger.handlers = []

    ''' HELPER METHODS '''

    def get_meta_data(self):
        if len(self.meta_data.keys()) == 0:
            raise RuntimeError('Meta data is only available after running main().')
        return self.meta_data

    def get_general_information(self):
        return self.general_information

    def dump_analog_data(self, name, data):
        try:
            with open(self.outfile_name, 'r') as yamlfile:
                results = yaml.safe_load(yamlfile)
                results[name] = data
        except (IOError, TypeError):
            results = {}
            results[name] = data
        with open(self.outfile_name, 'w') as yamlfile:
            yaml.safe_dump(results, yamlfile)

    def get_voltage_from_internal_mux(self, mux, samples=1, offset=0.):
        if type(mux) == str:
            mux = self.chip.voltage_mux[mux]

        self.chip.registers['MONITOR_SELECT'].write(mux)
        time.sleep(0.1)
        value = self.periphery.get_voltage_from_external_mux('VMUX_OUT', samples=samples, offset=offset)
        return value

    ''' ANALOG TESTS '''

    def trim_IREF(self, target_value=4e-6):
        self.logger.info('Start IREF trimming routine...')
        iref_results = {}
        diffs = {}
        for iref_trim in range(16):
            iref_results[iref_trim] = {}
            time.sleep(0.1)
            iref, v_iref = self.periphery.measure_iref(iref_trim)
            iref_results[iref_trim]['V_IREF'] = v_iref
            iref_results[iref_trim]['IREF'] = iref
            diffs[iref_trim] = abs(target_value - iref)
        self.dump_analog_data('IREF Trims', iref_results)

        return min(diffs, key=diffs.get)

    def measure_regulator_IV_curves(self, start=1.0, stop=0.5, step=-0.05):
        self.logger.info('Start regulator IV measurement routine...')

        self.periphery.power_off_chip()
        self.periphery.enable_SHUNT_mode(VINA=2.0, VIND=2.0, IINA=start, IIND=start)
        self.periphery.power_on_chip()

        iv_curves = {}
        actual_stop = stop + 1e-5 if step > 0 else stop - 1e-5
        for I_IN in np.arange(start, actual_stop, step):
            self.periphery._devices['VIND'].set_current_limit(I_IN + 0.001)
            self.periphery._devices['VINA'].set_current_limit(I_IN + 0.001)

            time.sleep(0.1)
            I_IN_i = round(float(I_IN), 2)
            iv_curves[I_IN_i] = {}
            iv_curves[I_IN_i]['VIN_D'] = self.periphery.get_voltage_from_external_mux('VIND', offset=self.gnd_offset)
            iv_curves[I_IN_i]['VIN_D_int'] = self.get_voltage_from_internal_mux('VIN_Dig_SLDO', offset=self.gnd_offset)     # FIXME: Will this ever yield a useful value?
            iv_curves[I_IN_i]['VIN_A'] = self.periphery.get_voltage_from_external_mux('VINA', offset=self.gnd_offset)
            iv_curves[I_IN_i]['VIN_A_int'] = self.get_voltage_from_internal_mux('VIN_Ana_SLDO', offset=self.gnd_offset)     # FIXME: Will this ever yield a useful value?
            iv_curves[I_IN_i]['VDDD'] = self.periphery.get_voltage_from_external_mux('VDDD', offset=self.gnd_offset)
            iv_curves[I_IN_i]['VDDA'] = self.periphery.get_voltage_from_external_mux('VDDA', offset=self.gnd_offset)
            iv_curves[I_IN_i]['VREF_A'] = self.periphery.get_voltage_from_external_mux('SLDO_VREFA', offset=self.gnd_offset)
            iv_curves[I_IN_i]['VREF_D'] = self.periphery.get_voltage_from_external_mux('SLDO_VREFD', offset=self.gnd_offset)
            iv_curves[I_IN_i]['VOFF_A'] = self.get_voltage_from_internal_mux('VOFF_Ana_SLDO')     # FIXME: Will this ever yield a useful value?
            iv_curves[I_IN_i]['VOFF_D'] = self.get_voltage_from_internal_mux('VOFF_Dig_SLDO')     # FIXME: Will this ever yield a useful value?

        self.dump_analog_data('IV curves', iv_curves)

        self.logger.success('IV curve measurement done!')
        self.periphery.power_off_chip()
        self.periphery.reset_SCC()

    ''' TESTS WITH CHIP COMMUNICATION '''

    def try_communication(self, max_tries=5):
        self.logger.info('Start communication test routine...')
        init_counter = 0

        # Measure actual untrimmed VDDA and set as VAUX1/2 for realistic test
        self.periphery.power_on_chip()
        VDDA = self.periphery.get_voltage_from_external_mux('VDDA', offset=self.gnd_offset)
        if VDDA > 1.3:
            self.logger.warning('Measured VDDA is too high: {0:1.3f}V Use 1.3V instead.'.format(VDDA))
            VDDA = 1.3

        self.periphery.set_AUX_voltage(VDDA, VDDA)
        self.periphery.power_off_chip()

        self.logger.info('Set VDD_CML/PLL = VDDA + GND shift = {0:1.3f}V'.format(VDDA))

        # Try up to 5 times to initialize communication
        for _ in range(max_tries):
            try:
                self.periphery.power_on_chip()
                init_counter += 1
                self.chip.init_communication()
                self.dump_analog_data('Initial Link', {'Init counter': init_counter, 'VDD_AUX': VDDA})
                break
            except RuntimeError:
                self.logger.warning('Could not establish link. Powercycle chip and try again...')
                self.periphery.power_off_chip()
        else:
            self.logger.error('Could not initialize communication with chip at VDD_PLL/CML = VDDA! Set VDD_PLL/CML to default values and try again...')
            self.periphery.set_AUX_voltage()
            VDDA = self.periphery.settings['VAUX1']
            self.periphery.powercycle(chip=True, bdaq=False)

            try:
                init_counter += 1
                self.chip.init_communication()
                self.dump_analog_data('Initial Link', {'Init counter': init_counter, 'VDD_AUX': self.periphery.get_AUX_voltage()[0]})
            except RuntimeError:
                self.logger.error('Could not initialize communication with chip even at default settings!')
                init_counter += 1
                return

        self.logger.success('Communication established at {0:1.3f}V after {1} trie(s)!'.format(VDDA, init_counter))

    def trim_VREF(self, target_value=1.2):
        postfix = ' (' + self.periphery._powering_mode + ')'
        self.logger.info('Start VREF trimming routine...')

        VrefD_results, VrefA_results = {}, {}
        diffs_D, diffs_A = {}, {}
        for Vref_val in range(31, -1, -1):
            VrefD_results[Vref_val], VrefA_results[Vref_val] = {}, {}
            self.chip.registers['VOLTAGE_TRIM'].write('0b{0:05b}{0:05b}'.format(Vref_val))
            time.sleep(0.1)

            VDDD = self.periphery.get_voltage_from_external_mux('VDDD', offset=self.gnd_offset)
            VrefD_results[Vref_val]['VREF_D'] = self.periphery.get_voltage_from_external_mux('SLDO_VREFD', offset=self.gnd_offset)
            VrefD_results[Vref_val]['VOFF_D'] = self.get_voltage_from_internal_mux('VOFF_Ana_SLDO')
            VrefD_results[Vref_val]['VDDD'] = VDDD
            diffs_D[Vref_val] = abs(target_value - VDDD)

            VDDA = self.periphery.get_voltage_from_external_mux('VDDA', offset=self.gnd_offset)
            VrefA_results[Vref_val]['VREF_A'] = self.periphery.get_voltage_from_external_mux('SLDO_VREFA', offset=self.gnd_offset)
            VrefA_results[Vref_val]['VOFF_A'] = self.get_voltage_from_internal_mux('VOFF_Dig_SLDO')
            VrefA_results[Vref_val]['VDDA'] = VDDA
            diffs_A[Vref_val] = abs(target_value - VDDA)

        self.dump_analog_data('VREF_D Trim' + postfix, VrefD_results)
        self.dump_analog_data('VREF_A Trim' + postfix, VrefA_results)

        if abs(VrefD_results[31]['VREF_D'] - VrefD_results[0]['VREF_D']) > 0.05:
            VrefD_opt = min(diffs_D, key=diffs_D.get)
            self.logger.success('Optimal VREFD_TRIM is {0}'.format(VrefD_opt))
        else:
            VrefD_opt = 0b10000
            self.logger.error('VREF_D trimming failed!')

        if abs(VrefA_results[31]['VREF_A'] - VrefA_results[0]['VREF_A']) > 0.05:
            VrefA_opt = min(diffs_A, key=diffs_A.get)
            self.logger.success('Optimal VREFA_TRIM is {0}'.format(VrefA_opt))
        else:
            VrefA_opt = 0b10000
            self.logger.error('VREF_A trimming failed!')

        self.chip.registers['VOLTAGE_TRIM'].write('0b{0:05b}{1:05b}'.format(VrefA_opt, VrefD_opt))
        self.chip_config['trim']['VREF_A_TRIM'] = VrefA_opt
        self.chip_config['trim']['VREF_D_TRIM'] = VrefD_opt

        return (VrefA_opt, VrefD_opt)

    def trim_VREF_ADC(self, target_value=0.9):
        postfix = ' (' + self.periphery._powering_mode + ')'
        self.logger.info('Start VREF_ADC trimming routine...')

        try:
            ADC_trim = int(format(self.chip.registers['MONITOR_CONFIG'].read(), '011b')[:-6], 2)
        except RuntimeError:
            self.logger.warning('No or wrong response from chip while trying to read ADC trimbits. Use default.')
            ADC_trim = 5

        VrefADC_results = {}
        diffs = {}
        for VrefADC_trim in range(31, -1, -1):
            VrefADC_results[VrefADC_trim] = {}
            self.chip.registers['MONITOR_CONFIG'].write('0b{0:05b}{1:06b}'.format(VrefADC_trim, ADC_trim))
            time.sleep(0.1)
            VREF_ADC = self.periphery.get_voltage_from_external_mux('VREF_ADC_OUT')
            VrefADC_results[VrefADC_trim]['VREF_ADC_OUT'] = VREF_ADC
            self.dump_analog_data('VREF_ADC Trim' + postfix, VrefADC_results)
            diffs[VrefADC_trim] = abs(target_value - VREF_ADC)

        VrefADC_opt = min(diffs, key=diffs.get)
        self.logger.success('Optimal MON_BG_TRIM is {0}'.format(VrefADC_opt))
        self.chip.registers['MONITOR_CONFIG'].write('0b{0:05b}{1:06b}'.format(VrefADC_opt, ADC_trim))

        return VrefADC_opt

    def trim_ADC(self, max_trim_value=10, steps=29, samples=1):
        def _find_scan_range(start_v=0.025, step_v=0.005, n_tries=20, samples=10):
            self.chip.get_ADC_value('ground')
            voltage_n = start_v
            direction = True
            closest_match = (128, 0.)
            for _ in range(n_tries):
                self.periphery._devices['smu'].set_voltage(voltage_n)
                self.periphery._devices['smu'].on()
                time.sleep(0.1)
                vals = []
                for _ in range(samples):
                    vals.append(self.chip.get_ADC_value('ground')[0])
                adc_value = int(round(np.mean(vals), 0))
                if adc_value == 128:
                    self.periphery._devices['smu'].off()
                    return voltage_n
                else:
                    diff = abs(adc_value - 128)
                    if diff < closest_match[0]:
                        closest_match = (diff, voltage_n)

                    if adc_value > 128:
                        voltage_n -= step_v
                        if direction:
                            step_v /= 2
                            direction = False
                    elif adc_value < 128:
                        voltage_n += step_v
                        if not direction:
                            step_v /= 2
                            direction = True

            self.periphery._devices['smu'].off()
            if closest_match[0] == 1:
                return closest_match[1]

            raise ValueError('Scan range could not be determined!')

        def _scan_one_param(scan_points, samples=1):
            output_array = np.zeros((len(scan_points)), dtype=[('adc', 'i8'), ('voltage', 'f8')])

            for scan_param_id, voltage_n in enumerate(scan_points):
                adc_voltages = []
                self.periphery._devices['smu'].set_voltage(voltage_n)
                self.periphery._devices['smu'].on()
                time.sleep(0.1)

                for _ in range(samples):
                    adc_voltages.append(self.chip.get_ADC_value('ground')[0])
                adc_voltage = int(round(np.mean(adc_voltages), 0))

                output_array[scan_param_id]['adc'] = adc_voltage
                output_array[scan_param_id]['voltage'] = voltage_n

            return output_array

        self.logger.info('Start ADC trimming routine...')
        trim_data = {}
        self.periphery._devices['smu'].source_volt()
        self.periphery._devices['smu'].off()
        self.periphery.needle_card.set_adc_mux('VMUX_OUT')

        try:
            VREF_ADC_trimbits = int(format(self.chip.registers['MONITOR_CONFIG'].read(), '011b')[:5], 2)
        except RuntimeError:
            self.logger.warning('No or wrong response from chip while trying to read VREF_ADC trimbits. Use default.')
            VREF_ADC_trimbits = 12

        adc_128_v = _find_scan_range()

        fit_quality = 9999
        for adc_trim in range(max_trim_value, -1, -1):
            trim_data[adc_trim] = {}
            self.chip.registers['MONITOR_CONFIG'].write('0b{0:05b}{1:06b}'.format(VREF_ADC_trimbits, adc_trim))
            scan_points = np.arange(adc_128_v - 0.002, adc_128_v + 0.001, 0.003 / steps)
            output_array = _scan_one_param(scan_points, samples=samples)
            trim_data[adc_trim]['voltage'] = output_array[:]['voltage'].tolist()
            trim_data[adc_trim]['adc'] = output_array[:]['adc'].tolist()
            chi2 = np.polyfit(output_array[:]['voltage'], output_array[:]['adc'], 1, cov=True)[1][1][1]
            if fit_quality > chi2:
                fit_quality = chi2
                best_adc_trim = adc_trim
            trim_data[adc_trim]['chi2'] = float(chi2)

        self.dump_analog_data('ADC Trim', trim_data)

        self.periphery._devices['smu'].off()
        self.chip.registers['MONITOR_CONFIG'].write('0b{0:05b}{1:06b}'.format(VREF_ADC_trimbits, best_adc_trim))
        self.chip_config['trim']['MON_ADC_TRIM'] = best_adc_trim
        self.logger.success('Optimal MON_ADC_TRIM is {0}.'.format(best_adc_trim))
        return best_adc_trim

    def read_ring_oscillators(self):
        self.logger.info('Start ring oscillator readout routine...')
        osc = self.chip.get_ring_oscillators()
        self.dump_analog_data('Ring oscillators', osc)

    def read_temperature_sensors(self):
        self.logger.info('Start temperature sensor readout routine...')
        temp = self.chip.get_temperature_sensors()[1]
        self.dump_analog_data('Temperature sensors', temp)

    def measure_injection_dacs(self, dac_settings=[500, 1250, 2000, 2750, 3500]):
        self.logger.info('Start injection DAC calibration routine...')
        inj_dacs = {}

        inj_dacs['VCAL_HIGH'], inj_dacs['VCAL_MED'] = [], []
        for setting in dac_settings:
            self.chip.registers['VCAL_MED'].write(setting)
            self.chip.registers['VCAL_HIGH'].write(setting)
            time.sleep(0.1)
            inj_dacs['VCAL_MED'].append([setting, self.periphery.get_voltage_from_external_mux('VINJ_MID')])
            inj_dacs['VCAL_HIGH'].append([setting, self.periphery.get_voltage_from_external_mux('VINJ_HI')])

        self.dump_analog_data('Injection DACs', inj_dacs)

    def full_powercycle(self):
        # Powercycle chip and make sure communication is working
        self.periphery.power_off_chip()
        for _ in range(5):
            try:
                self.periphery.power_on_chip()
                time.sleep(1)
                self.chip.init()
                break
            except RuntimeError:
                self.periphery.power_off_chip()
                time.sleep(1)
        else:
            self.logger.error('Communication with chip could not be established again!')

    ''' MAIN METHOD '''

    def main(self):
        self.logger.info('Start probing chip {0}'.format(self.chip_sn))
        timestamp_start_chip = time.time()
        power_results = {}
        gnd_results = {}

        try:
            # Power on chip in SHUNT mode
            self.periphery.enable_SHUNT_mode()
            self.periphery.power_on_chip()

            try:    # Trim IREF
                timestamp_start_test = time.time()
                IREF_opt = self.trim_IREF()  # Find optimal IREF_TRIM value
                self.periphery.needle_card.write_gpio_expander('IREF_TRIM', IREF_opt)   # Set optimal IREF_TRIM value on needlecard
                self.periphery.needle_card.write_gpio_expander('IREF_EN', 1)    # Make sure IREF is enabled on needlecard
                self.logger.success('Optimal IREF_TRIM is {0}'.format(IREF_opt))
                self.meta_data['duration']['iref_trim'] = time.time() - timestamp_start_test
            except DiscardException as e:
                self.periphery.power_off_chip()
                raise e
            except Exception as e:
                self.periphery.needle_card.write_gpio_expander('IREF_TRIM', 8)  # If something went wrong center IREF_TRIM
                self.periphery.needle_card.write_gpio_expander('IREF_EN', 1)    # Make sure IREF is enabled on needlecard
                self.logger.exception('Unhandled exception during IREF trim routine: {0}'.format(e))

            self.periphery.powercycle(chip=True, bdaq=False)

            try:    # Determine GND offset in SHUNT mode
                timestamp_start_test = time.time()

                v = []
                for mux in ['ground', 'ground1', 'ground2']:
                    v.append(self.get_voltage_from_internal_mux(mux, samples=10, offset=0.))
                self.gnd_offset = float(np.mean(v))
                gnd_results['SHUNT'] = self.gnd_offset
                if abs(self.gnd_offset) < 0.1:
                    self.logger.info('GND offset in SHUNT mode is {0:1.1f}mV'.format(self.gnd_offset * 1000))
                else:
                    self.logger.warning('GND offset in SHUNT mode is too high at {0:1.1f}mV. Set offset to 30mV'.format(self.gnd_offset * 1000))
                    self.gnd_offset = 0.03

                self.meta_data['duration']['gnd_offset_shunt'] = time.time() - timestamp_start_test
            except RuntimeError as e:
                self.logger.warning('GND offset in SHUNT mode could not be measured due to communication error. Set offset to 30mV')
                self.logger.error('Error: {}'.format(e))
                self.gnd_offset = 0.03
                gnd_results['SHUNT'] = 'error'

            try:    # Take regulator IV curves in SHUNT mode
                timestamp_start_test = time.time()
                self.measure_regulator_IV_curves()
                self.meta_data['duration']['regulator_iv_curves'] = time.time() - timestamp_start_test
            except DiscardException as e:
                self.periphery.power_off_chip()
                raise e
            except Exception as e:
                self.logger.exception('Unhandled exception during regulator IV curve measurement routine in SHUNT mode: {0}'.format(e))

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Block of tests that are repeated in SHUNT and LDO modes:
    # - IREF trimming
    # - Init test
    # - VREF trimming
    # - VREF_ADC trimming

            self.periphery.powercycle(chip=True, bdaq=False)
            try:    # Init test
                timestamp_start_test = time.time()
                self.try_communication()
                self.meta_data['duration']['init_shunt'] = time.time() - timestamp_start_test
            except DiscardException as e:
                self.periphery.power_off_chip()
                raise e
            except Exception as e:
                self.logger.exception('Unhandled exception during communication test routine: {0}'.format(e))

            try:    # VREF trimming
                timestamp_start_test = time.time()
                self.trim_VREF()
                self.meta_data['duration']['vref_trim_shunt'] = time.time() - timestamp_start_test
            except Exception as e:
                self.chip.registers['VOLTAGE_TRIM'].write(0b1000010000)  # If something went wrong center VOLTAGE_TRIM
                self.logger.exception('Unhandled exception during VREF trim routine: {0}'.format(e))

            self.periphery.set_AUX_voltage()    # After VREF trimming, we expect 1.2V for VDD_CML/PLL
            self.full_powercycle()

            try:    # Trim VREF_ADC
                timestamp_start_test = time.time()
                self.trim_VREF_ADC()
                self.meta_data['duration']['vref_adc_trim_shunt'] = time.time() - timestamp_start_test
            except Exception as e:
                self.chip.registers['MONITOR_CONFIG'].write(0b00000000000)  # If something went wrong set MONITOR_CONFIG to default
                self.logger.exception('Unhandled exception during VREF_ADC trim routine: {0}'.format(e))

        except Exception as e:
            self.logger.error('Testing in SHUNT mode failed: {}'.format(e))
            self.logger.info('Go to LDO mode...')

    #
    #
    #
    #
    #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Repeated block of tests now in LDO mode:
    # - IREF trimming
    # - Init test
    # - VREF trimming
    # - VREF_ADC trimming

        # Go back to LDO mode for further testing for RD53A, because of stability issues with SHUNT mode
        self.periphery.enable_LDO_mode()
        self.periphery.reset_SCC()
        self.periphery.power_on_chip()

        # Determine GND offset in LDO mode
        try:
            timestamp_start_test = time.time()
            self.chip.init()
            v = []
            for mux in ['ground', 'ground1', 'ground2']:
                v.append(self.get_voltage_from_internal_mux(mux, samples=10, offset=0.))
            self.gnd_offset = float(np.mean(v))
            gnd_results['LDO'] = self.gnd_offset
            if abs(self.gnd_offset) < 0.1:
                self.logger.info('GND offset in LDO mode is {0:1.1f}mV'.format(self.gnd_offset * 1000))
            else:
                self.logger.warning('GND offset in LDO mode is too high at {0:1.1f}mV. Set offset to 10mV'.format(self.gnd_offset * 1000))
                self.gnd_offset = 0.01
            self.meta_data['duration']['gnd_offset_ldo'] = time.time() - timestamp_start_test
        except RuntimeError as e:
            self.logger.warning('GND offset in LDO mode could not be measured due to communication error. Set offset to 10mV')
            self.logger.error('Error: {}'.format(e))
            self.gnd_offset = 0.01
            gnd_results['LDO'] = 'error'
        self.dump_analog_data('GND results', gnd_results)

        try:    # Init test
            timestamp_start_test = time.time()
            self.try_communication()
            self.meta_data['duration']['init_ldo'] = time.time() - timestamp_start_test
        except DiscardException as e:
            self.periphery.power_off_chip()
            raise e
        except Exception as e:
            self.logger.exception('Unhandled exception during communication test routine: {0}'.format(e))

        try:    # Power consumption in unconfigured state
            timestamp_start_test = time.time()
            power_results['Unconfigured'] = self.periphery.get_chip_power()
            self.meta_data['duration']['power_unconfigured'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during unconfigured power consumption measurement: {0}'.format(e))

        try:    # VREF trimming
            timestamp_start_test = time.time()
            self.trim_VREF()
            self.meta_data['duration']['vref_trim_ldo'] = time.time() - timestamp_start_test
        except Exception as e:
            self.chip.registers['VOLTAGE_TRIM'].write(0b1000010000)  # If something went wrong center VOLTAGE_TRIM
            self.logger.exception('Unhandled exception during VREF trim routine: {0}'.format(e))

        self.periphery.set_AUX_voltage()    # After VREF trimming, we expect 1.2V for VDD_CML/PLL
        self.full_powercycle()

        try:    # Trim VREF_ADC
            timestamp_start_test = time.time()
            _ = self.trim_VREF_ADC()
            self.meta_data['duration']['vref_adc_trim_ldo'] = time.time() - timestamp_start_test
        except Exception as e:
            self.chip.registers['MONITOR_CONFIG'].write(0b00000000000)  # If something went wrong set MONITOR_CONFIG to default
            self.logger.exception('Unhandled exception during VREF_ADC trim routine: {0}'.format(e))

    #
    #
    #
    #
    #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

        # Powercycle chip and make sure communication is working
        self.full_powercycle()

        try:    # Trim ADC
            timestamp_start_test = time.time()
            self.trim_ADC()
            self.meta_data['duration']['adc_trim'] = time.time() - timestamp_start_test
        except Exception as e:
            self.chip.registers['MONITOR_CONFIG'].write(0b00000000000)
            self.logger.exception('Unhandled exception during ADC trim routine: {0}'.format(e))

        try:    # Power consumption in std. configuration
            timestamp_start_test = time.time()
            power_results['Standard config'] = self.periphery.get_chip_power()
            self.meta_data['duration']['power_configured'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during configured power measurement: {0}'.format(e))

        try:    # Analog measurements using external MUX
            timestamp_start_test = time.time()
            self.logger.info('Start external MUX measurement routine...')
            external_mux_results = {}
            for mux in self.config['external_mux_measurements']:
                time.sleep(0.1)
                external_mux_results[mux] = self.periphery.get_voltage_from_external_mux(mux)
            self.dump_analog_data('External MUX measurements', external_mux_results)
            self.meta_data['duration']['external_mux_measurements'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during analog measurement routine: {0}'.format(e))

        try:    # Analog measurements using internal MUX
            timestamp_start_test = time.time()
            self.logger.info('Start internal MUX measurement routine...')
            internal_mux_results = {}
            for mux in self.config['internal_mux_measurements']:
                time.sleep(0.1)
                internal_mux_results[mux] = self.get_voltage_from_internal_mux(mux)
            self.dump_analog_data('Internal MUX measurements', internal_mux_results)
            self.meta_data['duration']['internal_mux_measurements'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during internal MUX measurement routine: {0}'.format(e))

        try:    # Read ring oscillators
            timestamp_start_test = time.time()
            self.read_ring_oscillators()
            self.meta_data['duration']['ring_oscillators'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during ring oscillator readout routine: {0}'.format(e))

        try:    # Read temperature sensors
            timestamp_start_test = time.time()
            self.read_temperature_sensors()
            self.meta_data['duration']['temperature_sensors'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during temperature sensor readout routine: {0}'.format(e))

        try:    # Injection DAC calibration
            timestamp_start_test = time.time()
            self.measure_injection_dacs()
            self.meta_data['duration']['injection_dacs'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during injection DAC calibration routine: {0}'.format(e))

        self.full_powercycle()
        self.periphery.bdaq.close()

        with open(os.path.join(self.proj_dir, 'testbench.yaml'), 'r') as f:
            original_testbench = yaml.safe_load(f)
        original_testbench['general']['output_directory'] = self.working_dir

        try:    # Register test
            timestamp_start_test = time.time()
            register_config = copy.deepcopy(self.chip_config)
            register_config.update(self.config['register_test'])
            testbench = copy.deepcopy(original_testbench)
            with RegisterTest(scan_config=register_config, bench_config=testbench, record_chip_status=False) as test:
                test.start()
                test.analyze()
            self.meta_data['duration']['register_test'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during register test: {0}'.format(e))

        try:    # Digital scan
            timestamp_start_test = time.time()
            digital_config = copy.deepcopy(self.chip_config)
            digital_config.update(self.config['digital_scan'])
            testbench = copy.deepcopy(original_testbench)
            with DigitalScan(scan_config=digital_config, bench_config=testbench, record_chip_status=False) as scan:
                scan.start()
                power_results['Advanced config'] = self.periphery.get_chip_power()  # Power consumption in advanced configuration
                scan.analyze()
            self.meta_data['duration']['digital_scan'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during digital scan: {0}'.format(e))

        try:    # Analog scan
            timestamp_start_test = time.time()
            analog_config = copy.deepcopy(self.chip_config)
            analog_config.update(self.config['analog_scan'])
            testbench = copy.deepcopy(original_testbench)
            with AnalogScan(scan_config=analog_config, bench_config=testbench, record_chip_status=False) as scan:
                scan.start()
                scan.analyze()
            self.meta_data['duration']['analog_scan'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during analog scan: {0}'.format(e))
        self.dump_analog_data('Power results', power_results)

        try:    # Threshold tuning
            timestamp_start_test = time.time()
            tuning_config = copy.deepcopy(self.chip_config)
            tuning_config.update(self.config['tuning'])
            testbench = copy.deepcopy(original_testbench)
            with GDACTuning(scan_config=tuning_config, bench_config=testbench, record_chip_status=False) as global_tuning:
                global_tuning.start()
                global_tuning.analyze()

            testbench = copy.deepcopy(original_testbench)
            with TDACTuning(scan_config=tuning_config, bench_config=testbench, record_chip_status=False) as local_tuning:
                local_tuning.start()
                local_tuning.analyze()

            tuning_config['maskfile'] = 'auto'    # Definitely use maskfile created by tuning from here on
            # tot_params = tuning_config.copy()
            # tot_params.update(self.config['tot_tuning'])
            # testbench = copy.deepcopy(original_testbench)
            # with TotTuning(scan_config=tot_params, bench_config=testbench, record_chip_status=False) as tot_tuning:
            #     tot_tuning.start()
            #     tot_tuning.analyze()

            # testbench = copy.deepcopy(original_testbench)
            # with TDACTuning(scan_config=tuning_config, bench_config=testbench, record_chip_status=False) as local_tuning:
            #     local_tuning.start()
            #     local_tuning.analyze()

            testbench = copy.deepcopy(original_testbench)
            with NoiseOccScan(scan_config=tuning_config, bench_config=testbench, record_chip_status=False) as noise_occ_scan:
                noise_occ_scan.start()
                noise_occ_scan.analyze()

            testbench = copy.deepcopy(original_testbench)
            with StuckPixelScan(scan_config=tuning_config, bench_config=testbench, record_chip_status=False) as stuck_pix_scan:
                stuck_pix_scan.start()
                stuck_pix_scan.analyze()

            # Calculate scan range for threshold scan
            target_threshold = tuning_config['VCAL_HIGH'] - tuning_config['VCAL_MED']
            tuning_config['VCAL_HIGH_start'] = tuning_config['VCAL_MED'] + target_threshold - 30
            tuning_config['VCAL_HIGH_stop'] = tuning_config['VCAL_MED'] + target_threshold + 30
            tuning_config['VCAL_HIGH_step'] = 2
            testbench = copy.deepcopy(original_testbench)
            with ThresholdScan(scan_config=tuning_config, bench_config=testbench, record_chip_status=False) as thr_scan:
                thr_scan.start()
                thr_scan.analyze()

            self.meta_data['duration']['threshold_tuning'] = time.time() - timestamp_start_test
        except Exception as e:
            self.logger.exception('Unhandled exception during tuning: {0}'.format(e))

        duration = time.time() - timestamp_start_chip
        self.meta_data['duration']['total'] = duration
        self.logger.success('Done! Probing this chip took {0:1.2f} minutes!'.format(duration / 60))
        self.periphery.power_off_chip()


if __name__ == '__main__':
    working_dir = os.getcwd()

    chipprober = ChipProber(chip_sn=CHIP_SN, configuration=configuration, working_dir=working_dir, restart_bdaq=False)
    try:
        chipprober.main()
    except Exception as e:
        chipprober.close()
        raise e
