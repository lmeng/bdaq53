#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Wafer probing script to probe a full wafer
'''

import time
import os
import yaml
import logging
import threading
from slack import WebClient
import shutil
from basil.dut import Dut
from bdaq53.tests.utils import MockLoggingHandler


class WaferProber(object):
    def __init__(self, chip_type, wafer_sn_rd53, n_chips, wafer_sn_tsmc, location, write_efuses,
                 working_directory, start_analysis=False, analysed_data=None, notifications=None):
        self.chip_type = chip_type
        self.wafer_sn_rd53 = wafer_sn_rd53
        self.wafer_sn_tsmc = wafer_sn_tsmc
        self.n_chips = n_chips
        self.probe_location = location
        self.write_efuses = write_efuses
        self.threads = []
        self.start_analysis = start_analysis
        self.analysed_data = analysed_data

        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        self.logger = logging.getLogger('Waferprobing')
        self.logger.success = lambda msg, *args, **kwargs: self.logger.log(logging.SUCCESS, msg, *args, **kwargs)

        self.confirm_configuration(chip_type, wafer_sn_rd53, n_chips, wafer_sn_tsmc, location, write_efuses)

        if self.chip_type.lower() == 'itkpixv1':
            testbench_path = os.path.join(self.proj_dir, 'waferprobing', 'testbench_ITkPixV1.yaml')
        elif self.chip_type.lower() == 'itkpixv2':
            testbench_path = os.path.join(self.proj_dir, 'waferprobing', 'testbench_ITkPixV2.yaml')
        else:
            testbench_path = os.path.join(self.proj_dir, 'testbench.yaml')
        with open(testbench_path, 'r') as f:
            self.testbench = yaml.safe_load(f)
        self.working_dir = os.path.join(working_directory, 'probing_wafer_' + self.wafer_sn_rd53)
        if not os.path.exists(self.working_dir):
            os.makedirs(self.working_dir)

        logfile = os.path.join(self.working_dir, 'probing_wafer_' + self.wafer_sn_rd53 + '.log')
        self.fh = logging.FileHandler(logfile)
        self.fh.setLevel(logging.INFO)
        self.fh.setFormatter(logging.Formatter("%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s"))
        self.logger.addHandler(self.fh)

        self.logging_handler = MockLoggingHandler()

        self.logger.info('Running with PID {}'.format(os.getpid()))

        if notifications is not None:
            self.testbench['notifications'] = notifications
        if self.testbench['notifications']['slack_token'] == '' or (len(self.testbench['notifications']['slack_users']) == 1 and self.testbench['notifications']['slack_users'][0] == 'AAAAAA123'):
            self.slack = None
        else:
            if os.path.isfile(os.path.expanduser(self.testbench['notifications']['slack_token'])):
                with open(os.path.expanduser(self.testbench['notifications']['slack_token']), 'r') as token_file:
                    token = token_file.read().strip()
            else:
                token = self.testbench['notifications']['slack_token']
            self.slack = WebClient(token)

        self.metadata_file = os.path.join(self.working_dir, 'probing_wafer_{}_metadata.yaml'.format(self.wafer_sn_rd53))

        self.init_devices()

    def confirm_configuration(self, chip_type, wafer_sn_rd53, n_chips, wafer_sn_tsmc, location, write_efuses):
        if any(v is None or v == "" for v in [wafer_sn_rd53, wafer_sn_tsmc, location]):
            raise ValueError("Invalid wafer configuration. Please check bdaq53/waferprobing/configuration.yaml")
        if location not in ["Unprobed", "Bonn", "Glasgow", "Paris", "HongKong"]:
            raise ValueError("Invalid probing location. Please check bdaq53/waferprobing/configuration.yaml")
        if wafer_sn_rd53 in ["0x000"]:
            raise ValueError("Invalid Atlas S/N. Please check bdaq53/waferprobing/configuration.yaml")
        if wafer_sn_tsmc in ["XXXXXXX-YYZZ"]:
            raise ValueError("Invalid TSMC S/N. Please check bdaq53/waferprobing/configuration.yaml")
        self.logger.info("Probing %d %s chips on wafer:" % (n_chips, chip_type))
        self.logger.info("RD53 S/N: %s" % (wafer_sn_rd53))
        self.logger.info("TSMC S/N: %s" % (wafer_sn_tsmc))
        self.logger.info("In %s" % (location))
        if not write_efuses:
            self.logger.warning("Efuses do not get written")
        inp = input("Do you confirm? [Y/n] ")
        if inp != "Y":
            raise ValueError("Wafer configuration not confirmed")

    def init_devices(self):
        devices = Dut(os.path.join(self.proj_dir, 'waferprobing' + os.sep + 'probestation.yaml'))
        devices.init()
        self.prober = devices['Prober']

    def notify(self, message):
        if self.slack is not None:
            try:
                for user in self.testbench['notifications']['slack_users']:
                    self.slack.chat_postMessage(channel=user, text=message, username='BDAQ53 Bot', icon_emoji=':robot_face:')
            except Exception as e:
                self.logger.error('Notification error: {0}'.format(e))
        else:
            self.logger.debug('Slack notifications are not set up in testbench.yaml')

    def post_file(self, message, file_name):
        if self.slack is not None:
            try:
                for user in self.testbench['notifications']['slack_users']:
                    self.slack.files_upload(channels=user, initial_comment=message, file=file_name)
            except Exception as e:
                self.logger.error('Notification error: {0}'.format(e))
        else:
            self.logger.debug('Slack notifications are not set up in testbench.yaml')

    def dump_metadata(self, name, data):
        try:
            with open(self.metadata_file, 'r') as yamlfile:
                metadata = yaml.safe_load(yamlfile)
                metadata[name] = data
        except (IOError, TypeError):
            metadata = {}
            metadata[name] = data
        with open(self.metadata_file, 'w') as yamlfile:
            yaml.safe_dump(metadata, yamlfile)

    def next_chip(self, wait_time=5):
        self.logger.info('Move to next chip...')
        self.prober.goto_next_die()
        time.sleep(wait_time)

    def main(self):
        self.notify('Start probing wafer {}...'.format(self.wafer_sn_rd53))
        timestamp_start_wafer = time.time()

        for i in range(1, self.n_chips + 1):
            kwargs = {}
            # Get chip probing class according to chip type
            if self.chip_type.lower() == 'rd53a':
                from bdaq53.waferprobing.probe_chip_RD53A import ChipProber, configuration as chip_prober_config
                cp_id = self.prober.get_die()
                chip_sn = '0x{0:02X}{1:0X}{2:0X}'.format(int(self.wafer_sn_rd53), int(cp_id[0]), int(cp_id[1]))
            elif self.chip_type.lower() == 'itkpixv1':
                from bdaq53.waferprobing.probe_chip_ITkPixV1 import ChipProber, configuration as chip_prober_config
                cp_id = self.prober.get_die()
                chip_sn = '0x{0:03X}{1:0X}{2:0X}'.format(int(self.wafer_sn_rd53, 16), int(cp_id[1]), int(cp_id[0]))
            elif self.chip_type.lower() == 'itkpixv2':
                from bdaq53.waferprobing.probe_chip_ITkPixV2 import ChipProber, configuration as chip_prober_config
                cp_id = self.prober.get_die()
                chip_sn = '0x{0:03X}{1:0X}{2:0X}'.format(int(self.wafer_sn_rd53, 16), int(cp_id[1]), int(cp_id[0]))
                kwargs["write_efuses"] = self.write_efuses
            else:
                raise NotImplementedError('Chip type {0} is not implemented yet!'.format(self.chip_type))

            # Get Chip ID from probestation
            print(chip_sn)

            # Make chip subdirectory
            chip_working_dir = os.path.join(self.working_dir, chip_sn)
            os.mkdir(chip_working_dir)

            def _probe_chip():
                self.logging_handler.reset()
                self.chip_prober = ChipProber(chip_sn=chip_sn, configuration=chip_prober_config, working_dir=chip_working_dir, external_logfile_handlers=[self.fh, self.logging_handler], restart_bdaq=False, probe_location=self.probe_location, testbench=self.testbench)

                if i == 1:  # First chip
                    wafer_sn_tsmc = {}
                    wafer_sn_tsmc['wafer_sn_tsmc'] = self.wafer_sn_tsmc
                    self.dump_metadata('general', self.chip_prober.get_general_information())
                    self.dump_metadata('tsmc_wno', wafer_sn_tsmc)

                self.chip_prober.main(**kwargs)
                if self.chip_type.lower() in ['itkpixv1', 'itkpixv2']:
                    source_dir = os.path.join(chip_working_dir, 'module_0', chip_sn)
                    target_dir = chip_working_dir

                    def move_files(sd, td):
                        for ana_proc in self.chip_prober.running_analysis_processes:
                            ana_proc.join()
                        file_names = os.listdir(sd)
                        for file_name in file_names:
                            shutil.move(os.path.join(sd, file_name), td)
                        shutil.rmtree(os.path.join(td, 'module_0'))
                    if len(self.chip_prober.running_analysis_processes) > 0:
                        x = threading.Thread(target=move_files, args=(source_dir, target_dir))
                        self.threads.append(x)
                        x.start()
                    else:
                        move_files(source_dir, target_dir)
            try:
                _probe_chip()
                ex = next((msg for err in ['does not satisfy version requirements']
                           for msg in self.logging_handler.messages['error'] if err in msg), None)
                if ex is not None:
                    raise Exception(ex)
            except (Exception, KeyboardInterrupt) as e:
                if type(e) == RuntimeError and 'Timeout while waiting for Aurora Sync on' in str(e):
                    self.logger.warning('Retry probing chip')
                    try:
                        self.chip_prober.close()
                        os.rename(chip_working_dir, chip_working_dir + '_old')
                        os.mkdir(chip_working_dir)
                        _probe_chip()
                    except (Exception, KeyboardInterrupt) as e2:
                        self.logger.error('ChipProber ended twice with exceptions: {0}, {1}'.format(e, e2))
                        self.notify('Probing chip {0} at position {1} ended twice with exceptions: {2}, {3}'.format(i, chip_sn, e, e2))
                if type(e) == Exception and 'does not satisfy version requirements' in str(e):
                    self.notify('Stop probing: chip {0} at position {1} ended with an exception: {2}'.format(i, chip_sn, e))
                    raise
                if type(e) == KeyboardInterrupt:
                    self.notify('Probing routine interrupted at chip {0} at position {1}.'.format(i, chip_sn))
                    raise
                else:
                    self.logger.error('ChipProber ended with an exception: {}'.format(e))
                    self.notify('Probing chip {0} at position {1} ended with an exception: {2}'.format(i, chip_sn, e))

            finally:
                self.dump_metadata(chip_sn, self.chip_prober.get_meta_data())
                self.chip_prober.close()

            if i == self.n_chips:  # Last chip
                self.prober.separate()
                self.logger.success('All done! Probing this wafer took %1.2f hours!' % ((time.time() - timestamp_start_wafer) / 3600))
                self.notify('All done! Probing this wafer took %1.2f hours!' % ((time.time() - timestamp_start_wafer) / 3600))
            else:
                self.next_chip()
        for thr in self.threads:
            thr.join()
        self.notify('WaferProber stopped after {} chips!'.format(i))

        if self.start_analysis:
            self.analyse_wafer()

    def analyse_wafer(self):
        # Get chip probing class according to chip type
        if self.chip_type.lower() == 'itkpixv1':
            from bdaq53.waferprobing.analysis.ITkPix.analyze_wafer_ITkPixV1 import WaferAnalysis, PlotItkPixWafer
        elif self.chip_type.lower() == 'itkpixv2':
            from bdaq53.waferprobing.analysis.ITkPix.analyze_wafer_ITkPixV2 import WaferAnalysis, PlotItkPixWafer
        else:
            raise NotImplementedError('Analysis for chip type {0} is not implemented yet!'.format(self.chip_type))

        with WaferAnalysis(self.working_dir, out_dir=self.analysed_data) as wpa:
            wpa.analyze()
        PLOT_DATA = os.path.join(self.analysed_data, self.wafer_sn_rd53, "wafer_summary_%s.h5" % self.wafer_sn_rd53)
        plotter = PlotItkPixWafer(summary_h5_filename=PLOT_DATA)
        plotter.plot()
        self.post_file('Wafer {} has final yield of {:.1f} %'.format(self.wafer_sn_rd53, plotter.final_yield),
                       plotter.outfile_name.replace('wafer_summary_', 'wafer_map_') + '-' + plotter.wafer_sn + '.pdf')


if __name__ == '__main__':
    waferprobing_dir = os.path.dirname(os.path.abspath(__file__))
    configuration_path = os.path.join(waferprobing_dir, 'configuration.yaml')
    with open(configuration_path, 'r') as f:
        conf = yaml.safe_load(f)
    print(conf)
    waferprober = WaferProber(**conf)
    waferprober.main()
