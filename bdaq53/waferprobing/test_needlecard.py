#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Test I2C communication to a probe card connected to bdaq53
'''

import logging

from bdaq53.system.bdaq53_base import BDAQ53
# import time
# from bdaq53.waferprobing.needlecard_RD53A import Needlecard
from bdaq53.waferprobing.needlecard_RD53B import Needlecard

logger = logging.getLogger('NeedleCardTest')

bdaq = BDAQ53()
bdaq.init()

card = Needlecard(bdaq['i2c'])
card.init()

logger.info("Edge Sensor output: {0}. Try to remove the 'DET_GRD' jumper (only on RD53A needlecard) and see if the output changes".format(card.get_edge_sensors()))

# Setup MUX to measure VINA
card.set_adc_mux('VINA')

# card.load_eeprom_config()
# print(card._eeprom_storage_map['PC_VERSION'][0])
# print(card._eeprom_storage_map['DATAFORMAT_VERSION'][0])
# print(card._eeprom_storage_map['LAST_CHANGE'][0])
# print(card._eeprom_storage_map['RIREF'][0])
# print(card._eeprom_storage_map['RIMUX'][0])
# print(card._eeprom_storage_map['RVREF_ADC'][0])
# print(card._eeprom_storage_map['REXTA'][0])
# print(card._eeprom_storage_map['REXTD'][0])
# print(card._eeprom_storage_map['RVREFA'][0])
# print(card._eeprom_storage_map['RVREFD'][0])
# print(card._eeprom_storage_map['RIOFS'][0])
# print(card._eeprom_storage_map['RIOFS_LP'][0])
# print(card._eeprom_storage_map['PC_ID'][0])
#
# card.write_from_xlsx('/faust/user/mstandke/RD53_env/RD53B_SIM/bdaq53/bdaq53/waferprobing/2021_06_02-probecard_resistances.xlsx', 7)
# print("\n\n")
# time.sleep(1)
# card.load_eeprom_config()
# print(card._eeprom_storage_map['PC_VERSION'][0])
# print(card._eeprom_storage_map['DATAFORMAT_VERSION'][0])
# print(card._eeprom_storage_map['LAST_CHANGE'][0])
# print(card._eeprom_storage_map['RIREF'][0])
# print(card._eeprom_storage_map['RIMUX'][0])
# print(card._eeprom_storage_map['RVREF_ADC'][0])
# print(card._eeprom_storage_map['REXTA'][0])
# print(card._eeprom_storage_map['REXTD'][0])
# print(card._eeprom_storage_map['RVREFA'][0])
# print(card._eeprom_storage_map['RVREFD'][0])
# print(card._eeprom_storage_map['RIOFS'][0])
# print(card._eeprom_storage_map['RIOFS_LP'][0])
# print(card._eeprom_storage_map['PC_ID'][0])

# state = False
# while True:
#     state = not state
#     time.sleep(3)
#     logger.info("Switching {}...".format('ON' if state else 'OFF'))
#     # card.write_gpio_expander('EN_VDD_SHUNT', state)
#     # card.write_gpio_expander('EN_VDD_EFUSE', state)
#     # card.write_gpio_expander('EN_R_IMUX', state)
