import time
from basil.dut import Dut


PS_STATE = False


devices = Dut('waferprobing.yaml')
devices.init()


print(devices['AUX_powersupply'].get_name())
devices['AUX1'].set_enable(on=PS_STATE)
devices['AUX2'].set_enable(on=PS_STATE)

time.sleep(1)

print(devices['SCC_powersupply'].get_name())
devices['BDAQ'].set_enable(on=True)
devices['VINA'].set_enable(on=PS_STATE)
devices['VIND'].set_enable(on=PS_STATE)

time.sleep(1)
devices['smu'].off()
print(devices['smu'].get_name())
