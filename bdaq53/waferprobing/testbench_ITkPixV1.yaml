general: # General configuration
  output_directory: # Top-level output data directory, default is the current folder where the bdaq script is started
  use_database: False # Check if chip is in data base, log error if not
  abort_on_rx_error: True # Abort scan when RX error occurs
  interlock: False #  If True, DCS data is written and scans abort if interlock is triggered. 
  max_reg_read_failures: 10  # Number of register read failures each chip can have in total (empty for no limit)

periphery: # Configuration of the BDAQ53 Periphery module
  enable_periphery: False
  monitoring: False # Monitor all connected powersupplies and sensors regularly
  monitoring_interval: 10 # Interval for DCS monitoring in seconds
  analog_monitoring_board: False

# Connected Modules
modules:
  module_0: # Arbitrary name of module, defines folder name with chip sub folders
    identifier: "unknown" # Module/wafer/PCB identifier, has to be given (e.g. SCC number)
    module_type: # Module_type defined in modules/module_types.yaml (e.g. dual, common_quad, SPQ). Leave empty for bare chip.
    powersupply:
    power_cycle: False # power cycle all chip of this module before scan start
    chip_0: # Arbitrary name of chip, defines folder name with chip data
      chip_sn: "0x0001"
      chip_type: "itkpixv1"
      chip_id: 15
      receiver: "rx0" # Aurora receiver channel (ranges from 'rx0' to 'rxN', N board-dependent)
      chip_config_file: # If defined: use config from in file (either .cfg.yaml or .h5). If not defined use chip config of latest scan and std. config if no previous scan exists
      record_chip_status: False # Add chip statuses to the output files after the scan (link errors and powering infos)
      use_good_pixels_diff: False
      use_ptot: False
      send_data: "tcp://127.0.0.1:5500" # Socket address of online monitor

hardware: # Setup-specific hardware settings
  bypass_mode: False # Configure chip and BDAQ board for bypass mode. You have to provide all clocks externally!
  enable_NTC: False # Only enable if you know you have the correct resistors mounted on the BDAQ board!

TLU:
  TRIGGER_MODE: 0 # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
  TRIGGER_SELECT: 1 # Selecting trigger input: HitOr (individual, TDC loop-through) (16), RX1 (multi purpose) (8), RX0 (TDC loop-trough) (4), HitOR [DP_ML_5 and mDP] (logical OR of all eight lines) (3), HitOR [mDP only] (logical OR of all four lines) (2), HitOR [DP_ML_5 only] (logical OR of all four lines) (1), disabled (0)
  TRIGGER_INVERT: 0 # Inverting trigger input: HitOr (individual, TDC loop-through) (16), RX1 (multi purpose) (8), RX0 (TDC loop-trough) (4), HitOR [DP_ML_5 and mDP] (logical OR of all eight lines) (3), HitOR [mDP only] (logical OR of all four lines) (2), HitOR [DP_ML_5 only] (logical OR of all four lines) (1), disabled (0)
  TRIGGER_LOW_TIMEOUT: 0 # Maximum wait cycles for TLU trigger low.
  TRIGGER_VETO_SELECT: 0 # Selecting trigger veto: AZ VETO (2), RX FIFO full (1), disabled (0). Set to (2) if SYNC FE is enabled.
  TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES: 5 # TLU trigger minimum length in TLU clock cycles
  DATA_FORMAT: 0 # Select trigger data format: only trigger number (0), only time stamp (1), combined, 15 bit time stamp + 16 bit trigger number (2)
  EN_TLU_VETO: 0 # Assert TLU veto when external veto. Activate this in order to VETO triggers if SYNC FE is enabled.
  TRIGGER_DATA_DELAY: 31 # Depends on the cable length and should be adjusted (run scan/tune_tlu.py)

TDC:
  EN_WRITE_TIMESTAMP: 1 # Writing trigger timestamp
  EN_TRIGGER_DIST: 1 # Measuring trigger to TDC delay with 640MHz clock
  EN_NO_WRITE_TRIG_ERR: 1 # Writing TDC word only if valid trigger occurred
  EN_INVERT_TDC: 0 # Inverting TDC input
  EN_INVERT_TRIGGER: 0 # Inverting trigger input, e.g. for using Test output from EUDET TLU.

calibration: # Setup-specific calibration constants
  bdaq_ntc: # Resistors on BDAQ board for NTC readout
    R16: 200.00e3
    R17: 4.75e3
    R19: 2.50e3

notifications: # Notification settings
  enable_notifications: False
  slack_token: "~/slack_api_token"
  slack_users:
    - AAAAAA123

# Standard analysis settings
# Scans might overwrite these settings if needed.
# Detailed description of parameters in bdaq53/analysis/analysis.py
analysis:
  skip: False # Omit analysis in scans
  create_pdf: True # Create analysis summary pdf
  module_plotting: True # Create combined plots for chip in a module
  store_hits: False # store hit table
  cluster_hits: False # store cluster data
  analyze_tdc: False # analyze TDC words
  analyze_ptot: False # analyze PTOT words (only possible for RD53B)
  use_tdc_trigger_dist: False # analyze TDC to TRG distance
  align_method: 0 # how to detect new events
  chunk_size: 1000000 # scales amount of data in RAM (~150 MB)
  blocking: False # block main process during analysis

