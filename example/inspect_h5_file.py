# Example how to inspect .h5 file
import os
import tables as tb
import bdaq53  # noqa: E731

# Use fixtures as example
bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))
data_file = os.path.join(data_folder, 'digital_scan_interpreted_result.h5') # path to interpreted.h5 file

# Open data file
with tb.open_file(data_file, mode='r') as in_file_h5:
    hist_occ = in_file_h5.root.HistOcc[:].sum(axis=-1) # 2D hit map
    hist_tot = in_file_h5.root.HistTot[:] # 2D tot histograms

# Print hist occ
print(hist_occ, hist_occ.shape)
print(hist_tot, hist_tot.shape)
