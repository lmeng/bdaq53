from bdaq53.analysis.plotting import Plotting

analyzed_data_file = '.h5'


with Plotting(analyzed_data_file=analyzed_data_file, pdf_file=None, level='preliminary',
              internal=False, save_single_pdf=False, save_png=False) as p:
    p.create_standard_plots()
