# ------------------------------------------------------------
# BDAQ53: Simple hardware test
# Basic DAQ hardware and chip configuration
#
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import os
import time
import numpy as np
import logging
import yaml
from basil.dut import Dut
from bdaq53.system.bdaq53_base import BDAQ53
from bdaq53.chips.rd53a import RD53A

rx_lanes = 1


# Initialization
with open('..' + os.sep + 'bdaq53' + os.sep + 'system' + os.sep + 'bdaq53.yaml', 'r') as f:
    cnfg = yaml.full_load(f)

bdaq = BDAQ53(cnfg)
bdaq.init()

chip = RD53A(bdaq)
chip.init(tx_lanes=rx_lanes)

# Testing
# TODO

# Closing
bdaq.close()
