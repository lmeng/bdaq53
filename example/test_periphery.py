#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Test if your periphery (e.g. SCC powersupply) is set up correctly
'''

import time

from bdaq53.system.bdaq53_base import BDAQ53
from bdaq53.chips.rd53a import RD53A
from bdaq53.system.periphery import BDAQ53Periphery


if __name__ == '__main__':
    periphery = BDAQ53Periphery()
    periphery.init()

    periphery.power_on_BDAQ()

    bdaq = BDAQ53()
    bdaq.init()

    periphery.power_on_module('module_0')

    chip = RD53A(bdaq=bdaq)
    chip.init()

    periphery.start_monitoring()

    time.sleep(2)
    periphery.power_off_module('module_0')
    time.sleep(20)
    periphery.power_on_module('module_0')
    time.sleep(15)

    periphery.stop_monitoring()

    periphery.close()
