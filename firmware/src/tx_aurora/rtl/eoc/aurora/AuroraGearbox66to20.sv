
`ifndef AURORA_GEARBOX_66TO20__SV   // include guard
`define AURORA_GEARBOX_66TO20__SV

`timescale 1ns / 1ps

module AuroraGearbox66to20 (

   input wire Rst_b,
   input wire Clk,
   input wire [65:0] Data66,
			
   output logic [19:0] Data20,
   output logic DataNext

   ) ;

   logic [5:0] 	     counter;
   logic [19:0]      selected_data_20;
   logic [131:0]     buffer_132;
   logic 	     upper;
   
   always_ff @(posedge Clk) begin
      if(Rst_b == 1'b0)
	counter <= '{default:0};
      else
	if (counter[5])
	//if (counter == 6'd32)
		counter <= '{default:0};
	else
		counter <= counter + 6'b000001;
   end


   always_ff @(posedge Clk) begin
      if(Rst_b == 1'b0) 
	 buffer_132 <= {Data66,Data66};
      else
	if (DataNext)
	  if (upper)
	    buffer_132 <= {Data66,buffer_132[65:0]};
	  else
	    buffer_132 <= {buffer_132[131:66],Data66};
   end

   assign DataNext = ((counter%3) == 2) & (counter != 5'h2);
   assign upper = ~((counter%6) == 5);

   function automatic logic [19:0] slice( logic [131:0] vector, logic [6:0] seg);
      logic [131:0] vector_rot;
      vector_rot = (vector >> ((seg*20)%132)) | (vector << (132-(seg*20)%132));
      return vector_rot[19:0];
   endfunction // slice

   always_comb begin
	unique case (counter)
	  6'd0 : selected_data_20 = slice(buffer_132,6'd0);
	  6'd1 : selected_data_20 = slice(buffer_132,6'd1);
	  6'd2 : selected_data_20 = slice(buffer_132,6'd2);
	  6'd3 : selected_data_20 = slice(buffer_132,6'd3);
	  6'd4 : selected_data_20 = slice(buffer_132,6'd4);
	  6'd5 : selected_data_20 = slice(buffer_132,6'd5);
	  6'd6 : selected_data_20 = slice(buffer_132,6'd6);
	  6'd7 : selected_data_20 = slice(buffer_132,6'd7);
	  6'd8 : selected_data_20 = slice(buffer_132,6'd8);
	  6'd9 : selected_data_20 = slice(buffer_132,6'd9);
	  6'd10 : selected_data_20 = slice(buffer_132,6'd10);
	  6'd11 : selected_data_20 = slice(buffer_132,6'd11);
	  6'd12 : selected_data_20 = slice(buffer_132,6'd12);
	  6'd13 : selected_data_20 = slice(buffer_132,6'd13);
	  6'd14 : selected_data_20 = slice(buffer_132,6'd14);
	  6'd15 : selected_data_20 = slice(buffer_132,6'd15);
	  6'd16 : selected_data_20 = slice(buffer_132,6'd16);
	  6'd17 : selected_data_20 = slice(buffer_132,6'd17);
	  6'd18 : selected_data_20 = slice(buffer_132,6'd18);
	  6'd19 : selected_data_20 = slice(buffer_132,6'd19);
	  6'd20 : selected_data_20 = slice(buffer_132,6'd20);
	  6'd21 : selected_data_20 = slice(buffer_132,6'd21);
	  6'd22 : selected_data_20 = slice(buffer_132,6'd22);
	  6'd23 : selected_data_20 = slice(buffer_132,6'd23);
	  6'd24 : selected_data_20 = slice(buffer_132,6'd24);
	  6'd25 : selected_data_20 = slice(buffer_132,6'd25);
	  6'd26 : selected_data_20 = slice(buffer_132,6'd26);
	  6'd27 : selected_data_20 = slice(buffer_132,6'd27);
	  6'd28 : selected_data_20 = slice(buffer_132,6'd28);
	  6'd29 : selected_data_20 = slice(buffer_132,6'd29);
	  6'd30 : selected_data_20 = slice(buffer_132,6'd30);
	  6'd31 : selected_data_20 = slice(buffer_132,6'd31);
	  6'd32 : selected_data_20 = slice(buffer_132,6'd32);
	endcase
   end

   always_ff @(posedge Clk) begin
      Data20 <= selected_data_20;
   end

endmodule : AuroraGearbox66to20

`endif   // AURORA_GEARBOX_66TO20__SV

