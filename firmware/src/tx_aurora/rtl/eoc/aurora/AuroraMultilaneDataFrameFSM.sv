
`ifndef AURORA_MULTILANE_DATA_FRAME_FSM__SV
`define AURORA_MULTILANE_DATA_FRAME_FSM__SV

`timescale 1ns/1ps

`include "rtl/eoc/aurora/AuroraDefines.sv"

module AuroraMultilaneDataFrameFSM (

   input wire LaneReady,
   input wire FIFO_Empty,
   input wire [7:0][31:0] FIFO_Data,
   input wire [7:0] FIFO_DataMask,
   input wire EndOfFrame,
   input wire BlockSent,

   input wire [7:0] CompleteDataMask,

   input wire Clk,
   input wire Rst_b,

   output logic FIFO_Read,
   output logic [3:0][63:0] DataToSend,
   output logic SendBlock,
   output logic [3:0][3:0] BytesToSend);

   enum 				{NOT_READY, NO_DATA, PREPARE_READ, READ_DATA, SEND_EOF, STORE_TO_SEND} data_fsm_state;//synopsys keep_signal_name "data_fsm_state"
   logic 				data_good;
   logic [3:0][63:0] 			stored_datatosend;
   //logic [3:0][3:0] 			stored_bytestosend;
   logic [7:0] 				stored_datamask;
   logic 				stored_eof;
   

   always_ff @(posedge Clk) begin
      if (Rst_b == 1'b0) begin
	 data_fsm_state <= NOT_READY;
	 FIFO_Read <= 1'b0;
	 DataToSend <= {4{64'h0}};
	 SendBlock <= 1'b0;
	 BytesToSend <= {4{4'h0}};
	 data_good <= 1'b0;
	 
      end else begin
	 if (LaneReady) begin
	    case(data_fsm_state)
	      NOT_READY : begin
		 if (FIFO_Empty) begin
		    data_fsm_state <= NO_DATA;
		 end else begin
		    data_fsm_state <= PREPARE_READ;
		    FIFO_Read <= 1'b1;
		 end
	      end

	      NO_DATA : begin
		 if (~FIFO_Empty) begin
		    data_fsm_state <= PREPARE_READ;
		    FIFO_Read <= 1'b1;
		 end
		 if (BlockSent) begin
		    SendBlock <= 1'b0;
		 end
	      end

	      PREPARE_READ : begin
		 data_fsm_state <= READ_DATA;
		 FIFO_Read <= 1'b1;
		 if (BlockSent) begin
		    SendBlock <= 1'b0;
		 end
	      end

	      SEND_EOF : begin
		 if (BlockSent) begin
		    SendBlock <= 1'b0;
		 end
		 if (~SendBlock | BlockSent) begin
		    SendBlock <= 1'b1;
		    DataToSend <= '0;
		    BytesToSend[0] <= 4'h0;
		    BytesToSend[1] <= `DISABLE_DATA;
		    BytesToSend[2] <= `DISABLE_DATA;
		    BytesToSend[3] <= `DISABLE_DATA;
		    if (~FIFO_Empty) begin
		       FIFO_Read <= 1'b1;
		       if (data_good) begin
			  data_fsm_state <= READ_DATA;
		       end else begin
			  data_fsm_state <= PREPARE_READ;
		       end
		    end else begin
		       FIFO_Read <= 1'b0;
		       if (data_good) begin
			  data_good <= 1'b0;
			  data_fsm_state <= STORE_TO_SEND;
			  stored_datatosend <= FIFO_Data;
			  /*
			  stored_bytestosend[0][1:0] <= FIFO_DataMask[0] ? 2'b11 : 2'b00;
			  stored_bytestosend[0][3:2] <= FIFO_DataMask[1] ? 2'b11 : 2'b00;
			  stored_bytestosend[1][1:0] <= FIFO_DataMask[2] ? 2'b11 : 2'b00;
			  stored_bytestosend[1][3:2] <= FIFO_DataMask[3] ? 2'b11 : 2'b00;
			  stored_bytestosend[2][1:0] <= FIFO_DataMask[4] ? 2'b11 : 2'b00;
			  stored_bytestosend[2][3:2] <= FIFO_DataMask[5] ? 2'b11 : 2'b00;
			  stored_bytestosend[3][1:0] <= FIFO_DataMask[6] ? 2'b11 : 2'b00;
			  stored_bytestosend[3][3:2] <= FIFO_DataMask[7] ? 2'b11 : 2'b00;
			   */
			  stored_datamask <= FIFO_DataMask;
			  stored_eof <= EndOfFrame;
		       end else begin
			  data_fsm_state <= NO_DATA;
		       end // else: !if(data_good)
		    end // else: !if(~FIFO_Empty)
		 end // if (~SendBlock | BlockSent)
	      end // case: SEND_EOF

	      STORE_TO_SEND : begin
		 if (BlockSent) begin
		    SendBlock <= 1'b0;
		 end
		 if (~SendBlock | BlockSent) begin
		    if (stored_eof & (stored_datamask == CompleteDataMask)) begin
		       data_fsm_state <= SEND_EOF;
		    end else begin
		       if (~FIFO_Empty) begin
			  FIFO_Read <= 1'b1;
			  if (data_good) begin
			     data_fsm_state <= READ_DATA;
			  end else begin
			     data_fsm_state <= PREPARE_READ;
			  end
		       end else begin
			  FIFO_Read <= 1'b0;
			  if (data_good) begin
			     data_good <= 1'b0;
			     data_fsm_state <= STORE_TO_SEND;
			     stored_datatosend <= FIFO_Data;
			     stored_datamask <= FIFO_DataMask;
			     stored_eof <= EndOfFrame;
			  end else begin
			     data_fsm_state <= NO_DATA;
			  end
		       end
		    end // else: !if(EndOfFrame & (FIFO_DataMask == CompleteDataMask))
		    BytesToSend[0] <= stored_datamask[1] ? `FULL_DATA :
				     (stored_datamask[0] ? 4'b0100 : 4'b0000);
		    BytesToSend[1] <= stored_datamask[3] ? `FULL_DATA :
				     (stored_datamask[2] ? 4'b0100 : 
				      (stored_datamask[1] == 1'b0 ? `DISABLE_DATA : 4'b0000));
		    BytesToSend[2] <= stored_datamask[5] ? `FULL_DATA :
				     (stored_datamask[4] ? 4'b0100 : 
				      (stored_datamask[3] == 1'b0 ? `DISABLE_DATA : 4'b0000));
		    BytesToSend[3] <= stored_datamask[7] ? `FULL_DATA :
				     (stored_datamask[6] ? 4'b0100 : 
				      (stored_datamask[5] == 1'b0 ? `DISABLE_DATA : 4'b0000));
		    SendBlock <= 1'b1;
		    DataToSend <= stored_datatosend;
		    
		 end else begin // if (~SendBlock | BlockSent)
		    data_fsm_state <= STORE_TO_SEND;		    
		 end
	      end

	      READ_DATA : begin	 
		 if (BlockSent) begin
		    SendBlock <= 1'b0;
		 end
		 if (~SendBlock | BlockSent) begin
		    if (EndOfFrame & (FIFO_DataMask == CompleteDataMask)) begin
		       data_fsm_state <= SEND_EOF;
		       FIFO_Read <= 1'b0;
		       if (~FIFO_Empty) begin
			  data_good <= 1'b1;
		       end
		    end else begin
		       if (~FIFO_Empty) begin
			  FIFO_Read <= 1'b1;
			  data_fsm_state <= READ_DATA;
		       end else begin
			  FIFO_Read <= 1'b0;
			  data_fsm_state <= NO_DATA;
		       end
		    end // else: !if(EndOfFrame & (FIFO_DataMask == CompleteDataMask))
		    DataToSend <= FIFO_Data;
		    SendBlock <= 1'b1;
		    BytesToSend[0] <= FIFO_DataMask[1] ? `FULL_DATA :
				     (FIFO_DataMask[0] ? 4'b0100 : 4'b0000);
		    BytesToSend[1] <= FIFO_DataMask[3] ? `FULL_DATA :
				     (FIFO_DataMask[2] ? 4'b0100 : 
				      (FIFO_DataMask[1] == 1'b0 ? `DISABLE_DATA : 4'b0000));
		    BytesToSend[2] <= FIFO_DataMask[5] ? `FULL_DATA :
				     (FIFO_DataMask[4] ? 4'b0100 : 
				      (FIFO_DataMask[3] == 1'b0 ? `DISABLE_DATA : 4'b0000));
		    BytesToSend[3] <= FIFO_DataMask[7] ? `FULL_DATA :
				     (FIFO_DataMask[6] ? 4'b0100 : 
				      (FIFO_DataMask[5] == 1'b0 ? `DISABLE_DATA : 4'b0000));

		 end else begin // if (~SendBlock | BlockSent)
		    data_fsm_state <= STORE_TO_SEND;
		    FIFO_Read <= 1'b0;
		    if (~FIFO_Empty) begin
		       data_good <= 1'b1;
		    end
		    stored_datatosend <= FIFO_Data;
		    stored_datamask <= FIFO_DataMask;
		    stored_eof <= EndOfFrame;
		    
		 end
	      end

	    endcase
	 end else begin
	    data_fsm_state <= NOT_READY;
	 end
      end
   end // always_ff @ (posedge Clk or posedge Rst)

//---------------------------------// SVA properties ------------------//---------------------------------//---------------------------------
  //synopsys translate_off
   default clocking @(posedge Clk); endclocking

//--------------------------------NOT READY --------------------------------//
    property S_DEFAULT  ;
    disable iff (Rst_b)       (Rst_b == 1'b0)                                    |-> ##1 (data_fsm_state == NOT_READY) ;  
    endproperty 
    
    property STAY_NOT_READY  ;
    disable iff (Rst_b == 1'b0)   (LaneReady == 1'b0)  ##0  (data_fsm_state == NOT_READY)                           |-> ##1 (data_fsm_state == NOT_READY) ;  
    endproperty 

    property S_NOT_READY  ;
    disable iff (Rst_b == 1'b0)   (LaneReady == 1'b1)  ##0  (data_fsm_state == NOT_READY) ##0 (FIFO_Empty == 1'b1)  |-> ##1 (data_fsm_state == NO_DATA) ;  
    endproperty 

    property NOT_READY_ELSE  ;
    disable iff (Rst_b == 1'b0)   (LaneReady == 1'b1)  ##0  (data_fsm_state == NOT_READY) ##0 (FIFO_Empty == 1'b0)  |-> ##1 (data_fsm_state == PREPARE_READ) ;  
    endproperty 
//--------------------------------NO DATA ---------------------------------//
    property S_NO_DATA  ;
    disable iff (Rst_b == 1'b0)   (FIFO_Empty == 1'b0)  ##0  (data_fsm_state == NO_DATA)                            |-> ##1 (data_fsm_state == PREPARE_READ) ;  
    endproperty 

    property NO_DATA_ELSE  ;
    disable iff (Rst_b == 1'b0)   (FIFO_Empty == 1'b1)  ##0  (data_fsm_state == NO_DATA)                            |-> ##1 (data_fsm_state == NO_DATA) ;  
    endproperty 
//--------------------------------PREPEARE READ ---------------------------//
    property S_PREPARE_READ  ;
    disable iff (Rst_b == 1'b0)      (data_fsm_state == PREPARE_READ)                                              |-> ##1 (data_fsm_state == READ_DATA) ;  
    endproperty 
//--------------------------------SEND EOF --------------------------------//
    property S_SEND_EOF    ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == SEND_EOF) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b0) ##0 (data_good== 1'b1) |-> ##1 (data_fsm_state == READ_DATA) ;  
    endproperty 

    property SEND_EOF_ELSE0 ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == SEND_EOF) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b0) ##0 (data_good== 1'b0) |-> ##1 (data_fsm_state == PREPARE_READ) ;  
    endproperty 
 
    property S_SEND_EOF_ELSE1    ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == SEND_EOF) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b0) ##0 (data_good== 1'b1) |-> ##1 (data_fsm_state == STORE_TO_SEND) ;  
    endproperty 

    property SEND_EOF_ELSE2 ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == SEND_EOF) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b1) ##0 (data_good== 1'b0) |-> ##1 (data_fsm_state == NO_DATA) ;  
    endproperty 
//--------------------------------STORE TO SEND -----------------------------//
    property S_STORE_TO_SEND    ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == STORE_TO_SEND) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (stored_eof & (stored_datamask == CompleteDataMask)) |-> ##1 (data_fsm_state == SEND_EOF) ; 
    endproperty 

    property S_STORE_TO_SEND_ELSE0 ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == STORE_TO_SEND) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b0) ##0 (data_good== 1'b1) |-> ##1 (data_fsm_state == READ_DATA) ;  
    endproperty 
 
    property S_STORE_TO_SEND1    ; 
    disable iff (Rst_b == 1'b0)  (data_fsm_state == STORE_TO_SEND) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b0) ##0 (data_good== 1'b0) ##0 !(stored_eof & (stored_datamask == CompleteDataMask)) |-> ##1 (data_fsm_state == PREPARE_READ) ;  
    endproperty 

    property S_STORE_TO_SEND2 ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == STORE_TO_SEND) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b1) ##0 (data_good== 1'b0) ##0 !(stored_eof & (stored_datamask == CompleteDataMask)) |-> ##1 (data_fsm_state == NO_DATA) ;  
    endproperty 

    property S_STORE_TO_SEND3 ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == STORE_TO_SEND) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b0) ##0 (data_good== 1'b1) |-> ##1 (data_fsm_state == READ_DATA) ;  
    endproperty 

    property S_STORE_TO_SEND_ELSE    ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == STORE_TO_SEND) ##0 ((SendBlock == 1'b1) | (BlockSent== 1'b0)) |-> (data_fsm_state == STORE_TO_SEND) ; 
    endproperty 
//--------------------------------READ DATA -- -----------------------------//
    property S_READ_DATA    ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == READ_DATA) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (EndOfFrame & (FIFO_DataMask == CompleteDataMask)) |-> ##1 (data_fsm_state == SEND_EOF) ; 
    endproperty 

    property S_READ_DATA_ELSE0 ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == READ_DATA) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b0) ##0 !(EndOfFrame & (FIFO_DataMask == CompleteDataMask))  |-> ##1 (data_fsm_state == READ_DATA) ;  
    endproperty 
 
    property S_READ_DATA_ELSE1    ; 
    disable iff (Rst_b == 1'b0)  (data_fsm_state == READ_DATA) ##0 ((SendBlock == 1'b0) | (BlockSent== 1'b1)) ##0 (FIFO_Empty== 1'b1) ##0 !(EndOfFrame & (FIFO_DataMask == CompleteDataMask))   |-> ##1 (data_fsm_state == NO_DATA) ;  
    endproperty 

    property S_READ_DATA_ELSE2 ;
    disable iff (Rst_b == 1'b0)  (data_fsm_state == READ_DATA) ##0 ((SendBlock == 1'b1) & (BlockSent== 1'b0)) ##0 !(EndOfFrame & (FIFO_DataMask == CompleteDataMask))           |-> ##1 (data_fsm_state == STORE_TO_SEND) ;  
    endproperty 
    
//---------------------------------//---------------------------------//---------------------------------//----------------------------------
 `ifdef SVA_EN
    generate
     begin : SVA_AMLDFrame_FSM    
     // assert NOT READY
     ERR_S_DEFAULT:             assert property (S_DEFAULT); 
     ERR_STAY_NOT_READY:        assert property (STAY_NOT_READY);
     ERR_S_NOT_READY:           assert property (S_NOT_READY);
     ERR_NOT_READY_ELSE:        assert property (NOT_READY_ELSE); 
     // assert NO DATA
     ERR_S_NO_DATA:             assert property (S_NO_DATA);
     ERR_NO_DATA_ELSE:          assert property (NO_DATA_ELSE);
     // PREPEARE READ
     ERR_S_PREPARE_READ:        assert property (S_PREPARE_READ); 
     // assert SEND EOF
     ERR_S_SEND_EOF:            assert property (S_SEND_EOF); 
     ERR_SEND_EOF_ELSE0:        assert property (SEND_EOF_ELSE0);
     ERR_S_SEND_EOF_ELSE1:      assert property (S_SEND_EOF_ELSE1);
     ERR_SEND_EOF_ELSE2:        assert property (SEND_EOF_ELSE2); 
     // assert STORE TO SEND
     ERR_S_STORE_TO_SEND:       assert property (S_STORE_TO_SEND); 
     ERR_S_STORE_TO_SEND_ELSE0: assert property (S_STORE_TO_SEND_ELSE0);
     ERR_S_STORE_TO_SEND1:      assert property (S_STORE_TO_SEND1);
     ERR_S_STORE_TO_SEND2:      assert property (S_STORE_TO_SEND2); 
     ERR_S_STORE_TO_SEND3:      assert property (S_STORE_TO_SEND3); 
     ERR_S_STORE_TO_SEND_ELSE:      assert property (S_STORE_TO_SEND_ELSE);      
     
     // assert READ DATA
     ERR_S_READ_DATA:           assert property (S_READ_DATA); 
     ERR_S_READ_DATA_ELSE0:     assert property (S_READ_DATA_ELSE0);
     ERR_S_READ_DATA_ELSE1:     assert property (S_READ_DATA_ELSE1);
     ERR_S_READ_DATA_ELSE2:     assert property (S_READ_DATA_ELSE2); 
//---------------------------------//---------------------------------//---------------------------------//---------------------------------
   end
   endgenerate
  `endif
   //synopsys translate_on

endmodule : AuroraMultilaneDataFrameFSM

`endif   // AURORA_MULTILANE_DATA_FRAME_FSM__SV

