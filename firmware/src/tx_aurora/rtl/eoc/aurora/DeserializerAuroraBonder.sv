`ifndef DESERIALIZER_AURORA_BONDER__SV
 `define DESERIALIZER_AURORA_BONDER__SV

module DeserializerAuroraBonder
  #(
    CHANNELS = 4
    )
   (
    input wire 			     Reset_b,
    input wire 			     Clk,

    input wire [CHANNELS-1:0] 	     ActiveLanes,
    
    input wire [CHANNELS-1:0] 	     LaneUp,
    
    input wire [65:0] 		     BlockData [CHANNELS],
    input wire 			     BlockValid [CHANNELS],
    output logic 		     BlockRead [CHANNELS],

    output logic [(66*CHANNELS)-1:0] BondedData,
    output logic 		     BondedValid,
    output logic 		     BondedChannel
    
    );

   localparam WWIDTH = 3; // data is allowed to be sweked up to 2**WWIDTH

   logic 			     findCBReset [CHANNELS];
   logic 			     findCBResetGlobal;
   logic 			     findCBValid [CHANNELS];
   logic 			     findCBHalt;
   logic [CHANNELS-1:0] 	     findCBWrapped;
   

   logic [CHANNELS-1:0] 	     foundCB;
   logic [WWIDTH-1:0] 		     positionCB [CHANNELS];

   logic [CHANNELS-1:0] 	     magicValid; // this variable will store the real valid only for the latest lane (tap=0) to generate the final valid    


   enum 			     { WAIT_LANES, RESET_FINDCB, FINDCB, BONDED } state;

   logic [CHANNELS-1:0] 	     bondingBlock;
   logic 			     lostBonding; 
   logic 			     allLanesUp;
   
   logic [65:0] 		     localSyncData [CHANNELS];
 	
   genvar 			     channel;
   generate
      
      for (channel=0; channel < CHANNELS; channel++) begin
	 DeserializerFindCB #(.WWIDTH(WWIDTH)) FindCB
		   (
		    .Reset_b(findCBReset[channel]),
		    .Clk(Clk),
		    .Block(BlockData[channel]),
		    .Valid(findCBValid[channel]),
		    .Wrapped(findCBWrapped[channel]),
		    .FoundCB(foundCB[channel]),
		    .PositionCB(positionCB[channel])
		    );
	 assign findCBValid[channel] = BlockValid[channel] && ~findCBHalt;
	 assign findCBReset[channel] = findCBResetGlobal ;
	 
	 DeserializerShiftRTap #(.WWIDTH(66),.TWIDTH(WWIDTH)) ShiftData
	   (
	    .Rst_b(Reset_b),
	    .Clk(Clk),
	    .Ena(BlockValid[channel]),
	    .DataIn(BlockData[channel]),
	    .Tap(positionCB[channel]),
	    .DataOut(localSyncData[channel])
	    );

	 assign BondedData[(66*channel)+:66] = localSyncData[channel];
	 
	 assign magicValid[channel] = (positionCB[channel] == '0) ? BlockValid[channel] : 1'b1;

	 assign bondingBlock[channel] = ActiveLanes[channel] ? 
					((localSyncData[channel][65-:2] == 2'b10) && 
					 (localSyncData[channel][63-:8] == 8'h78) && 
					 (localSyncData[channel][55-:4] == 4'b0100)) : 1'b0;

	 always_ff @(posedge Clk)
	   if (Reset_b == 1'b0)
	     BlockRead[channel] <= 1'b0;
	   else
	     BlockRead[channel] <= 1'b1;
	 
      end
   endgenerate

   assign findCBHalt = & (foundCB | ~ActiveLanes);
   assign BondedValid = & (magicValid | ~ActiveLanes);
   assign allLanesUp = & (LaneUp | ~ActiveLanes);
   
   always_ff @(posedge Clk)
     begin
	if (Reset_b == 1'b0) begin
	   state <= WAIT_LANES;
	   findCBResetGlobal <= 1'b0;
	   BondedChannel <= 1'b0;	   
	end else begin
	   findCBResetGlobal <= 1'b0;
	   case(state)
	     WAIT_LANES : begin
		if (allLanesUp) state <= RESET_FINDCB;
	     end

	     RESET_FINDCB : begin
		state <= FINDCB;
	     end

	     FINDCB : begin
		findCBResetGlobal <= 1'b1;
		if (| (findCBWrapped)) state <= RESET_FINDCB;
		if (findCBHalt) state <= BONDED;
	     end

	     BONDED : begin
		BondedChannel <= 1'b1;
		findCBResetGlobal <= 1'b1;
		if (lostBonding | ~allLanesUp) begin
		   BondedChannel <= 1'b0;
		   state <= WAIT_LANES;
		end
	     end
	   endcase // case (state)
	end
     end // always_ff @

   always_ff @(posedge Clk)
     begin
	lostBonding <= 1'b0;
	//if (BondedValid && BondedChannel && (| (bondingBlock)))
	//  lostBonding <= | ((bondingBlock ^ ActiveLanes) & ActiveLanes);
     end

   
endmodule // DeserializerAuroraBonder


`endif
