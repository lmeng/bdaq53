`ifndef DESERIALIZER_MODULE
 `define DESERIALIZER_MODULE

`include "rtl/eoc/aurora/DeserializerSim.sv"
`include "rtl/eoc/aurora/DeserializerPhaseDet.sv"
`include "rtl/eoc/aurora/DeserializerGearbox4to66.sv"
`include "rtl/eoc/aurora/DeserializerFindSyncHeader.sv"
`include "rtl/eoc/aurora/DeserializerDescrambler.sv"

module DeserializerModule (
   input wire ClkSampling,
   input wire ClkParallel,

   input wire Reset_b,

   input wire SerialIn,

   output logic [65:0] AuroraData,
   output logic AuroraDataValid
   
   );

      logic [3:0] Qa, Qb, Qc, Qd;
   

   DeserializerSim Des (
			.SerIn(SerialIn),
			.Clk1280(ClkSampling),
			.Qa(Qa),
			.Qb(Qb),
			.Qc(Qc),
			.Qd(Qd)
			);

   logic [3:0] DeserializedData;
   logic       DeserializedValid;
   
   DeserializerPhaseDet PhaseDet (
				  .Qa(Qa),
				  .Qb(Qb),
				  .Qc(Qc),
				  .Qd(Qd),
				  .Reset_b(Reset_b),
				  .Clk160(ClkParallel),
				  .DesData(DeserializedData),
				  .Valid(DeserializedValid)
				  );

   logic [65:0] Data66;
   logic 	DataValid;
   
   DeserializerGearbox4to66 Gearbox (
				     .Data4(DeserializedData),
				     .Rst_b(Reset_b),
				     .Clk(ClkParallel),
				     .Data66(Data66),
				     .DataValid(DataValid)
				     );

   logic [65:0] SyncData66;
   logic 	SyncDataValid;

   
   DeserializerFindSyncHeader Header (
				      .Data66(Data66),
				      .DataValid(DataValid),
				      .Clk(ClkParallel),
				      .Rst_b(Reset_b),
				      .SyncData66(SyncData66),
				      .SyncDataValid(SyncDataValid)
				      );

   DeserializerDescrambler Descrambler (
					.DataIn(SyncData66[65:2]),
					.SyncBits({<<{SyncData66[1:0]}}),
					.Ena(SyncDataValid),
					.Clk(ClkParallel),
				       	.Rst_b(Reset_b),
					.DataOut(AuroraData)
					);

   always @(posedge ClkParallel)
     AuroraDataValid = SyncDataValid;
   
endmodule // DeserializerModule


`endif

