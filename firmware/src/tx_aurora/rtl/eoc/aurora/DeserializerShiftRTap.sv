`ifndef DESERIALIZER_SHIFT_R_TAP__SV
 `define DESERIALIZER_SHIFT_R_TAP__SV

module DeserializerShiftRTap
  #(
    WWIDTH = 67,
    TWIDTH = 3
    )
  (
   input wire 		     Rst_b,
   input wire 		     Clk,
   input wire 		     Ena,
   input wire [WWIDTH-1:0]   DataIn,
   input wire [TWIDTH-1:0]   Tap,
   output logic [WWIDTH-1:0] DataOut
   );

   localparam RLENGTH = TWIDTH**2;
   
   logic [RLENGTH-1:0] [WWIDTH-1:0] shiftRegister;

   
   always @(posedge Clk)
     if (Rst_b == 1'b0)
       shiftRegister <= '0;
     else begin
	if (Ena) begin
	   shiftRegister[0] <= DataIn;
	   shiftRegister[RLENGTH-1:1] <= shiftRegister[RLENGTH-2:0];
	end
     end
   
   assign DataOut = shiftRegister[Tap];
   
endmodule // DeserializerShiftRTap

`endif
