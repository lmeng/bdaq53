`ifndef DESERIALIZER_TEST_MODULE
 `define DESERIALIZER_TEST_MODULE

`include "rtl/eoc/aurora/DeserializerSim.sv"
`include "rtl/eoc/aurora/DeserializerPhaseDet.sv"
`include "rtl/eoc/aurora/DeserializerGearbox4to66.sv"
`include "rtl/eoc/aurora/DeserializerFindSyncHeader.sv"
`include "rtl/eoc/aurora/DeserializerDescrambler.sv"
`include "rtl/eoc/aurora/DeserializerModule.sv"

module DeserializerTestModule
  (input logic SerialIn,
   input logic Reset,
   output logic SerialOut);
 
   bit 		clkfast = 1'b0;
   bit 		clkdelay;		
   bit 		clkslow = 1'b0;

   timeunit 1ps ;
   timeprecision 100fs ;


   // nominal 1.28 GHz PLL clock

   parameter period = 782 ;   // ps
   parameter jitter =  50 ;   // ps


   always #( 0.5*0.5*period ) clkfast = ~ clkfast ;
   always #( 2*period ) clkslow = ~ clkslow ;
   always #( 50 ) clkdelay = clkfast;
 
      
   
   logic [3:0] Qa, Qb, Qc, Qd;
   

   DeserializerModule DUTDesModule (
				    .SerialIn(SerialIn),
				    .ClkSampling(clkdelay),
				    .ClkParallel(clkslow),
				    .Reset(Reset));
   

endmodule // DeserializerTestModule


`endif