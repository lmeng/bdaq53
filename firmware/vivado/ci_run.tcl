
# ---------------------------------------------------------------
# Copyright (c) SILAB ,  Institute of Physics, University of Bonn
# ---------------------------------------------------------------
#
#   This script creates Vivado projects and bitfiles for the supported hardware platforms
#
#   Start vivado in tcl mode by typing:
#       vivado -mode tcl -source run.tcl
#


set basil_dir [exec python -c "import basil, os; print(str(os.path.dirname(os.path.dirname(basil.__file__))))"]
set include_dirs [list $basil_dir/basil/firmware/modules $basil_dir/basil/firmware/modules/utils]

file mkdir output reports


proc read_design_files {} {
    read_verilog ../src/bdaq53.v
    read_verilog ../src/bdaq53_core.v

    read_edif ../SiTCP/SiTCP_XC7K_32K_BBT_V110.ngc
    read_verilog ../SiTCP/TIMER.v
    read_verilog ../SiTCP/SiTCP_XC7K_32K_BBT_V110.V
    read_verilog ../SiTCP/WRAP_SiTCP_GMII_XC7K_32K.V


    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b_core.v

    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/exdes/aurora_64b66b_1lane_exdes.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/exdes/aurora_64b66b_1lane_cdc_sync_exdes.v

    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane_core.v

    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_64b66b_descrambler.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_block_sync_sm.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_cbcc_gtx_6466.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_cdc_sync.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_common_logic_cbcc.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_common_reset_cbcc.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_ll_to_axi.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_reset_logic.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_aurora_lane_simplex.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_channel_err_detect_simplex.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_channel_init_sm_simplex.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_err_detect_simplex.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_global_logic_simplex.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_lane_init_sm_simplex.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_ll_datapath_simplex.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_ll_simplex.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_ll_user_k_datapath_simplex.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_rx_startup_fsm.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/src/aurora_64b66b_1lane_sym_dec.v

    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/example_design/support/aurora_64b66b_1lane_clock_module.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/example_design/support/aurora_64b66b_1lane_gt_common_wrapper.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/example_design/support/aurora_64b66b_1lane_support.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/example_design/support/aurora_64b66b_1lane_support_reset_logic.v

    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/example_design/gt/aurora_64b66b_1lane_multi_wrapper.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/example_design/gt/aurora_64b66b_1lane_wrapper.v
    read_verilog ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane/example_design/gt/aurora_64b66b_1lane_gtx.v
}


proc run_bit { part board connector xdc_file size option} {
    create_project -force -part $part $board$option$connector designs

    read_design_files
    read_xdc $xdc_file

    #read_ip ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane.xci
    global include_dirs

    synth_design -top bdaq53 -include_dirs $include_dirs -verilog_define "$board=1" -verilog_define "$connector=1" -verilog_define "SYNTHESIS=1" -verilog_define "$option=1"
    opt_design
    place_design
    phys_opt_design
    route_design
    report_utilization
    report_timing -file "reports/report_timing.$board$option$connector.log"
    write_bitstream -force -file output/$board$option$connector
    write_cfgmem -format mcs -size $size -interface SPIx4 -loadbit "up 0x0 output/$board$option$connector.bit" -force -file output/$board$option$connector
    close_project

    exec tar -C ./output -cvzf output/$board$option$connector.tar.gz $board$option$connector.bit $board$option$connector.mcs
}

