
# ---------------------------------------------------------------
# Copyright (c) SILAB ,  Institute of Physics, University of Bonn
# ---------------------------------------------------------------
#
#   This script creates Vivado projects and bitfiles for the supported hardware platforms.
#
#   Start Vivado in tcl/batch mode by executing:
#       vivado -mode batch -source run.tcl
#
#   NOTE: This will build firmware versions for every supported hardware. See the section "Create projects and bitfiles" below.
#   Alternatively, a list of 8 arguments can be used to build only the specified firmware.
#   The arguments have to be in the correct order. Just copy&paste from the "Create projects and bitfiles" section and remove all but one space in between the args. Empty arguments must be set to "".
#       vivado -mode tcl -source run.tcl -tclargs xc7k160tffg676-2 BDAQ53 "" ../src/bdaq53.xdc 64 "" _1LANE _RX640
#

# Use current environment python instead of vivado included python
if {[info exists ::env(PYTHONPATH)]} {
    unset ::env(PYTHONPATH)
}
if {[info exists ::env(PYTHONHOME)]} {
    unset ::env(PYTHONHOME)
}
# Get rid of Vivado python (since Vivado 2021) in PATH and use python from calling shell
set env(PATH) [join [lsearch -inline -all -not -regexp [split $::env(PATH) ":"] (.*)lnx64\/python(.*)] ":"]

set basil_dir [exec python -c "import basil, os; print(str(os.path.dirname(os.path.dirname(basil.__file__))))"]
set firmware_dir [exec python -c "import os; print(os.path.dirname(os.getcwd()))"]
set RD53B_dir $firmware_dir/src/tx_aurora
set include_dirs [list $basil_dir/basil/firmware/modules $basil_dir/basil/firmware/modules/utils $firmware_dir/src/rx_aurora $firmware_dir $RD53B_dir $RD53B_dir/rtl/eoc/aurora/ $firmware_dir/IP/synth $firmware_dir/IP]

file mkdir output reports

proc read_design_files {part ethernet} {
    global firmware_dir
    if {$ethernet == "10G"} {
        generate_ip_cores "ten_gig_eth_pcs_pma" $part
        add_files -norecurse $firmware_dir/IP/ten_gig_eth_pcs_pma.xci

        read_edif $firmware_dir/SiTCP10G/SiTCPXG_XC7K_128K_V3.edf
        read_verilog $firmware_dir/SiTCP10G/SiTCPXG_XC7K_128K_V3.v
        read_verilog $firmware_dir/SiTCP10G/TIMER_SiTCPXG.v
        read_verilog $firmware_dir/SiTCP10G/WRAP_SiTCPXG_XC7K_128K.v
    } else {
        read_edif $firmware_dir/SiTCP/SiTCP_XC7K_32K_BBT_V110.ngc
    }
    read_verilog $firmware_dir/src/bdaq53.v
    read_verilog -sv $firmware_dir/src/bdaq53_core.v
}

proc generate_ip_cores {name part} {
    global firmware_dir
    set ipname $name
    set xci_file $firmware_dir/IP/$name.xci

    create_project -force ipcore -part $part
    read_ip $xci_file
    upgrade_ip [get_ips *]
    generate_target -verbose -force all [get_ips]
    create_ip_run [get_files $xci_file]
    launch_runs $ipname\_synth_1 -jobs 12
    wait_on_run $ipname\_synth_1
    close_project
}

proc run_bit {part board connector xdc_file size option lanes speed ethernet} {
    global firmware_dir
    global include_dirs

    set branch [if [catch {exec python -c "from bdaq53 import utils; print(utils.get_software_version())"}] {puts "none"} else {exec python -c "from bdaq53 import utils; print(utils.get_software_version().split('@')[0])"}]
    set version [exec python -c "from importlib.metadata import version; print(version('bdaq53'))"]
    lindex [split $version '.'] 
    set version_major [lindex [split $version '.'] 0]
    set version_minor [lindex [split $version '.'] 1]
    set version_patch [lindex [split $version '.'] 2]

    create_project -force -part $part $board$option$connector$lanes$speed\_$ethernet designs

    read_design_files $part $ethernet
    read_xdc $firmware_dir/src/$xdc_file

    if {$ethernet == "10G"} {
        read_xdc $firmware_dir/src/SiTCP10G.xdc
    } else {
        read_xdc $firmware_dir/src/SiTCP.xdc
    }

    synth_design -top bdaq53 -include_dirs $include_dirs -verilog_define "$board=1" -verilog_define "$connector=1" -verilog_define "SYNTHESIS=1" -verilog_define "$option=1" -verilog_define "$lanes=1" -verilog_define "$speed=1" -verilog_define $ethernet -generic VERSION_MAJOR=8'd$version_major -generic VERSION_MINOR=8'd$version_minor -generic VERSION_PATCH=8'd$version_patch
    opt_design
    place_design
    phys_opt_design
    route_design
    report_utilization -file "reports/report_utilization_$board$option$connector$lanes$speed\_$ethernet.log"
    report_timing_summary -file "reports/report_timing_$board$option$connector$lanes$speed\_$ethernet.log"
    write_bitstream -force -file output/$board$option$connector$lanes$speed\_$ethernet
    write_cfgmem -format mcs -size $size -interface SPIx4 -loadbit "up 0x0 output/$board$option$connector$lanes$speed\_$ethernet.bit" -force -file output/$board$option$connector$lanes$speed\_$ethernet
    close_project

    exec tar -C ./output -cvzf output/$version\_$branch\_$board$option$connector$lanes$speed\_$ethernet.tar.gz $board$option$connector$lanes$speed\_$ethernet.bit $board$option$connector$lanes$speed\_$ethernet.mcs
}


#########

#
# Create projects and bitfiles
#

if {$argc == 0} {
# By default, all firmware versions are generated. You can comment the ones you don't need.

# Bitfiles for the 640 Mb/s Aurora ip core configuration
#       FPGA type           board name  connector   constraints file    flash   option  lanes   speed       eth
run_bit xc7k160tffg676-2    BDAQ53      ""          bdaq53.xdc          64      ""      _1LANE   _RX640     1G
# run_bit xc7k160tfbg676-1    USBPIX3     ""          usbpix3.xdc         64      ""      _1LANE   _RX640     1G
# run_bit xc7k325tffg900-2    KC705       _SMA        kc705_gmii.xdc      16      ""      _1LANE   _RX640     1G
run_bit xc7k325tffg900-2    KC705       _FMC_LPC    kc705_gmii.xdc      16      ""      _1LANE   _RX640     1G

# Bitfiles for the 1.28 Gb/s Aurora ip core configuration
#       FPGA type           board name  connector   constraints file    flash   option  lanes    speed      eth
run_bit xc7k160tffg676-2    BDAQ53      ""          bdaq53.xdc          64      ""      _1LANE   _RX1280    1G
# run_bit xc7k160tfbg676-1    USBPIX3     ""          usbpix3.xdc         64      ""      _1LANE   _RX1280    1G
# run_bit xc7k325tffg900-2    KC705       _SMA        kc705_gmii.xdc      16      ""      _1LANE   _RX1280    1G
run_bit xc7k325tffg900-2    KC705       _FMC_LPC    kc705_gmii.xdc      16      ""      _1LANE   _RX1280    1G

# Bitfiles for special purposes
#       FPGA type           board name  connector   constraints file    flash   option  lanes    speed      eth
# run_bit xc7k160tffg676-2    BDAQ53      ""          bdaq53.xdc          64      ""      _2LANE   _RX1280    1G
# run_bit xc7k160tffg676-2    BDAQ53      ""          bdaq53.xdc          64      ""      _3LANE   _RX1280    1G
run_bit xc7k160tffg676-2    BDAQ53      ""          bdaq53.xdc          64      ""      _4LANE   _RX1280    1G
run_bit xc7k160tffg676-2    BDAQ53      ""          bdaq53_10G.xdc      64      ""      _4LANE   _RX1280    10G
#run_bit xc7k160tfbg676-1    BDAQ53      ""          bdaq53_KX1.xdc      64      _KX1    _1LANE   _RX640     1G
#run_bit xc7k160tfbg676-1    BDAQ53      ""          bdaq53_KX1.xdc      64      _KX1    _1LANE   _RX1280    1G
run_bit xc7k160tffg676-2    BDAQ53      ""          bdaq53_10G.xdc      64      ""      _1LANE   _RX1280    10G
#run_bit xc7k325tffg900-2    KC705       _FMC_LPC    kc705_10G.xdc       16      ""      _1LANE   _RX640     10G

# In order to build only one specific firmware version, the tun.tcl can be executed with arguments
} else {
    if {$argc == 9} {
        run_bit {*}$argv
    } else {
        puts "ERROR: Invalid args"
    }
}
exit
